del %USERPROFILE%\wake.zip
mkdir %USERPROFILE%\back\wake
copy *.* %USERPROFILE%\back\wake\
pushd ..
7z a -tzip %USERPROFILE%\wake.zip gibberishwake/* gibberishwake/bin-linux/wk gibberishwake/bin-mac/wk gibberishwake/bin-win32/wk.exe -x!gibberishwake/lib/* -x!gibberishwake/o/* -x!gibberishwake/obj/* -x!gibberishwake/*~ -x!gibberishwake/.git/*
dir %USERPROFILE%\wake.zip
popd
