
/* wk_gra.c */

#include "wake.h"
#include "wk_mat.h"
#define MAIN
#undef VARIABLE
#include "wk_gra.h"

extern void wk_init(void);
extern void wk_quit(void);
extern int init_gra2(void);

extern char *whites;

#if defined(AMIGA) || defined(VGALIB)

#define NUM_LINES NUM_POINTS
#define INC_LINES (INC_POINTS/2)

typedef struct LINE
{
    struct LINE *chain;
    signed char cdir, fdir;
    short filler;
    short y1, y2;
    int xx, dx, d1, d2;
} LINE;

LINE *linearray;
LINE **lineptr;
LINE **ybucket;

int linesize;
int lineend;
float ymin, ymax, yupb, xupb;

void fillxyxy(float x1, float y1, float x2, float y2, int cdir, int fdir)
{
    float yy;
    LINE *pline;
    if (y1<0.0) {
        if (y2<0.0) return;
        x1=floor(x1-y1*(x2-x1)/(y2-y1)+0.5);
        y1=0.0;
    }
    if (y2>yupb) {
        if (y1>yupb) return;
        x2=floor(x2-(y2-yupb)*(x2-x1)/(y2-y1)+0.5);
        y2=yupb;
    }
    if (y1<ymin) ymin=y1;
    if (y2>ymax) ymax=y2;
    if (x1<0.0)
        if (x2<0.0) x1=x2=0.0;
        else {
            yy=floor(y1-x1*(y2-y1)/(x2-x1)+0.5);
            if (yy>y1&&yy<y2) {
                fillxyxy((float)0.0, y1, (float)0.0, yy, cdir, fdir);
                y1=yy;
            }
            x1=0.0;
        }
    else if (x2<0.0) {
        yy=floor(y2-x2*(y1-y2)/(x1-x2)+0.5);
        if (yy>y1&&yy<y2) {
            fillxyxy((float)0.0, yy, (float)0.0, y2, cdir, fdir);
            y2=yy;
        }
        x2=0.0;
    }
    if (x1>xupb)
        if (x2>xupb) x1=x2=xupb;
        else {
            yy=floor(y1-(x1-xupb)*(y1-y2)/(x1-x2)+0.5);
            if (yy>y1&&yy<y2) {
                fillxyxy(xupb, y1, xupb, yy, cdir, fdir);
                y1=yy;
            }
            x1=xupb;
        }
    else if (x2>xupb) {
        yy=floor(y2-(x2-xupb)*(y2-y1)/(x2-x1)+0.5);
        if (yy>y1&&yy<y2) {
            fillxyxy(xupb, yy, xupb, y2, cdir, fdir);
            y2=yy;
        }
        x2=xupb;
    }
    if (y1>=yupb) return;
    if (lineend==linesize) {
        LINE *new=(LINE *)memrealloc(linearray,
                                     (linesize+=INC_LINES)*sizeof(LINE));
        if (!new) {
            memfree(linearray);
            error(WAKE_CANNOT_ALLOCATE);
        }
        linearray=new;
    }
    pline=linearray+lineend++;
    pline->chain=NULL;
    pline->cdir=cdir;
    pline->fdir=fdir;
    pline->xx=x1*256.0;
    pline->y1=((int)y1)/256;
    pline->y2=((int)y2)/256;
    if (pline->y2==pline->y1) {
        pline->dx=0;
        pline->d1=0;
        pline->d2=(x2-x1)*256.0;
    }
    else {
        yy=(x2-x1)/(y2-y1)*256.0;
        pline->dx=yy*256.0;
        pline->d1=((pline->y1+1)*256.0-y1)*yy;
        pline->d2=(y2-pline->y2*256.0)*yy;
    }
}

void fill_area(void)
{
    int i, m, n;
    int y2, yy;
    int x1, x2, xp, xx;
    int active, count, discard, sort;
    int fdir;
    int s1=1, s2=0;
    LINE *pline, **ppline;
    if (n_points==0) return;
    if ((linearray=(LINE *)memalloc((linesize=NUM_LINES)*sizeof(LINE)))==NULL)
        error(WAKE_CANNOT_ALLOCATE);
    ymax=0.0;
    ymin=yupb=(float)(height-1)*256.0;
    xupb=(float)(width-1)*256.0;
    lineend=0;
    for (i=0, m=0, n=0; i<n_counts; i++) {
        m+=counts[i];
        while (++n<m)
            {
                if (points[n-1].y<points[n].y)
                    fillxyxy((float)(points[n-1].x*256.0),
                             (float)(points[n-1].y*256.0),
                             (float)(points[n].x*256.0),
                             (float)(points[n].y*256.0), 0, 1);
                else fillxyxy((float)(points[n].x*256.0),
                              (float)(points[n].y*256.0),
                              (float)(points[n-1].x*256.0),
                              (float)(points[n-1].y*256.0), 0, -1);
            }
    }
    if (!lineend) {
        memfree(linearray);
        return;
    }
    yy=((int)ymin)>>8;
    y2=(((int)ymax)>>8)+1;
    if (y2>height) y2=height;
    if ((ybucket=(LINE **)memcalloc(height*sizeof(LINE *)))==NULL) {
        memfree(linearray);
        error(WAKE_CANNOT_ALLOCATE);
    }
    for (i=0; i<lineend; i++) {
        linearray[i].chain=ybucket[linearray[i].y1];
        ybucket[linearray[i].y1]=linearray+i;
    }
    if ((lineptr=(LINE **)memcalloc(lineend*sizeof(LINE *)))==NULL) {
        memfree(linearray);
        memfree(ybucket);
        error(WAKE_CANNOT_ALLOCATE);
    }
    active=discard=sort=0;
    while (yy<y2) {
        pline=ybucket[yy];
        ybucket[yy]=NULL;
        while (pline)
            {
                lineptr[active++]=pline;
                pline=pline->chain;
                sort++;
            }
        sort+=discard;
        if (sort) {
            count=active;
            for (;;) {
                count=count/3+1;
                for (x1=count; x1<active; x1++)
                    for (x2=x1-count; x2>=0&&
                             lineptr[x2]->xx>lineptr[x2+count]->xx;
                         x2-=count) {
                        pline=lineptr[x2];
                        lineptr[x2]=lineptr[x2+count];
                        lineptr[x2+count]=pline;
                    }
                if (count==1) break;
            }
            active-=discard;
            discard=sort=0;
        }
        count=active;
        fdir=0;
        ppline=lineptr;
        xp=-32767;
        while (count--) {
            pline=*ppline++;
            x1=pline->xx>>16;
            if (yy==pline->y2) {
                pline->xx+=pline->d2;
                x2=pline->xx>>16;
                pline->xx=0x7fffffff;
                discard++;
                if (x2<x1) {
                    xx=x1;
                    x1=x2;
                    x2=xx;
                }
                if ((fdir&rule)==0) {
                    s1=x1;
                    s2=x2;
                }
                else {
                    if (x1<s1) s1=x1;
                    if (x2>s2) s2=x2;
                    continue;
                }
            }
            else {
                if (yy==pline->y1) pline->xx+=pline->d1;
                else pline->xx+=pline->dx;
                x2=pline->xx>>16;
                if (x2<xp) sort++;
                xp=x2;
                if (x2<x1) {
                    xx=x1;
                    x1=x2;
                    x2=xx;
                }
                if ((fdir&rule)==0) {
                    fdir+=pline->fdir;
                    s1=x1;
                    s2=x2;
                    continue;
                }
                if (x1<s1) s1=x1;
                if (x2>s2) s2=x2;
                fdir+=pline->fdir;
                if ((fdir&rule)!=0) continue;
            }
            s2++;
            if (s1<0) s1=0;
            if (s2>width) s2=width;
            if (s1>=s2) continue;
#ifdef AMIGA
            Move(rastport, s1, yy);
            Draw(rastport, s2-1, yy);
#elif defined(VGALIB)
            vga_drawline(s1, yy, s2-1, yy);
#endif
        }
        yy++;
    }
    memfree(linearray);
    memfree(ybucket);
    memfree(lineptr);
}
#endif

#ifdef AMIGA
int handle_imsg(struct IntuiMessage *imsg)
{
    switch (imsg->Class) {
    case MOUSEBUTTONS:
        if (imsg->Qualifier&IEQUALIFIER_RBUTTON||
            imsg->Code&IECODE_UP_PREFIX) {
            ReplyMsg((struct Message *)imsg);
            return 0;
        }
        ReplyMsg((struct Message *)imsg);
        stack(3);
        if (gflag&GENERAL) {
            float x, y;
            x=(float)window->MouseX;
            y=(float)window->MouseY;
            inv_trans(&x, &y, trans);
            wsp->flag=NUMBER;
            wsp->type=FLOATING_TYPE;
            wsp->unit.floating=(double)x;
            wsp++;
            wsp->flag=NUMBER;
            wsp->type=FLOATING_TYPE;
            wsp->unit.floating=(double)y;
            wsp++;
        }
        else {
            wsp->flag=NUMBER|INTEGER;
            wsp->type=INTEGER_TYPE;
            wsp->unit.integer=window->MouseX;
            wsp++;
            wsp->flag=NUMBER|INTEGER;
            wsp->type=INTEGER_TYPE;
            wsp->unit.integer=window->MouseY;
            wsp++;
        }
        wsp->flag=0;
        wsp->type=TRUE_TYPE;
        wsp++;
        return 1;
    case RAWKEY: {
        UBYTE cc;
        inputevent->ie_Class=IECLASS_RAWKEY;
        inputevent->ie_Code=imsg->Code;
        inputevent->ie_Qualifier=imsg->Qualifier;
        ReplyMsg((struct Message *)imsg);
        if (RawKeyConvert(inputevent, &cc, 1, NULL)!=1) return 0;
        stack(1);
        wsp->flag=NUMBER|INTEGER;
        wsp->type=INTEGER_TYPE;
        wsp->unit.integer=(long)cc;
        wsp++;
        return 1;
    } }
    ReplyMsg((struct Message *)imsg);
    return 0;
}

int setrgb(int r, int g, int b)
{
    int i, n=0;
    RGBVAL l, m;
    UWORD v=GetRGB4(colormap, 0);
    m.red=labs((v>>8&0xf)-r);
    m.green=labs((v>>4&0xf)-g);
    m.blue=labs((v&0xf)-b);
    if (!m.red&&!m.green&&!m.blue) return 0;
    for (i=1; i<=color_n; i++) {
        v=GetRGB4(colormap, i);
        l.red=labs((v>>8&0xf)-r);
        l.green=labs((v>>4&0xf)-g);
        l.blue=labs((v&0xf)-b);
        if (!l.red&&!l.green&&!l.blue) return i;
        if (l.red<=m.red&&l.green<=m.green&&l.blue<=m.blue) {
            m=l;
            n=i;
        }
    }
    if (color_n<n_colors) SetRGB4(viewport, n= ++color_n, r, g, b);
    return n;
}

void *make_image(int width, int height)
{
    UBYTE i;
    void **ip;
    struct BitMap *bp;
    if ((bp=(struct BitMap *)AllocMem(sizeof(struct BitMap),
                                      MEMF_PUBLIC|MEMF_CLEAR))==NULL) return NULL;
    if ((ip=(void **)get_mem(1, IMAGE_TYPE, FALSE))==NULL) {
        FreeMem(bp, sizeof(struct BitMap));
        return NULL;
    }
    ip[1]=(void *)bp;
    for (i=0; i<depth; i++)
        if ((bp->Planes[i]=(PLANEPTR)AllocRaster(width, height))==NULL) {
            while (i--) FreeRaster(bp->Planes[i], width, height);
            FreeMem(bp, sizeof(struct BitMap));
            last_save= *(HEAD **)((HEAD *)ip-1);
            memfree((HEAD *)ip-1);
            return NULL;
        }
    InitBitMap(bp, depth, width, height);
    *(UWORD *)ip=width;
    *((UWORD *)ip+1)=height;
    return (void *)ip;
}

LONG look_image(char *name, BMHD *bmhd)
{
    LONG file, i, n;
    ULONG chunk_size;
    if ((file=Open(name, MODE_OLDFILE))==NULL) access_error(name);
    if (test(file, "FORM")||
        Read(file, &chunk_size, sizeof chunk_size)!=sizeof chunk_size||
        test(file, "ILBMBMHD")||
        Read(file, &chunk_size, sizeof chunk_size)!=sizeof chunk_size||
        Read(file, bmhd, sizeof(BMHD))!=sizeof(BMHD)) goto fault;
    for (;;)
        if (!test(file, "CMAP")) {
            RGBVAL l;
            if (Read(file, &chunk_size, sizeof chunk_size)!=
                sizeof chunk_size) goto fault;
            n=chunk_size/3;
            for (i=0; i<n; i++)
                {
                    if (Read(file, &l, 3)!=3) goto fault;
                    if (i<n_colors)
                        SetRGB4(viewport, i, l.red>>4, l.green>>4, l.blue>>4);
                }
            if (chunk_size&1&&Seek(file, 1, OFFSET_CURRENT)==-1) goto fault;
        }
        else if (Seek(file, -4, OFFSET_CURRENT)!=-1&&!test(file, "BODY")) {
            if (Read(file, &chunk_size, sizeof chunk_size)!=
                sizeof chunk_size) goto fault;
            return file;
        }
        else if (skip(file)==-1) break;
    Close(file);
    return 0;
 fault: Close(file);
    error(WAKE_FAULTY_FORM);
}

int read_scanline(LONG file, struct BitMap *bp, int offset, int rrl)
{
    int pn;
    for (pn=0; pn<bp->Depth; pn++)
        if (Read(file, bp->Planes[pn]+offset, bp->BytesPerRow)!=
            bp->BytesPerRow||rrl&&Seek(file, rrl, OFFSET_CURRENT)==-1) return -1;
    return 0;
}

int read_packed_scanline(LONG file, struct BitMap *bp, int offset, int rrl)
{
    char c;
    UBYTE *plane;
    int m, n, num, pn;
    rrl=-rrl;
    for (pn=0; pn<bp->Depth; pn++)
        {
            plane=bp->Planes[pn]+offset;
            num=bp->BytesPerRow;
            while (num>rrl) {
                if (Read(file, &c, 1)!=1) return -1;
                if (c>=0) {
                    n=(int)c+1;
                    if (num>0) {
                        m=min(num, n);
                        num-=n;
                        if (Read(file, plane, m)!=m||(n-=m)&&
                            Seek(file, n, OFFSET_CURRENT)==-1)
                            return -1;
                        plane+=m;
                    }
                    else if ((num-=n)<=rrl||
                             Seek(file, n, OFFSET_CURRENT)==-1) return -1;
                }
                else if (c!=-128) {
                    n=-(int)c+1;
                    if (Read(file, &c, 1)!=1) return -1;
                    if (num>0) {
                        m=min(num, n);
                        num-=n;
                        memset(plane, c, m);
                        plane+=m;
                    }
                    else if ((num-=n)<=rrl) return -1;
                }
            }
        }
    return 0;
}
#elif defined(VGALIB)
int setrgb(int r, int g, int b)
{
    if (n_colors>256) {
        vga_setrgbcolor(r, g, b);
        return 0;
    }
    {
        RGBVAL l, m;
        int i, n, rr, gg, bb;
        n=0;
        r>>=2;
        g>>=2;
        b>>=2;
        vga_getpalette(0, &rr, &gg, &bb);
        m.red=labs(rr-r);
        m.green=labs(gg-g);
        m.blue=labs(bb-b);
        if (!m.red&&!m.green&&!m.blue) {
            vga_setcolor(0);
            return 0;
        }
        for (i=1; i<=color_n; i++) {
            vga_getpalette(i, &rr, &gg, &bb);
            l.red=labs(rr-r);
            l.green=labs(gg-g);
            l.blue=labs(bb-b);
            if (!l.red&&!l.green&&!l.blue) {
                vga_setcolor(i);
                return i;
            }
            if (l.red<=m.red&&l.green<=m.green&&l.blue<=m.blue) {
                m=l;
                n=i;
            }
        }
        if (color_n<n_colors) vga_setpalette(n= ++color_n, r, g, b);
        vga_setcolor(n);
        return n;
    }
}

void *make_image(int width, int height)
{
    return NULL;
}
#elif defined(_WINDOWS)
int input_buffer[30], input_buffer_index, input_buffer_length;
int handle_msg()
{
    float x, y;
    int i;
    if (input_buffer_length == 0) return 0;
    i = input_buffer_index - input_buffer_length;
    if (i < 0) i = 30 + i;
    input_buffer_length -= 3;
    switch (input_buffer[i]) {
    case 0:
        stack(1);
        wsp->flag = NUMBER | INTEGER;
        wsp->type = INTEGER_TYPE;
        wsp->unit.integer = input_buffer[i + 1];
        wsp++;
        return 1;
    case 1:
        stack(3);
        x = (float)input_buffer[i + 1];
        y = (float)input_buffer[i + 2];
        inv_trans(&x, &y, trans);
        wsp->flag = NUMBER;
        wsp->type = FLOATING_TYPE;
        wsp->unit.floating = (double)x;
        wsp++;
        wsp->flag = NUMBER;
        wsp->type = FLOATING_TYPE;
        wsp->unit.floating = (double)y;
        wsp++;
    case 2:
        wsp->flag= NUMBER | INTEGER;
        wsp->type= INTEGER_TYPE;
        wsp->unit.integer = input_buffer[i + 1];
        wsp++;
        wsp->flag= NUMBER | INTEGER;
        wsp->type= INTEGER_TYPE;
        wsp->unit.integer = input_buffer[i + 2];
        wsp++;
    }
    wsp->flag = 0;
    wsp->type = TRUE_TYPE;
    wsp++;
    return 1;
}

LONG EXPORT FAR PASCAL WakeProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message) {
    case WM_CHAR:
        if (input_buffer_index >= 30) input_buffer_index = 0;
        else input_buffer_length += 3;
        input_buffer[input_buffer_index++] = 0;
        input_buffer[input_buffer_index++] = wParam;
        input_buffer_index++;
        return 0;
    case WM_LBUTTONDOWN:
        if (input_buffer_index >= 30) input_buffer_index = 0;
        else input_buffer_length += 3;
        if (gflag & GENERAL) {
            float x, y;
            x = (float)LOWORD(lParam);
            y = (float)HIWORD(lParam);
            inv_trans(&x, &y, trans);
            input_buffer[input_buffer_index++] = 1;
            input_buffer[input_buffer_index++] = x;
            input_buffer[input_buffer_index++] = y;
        }
        else {
            input_buffer[input_buffer_index++] = 2;
            input_buffer[input_buffer_index++] = LOWORD(lParam);
            input_buffer[input_buffer_index++] = HIWORD(lParam);
        }
        return 0;
    }
    return DefWindowProc(hwnd, message, wParam, lParam);
}

COLORREF setrgb(int r, int g, int b, BOOL rlz)
{
    if (paletteBased) {
        if (color_n<n_colors) {
            PALETTEENTRY pe;
            int n=GetNearestPaletteIndex(palette, RGB(r, g, b));
            GetPaletteEntries(palette, n, 1, &pe);
            if (pe.peRed-r||pe.peGreen-g||pe.peBlue-b) {
                pe.peRed=(UBYTE)r;
                pe.peGreen=(UBYTE)g;
                pe.peBlue=(UBYTE)b;
                pe.peFlags=0;
                SetPaletteEntries(palette, n= ++color_n, 1, &pe);
                if (rlz) RealizePalette(dc);
            }
            return PALETTEINDEX(n);
        }
        else return PALETTEINDEX(GetNearestPaletteIndex(palette, RGB(r, g, b)));
    }
    return RGB(r, g, b);
}

void setpen(void)
{
    if (dpen) SelectObject(dc, dpen);
    if (pen) DeleteObject(pen);
    if ((pen=CreatePenIndirect(&logpen))==NULL)
        error(WAKE_CANNOT_ALLOCATE);
    dpen=SelectObject(dc, pen);
    if (dbrush) SelectObject(dc, dbrush);
    if (brush) DeleteObject(brush);
    if ((brush=CreateSolidBrush(logpen.lopnColor))==NULL)
        error(WAKE_CANNOT_ALLOCATE);
    dbrush=SelectObject(dc, brush);
    SetTextColor(dc, logpen.lopnColor);
}

void setbackbrush(void)
{
    if (paletteBased) {
        PALETTEENTRY l;
        l.peRed=rgbbackcolor.red;
        l.peGreen=rgbbackcolor.green;
        l.peBlue=rgbbackcolor.blue;
        l.peFlags=0;
        SetPaletteEntries(palette, 0, 1, &l);
        RealizePalette(dc);
        backcolor=PALETTEINDEX(0);
    }
    else backcolor=RGB(rgbbackcolor.red, rgbbackcolor.green,
                       rgbbackcolor.blue);
    if (cbrush) DeleteObject(cbrush);
    if ((cbrush=CreateSolidBrush(backcolor))==NULL)
        error(WAKE_CANNOT_ALLOCATE);
    SetBkColor(dc, backcolor);
}

void *make_image(int width, int height)
{
    void **ip;
    if ((ip=(void **)get_mem(1, IMAGE_TYPE, FALSE))==NULL) return NULL;
    if ((*ip=(void *)CreateCompatibleBitmap(dc, width, height))==NULL) {
        last_save= *(HEAD **)((HEAD *)ip-1);
        memfree((HEAD *)ip-1);
        return NULL;
    }
    return (void *)ip;
}

UBYTE MQ *read_bitmap(char *name, BITMAPINFO **bmi)
{
    FILE *f;
    DWORD size;
    BITMAPFILEHEADER bmfh;
    BITMAPINFOHEADER *bmih;
    UBYTE MQ *bits=NULL, MQ *p;
    int c,  i,  j,  n, scanlinelength;
    if ((f = fopen(name, "rb"))==NULL)
        access_error(name);
    if (paletteBased) {
        int n;
        if ((*bmi=(BITMAPINFO *)memcalloc(sizeof(BITMAPINFO)+
                                          256*sizeof(unsigned)))==NULL) {
            fclose(f);
            error(WAKE_CANNOT_ALLOCATE);
        }
        for (n=0; n<256; n++) ((unsigned *)(*bmi)->bmiColors)[n]=n;
    }
    else if ((*bmi=(BITMAPINFO *)memcalloc(sizeof(BITMAPINFO)+
                                           256*sizeof(RGBQUAD)))==NULL) {
        fclose(f);
        error(WAKE_CANNOT_ALLOCATE);
    }
    bmih=&(*bmi)->bmiHeader;
    if (fread(&bmfh, 1, sizeof(BITMAPFILEHEADER), f)!=
        sizeof(BITMAPFILEHEADER)||bmfh.bfType!=((WORD)'B'|(WORD)'M'<<8)||
        fread(&size, 1, sizeof size, f)!=sizeof size) goto fault;
    if (fseek(f, sizeof(BITMAPFILEHEADER), SEEK_SET)) goto fault;
    if (size==sizeof(BITMAPCOREHEADER)) {
        BITMAPCOREHEADER bmch;
        if (fread(&bmch, 1, sizeof(BITMAPCOREHEADER), f)!=
            sizeof(BITMAPCOREHEADER)) goto fault;
        if (bmch.bcBitCount!=24) {
            RGBTRIPLE rgbt;
            n=1<<bmch.bcBitCount;
            if (n_colors<=16) {
                for (i=0; i<n; i++)
                    if (fread(&rgbt, 1, 3, f)!=3) goto fault;
                    else if (i<256) {
                        COLORREF cr;
                        PALETTEENTRY pe;
                        cr=GetNearestColor(dc, RGB(rgbt.rgbtRed,
                                                   rgbt.rgbtGreen, rgbt.rgbtBlue));
                        pe.peRed=GetRValue(cr);
                        pe.peGreen=GetGValue(cr);
                        pe.peBlue=GetBValue(cr);
                        pe.peFlags=0;
                        SetPaletteEntries(palette, i, 1, &pe);
                    }
                RealizePalette(dc); }
            else if (paletteBased) {
                for (i=0; i<n; i++)
                    if (fread(&rgbt, 1, 3, f)!=3) goto fault;
                    else if (i<256) {
                        PALETTEENTRY pe;
                        pe.peRed=rgbt.rgbtRed;
                        pe.peGreen=rgbt.rgbtGreen;
                        pe.peBlue=rgbt.rgbtBlue;
                        pe.peFlags=0;
                        SetPaletteEntries(palette, i, 1, &pe);
                    }
                RealizePalette(dc); }
            else {
                RGBQUAD *colors=(*bmi)->bmiColors;
                for (i=0; i<n; i++)
                    if (fread(&rgbt, 1, 3, f)!=3) goto fault;
                    else {
                        colors[i].rgbRed=rgbt.rgbtRed;
                        colors[i].rgbGreen=rgbt.rgbtGreen;
                        colors[i].rgbBlue=rgbt.rgbtBlue;
                    } }
        }
        bmih->biSize=sizeof(BITMAPINFOHEADER);
        bmih->biWidth=bmch.bcWidth;
        bmih->biHeight=bmch.bcHeight;
        bmih->biPlanes=bmch.bcPlanes;
        bmih->biBitCount=bmch.bcBitCount;
        bmih->biCompression=BI_RGB;
    }
    else {
        if (fread(bmih, 1, sizeof(BITMAPINFOHEADER), f)!=
            sizeof(BITMAPINFOHEADER)) goto fault;
        if (bmih->biBitCount!=24) {
            RGBQUAD rgbq;
            n=bmih->biClrUsed?bmih->biClrUsed:1<<bmih->biBitCount;
            if (n_colors<=16) {
                for (i=0; i<n; i++)
                    if (fread(&rgbq, 1, 4, f)!=4) goto fault;
                    else if (i<256) {
                        COLORREF cr;
                        PALETTEENTRY pe;
                        cr=GetNearestColor(dc, RGB(rgbq.rgbRed,
                                                   rgbq.rgbGreen, rgbq.rgbBlue));
                        pe.peRed=GetRValue(cr);
                        pe.peGreen=GetGValue(cr);
                        pe.peBlue=GetBValue(cr);
                        pe.peFlags=0;
                        SetPaletteEntries(palette, i, 1, &pe);
                    }
                RealizePalette(dc); }
            else if (paletteBased) {
                for (i=0; i<n; i++)
                    if (fread(&rgbq, 1, 4, f)!=4) goto fault;
                    else if (i<256) {
                        PALETTEENTRY pe;
                        pe.peRed=rgbq.rgbRed;
                        pe.peGreen=rgbq.rgbGreen;
                        pe.peBlue=rgbq.rgbBlue;
                        pe.peFlags=0;
                        SetPaletteEntries(palette, i, 1, &pe);
                    }
                RealizePalette(dc); }
            else {
                RGBQUAD *colors=(*bmi)->bmiColors;
                for (i=0; i<n; i++)
                    if (fread(colors+i, 1, 4, f)!=4) goto fault; }
        }
    }
    i=j=0;
    scanlinelength=(bmih->biWidth*(long)bmih->biBitCount+31)>>5<<2;
    if ((p=bits=(UBYTE MQ *)memcalloc(bmih->biHeight*
                                      (long)scanlinelength))==NULL) {
        fclose(f);
        memfree(*bmi);
        error(WAKE_CANNOT_ALLOCATE);
    }
    switch (bmih->biCompression) {
    case BI_RGB:
        if (fread(bits, scanlinelength, (int)bmih->biHeight, f)!=
            (unsigned)bmih->biHeight) goto fault;
        fclose(f);
        return bits;
    case BI_RLE8:
        while ((c=fgetc(f))!=EOF)
            if (c==0) {
                switch (c=fgetc(f)) {
                case EOF:
                    goto fault;
                case 0:
                    j++;
                    p+=scanlinelength-i;
                    continue;
                case 1:
                    fclose(f);
                    return bits;
                case 2:
                    if ((c=fgetc(f))!=EOF||(i+=c)>bmih->biWidth||
                        (c=fgetc(f))!=EOF||(j+=c)>bmih->biHeight) goto fault;
                    p=bits+(long)j*(long)scanlinelength+(long)i;
                    continue;
                default:
                    if ((i+=c)>bmih->biWidth||fread(p, 1, c, f)!=(unsigned)c) goto fault;
                    p+=c;
                    continue;
                } }
            else
                {
                    n=c;
                    if ((i+=c)>bmih->biWidth||(c=fgetc(f))!=EOF) goto fault;
                    memset(p, c, n);
                    p+=n;
                } }
 fault: if (bits) memfree(bits);
    memfree(*bmi);
    fclose(f);
    error(WAKE_FAULTY_FORM);
    return NULL;
}
#elif defined(XLIB)
int handle_event(XEvent *event)
{
    switch (event->type) {
    case KeyPress: {
        char c;
        KeySym ks;
        if (XLookupString(&event->xkey, &c, 1, &ks, NULL)!=1)
            return 0;
        stack(1);
        wsp->flag=NUMBER|INTEGER;
        wsp->type=INTEGER_TYPE;
        wsp->unit.integer=(UBYTE)c;
        wsp++;
        return 1;
    }
    case ButtonPress:
        stack(3);
        if (gflag&GENERAL) {
            float x, y;
            x=(float)event->xbutton.x;
            y=(float)event->xbutton.y;
            inv_trans(&x, &y, trans);
            wsp->flag=NUMBER;
            wsp->type=FLOATING_TYPE;
            wsp->unit.floating=(double)x;
            wsp++;
            wsp->flag=NUMBER;
            wsp->type=FLOATING_TYPE;
            wsp->unit.floating=(double)y;
            wsp++;
        }
        else {
            wsp->flag=NUMBER|INTEGER;
            wsp->type=INTEGER_TYPE;
            wsp->unit.integer=event->xbutton.x;
            wsp++;
            wsp->flag=NUMBER|INTEGER;
            wsp->type=INTEGER_TYPE;
            wsp->unit.integer=event->xbutton.y;
            wsp++;
        }
        wsp->flag=0;
        wsp->type=TRUE_TYPE;
        wsp++;
        return 1;
    }
    return 0;
}

int error_handler(Display *d, XErrorEvent *ee)
{
    xerror=ee->serial==serial;
    return 0;
}

void free_tiles(void)
{
    if (tiles) {
        int i;
        for (i=0; i<n_colors-2; i++)
            if (tiles[i]) XFreePixmap(display, tiles[i]);
        tiles=NULL;
    }
}

int setup_halftone(float screenangle, float screenfreq)
{
    Pixmap tile;
    float *spotval=NULL;
    short *spotkey=NULL;
    int harea, hxbits, hxrep, hxsize, hysize, hpsize, hdx, hdy,
        ia, ix, iy, ii, xxu, xyu, yxu, yyu, xxs, xys, yxs, yys, rv= -1;
    float cosa, sina, xlen, ylen, rx, ry, cx, cy;
    cosa=cos(screenangle);
    sina=sin(screenangle);
    if ((xlen=xdensity/screenfreq)<1.0) xlen=1.0;
    if ((ylen=ydensity/screenfreq)<1.0) ylen=1.0;
    xxu=(int)(abs(xlen*cosa)+.5);
    xyu=(int)(abs(xlen*sina)+.5);
    yxu=(int)(abs(ylen*sina)+.5);
    yyu=(int)(abs(ylen*cosa)+.5);
    xxs=cosa<0.0?-xxu:xxu;
    xys=sina<0.0?xyu:-xyu;
    yxs=sina<0.0?-yxu:yxu;
    yys=cosa<0.0?-yyu:yyu;
    harea=xxu*yyu+xyu*yxu;
    if (harea<=1) goto err;
    hxrep=harea/gcd(yyu, yxu);
    hysize=harea/gcd(xxu, xyu);
    hxsize=hxrep/gcd(hxrep, 8);
    if (hxsize<20) hxsize*=20/hxsize;
    hpsize=hxsize*hysize;
    hdx=0;
    if ((ii=harea/hxrep)<hysize) {
        hdy=0;
        while (hdy!=ii)
            if (hdy>ii) {
                hdy-=yyu;
                if (yys<0) hdx+=xys;
                else hdx-=xys;
            }
            else {
                hdy+=yxu;
                if (yxs<0) hdx-=xxs;
                else hdx+=xxs;
            }
        if (hdx<0) hdx+=hxrep;
    }
    hdy=ii;
    if ((spotkey=(short *)memcalloc(harea*sizeof(short)))==NULL||
        (spotval=(float *)memcalloc(harea*sizeof(float)))==NULL) goto err;
    for (ia=0, ix=0, iy=0; ia<harea; ia++, ix++) {
        if (ix==hxrep) {
            ix=0;
            iy++;
        }
        rx=ix+.5;
        ry=iy+.5;
        cx=(rx*yys-ry*xys)/(float)harea;
        cy=(ry*xxs-rx*yxs)/(float)harea;
        cx-=floor(cx);
        cy-=floor(cy);
        cx+=cx-1.0;
        cy+=cy-1.0;
        spotval[ia]=1.0-(cx*cx+cy*cy);
        spotkey[ia]=ia;
    }
    ia=harea;
    for (;;) {
        ia=ia/3+1;
        for (ix=ia; ix<harea; ix++)
            for (iy=ix-ia; iy>=0&&
                     spotval[spotkey[iy]]>spotval[spotkey[iy+ia]]; iy-=ia) {
                ii=spotkey[iy];
                spotkey[iy]=spotkey[iy+ia];
                spotkey[iy+ia]=ii;
            }
        if (ia==1) break;
    }
    hxbits=hxsize*8;
    if ((tiles=(Pixmap *)memcalloc((harea-1)*sizeof(Pixmap)))==NULL) goto err;
    XSetForeground(display, gc, XWhitePixel(display, screen));
    for (ia=0; ia<harea-1; ia++) {
        serial=XNextRequest(display);
        tile=tiles[ia]=XCreatePixmap(display, window,
                                     hxbits, hysize, XDefaultDepth(display, screen));
        XSync(display, FALSE);
        if (xerror) {
            tiles[ia]=(Pixmap)NULL;
            goto err;
        }
        if (ia) XCopyArea(display, tiles[ia-1], tile, gc,
                          0, 0, hxbits, hysize, 0, 0);
        ii=spotkey[ia];
        iy=ii/hxrep;
        ii%=hxrep;
        while (iy<hysize) {
            for (ix=ii; ix<hxbits; ix+=hxrep)
                XDrawPoint(display, tile, gc, ix, iy);
            if ((ii+=hdx)>=hxrep) ii-=hxrep;
            iy+=hdy;
        }
    }
    n_colors=harea+1;
    dither=1;
    rv=0;
 err:
    if (spotkey) memfree(spotkey);
    if (spotval) memfree(spotval);
    if (rv) free_tiles();
    return rv;
}

unsigned long alloc_color(UWORD r, UWORD g, UWORD b)
{
    XColor xc;
    memset(&xc, 0, sizeof xc);
    xc.red=r;
    xc.green=g;
    xc.blue=b;
    xc.flags=DoRed|DoGreen|DoBlue;
    if (!XAllocColor(display, XDefaultColormap(display, screen), &xc))
        error(WAKE_CANNOT_ALLOCATE);
    pixel_min=min(pixel_min, xc.pixel);
    pixel_max=max(pixel_max, xc.pixel);
    return xc.pixel;
}

void set_dithered_fg(float gray)
{
    XGCValues gcv;
    int gi=(int)(gray*(float)(n_colors-1)+.5);
    if (gi<0) gi=0;
    else if (gi>=n_colors) gi=n_colors-1;
    if (gi==0||gi==n_colors-1) {
        gcv.foreground=gi?
            XWhitePixel(display, screen):XBlackPixel(display, screen);
        gcv.line_style=LineSolid;
        gcv.fill_style=FillSolid;
        XChangeGC(display, gc, GCForeground|GCLineStyle|GCFillStyle, &gcv);
        return;
    }
    gcv.foreground=XWhitePixel(display, screen);
    gcv.line_style=LineOnOffDash;
    gcv.fill_style=FillTiled;
    gcv.tile=tiles[gi-1];
    gcv.dashes=gi;
    XChangeGC(display, gc,
              GCForeground|GCLineStyle|GCFillStyle|GCTile|GCDashList, &gcv);
}

void set_dithered_bg(float gray)
{
    int gi=(int)(gray*(float)(n_colors-1)+.5);
    if (gi<0) gi=0;
    else if (gi>=n_colors) gi=n_colors-1;
    if (gi==0||gi==n_colors-1) {
        XSetBackground(display, gc, gi?
                       XWhitePixel(display, screen):XBlackPixel(display, screen));
        XSetWindowBackgroundPixmap(display, window, None);
        XSetWindowBackground(display, window, gi?
                             XWhitePixel(display, screen):XBlackPixel(display, screen));
        return;
    }
    XSetBackground(display, gc, XBlackPixel(display, screen));
    XSetWindowBackgroundPixmap(display, window, tiles[gi-1]);
}

void *make_image(int width, int height)
{
    void **ip;
    if ((ip=(void **)get_mem(1, IMAGE_TYPE, FALSE))==NULL) return NULL;
    serial=XNextRequest(display);
    *ip=(void *)XCreatePixmap(display, window, width, height,
                              XDefaultDepth(display, screen));
    XSync(display, FALSE);
    if (xerror) {
        last_save= *(HEAD **)((HEAD *)ip-1);
        memfree((HEAD *)ip-1);
        return NULL;
    }
    return (void *)ip;
}
#else
int setrgb(int r, int g, int b)
{
    int i, n=0;
    RGBVAL l, m;
    m.red=labs(rgb_map[0].red-r);
    m.green=labs(rgb_map[0].green-g);
    m.blue=labs(rgb_map[0].blue-b);
    if (!m.red&&!m.green&&!m.blue) return 0;
    for (i=1; i<=color_n; i++) {
        l.red=labs(rgb_map[i].red-r);
        l.green=labs(rgb_map[i].green-g);
        l.blue=labs(rgb_map[i].blue-b);
        if (!l.red&&!l.green&&!l.blue) return i;
        if (l.red<=m.red&&l.green<=m.green&&l.blue<=m.blue) {
            m=l;
            n=i;
        }
    }
    if (color_n<n_colors) {
        setrgbpalette(n= ++color_n, r, g, b);
        rgb_map[n].red=r;
        rgb_map[n].green=g;
        rgb_map[n].blue=b;
    }
    return n;
}

void *make_image(int width, int height)
{
    void **ip;
    if ((ip=(void **)get_mem(1, IMAGE_TYPE, FALSE))==NULL) return NULL;
    if ((*ip=(void *)_graphgetmem(imagesize(1, 1, width, height)))==NULL) {
        last_save= *(HEAD **)((HEAD *)ip-1);
        memfree((HEAD *)ip-1);
        return NULL;
    }
    *(short *)ip=width;
    *((short *)ip+1)=height;
    return (void *)ip;
}
#endif

float get_float(struct ELEMENT *elem)
{
    switch (elem->type) {
    case FLOATING_TYPE:
        return (float)elem->unit.floating;
    case FRACTION_TYPE:
        return (float)elem->unit.fract.num/(float)elem->unit.fract.num;
    case INTEGER_TYPE:
        return (float)elem->unit.integer;
    case UINTEGER_TYPE:
        return (float)elem->unit.uinteger;
    default:
        error(WAKE_INVALID_TYPE);
    }
    return 0.0;
}

void mul_mat_array(float *mat, struct ELEMENT *mat1, float *mat2)
{
    mat[A]=get_float(mat1 + A)*mat2[A]+get_float(mat1 + B)*mat2[C];
    mat[B]=get_float(mat1 + A)*mat2[B]+get_float(mat1 + B)*mat2[D];
    mat[C]=get_float(mat1 + C)*mat2[A]+get_float(mat1 + D)*mat2[C];
    mat[D]=get_float(mat1 + C)*mat2[B]+get_float(mat1 + D)*mat2[D];
    mat[Tx]=get_float(mat1 + Tx)*mat2[A]+get_float(mat1 + Ty)*mat2[C]+mat2[Tx];
    mat[Ty]=get_float(mat1 + Tx)*mat2[B]+get_float(mat1 + Ty)*mat2[D]+mat2[Ty];
}

void mul_mat(float *mat, float *mat1, float *mat2)
{
    mat[A]=mat1[A]*mat2[A]+mat1[B]*mat2[C];
    mat[B]=mat1[A]*mat2[B]+mat1[B]*mat2[D];
    mat[C]=mat1[C]*mat2[A]+mat1[D]*mat2[C];
    mat[D]=mat1[C]*mat2[B]+mat1[D]*mat2[D];
    mat[Tx]=mat1[Tx]*mat2[A]+mat1[Ty]*mat2[C]+mat2[Tx];
    mat[Ty]=mat1[Tx]*mat2[B]+mat1[Ty]*mat2[D]+mat2[Ty];
}

void mul_mat_float(float *mat, double *mat1, float *mat2)
{
    mat[A]=(float)mat1[A]*mat2[A]+(float)mat1[B]*mat2[C];
    mat[B]=(float)mat1[A]*mat2[B]+(float)mat1[B]*mat2[D];
    mat[C]=(float)mat1[C]*mat2[A]+(float)mat1[D]*mat2[C];
    mat[D]=(float)mat1[C]*mat2[B]+(float)mat1[D]*mat2[D];
    mat[Tx]=(float)mat1[Tx]*mat2[A]+(float)mat1[Ty]*mat2[C]+mat2[Tx];
    mat[Ty]=(float)mat1[Tx]*mat2[B]+(float)mat1[Ty]*mat2[D]+mat2[Ty];
}

void mul_mat_int(float *mat, long *mat1, float *mat2)
{
    mat[A]=(float)mat1[A]*mat2[A]+(float)mat1[B]*mat2[C];
    mat[B]=(float)mat1[A]*mat2[B]+(float)mat1[B]*mat2[D];
    mat[C]=(float)mat1[C]*mat2[A]+(float)mat1[D]*mat2[C];
    mat[D]=(float)mat1[C]*mat2[B]+(float)mat1[D]*mat2[D];
    mat[Tx]=(float)mat1[Tx]*mat2[A]+(float)mat1[Ty]*mat2[C]+mat2[Tx];
    mat[Ty]=(float)mat1[Tx]*mat2[B]+(float)mat1[Ty]*mat2[D]+mat2[Ty];
}

void mul_mat_uint(float *mat, ULONG *mat1, float *mat2)
{
    mat[A]=(float)mat1[A]*mat2[A]+(float)mat1[B]*mat2[C];
    mat[B]=(float)mat1[A]*mat2[B]+(float)mat1[B]*mat2[D];
    mat[C]=(float)mat1[C]*mat2[A]+(float)mat1[D]*mat2[C];
    mat[D]=(float)mat1[C]*mat2[B]+(float)mat1[D]*mat2[D];
    mat[Tx]=(float)mat1[Tx]*mat2[A]+(float)mat1[Ty]*mat2[C]+mat2[Tx];
    mat[Ty]=(float)mat1[Tx]*mat2[B]+(float)mat1[Ty]*mat2[D]+mat2[Ty];
}

void mul_mat_short(float *mat, short *mat1, float *mat2)
{
    mat[A]=(float)mat1[A]*mat2[A]+(float)mat1[B]*mat2[C];
    mat[B]=(float)mat1[A]*mat2[B]+(float)mat1[B]*mat2[D];
    mat[C]=(float)mat1[C]*mat2[A]+(float)mat1[D]*mat2[C];
    mat[D]=(float)mat1[C]*mat2[B]+(float)mat1[D]*mat2[D];
    mat[Tx]=(float)mat1[Tx]*mat2[A]+(float)mat1[Ty]*mat2[C]+mat2[Tx];
    mat[Ty]=(float)mat1[Tx]*mat2[B]+(float)mat1[Ty]*mat2[D]+mat2[Ty];
}

void mul_mat_ushort(float *mat, UWORD *mat1, float *mat2)
{
    mat[A]=(float)mat1[A]*mat2[A]+(float)mat1[B]*mat2[C];
    mat[B]=(float)mat1[A]*mat2[B]+(float)mat1[B]*mat2[D];
    mat[C]=(float)mat1[C]*mat2[A]+(float)mat1[D]*mat2[C];
    mat[D]=(float)mat1[C]*mat2[B]+(float)mat1[D]*mat2[D];
    mat[Tx]=(float)mat1[Tx]*mat2[A]+(float)mat1[Ty]*mat2[C]+mat2[Tx];
    mat[Ty]=(float)mat1[Tx]*mat2[B]+(float)mat1[Ty]*mat2[D]+mat2[Ty];
}

void mul_mat_string(float *mat, UBYTE *mat1, float *mat2)
{
    mat[A]=(float)mat1[A]*mat2[A]+(float)mat1[B]*mat2[C];
    mat[B]=(float)mat1[A]*mat2[B]+(float)mat1[B]*mat2[D];
    mat[C]=(float)mat1[C]*mat2[A]+(float)mat1[D]*mat2[C];
    mat[D]=(float)mat1[C]*mat2[B]+(float)mat1[D]*mat2[D];
    mat[Tx]=(float)mat1[Tx]*mat2[A]+(float)mat1[Ty]*mat2[C]+mat2[Tx];
    mat[Ty]=(float)mat1[Tx]*mat2[B]+(float)mat1[Ty]*mat2[D]+mat2[Ty];
}

void inv_mat(float *mat, float *mat1)
{
    float d=mat1[A]*mat1[D]-mat1[B]*mat1[C];
    if (d==(float)0.0) error(WAKE_DIVISION_BY_ZERO);
    mat[A]=mat1[D]/d;
    mat[B]= -mat1[B]/d;
    mat[C]= -mat1[C]/d;
    mat[D]=mat1[A]/d;
    mat[Tx]=(mat1[C]*mat1[Ty]-mat1[D]*mat1[Tx])/d;
    mat[Ty]=(mat1[B]*mat1[Tx]-mat1[A]*mat1[Ty])/d;
}

void inv_mat_float(float *mat, double *mat1)
{
    float d=(float)mat1[A]*(float)mat1[D]-(float)mat1[B]*(float)mat1[C];
    if (d==(float)0.0) error(WAKE_DIVISION_BY_ZERO);
    mat[A]=(float)mat1[D]/d;
    mat[B]= -(float)mat1[B]/d;
    mat[C]= -(float)mat1[C]/d;
    mat[D]=(float)mat1[A]/d;
    mat[Tx]=((float)mat1[C]*(float)mat1[Ty]-(float)mat1[D]*(float)mat1[Tx])/d;
    mat[Ty]=((float)mat1[B]*(float)mat1[Tx]-(float)mat1[A]*(float)mat1[Ty])/d;
}

void inv_trans(float *x, float *y, float *mat)
{
    float d, x1, y1;
    d=mat[A]*mat[D]-mat[B]*mat[C];
    if (d==(float)0.0) error(WAKE_DIVISION_BY_ZERO);
    x1=(*x*mat[D]-*y*mat[C]+mat[Ty]*mat[C]-mat[Tx]*mat[D])/d;
    y1=(*y*mat[A]-*x*mat[B]+mat[Tx]*mat[B]-mat[Ty]*mat[A])/d;
    *x=x1;
    *y=y1;
}

void inv_dtrans(float *dx, float *dy, float *mat)
{
    float d, dx1, dy1;
    d=mat[A]*mat[D]-mat[B]*mat[C];
    if (d==(float)0.0) error(WAKE_DIVISION_BY_ZERO);
    dx1=(*dx*mat[D]-*dy*mat[C])/d;
    dy1=(*dy*mat[A]-*dx*mat[B])/d;
    *dx=dx1;
    *dy=dy1;
}

void add_point(int x, int y)
{
    if (n_points==num_points) {
        POINT MQ *new=(POINT MQ *)memrealloc(UN points,
                                             (num_points+=INC_POINTS)*sizeof(POINT));
        if (!new) {
            memfree(UN points);
            n_counts=n_points=0;
            num_points=NUM_POINTS;
            if ((points=(POINT MQ *)memalloc(num_points*
                                             sizeof(POINT)))==NULL) memory_overflow();
            fpts("memory overflow,  point array restored\n",
                 stdout);
            wk_quit();
        }
        points=new;
    }
    points[n_points].x=x;
    points[n_points].y=y;
    n_points++;
}

void close_poly(void)
{
    if (points[n_points-1].x!=points[point_o].x||
        points[n_points-1].y!=points[point_o].y)
        add_point(points[point_o].x, points[point_o].y);
}

void store_poly(void)
{
    if (n_counts==no_counts) {
        int *new=(int *)memrealloc(counts,
                                   (no_counts+=INC_COUNTS)*sizeof(int));
        if (!new) {
            memfree(counts);
            n_counts=n_points=0;
            no_counts=NUM_COUNTS;
            counts=(int *)memalloc(no_counts*sizeof(int));
            if (!counts) memory_overflow();
            fpts("memory overflow, count array restored\n", stdout);
            wk_quit();
        }
        counts=new;
    }
    counts[n_counts++]=n_points-point_o;
}

void start_poly(int x, int y)
{
    if (gflag&AREA) {
        if (n_points>point_o+1) {
            close_poly();
            store_poly();
        }
        else if (n_points==point_o+1) n_points--;
        point_o=n_points;
        add_point(x, y);
    }
    else {
#ifdef AMIGA
        first.x=x;
        first.y=y;
        Move(rastport, x, y);
#elif defined(_WINDOWS)
        first.x=x;
        first.y=y;
        MoveTo(dc, x, y);
#elif defined(__TURBOC__)
        first.x=x;
        first.y=y;
        moveto(x, y);
#else
        first.x=x_coord=x;
        first.y=y_coord=y;
#endif
    }
}

void start_rpoly(int x, int y)
{
    if (gflag&AREA) {
        if (n_points)
            start_poly(points[n_points-1].x+x, points[n_points-1].y+y);
        else error(WAKE_ILLEGAL_USE);
    }
    else {
#ifdef AMIGA
        first.x=rastport->cp_x+x;
        first.y=rastport->cp_y+y;
        Move(rastport, first.x, first.y);
#elif defined(_WINDOWS)
        GetCurrentPositionEx(dc, &pt);
        first.x=pt.x+x;
        first.y=pt.y+y;
        MoveTo(dc, first.x, first.y);
#elif defined(__TURBOC__)
        first.x=getx();
        first.y=gety();
        moverel(x, y);
#else
        first.x=x_coord+=x;
        first.y=y_coord+=y;
#endif
    }
}

void free_image(void *image)
{
#ifdef AMIGA
    UBYTE i;
    struct BitMap *bp= *((struct BitMap **)image+1);
    for (i=0; i<depth; i++) FreeRaster(bp->Planes[i],
                                       *(UWORD *)image, *((UWORD *)image+1));
    FreeMem(bp, sizeof(struct BitMap));
#elif defined(VGALIB)
#elif defined(_WINDOWS)
    SelectObject(dcmem, bitmap);
    DeleteObject(*(HBITMAP *)image);
#elif defined(XLIB)
    XFreePixmap(display, *(Pixmap *)image);
#else
    _graphfreemem(*(void **)image, imagesize(0, 0, *(short *)image,
                                             *((short *)image+1)));
#endif
}

void free_state(STATE *state)
{
    if (state->points) memfree(state->points);
    if (state->counts) memfree(state->counts);
}

void shut_gra(void)
{
#ifdef AMIGA
    int i;
    if (!window) return;
    if (window) {
        CloseWindow(window);
        window=NULL;
    }
    if (layerinfo) {
        if (layer) {
            DeleteLayer((long)layerinfo, layer);
            layer=NULL;
        }
        DisposeLayerInfo(layerinfo);
        layerinfo=NULL;
    }
    if (inputevent) {
        FreeMem(inputevent, sizeof(struct InputEvent));
        inputevent=NULL;
    }
    if (conIOStdReq) {
        if (conIOStdReq->io_Device)
            CloseDevice((struct IORequest *)conIOStdReq);
        FreeMem(conIOStdReq, sizeof(struct IOStdReq));
        conIOStdReq=NULL;
    }
    if (bitmap) {
        for (i=0; i<depth; i++)
            if (bitmap->Planes[i]) FreeRaster(bitmap->Planes[i], width, 1);
        FreeMem(bitmap, sizeof(struct BitMap));
        bitmap=NULL;
    }
    if (rastport2) {
        FreeMem(rastport2, sizeof(struct RastPort));
        rastport2=NULL;
    }
    if (textfont) {
        CloseFont(textfont);
        textfont=NULL;
    }
    if (palette) {
        FreeMem(palette, n_colors*sizeof(UWORD));
        palette=NULL;
    }
    window=NULL;
#elif defined(VGALIB)
    if (!exposed) vga_flip();
    vga_setmode(TEXT);
#elif defined(_WINDOWS)
    if (!window) return;
    if (ddc) {
        ReleaseDC(window, ddc);
        ddc=(HDC)NULL;
    }
    DestroyWindow(window);
    if (dcalt) {
        DeleteDC(dcalt);
        dcalt=(HDC)NULL;
    }
    if (dcmem) {
        DeleteDC(dcmem);
        dcmem=(HDC)NULL;
    }
    if (rgn) {
        DeleteObject(rgn);
        rgn=(HRGN)NULL;
    }
    if (pen) {
        DeleteObject(pen);
        pen=(HPEN)NULL;
    }
    if (brush) {
        DeleteObject(brush);
        brush=(HBRUSH)NULL;
    }
    if (out_pen) {
        DeleteObject(out_pen);
        out_pen=(HPEN)NULL;
    }
    if (font) {
        DeleteObject(font);
        font=(HFONT)NULL;
    }
    if (bitmap) {
        DeleteObject(bitmap);
        bitmap=(HBITMAP)NULL;
    }
    if (palette) {
        DeleteObject(palette);
        palette=(HPALETTE)NULL;
    }
    if (points) {
        memfree(points);
        points=(POINT MQ *)NULL;
    }
    if (counts) {
        memfree(counts);
        counts=(int *)NULL;
    }
    dpen=(HPEN)NULL;
    dfont=(HFONT)NULL;
    dbrush=(HBRUSH)NULL;
    window=(HWND)NULL;
#elif defined(XLIB)
    /*
      unsigned long pixel;
    */
    if (!display) return;
    if (pixmap) {
        XFreePixmap(display, pixmap);
        pixmap=(Pixmap)NULL;
    }
    if (bitmap) {
        XFreePixmap(display, bitmap);
        bitmap=(Pixmap)NULL;
    }
    if (gcb) {
        XFreeGC(display, gcb);
        gcb=NULL;
    }
    if (gcp) {
        XFreeGC(display, gcp);
        gcp=NULL;
    }
    if (font_ptr&&font_ptr->fid) XUnloadFont(display, font_ptr->fid);
    /*
      for (pixel=pixel_min; pixel<=pixel_max; pixel++)
      XFreeColors(display, XDefaultColormap(display, screen), &pixel, 1, 0);
    */
    XDestroyWindow(display, window);
    if (region) {
        XDestroyRegion(region);
        region=NULL;
    }
    if (gc) {
        XFreeGC(display, gc);
        gc=NULL;
    }
    if (points) {
        memfree(points);
        points=(POINT MQ *)NULL;
    }
    if (counts) {
        memfree(counts);
        counts=(int *)NULL;
    }
    if (dither) free_tiles();
    XCloseDisplay(display);
    display=NULL;
#else
    if (!graphready) return;
    if (rgb_map) {
        memfree(rgb_map);
        rgb_map=NULL;
    }
    if (bitmap) {
        _graphfreemem(bitmap, bitmap_size);
        bitmap=NULL;
    }
    if (points) {
        memfree(UN points);
        points=NULL;
    }
    if (counts) {
        memfree(counts);
        counts=NULL;
    }
    closegraph();
    graphready=0;
#endif
}

/* radius angle endangle - arc - */
void wk_arc(void)
{
    int x, y;
    float a, e, i, r, ax, ay;
    check(3);
    r=(float)check_double(wsp-3);
    if (gflag&GENERAL) {
        a=(float)check_double(wsp-2);
        e=(float)check_double(wsp-1);
        i=(float)((XRELAT(r, 0)+YRELAT(0, r))/2.0);
        if (i==(float)0.0) i=e;
        else i=(float)(fabs(e-a)/i);
        ax=(float)(r*cos(a));
        ay=(float)(r*sin(a));
        if (gflag&AREA) {
            if (!n_points) error(WAKE_ILLEGAL_USE);
            x=points[n_points-1].x-XRELAT(ax, ay);
            y=points[n_points-1].y-YRELAT(ax, ay);
            while ((a+=i)<e) {
                ax=(float)(r*cos(a));
                ay=(float)(r*sin(a));
                add_point(x+XRELAT(ax, ay), y+YRELAT(ax, ay));
            }
            ax=(float)(r*cos(e));
            ay=(float)(r*sin(e));
            add_point(x+XRELAT(ax, ay), y+YRELAT(ax, ay));
        }
        else {
#ifdef AMIGA
            x=rastport->cp_x-XRELAT(ax, ay);
            y=rastport->cp_y-YRELAT(ax, ay);
#elif defined(_WINDOWS)
            GetCurrentPositionEx(dc, &pt);
            x=pt.x-XRELAT(ax, ay);
            y=pt.y-YRELAT(ax, ay);
#elif defined(__TURBOC__)
            x=getx()-XRELAT(ax, ay);
            y=gety()-YRELAT(ax, ay);
#else
            int x1, y1;
            x=x_coord-XRELAT(ax, ay);
            y=y_coord-YRELAT(ax, ay);
#endif
            while ((a+=i)<e) {
                ax=(float)(r*cos(a));
                ay=(float)(r*sin(a));
#ifdef AMIGA
                Draw(rastport, x+XRELAT(ax, ay), y+YRELAT(ax, ay));
#elif defined(VGALIB)
                x1=x+XRELAT(ax, ay);
                y1=y+YRELAT(ax, ay);
                vga_drawline(x_coord, y_coord, x1, y1);
                x_coord=x1;
                y_coord=y1;
#elif defined(_WINDOWS)
                LineTo(dc, x+XRELAT(ax, ay), y+YRELAT(ax, ay));
#elif defined(XLIB)
                x1=x+XRELAT(ax, ay);
                y1=y+YRELAT(ax, ay);
                XDrawLine(display, drawable, gc,
                          x_coord, y_coord, x1, y1);
                x_coord=x1;
                y_coord=y1;
#else
                lineto(x+XRELAT(ax, ay), y+YRELAT(ax, ay));
#endif
            }
            ax=(float)(r*cos(e));
            ay=(float)(r*sin(e));
#ifdef AMIGA
            Draw(rastport, x+XRELAT(ax, ay), y+YRELAT(ax, ay));
#elif defined(VGALIB)
            x1=x+XRELAT(ax, ay);
            y1=y+YRELAT(ax, ay);
            vga_drawline(x_coord, y_coord, x1, y1);
            x_coord=x1;
            y_coord=y1;
#elif defined(_WINDOWS)
            LineTo(dc, x+XRELAT(ax, ay), y+YRELAT(ax, ay));
#elif defined(XLIB)
            x1=x+XRELAT(ax, ay);
            y1=y+YRELAT(ax, ay);
            XDrawLine(display, drawable, gc, x_coord, y_coord, x1, y1);
            x_coord=x1;
            y_coord=y1;
#else
            lineto(x+XRELAT(ax, ay), y+YRELAT(ax, ay));
#endif
        }
    }
    else
        {
            a=(float)check_double(wsp-2);
            e=(float)check_double(wsp-1);
            if (r==(float)0.0) i=e;
            else i=(float)(fabs(e-a)/r);
            if (gflag&AREA) {
                if (!n_points) error(WAKE_ILLEGAL_USE);
                x=(int)(points[n_points-1].x-XARC(a));
                y=(int)(points[n_points-1].y-YARC(a));
                while ((a+=i)<e) add_point((int)(x+XARC(a)), (int)(y+YARC(a)));
                add_point((int)(x+XARC(e)), (int)(y+YARC(e)));
            }
            else {
#ifdef AMIGA
                x=rastport->cp_x-XARC(a);
                y=rastport->cp_y-YARC(a);
#elif defined(_WINDOWS)
                GetCurrentPositionEx(dc, &pt);
                x=(int)(pt.x-XARC(a));
                y=(int)(pt.y-YARC(a));
#elif defined(__TURBOC__)
                x=getx()-XARC(a);
                y=gety()-YARC(a);
#else
                int x1, y1;
                x=x_coord-XARC(a);
                y=y_coord-YARC(a);
#endif
                while ((a+=i)<e)
#ifdef AMIGA
                    Draw(rastport, x+XARC(a), y+YARC(a));
                Draw(rastport, x+XARC(e), y+YARC(e));
#elif defined(VGALIB)
                {
                    x1=x+XARC(a);
                    y1=y+YARC(a);
                    vga_drawline(x_coord, y_coord, x1, y1);
                    x_coord=x1;
                    y_coord=y1;
                }
                x1=x+XARC(e);
                y1=y+YARC(e);
                vga_drawline(x_coord, y_coord, x1, y1);
                x_coord=x1;
                y_coord=y1;
#elif defined(_WINDOWS)
                LineTo(dc, x+XARC(a), y+YARC(a));
                LineTo(dc, x+XARC(e), y+YARC(e));
#elif defined(XLIB)
                {
                    x1=x+XARC(a);
                    y1=y+YARC(a);
                    XDrawLine(display, drawable, gc, x_coord, y_coord,
                              x1, y1);
                    x_coord=x1;
                    y_coord=y1;
                }
                x1=x+XARC(e);
                y1=y+YARC(e);
                XDrawLine(display, drawable, gc, x_coord, y_coord, x1, y1);
                x_coord=x1;
                y_coord=y1;
#else
                lineto(x+XARC(a), y+YARC(a));
                lineto(x+XARC(e), y+YARC(e));
#endif
            }
        }
    wsp-=3;
}

/* - area - */
void wk_area(void)
{
    gflag|=AREA;
}

/* - aspectratio - ratio */
void wk_aspectratio(void)
{
    stack(1);
    wsp->flag=NUMBER;
    wsp->unit.fract.num=aspecty;
    wsp->unit.fract.den=aspectx;
    canon(wsp);
    wsp++;
}

/* - catch - integer | x y true */
void wk_catch(void)
{
#ifdef AMIGA
    struct IntuiMessage *imsg;
    for (;;)
        {
            Wait(1<<window->UserPort->mp_SigBit);
            while (imsg=(struct IntuiMessage *)GetMsg(window->UserPort))
                if (handle_imsg(imsg)) return;
        }
#elif defined(VGALIB)
    stack(1);
    wsp->unit.integer = getchar();
    wsp->type = INTEGER_TYPE;
    wsp->flag = NUMBER | INTEGER;
    wsp++;
#elif defined(_WINDOWS)
    MSG msg;
    while (GetMessage(&msg, (HWND)NULL, 0, 0) != -1) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
        if (handle_msg()) return;
    }
#elif defined(XLIB)
    XEvent event;
    for (;;) {
        XWindowEvent(display, window, KeyPressMask|ButtonPressMask,
                     &event);
        if (handle_event(&event)) return;
    }
#else
    stack(1);
    wsp->unit.integer = bioskey(0);
    wsp->type = INTEGER_TYPE;
    wsp->flag = NUMBER | INTEGER;
    wsp++;
#endif
}

/* - complete - */
void wk_complete(void)
{
    if (gflag&AREA) {
        if (n_points>point_o+1) close_poly();
        return;
    }
#ifdef AMIGA
    if (rastport->cp_x!=first.x||rastport->cp_y!=first.y)
        Draw(rastport, first.x, first.y);
#elif defined(VGALIB)
    if (x_coord!=first.x||y_coord!=first.y) {
        vga_drawline(x_coord, y_coord, first.x, first.y);
        x_coord=first.x;
        y_coord=first.y;
    }
#elif defined(_WINDOWS)
    GetCurrentPositionEx(dc, &pt);
    if (pt.x!=first.x||pt.y!=first.y)
        LineTo(dc, first.x, first.y);
#elif defined(XLIB)
    if (x_coord!=first.x||y_coord!=first.y) {
        XDrawLine(display, drawable, gc, x_coord, y_coord, first.x,
                  first.y);
        x_coord=first.x;
        y_coord=first.y;
    }
#else
    if (getx()!=first.x||gety()!=first.y) lineto(first.x, first.y);
#endif
}

/* matrix - concat - */
void wk_concat(void)
{
    float t[6];
    check(1);
    if (wsp[-1].type!=ARRAY_TYPE &&
        (wsp[-1].type<FLOATARRAY_TYPE ||
         wsp[-1].type>STRING_TYPE))
        error(WAKE_INVALID_TYPE);
    if (wsp[-1].unit.line.length<6) error(WAKE_INDEX_OUT_OF_RANGE);
    switch (wsp[-1].type) {
    case ARRAY_TYPE:
        mul_mat_array(t, wsp[-1].unit.array, trans);
        break;
    case FLOATARRAY_TYPE:
        mul_mat_float(t, wsp[-1].unit.floatarray, trans);
        break;
    case INTARRAY_TYPE:
        mul_mat_int(t, wsp[-1].unit.intarray, trans);
        break;
    case UINTARRAY_TYPE:
        mul_mat_uint(t, wsp[-1].unit.uintarray, trans);
        break;
    case SINGLEARRAY_TYPE:
        mul_mat(t, wsp[-1].unit.singlearray, trans);
        break;
    case SHORTARRAY_TYPE:
        mul_mat_short(t, wsp[-1].unit.shortarray, trans);
        break;
    case USHORTARRAY_TYPE:
        mul_mat_ushort(t, wsp[-1].unit.ushortarray, trans);
        break;
    case STRING_TYPE:
        mul_mat_string(t, wsp[-1].unit.string, trans);
        break;
    }
    memcpy(trans, t, sizeof trans);
    wsp--;
}

/* matrix1 matrix2 matrix3 - concatmatrix - matrix3 */
void wk_concatmatrix(void)
{
    int i;
    check(3);
    for (i= -3; i<0; i++) {
        if (wsp[i].type!=SINGLEARRAY_TYPE) error(WAKE_INVALID_TYPE);
        if (wsp[i].unit.line.length<6) error(WAKE_INDEX_OUT_OF_RANGE);
    }
    mul_mat(wsp[-1].unit.singlearray, wsp[-3].unit.singlearray,
            wsp[-2].unit.singlearray);
    wsp[-3]=wsp[-1];
    wsp-=2;
}

/* - cover - */
void wk_cover(void)
{
#if defined(VGALIB)
    if (exposed) {
        vga_flip();
        exposed=0;
    }
#elif defined(_WINDOWS)
    extern HWND hwnd;
    BringWindowToTop(hwnd);
    SetActiveWindow(hwnd);
    SetFocus(hwnd);
#elif defined(XLIB)
    extern Window wnd;
    XRaiseWindow(display, wnd);
    XSetInputFocus(display, wnd, RevertToNone, CurrentTime);
#endif
}

/* matrix - currentmatrix - matrix */
void wk_currentmatrix(void)
{
    int i;
    check(1);
    if (wsp[-1].type != ARRAY_TYPE &&
        (wsp[-1].type < FLOATARRAY_TYPE ||
         wsp[-1].type > STRING_TYPE))
        error(WAKE_INVALID_TYPE);
    if (wsp[-1].unit.line.length<6) error(WAKE_INDEX_OUT_OF_RANGE);
    switch (wsp[-1].type) {
    case ARRAY_TYPE:
        for (i = 0; i < 6; i++) {
            wsp[-1].unit.area[i].flag = NUMBER;
            wsp[-1].unit.area[i].type = FLOATING_TYPE;
            wsp[-1].unit.area[i].unit.floating = (double)trans[i];
        }
        break;
    case FLOATARRAY_TYPE:
        for (i = 0; i < 6; i++)
            wsp[-1].unit.floatarray[i] = (double)trans[i];
        break;
    case INTARRAY_TYPE:
        for (i = 0; i < 6; i++)
            wsp[-1].unit.intarray[i] = (long)trans[i];
        break;
    case UINTARRAY_TYPE:
        for (i = 0; i < 6; i++)
            wsp[-1].unit.uintarray[i] = (ULONG)trans[i];
        break;
    case SINGLEARRAY_TYPE:
        memcpy(wsp[-1].unit.singlearray, trans, sizeof trans);
        break;
    case SHORTARRAY_TYPE:
        for (i = 0; i < 6; i++)
            wsp[-1].unit.shortarray[i] = (short)trans[i];
        break;
    case USHORTARRAY_TYPE:
        for (i = 0; i < 6; i++)
            wsp[-1].unit.ushortarray[i] = (UWORD)trans[i];
        break;
    case STRING_TYPE:
        for (i = 0; i < 6; i++)
            wsp[-1].unit.string[i] = (UBYTE)trans[i];
        break;
    }
}

/* - currentpoint - x y */
void wk_currentpoint(void)
{
    stack(2);
    if (gflag&GENERAL) {
        float x, y;
        if (gflag&AREA) {
            if (!n_points) error(WAKE_ILLEGAL_USE);
            x=(float)points[n_points-1].x;
            y=(float)points[n_points-1].y;
        }
        else {
#ifdef AMIGA
            x=(float)rastport->cp_x;
            y=(float)rastport->cp_y;
#elif defined(_WINDOWS)
            GetCurrentPositionEx(dc, &pt);
            x=(float)pt.x;
            y=(float)pt.y;
#elif defined(__TURBOC__)
            x=(float)getx();
            y=(float)gety();
#else
            x=(float)x_coord;
            y=(float)y_coord;
#endif
        }
        inv_trans(&x, &y, trans);
        wsp->flag=NUMBER;
        wsp->type=FLOATING_TYPE;
        wsp->unit.floating=(double)x;
        wsp++;
        wsp->flag=NUMBER;
        wsp->type=FLOATING_TYPE;
        wsp->unit.floating=(double)y;
        wsp++;
    }
    else if (gflag&AREA) {
        if (!n_points) error(WAKE_ILLEGAL_USE);
        wsp->flag=NUMBER|INTEGER;
        wsp->type=INTEGER_TYPE;
        wsp->unit.integer=(long)points[n_points-1].x;
        wsp++;
        wsp->flag=NUMBER|INTEGER;
        wsp->type=INTEGER_TYPE;
        wsp->unit.integer=(long)points[n_points-1].y;
        wsp++;
    }
    else {
#ifdef AMIGA
        wsp->flag=NUMBER|INTEGER;
        wsp->type=INTEGER_TYPE;
        wsp->unit.integer=(long)rastport->cp_x;
        wsp++;
        wsp->flag=NUMBER|INTEGER;
        wsp->type=INTEGER_TYPE;
        wsp->unit.integer=(long)rastport->cp_y;
        wsp++;
#elif defined(_WINDOWS)
        GetCurrentPositionEx(dc, &pt);
        wsp->flag=NUMBER|INTEGER;
        wsp->type=INTEGER_TYPE;
        wsp->unit.integer=pt.x;
        wsp++;
        wsp->flag=NUMBER|INTEGER;
        wsp->type=INTEGER_TYPE;
        wsp->unit.integer=pt.y;
        wsp++;
#elif defined(__TURBOC__)
        wsp->flag=NUMBER|INTEGER;
        wsp->type=INTEGER_TYPE;
        wsp->unit.integer=(long)getx();
        wsp++;
        wsp->flag=NUMBER|INTEGER;
        wsp->type=INTEGER_TYPE;
        wsp->unit.integer=(long)gety();
        wsp++;
#else
        wsp->flag=NUMBER|INTEGER;
        wsp->type=INTEGER_TYPE;
        wsp->unit.integer=x_coord;
        wsp++;
        wsp->flag=NUMBER|INTEGER;
        wsp->type=INTEGER_TYPE;
        wsp->unit.integer=y_coord;
        wsp++;
#endif
    }
}

void curve(int relat)
{
    register short s, t;
    register int x, y;
    long ttt3, t3, tt3;
    long tt, ttt;
    long mr[3];
    long v[4][2];
    float dx, dy;
    UBYTE i;
    check(6);
    if (gflag&AREA) {
        if (!n_points) error(WAKE_ILLEGAL_USE);
        x=v[0][0]=points[n_points-1].x;
        y=v[0][1]=points[n_points-1].y;
    }
    else {
#ifdef AMIGA
        x=v[0][0]=rastport->cp_x;
        y=v[0][1]=rastport->cp_y;
#elif defined(_WINDOWS)
        GetCurrentPositionEx(dc, &pt);
        x=v[0][0]=pt.x;
        y=v[0][1]=pt.y;
#elif defined(__TURBOC__)
        x=v[0][0]=getx();
        y=v[0][1]=gety();
#else
        x=v[0][0]=x_coord;
        y=v[0][1]=y_coord;
#endif
    }
    for (i=1; i<4; i++) {
        if (gflag&GENERAL) {
            dx=(float)check_double(wsp+(i-1)*2-6);
            dy=(float)check_double(wsp+(i-1)*2-5);
            if (relat) {
                v[i][0]=x+XRELAT(dx, dy);
                v[i][1]=y+YRELAT(dx, dy);
            }
            else {
                v[i][0]=XCOORD(dx, dy);
                v[i][1]=YCOORD(dx, dy);
            }
        }
        else {
            v[i][0]=check_integer(wsp+(i-1)*2-6);
            v[i][1]=check_integer(wsp+(i-1)*2-5);
            if (relat)
                {
                    v[i][0]+=x;
                    v[i][1]+=y;
                }
        }
    }
    dx=(float)((s=(short)(v[0][1]-v[1][1]))?(float)(v[0][0]-v[1][0])/(float)s:0.0);
    dy=(float)((s=(short)(v[1][1]-v[2][1]))?(float)(v[1][0]-v[2][0])/(float)s:0.0);
    if (fabs(dx-dy)<0.01&&
        fabs(dy-((s=(short)(v[2][1]-v[3][1]))?(float)(v[2][0]-v[3][0])/(float)s:0.0))<0.01) {
        if (gflag&AREA) add_point(x, y);
        else
#ifdef AMIGA
            Draw(rastport, x, y);
#elif defined(VGALIB)
        {
            vga_drawline(x_coord, y_coord, x, y);
            x_coord=x;
            y_coord=y;
        }
#elif defined(_WINDOWS)
        LineTo(dc, x, y);
#elif defined(XLIB)
        {
            XDrawLine(display, drawable, gc, x_coord, y_coord, x, y);
            x_coord=x;
            y_coord=y;
        }
#else
        lineto(x, y);
#endif
        wsp-=6;
        return;
    }
    s=(short)(abs((int)(v[3][0]-v[0][0]))+abs((int)(v[3][1]-v[0][1])));
    s>>=2;
    if (s==0) s=ONE;
    else if (s>ONE) s=1;
    else s=(short)(ONE/s);
    if (gflag&AREA)
        for (t=s; t<=ONE; t=(short)(t+s)) {
            tt=(long)t*(long)t;
            ttt=tt*(long)t;
            ttt3=S20(MUL3(ttt));
            tt3=S10(MUL3(tt));
            t3=MUL3((long)t);
            ttt=S20(ttt);
            mr[0]= -ttt+tt3-t3+ONE;
            mr[1]=ttt3-(tt3<<1)+t3;
            mr[2]= -ttt3+tt3;
            x=(mr[0]*v[0][0]+mr[1]*v[1][0]+
               mr[2]*v[2][0]+ttt*v[3][0])>>SHIFTS;
            y=(mr[0]*v[0][1]+mr[1]*v[1][1]+
               mr[2]*v[2][1]+ttt*v[3][1])>>SHIFTS;
            add_point(x, y);
        }
    else for (t=0; t<=ONE; t=(short)(t+s)) {
            tt=(long)t*(long)t;
            ttt=tt*(long)t;
            ttt3=S20(MUL3(ttt));
            tt3=S10(MUL3(tt));
            t3=MUL3((long)t);
            ttt=S20(ttt);
            mr[0]= -ttt+tt3-t3+ONE;
            mr[1]=ttt3-(tt3<<1)+t3;
            mr[2]= -ttt3+tt3;
            x=(mr[0]*v[0][0]+mr[1]*v[1][0]+
               mr[2]*v[2][0]+ttt*v[3][0])>>SHIFTS;
            y=(mr[0]*v[0][1]+mr[1]*v[1][1]+
               mr[2]*v[2][1]+ttt*v[3][1])>>SHIFTS;
#ifdef AMIGA
            Draw(rastport, x, y);
#elif defined(VGALIB)
            vga_drawline(x_coord, y_coord, x, y);
            x_coord=x;
            y_coord=y;
#elif defined(_WINDOWS)
            LineTo(dc, x, y);
#elif defined(XLIB)
            XDrawLine(display, drawable, gc, x_coord, y_coord, x, y);
            x_coord=x;
            y_coord=y;
#else
            lineto(x, y);
#endif
        }
    wsp-=6;
}

/* dx1 dy1 dx2 dy2 ax2 ay2 - curve - */
void wk_curve(void)
{
    curve(0);
}

/* rdx1 rdy1 rdx2 rdy2 rax2 ray2 - curverel - */
void wk_curverel(void)
{
    curve(-1);
}

/* matrix - defaultmatrix - matrix */
void wk_defaultmatrix(void)
{
    float *r;
    check(1);
    if (wsp[-1].type!=SINGLEARRAY_TYPE) error(WAKE_INVALID_TYPE);
    if (wsp[-1].unit.line.length<6) error(WAKE_INDEX_OUT_OF_RANGE);
    r=wsp[-1].unit.singlearray;
    r[A]=(float)width;
    r[D]=(float)height;
    r[B]=r[C]=r[Tx]=r[Ty]=(float)0.0;
}

/* - dimensions - width height */
void wk_dimensions(void)
{
    stack(2);
    wsp->flag=NUMBER|INTEGER;
    wsp->type=INTEGER_TYPE;
    wsp->unit.integer=width;
    wsp++;
    wsp->flag=NUMBER|INTEGER;
    wsp->type=INTEGER_TYPE;
    wsp->unit.integer=height;
    wsp++;
}

/* x y - draw - */
void wk_draw(void)
{
    check(2);
    if (gflag&GENERAL) {
        float x=(float)check_double(wsp-2), y=(float)check_double(wsp-1);
        if (gflag&AREA) add_point(XCOORD(x, y), YCOORD(x, y));
        else
#ifdef AMIGA
            Draw(rastport, XCOORD(x, y), YCOORD(x, y));
#elif defined(VGALIB)
        {
            int x1=XCOORD(x, y), y1=YCOORD(x, y);
            vga_drawline(x_coord, y_coord, x1, y1);
            x_coord=x1;
            y_coord=y1;
        }
#elif defined(_WINDOWS)
        LineTo(dc, XCOORD(x, y), YCOORD(x, y));
#elif defined(XLIB)
        {
            int x1=XCOORD(x, y), y1=YCOORD(x, y);
            XDrawLine(display, drawable, gc, x_coord, y_coord, x1, y1);
            x_coord=x1;
            y_coord=y1;
        }
#else
        lineto(XCOORD(x, y), YCOORD(x, y));
#endif
    }
    else if (gflag&AREA)
        add_point((int)check_integer(wsp-2), (int)check_integer(wsp-1));
    else
#ifdef AMIGA
        Draw(rastport, check_integer(wsp-2), check_integer(wsp-1));
#elif defined(VGALIB)
    {
        int x1=check_integer(wsp-2), y1=check_integer(wsp-1);
        vga_drawline(x_coord, y_coord, x1, y1);
        x_coord=x1;
        y_coord=y1;
    }
#elif defined(_WINDOWS)
    LineTo(dc, (int)check_integer(wsp-2), (int)check_integer(wsp-1));
#elif defined(XLIB)
    {
        int x1=check_integer(wsp-2), y1=check_integer(wsp-1);
        XDrawLine(display, drawable, gc, x_coord, y_coord, x1, y1);
        x_coord=x1;
        y_coord=y1;
    }
#else
    lineto((int)check_integer(wsp-2), (int)check_integer(wsp-1));
#endif
    wsp-=2;
}

/* a b - drawrel - */
void wk_drawrel(void)
{
    check(2);
    if (gflag&GENERAL) {
        float x=(float)check_double(wsp-2), y=(float)check_double(wsp-1);
        if (gflag&AREA) {
            if (!n_points) error(WAKE_ILLEGAL_USE);
            add_point(points[n_points-1].x+XRELAT(x, y),
                      points[n_points-1].y+YRELAT(x, y));
        }
        else
#ifdef AMIGA
            Draw(rastport, rastport->cp_x+XRELAT(x, y),
                 rastport->cp_y+YRELAT(x, y));
#elif defined(VGALIB)
        {
            int x1=x_coord+XRELAT(x, y), y1=y_coord+YRELAT(x, y);
            vga_drawline(x_coord, y_coord, x1, y1);
            x_coord=x1;
            y_coord=y1;
        }
#elif defined(_WINDOWS)
        {
            GetCurrentPositionEx(dc, &pt);
            LineTo(dc, pt.x+XRELAT(x, y),
                   pt.y+YRELAT(x, y));
        }
#elif defined(XLIB)
        {
            int x1=x_coord+XRELAT(x, y), y1=y_coord+YRELAT(x, y);
            XDrawLine(display, drawable, gc, x_coord, y_coord, x1, y1);
            x_coord=x1;
            y_coord=y1;
        }
#else
        linerel(XCOORD(x, y), YCOORD(x, y));
#endif
    }
    else if (gflag&AREA) {
        if (!n_points) error(WAKE_ILLEGAL_USE);
        add_point(points[n_points-1].x+(int)check_integer(wsp-2),
                  points[n_points-1].y+(int)check_integer(wsp-1));
    }
    else
#ifdef AMIGA
        Draw(rastport, rastport->cp_x+check_integer(wsp-2),
             rastport->cp_y+check_integer(wsp-1));
#elif defined(VGALIB)
    {
        int x1=x_coord+check_integer(wsp-2),
            y1=y_coord+check_integer(wsp-1);
        vga_drawline(x_coord, y_coord, x1, y1);
        x_coord=x1;
        y_coord=y1;
    }
#elif defined(_WINDOWS)
    {
        GetCurrentPositionEx(dc, &pt);
        LineTo(dc, pt.x+(int)check_integer(wsp-2),
               pt.y+(int)check_integer(wsp-1));
    }
#elif defined(XLIB)
    {
        int x1=x_coord+check_integer(wsp-2),
            y1=y_coord+check_integer(wsp-1);
        XDrawLine(display, drawable, gc, x_coord, y_coord, x1, y1);
        x_coord=x1;
        y_coord=y1;
    }
#else
    linerel((int)check_integer(wsp-2), (int)check_integer(wsp-1));
#endif
    wsp-=2;
}

WAKE_FUNC gra_funcs[]={
    {"arc",         wk_arc},
    {"area",        wk_area},
    {"aspectratio",     wk_aspectratio},
    {"catch",       wk_catch},
    {"complete",        wk_complete},
    {"concat",      wk_concat},
    {"concatmatrix",    wk_concatmatrix},
    {"cover",       wk_cover},
    {"currentmatrix",   wk_currentmatrix},
    {"currentpoint",    wk_currentpoint},
    {"curve",       wk_curve},
    {"curverel",        wk_curverel},
    {"defaultmatrix",   wk_defaultmatrix},
    {"dimensions",      wk_dimensions},
    {"draw",        wk_draw},
    {"drawrel",     wk_drawrel}};

int init_gra(void)
{
    if (enter_funcs(gra_funcs,ELEMS(gra_funcs))||init_gra2()||
        (points=(POINT MQ *)memalloc(num_points*sizeof(POINT)))==NULL||
        (counts=(int *)memalloc(no_counts*sizeof(int)))==NULL) return -1;
    {
#ifdef AMIGA
        UBYTE i;
        struct NewWindow *nw;
        viewport=GfxBase->ActiView->ViewPort;
        if ((nw=(struct NewWindow *)AllocMem(
                                             sizeof(struct NewWindow),MEMF_PUBLIC|MEMF_CLEAR))==NULL) return 100;
        nw->Width=nw->MinWidth=nw->MaxWidth=width=viewport->DWidth;
        nw->Height=nw->MaxWidth=nw->MaxHeight=height=viewport->DHeight;
        nw->IDCMPFlags=MOUSEBUTTONS|RAWKEY;
        nw->Flags=WINDOWDEPTH|BORDERLESS|ACTIVATE;
        nw->Type=WBENCHSCREEN;
        window=(struct Window *)OpenWindow(nw);
        FreeMem(nw,sizeof(struct NewWindow));
        if (!window) return 101;
        rastport=window->RPort;
        colormap=viewport->ColorMap;
        bbitmap=dbitmap=rastport->BitMap;
        depth=dbitmap->Depth;
        if (viewport->Modes&LACE) {
            aspectx=2;
            aspecty=1;
            n_colors=1<<(depth-2);
        }
        else {
            aspectx=aspecty=1;
            n_colors=1<<depth;
        }
        if ((layerinfo=(struct Layer_Info *)NewLayerInfo())==NULL||
            (layer=(struct Layer *)CreateUpfrontLayer(layerinfo,
                                                      dbitmap,0,0,width-1,height-1,LAYERSIMPLE,NULL))==NULL) return 102;
        rastport->Layer=layer;
        if ((bitmap=(struct BitMap *)AllocMem(sizeof(struct BitMap),
                                              MEMF_PUBLIC|MEMF_CLEAR))==NULL) return 103;
        for (i=0; i<depth; i++)
            if ((bitmap->Planes[i]=(PLANEPTR)AllocRaster(width,1))==NULL) return 104;
        InitBitMap(bitmap,depth,width,1);
        if ((rastport2=(struct RastPort *)AllocMem(sizeof(struct RastPort),
                                                   MEMF_PUBLIC|MEMF_CLEAR))==NULL) return 105;
        InitRastPort(rastport2);
        rastport2->BitMap=bitmap;
        if ((conIOStdReq=(struct IOStdReq *)AllocMem(
                                                     sizeof(struct IOStdReq),MEMF_PUBLIC|MEMF_CLEAR))==NULL||
            OpenDevice("console.device",-1,
                       (struct IORequest *)conIOStdReq,0)||
            (inputevent=(struct InputEvent *)AllocMem(
                                                      sizeof(struct InputEvent),MEMF_PUBLIC|MEMF_CLEAR))==NULL) return 106;
        ConsoleDevice=(struct Library *)conIOStdReq->io_Device;
        if ((palette=(UWORD *)AllocMem(
                                       n_colors*sizeof(UWORD),MEMF_PUBLIC))==NULL) return 107;
        for (i=0; i<n_colors; i++) palette[i]=GetRGB4(colormap,i);
        SetRGB4(viewport,0,rgbbackcolor.red,rgbbackcolor.green,
                rgbbackcolor.blue);
        SetRGB4(viewport,1,rgbcolor.red,rgbcolor.green,rgbcolor.blue);
        SetAPen(rastport,1);
        SetBPen(rastport,0);
        SetOPen(rastport,1);
#elif defined(VGALIB)
        {
            int m;
            vga_init();
            if ((m=vga_getdefaultmode())==-1) {
                m=37;
                while (m>0&&
                       (!vga_hasmode(m)||vga_getmodeinfo(m)->colors<256||
                        vga_getmodeinfo(m)->flags&IS_MODEX)) m--;
                if (m==0) return 100;
            }
            vga_setmode(m);
            gl_setcontextvga(m);
            bytesperpixel=BYTESPERPIXEL;
            width=WIDTH;
            height=HEIGHT;
            n_colors=COLORS;
            aspectx=aspecty=1;
            vga_setpalette(0,rgbbackcolor.red>>2,rgbbackcolor.green>>2,
                           rgbbackcolor.blue>>2);
            setrgb(rgbcolor.red,rgbcolor.green,rgbcolor.blue);
            fontbmap=gl_font8x8;
            fontsize=8;
            color_n=0;
        }
#elif defined(_WINDOWS)
        BITMAP bm;
        LOGPALETTE *lp;
        if ((dc=CreateIC("DISPLAY",NULL,NULL,NULL))==NULL) return 100;
        paletteBased=(char)((GetDeviceCaps(dc,RASTERCAPS)&RC_PALETTE)!=0);
        width=GetDeviceCaps(dc,HORZRES);
        height=GetDeviceCaps(dc,VERTRES);
        n_planes=GetDeviceCaps(dc,PLANES);
        n_colors=GetDeviceCaps(dc,NUMCOLORS);
        if (n_colors<=256) n_colors=1<<n_planes*GetDeviceCaps(dc,BITSPIXEL);
        aspectx=GetDeviceCaps(dc,ASPECTX);
        aspecty=GetDeviceCaps(dc,ASPECTY);
        DeleteDC(dc);
        logpen.lopnColor=RGB(255,255,255);
        if ((window=CreateWindow(wake_name,wake_name,WS_POPUP|WS_VISIBLE|WS_SYSMENU|WS_MINIMIZEBOX|
                                 WS_MAXIMIZEBOX,0,0,width,height,(HWND)NULL,(HMENU)NULL,instance,NULL))==NULL) return 101;
        if ((dc=ddc=GetWindowDC(window))==NULL||
            (dcmem=CreateCompatibleDC(dc))==NULL||
            (bitmap=CreateCompatibleBitmap(dc,width,1))==NULL) return 102;
        GetObject(bitmap,sizeof(BITMAP),(LPSTR)&bm);
        line_size=bm.bmWidthBytes;
        row_size=bm.bmWidthBytes*(int)bm.bmPlanes;
        if (paletteBased) {
            if ((lp=(LOGPALETTE *)memcalloc(sizeof(LOGPALETTE)+n_colors*sizeof(PALETTEENTRY)))==NULL)
                return 103;
            lp->palNumEntries=256;
            lp->palVersion=0x300;
            if ((palette=CreatePalette(lp))==NULL) {
                memfree(lp);
                return 104;
            }
            memfree(lp);
            if ((dpalette=SelectPalette(dc,palette,FALSE))==NULL) return 105;
        }
        SetPolyFillMode(dc,ALTERNATE);
        setbackbrush();
        logpen.lopnColor=setrgb(rgbcolor.red,rgbcolor.green,rgbcolor.blue,
                                TRUE);
        logpen.lopnStyle=PS_SOLID;
        logpen.lopnWidth.x=1;
        setpen();
        out_pen=CreatePenIndirect(&logpen);
#elif defined(XLIB)
        XGCValues gcv;
        XSetWindowAttributes swa;
        XSizeHints *sizeHints;
        XWMHints *wmHints;
        XClassHint *classHint;
        XTextProperty windowName, iconName;
        if (!(XInitThreads())) return 100;
        if (!(display=XOpenDisplay(NULL))) return 101;
        screen=XDefaultScreen(display);
        width=XDisplayWidth(display,screen);
        height=XDisplayHeight(display,screen)-21;
        aspectx=aspecty=1;
        n_colors=1<<XDisplayPlanes(display,screen);
        xdensity=(float)width/(float)XDisplayWidthMM(display,screen)*
            (float)25.4;
        ydensity=(float)height/(float)XDisplayHeightMM(display,screen)*
            (float)25.4;
        XSetErrorHandler(error_handler);
        pixel_min=0xffffffff;
        pixel_max=0x0;
        if (!(sizeHints = XAllocSizeHints()) ||
            !(wmHints = XAllocWMHints()) ||
            !(classHint = XAllocClassHint()) ||
            !XStringListToTextProperty(&wake_name, 1, &windowName) ||
            !XStringListToTextProperty(&wake_name, 1, &iconName)) return 102;
        sizeHints->flags = PPosition | PSize | PMinSize;
        sizeHints->min_width=width;
        sizeHints->min_height=height;
        wmHints->flags = StateHint | InputHint;
        wmHints->initial_state = NormalState;
        wmHints->input = True;
        classHint->res_name = wake_name;
        classHint->res_class = wake_name;
        swa.background_pixel=alloc_color(rgbbackcolor.red,
                                         rgbbackcolor.green,rgbbackcolor.blue);
        swa.event_mask=KeyPressMask|ButtonPressMask;
        serial=XNextRequest(display);
        window=XCreateWindow(display,XDefaultRootWindow(display),0,0,
                             width,height,0,XDefaultDepth(display,screen),InputOutput,
                             XDefaultVisual(display,screen),CWBackPixel|CWEventMask,&swa);
        XSync(display,FALSE);
        if (xerror) {
            window=(Window)NULL;
            return 103;
        }
        XSetWMProperties(display, window, &windowName, &iconName, (char **)NULL, 0,
                         sizeHints, wmHints, classHint);
        XSync(display, FALSE);
        if (xerror) return 104;
        XStoreName(display,window,wake_name);
        serial=XNextRequest(display);
        gc=XCreateGC(display,window,0,&gcv);
        XSync(display,FALSE);
        if (xerror) {
            gc=NULL;
            return 105;
        }
        if (!(font_ptr=XLoadQueryFont(display,"FIXED"))) return 106;
        def_font=font_ptr->fid;
        XSetFont(display,gc,def_font);
        serial=XNextRequest(display);
        pixmap=XCreatePixmap(display,window,width,1,
                             XDefaultDepth(display,screen));
        XSync(display,FALSE);
        if (xerror) {
            pixmap=(Pixmap)NULL;
            return 107;
        }
        serial=XNextRequest(display);
        bitmap=XCreatePixmap(display,window,width,height,1);
        XSync(display,FALSE);
        if (xerror) {
            bitmap=(Pixmap)NULL;
            return 108;
        }
        gcv.foreground=1;
        gcv.background=0;
        serial=XNextRequest(display);
        gcb=XCreateGC(display,bitmap,GCForeground|GCBackground,&gcv);
        XSync(display,FALSE);
        if (xerror) {
            gcb=NULL;
            return 109;
        }
        serial=XNextRequest(display);
        gcp=XCreateGC(display,pixmap,0,&gcv);
        XSync(display,FALSE);
        if (xerror) {
            gcp=NULL;
            return 110;
        }
        if (n_colors==2&&setup_halftone(PI/4.0,17.0)) return 111;
        drawable=window;
        XSetFillRule(display,gc,EvenOddRule);
        pixel_min=0xffffffff;
        pixel_max=0x0;
        if (dither) {
            set_dithered_fg(RGB2GRAY(rgbcolor.red,rgbcolor.green,
                                     rgbcolor.blue));
            set_dithered_bg(RGB2GRAY(rgbbackcolor.red,rgbbackcolor.green,
                                     rgbbackcolor.blue));
        }
        else {
            XSetForeground(display,gc,alloc_color(rgbcolor.red,
                                                  rgbcolor.green,rgbcolor.blue));
            XSetBackground(display,gc,alloc_color(rgbbackcolor.red,
                                                  rgbbackcolor.green,rgbbackcolor.blue));
        }
        serial=XNextRequest(display);
        XMapRaised(display,window);
        XSync(display, FALSE);
        if (xerror) return 112;
        XFree(sizeHints);
        XFree(wmHints);
        XFree(classHint);
#else
        int n;
        char c;
        if (graphpath) {
            c=graphpath[n=strcspn(graphpath,whites)];
            graphpath[n]='\0';
            initgraph(&graphdriver,&graphmode,graphpath);
            graphpath[n]=c;
        }
        else initgraph(&graphdriver,&graphmode,"");
        if (graphresult()!=grOk) return 100;
        graphready=1;
        n_colors=getmaxcolor()+1;
        width=getmaxx()+1;
        height=getmaxy()+1;
        n_planes=0;
        n=n_colors;
        while (n>>=1) n_planes++;
        getaspectratio(&aspectx,&aspecty);
        if (!(rgb_map=(RGBVAL *)memalloc(n_colors*sizeof(RGBVAL))))
            return 101;
        bitmap_size=imagesize(0,0,width-1,0);
        if (!(bitmap=(char far *)_graphgetmem(bitmap_size))) return 102;
        *(short *)bitmap=width-1;
        *((short *)bitmap+1)=0;
        line_size=(width+7)/8;
        row_size=line_size*n_planes;
        setrgbpalette(1,rgbcolor.red,rgbcolor.green,rgbcolor.blue);
        setrgbpalette(0,rgbbackcolor.red,rgbbackcolor.green,
                      rgbbackcolor.blue);
        setcolor(1);
        setbkcolor(0);
        setfillstyle(SOLID_FILL,1);
        getlinesettings(&linesettings);
        getfillsettings(&fillsettings);
#endif
    }
    wk_init();
    return 0;
}
