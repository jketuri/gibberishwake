
/* wk_seq.c */

#include "wake.h"
#include "wk_fun.h"
#include "wk_gra.h"
#define MAIN
#undef VARIABLE
#include "wk_seq.h"

char *wake_key_name = "Wake Key";

#ifdef __APPLE__
UBYTE key_volume_table[] = {12, 25, 38, 51, 76, 64, 0, 114, 89, 0, 102, 127};
UBYTE key_chan_table[] = {1, 2, 3, 4, 6, 5, 0, 9, 7, 0, 8, 0};
#endif

extern int init_seq2(void);
extern void wk_reset(void);

UBYTE MQ *getvarnum(UBYTE MQ *pos, ULONG *num)
{
    *num = (ULONG)*pos++;
    if (*num & 0x80) {
        *num &= 0x7f;
        while (*pos & 0x80) *num = (*num << 7) + (ULONG)(*pos++ & 0x7f);
        *num = (*num << 7) + (ULONG)*pos++;
    }
    return pos;
}

UBYTE MQ *putvarnum(UBYTE MQ *pos, ULONG num)
{
    ULONG m = num & 0x7f;
    while (num >>= 7) {
        m <<= 8;
        m |= 0x80;
        m += num & 0x7f;
    }
    for (;;) {
        *pos++ = (UBYTE)(m & 0xff);
        if (m & 0x80) m >>= 8;
        else break;
    }
    return pos;
}

#ifdef AMIGA
void show_note(int c, int n, int s)
{
    SetAPen(rastport2, s?n%12+1:
            ((c == 9?(seqshare->num_chans&1):(c&1))^
             ((seqshare->gflag&INVERTING) != 0)?15:0));
    WritePixel(rastport2, seqshare->disp_pos+
               (c == 9?seqshare->drum_pos+(UWORD)n:((UWORD)c<<7)+(UWORD)n), 0);
}
#elif defined(VGALIB)
void show_note(int c, int n, int s)
{
}
#elif defined(_WINDOWS)
void show_note(SEQSHARE *seqshare, int c, int n, int s)
{
    SetPixel(seqshare->dcmem, seqshare->disp_pos+
             (c == 9?seqshare->drum_pos+(int)n:((int)c<<7)+(int)n), 0, 
             s?PALETTEINDEX(n%12+1):
             seqshare->colors[!(c == 9?(seqshare->num_chans&1):(c&1))^
                              ((seqshare->gflag&INVERTING) != 0)]);
}
#elif defined(XLIB)
void show_note(int c, int n, int s)
{
    XSetForeground(display, gcp, s?n%12+1:
                   pennum[!(c == 9?(seqshare->num_chans&1):(c&1))^
                          ((seqshare->gflag&INVERTING) != 0)]);
    XDrawPoint(display, pixmap, gcp, seqshare->disp_pos+
               (c == 9?seqshare->drum_pos+(int)n:((int)c<<7)+(int)n), 0);
}
#endif

void free_voice(void *voice)
{
#ifdef AMIGA
    FreeMem(((VOICE *)voice)->data, ((VOICE *)voice)->length);
#endif
}

#ifdef AMIGA
BOOL start_note(VOICE *voice, UBYTE note, UBYTE volume)
{
    ULONG s;
    UBYTE k, m, o;
    if ((voice->note_byte[note]&0x80)||
        (k=free_chan[aud_use]) == 255) return FALSE;
    o=voice->note_byte[note];
    voice->note_byte[note]|=k<<4|0x80;
    m=btms[k];
    aud_use|=m;
    s=voice->vhdr.samplesperHiCycle<<o;
    while (GetMsg(IOAudio[k][0]->ioa_Request.io_Message.mn_ReplyPort));
    IOAudio[k][0]->ioa_Request.io_Unit=
        IOAudio[k][1]->ioa_Request.io_Unit=(struct Unit *)m;
    IOAudio[k][0]->ioa_Request.io_Command=
        IOAudio[k][1]->ioa_Request.io_Command=CMD_WRITE;
    IOAudio[k][0]->ioa_Volume=
        IOAudio[k][1]->ioa_Volume=volume;
    IOAudio[k][0]->ioa_Data=
        IOAudio[k][1]->ioa_Data=voice->wave_pos[o];
    IOAudio[k][0]->ioa_Period=
        IOAudio[k][1]->ioa_Period=seqshare->note_freq[note] == 0.0?0xffff:
        (UWORD)((float)clock_value/(seqshare->note_freq[note]*(float)s)+.5);
    if (voice->vhdr.oneShotHiSamples) {
        IOAudio[k][0]->ioa_Request.io_Flags=IOF_QUICK|ADIOF_PERVOL;
        IOAudio[k][0]->ioa_Length=voice->vhdr.oneShotHiSamples<<o;
        IOAudio[k][0]->ioa_Cycles=1;
        BeginIO((struct IORequest *)IOAudio[k][0]);
        IOAudio[k][1]->ioa_Data+=IOAudio[k][0]->ioa_Length;
        IOAudio[k][1]->ioa_Request.io_Flags=IOF_QUICK;
    }
    else IOAudio[k][1]->ioa_Request.io_Flags=IOF_QUICK|ADIOF_PERVOL;
    if (voice->vhdr.repeatHiSamples) {
        IOAudio[k][1]->ioa_Length=voice->vhdr.repeatHiSamples<<o;
        IOAudio[k][1]->ioa_Cycles=0;
        BeginIO((struct IORequest *)IOAudio[k][1]);
    }
    recent[k]=note;
    return TRUE;
}

void clear_note(VOICE *voice, UBYTE note)
{
    UBYTE k, m;
    if (!(voice->note_byte[note]&0x80)) return;
    k=voice->note_byte[note]>>4&0x3;
    voice->note_byte[note]&=0xf;
    m=btms[k];
    aud_use&=0xf^m;
    while (GetMsg(IOAudio[k][0]->ioa_Request.io_Message.mn_ReplyPort));
    if (pitchbend[k]) {
        AbortIO((struct IORequest *)IOAudio[k][2]);
        pitchbend[k]=0;
        n_pitchbends--;
    }
    if (voice->vhdr.repeatHiSamples)
        AbortIO((struct IORequest *)IOAudio[k][1]);
    if (voice->vhdr.oneShotHiSamples)
        AbortIO((struct IORequest *)IOAudio[k][0]);
}

void bend_pitch(UWORD amount, UBYTE channel)
{
    UBYTE k;
    float v;
    if (!seqshare->voice_map[channel]||
        !seqshare->voice_map[channel]->vhdr.repeatHiSamples||
        !(seqshare->voice_map[channel]->note_byte[recent[channel]]&0x80))
        return;
    k=seqshare->voice_map[channel]->note_byte[recent[channel]]>>4&0x3;
    v=seqshare->note_freq[recent[channel]]*(float)(seqshare->
                                                   voice_map[channel]->vhdr.samplesperHiCycle<<(seqshare->
                                                                                                voice_map[channel]->note_byte[recent[channel]]&0xf));
    if (amount == 0x3f3f) {
        if (pitchbend[k]) {
            pitchbend[k]=0;
            n_pitchbends--;
        }
        IOAudio[k][2]->ioa_Period=(UWORD)((float)clock_value/v+.5);
        IOAudio[k][2]->ioa_Request.io_Unit=(struct Unit *)btms[k];
        IOAudio[k][2]->ioa_Request.io_Command=ADCMD_PERVOL;
        IOAudio[k][2]->ioa_Request.io_Flags=IOF_QUICK|ADIOF_SYNCCYCLE;
        IOAudio[k][2]->ioa_Volume=IOAudio[k][1]->ioa_Volume;
        IOAudio[k][2]->ioa_Data=IOAudio[k][1]->ioa_Data;
        BeginIO((struct IORequest *)IOAudio[k][2]);
        return;
    }
    else if (amount < 0x3f3f) {
        period[k]=(UWORD)((float)clock_value/(v/2.0)+.5);
        pitchbend[k]=1;
    }
    else {
        period[k] = (UWORD)((float)clock_value/(v*2.0)+.5);
        pitchbend[k] = -1;
    }
    if (!pitchbend[k]) n_pitchbends++;
    IOAudio[k][2]->ioa_Period=IOAudio[k][1]->ioa_Period;
}

void emulate(UBYTE *msg, UBYTE len)
{
    UBYTE channel, command, *end=msg+len;
    while (msg<end && (*msg&0x80) == 0) msg++;
    while (msg<end) {
        command= *msg&0xf0;
        channel= *msg&0xf;
        msg++;
        switch (command) {
        case NOTE_ON:
        case NOTE_OFF:
            while (msg<end && (*msg&0x80) == 0) {
                if (seqshare->voice_map[channel])
                    if (command == NOTE_ON && msg[1])
                        start_note(seqshare->voice_map[channel], *msg, (msg[1]+1)>>1);
                    else clear_note(seqshare->voice_map[channel], *msg);
                msg+=2; }
            continue;
        case PROGRAM_CHANGE:
        case CHANNEL_PRESSURE:
            while (msg<end && (*msg&0x80) == 0) msg++;
            continue;
        case PITCH_BEND_CHANGE:
            while (msg<end && (*msg&0x80) == 0) {
                bend_pitch((UWORD)msg[0]|(((UWORD)msg[1])<<8), channel);
                msg+=2; }
            continue;
        case CONTROL_CHANGE:
            while (msg<end && (*msg&0x80) == 0) {
                if (*msg == ALL_NOTES_OFF) {
                    UBYTE n, m;
                    for (n=0; n<4; n++)
                        for (m=0; m<3; m++)
                            AbortIO((struct IORequest *)IOAudio[n][m]);
                    aud_use=0;
                }
                msg+=2; }
            continue;
        case SYSTEM_EXCLUSIVE:
            while (msg<end && (*msg&0x80) == 0) msg++;
            continue;
        default:
            while (msg<end && (*msg&0x80) == 0) msg+=2;
        } }
}
#elif defined(__APPLE__)
int start_note(UBYTE channel, UBYTE note, UBYTE volume)
{
    OSStatus result;
    result = MusicDeviceMIDIEvent(audioUnit, NOTE_ON | channel, note, volume, 0);
    return -1;
}

int clear_note(UBYTE channel, UBYTE note, UBYTE volume)
{
    OSStatus result;
    result = MusicDeviceMIDIEvent(audioUnit, NOTE_OFF | channel, note, volume, 0);
    return -1;
}

void emulate(UBYTE *msg, UBYTE len)
{
    OSStatus result;
    UBYTE channel, command, *end=msg+len;
    while (msg<end && (*msg&0x80) == 0) msg++;
    while (msg<end) {
        command= *msg&0xf0;
        channel= *msg&0xf;
        msg++;
        switch (command) {
        case NOTE_ON:
        case NOTE_OFF:
            while (msg<end && (*msg&0x80) == 0) {
                if (command == NOTE_ON && msg[1]) start_note(channel, *msg, msg[1]);
                else clear_note(channel, *msg, msg[1]);
                msg+=2; }
            continue;
        case PROGRAM_CHANGE:
            while (msg<end && (*msg&0x80) == 0) {
                result = MusicDeviceMIDIEvent(audioUnit,
                                              PROGRAM_CHANGE | channel,
                                              *msg, 0, 0);
                msg++;
            }
            continue;
        case CHANNEL_PRESSURE:
            while (msg<end && (*msg&0x80) == 0) {
                result = MusicDeviceMIDIEvent(audioUnit,
                                              CHANNEL_PRESSURE | channel,
                                              *msg, 0, 0);
                msg++;
            }
            continue;
        case PITCH_BEND_CHANGE:
            while (msg<end && (*msg&0x80) == 0) {
                result = MusicDeviceMIDIEvent(audioUnit,
                                              PITCH_BEND_CHANGE | channel,
                                              msg[0], msg[1], 0);
                msg+=2; }
            continue;
        case CONTROL_CHANGE:
            while (msg<end && (*msg&0x80) == 0) {
                result = MusicDeviceMIDIEvent(audioUnit,
                                              CONTROL_CHANGE | channel,
                                              *msg, msg[1], 0);
                msg+=2; }
            continue;
        case SYSTEM_EXCLUSIVE:
            while (msg<end && (*msg&0x80) == 0) msg++;
            continue;
        default:
            while (msg<end && (*msg&0x80) == 0) msg+=2;
        } }
}
#elif defined(__linux__)
int start_note(UBYTE channel, UBYTE note, UBYTE volume)
{
    snd_seq_ev_clear(&event);
    snd_seq_ev_set_source(&event, port_out_id);
    snd_seq_ev_set_subs(&event);
    snd_seq_ev_set_direct(&event);
    event.type=SND_SEQ_EVENT_NOTEON;
    event.data.note.channel=channel;
    event.data.note.note=note;
    event.data.note.velocity=volume;
    snd_seq_event_output(seq_handle, &event);
    snd_seq_drain_output(seq_handle);
    return -1;
}

int clear_note(UBYTE channel, UBYTE note, UBYTE volume)
{
    snd_seq_ev_clear(&event);
    snd_seq_ev_set_source(&event, port_out_id);
    snd_seq_ev_set_subs(&event);
    snd_seq_ev_set_direct(&event);
    event.type=SND_SEQ_EVENT_NOTEOFF;
    event.data.note.channel=channel;
    event.data.note.note=note;
    event.data.note.velocity=volume;
    snd_seq_event_output(seq_handle, &event);
    snd_seq_drain_output(seq_handle);
    return -1;
}

void emulate(UBYTE *msg, UBYTE len)
{
    UBYTE channel, command, *end=msg+len;
    while (msg<end && (*msg&0x80) == 0) msg++;
    while (msg<end) {
        command= *msg&0xf0;
        channel= *msg&0xf;
        msg++;
        switch (command) {
        case NOTE_ON:
        case NOTE_OFF:
            while (msg<end && (*msg&0x80) == 0) {
                if (command == NOTE_ON && msg[1]) start_note(channel, *msg, msg[1]);
                else clear_note(channel, *msg, msg[1]);
                msg+=2; }
            continue;
        case PROGRAM_CHANGE:
            while (msg<end && (*msg&0x80) == 0) {
                snd_seq_ev_clear(&event);
                snd_seq_ev_set_source(&event, port_out_id);
                snd_seq_ev_set_subs(&event);
                snd_seq_ev_set_direct(&event);
                event.type=SND_SEQ_EVENT_PGMCHANGE;
                event.data.note.channel=channel;
                event.data.control.value=*msg;
                snd_seq_event_output(seq_handle, &event);
                snd_seq_drain_output(seq_handle);
                msg++;
            }
            continue;
        case CHANNEL_PRESSURE:
            while (msg<end && (*msg&0x80) == 0) {
                snd_seq_ev_clear(&event);
                snd_seq_ev_set_source(&event, port_out_id);
                snd_seq_ev_set_subs(&event);
                snd_seq_ev_set_direct(&event);
                event.type=SND_SEQ_EVENT_CHANPRESS;
                event.data.note.channel=channel;
                event.data.control.value=*msg;
                snd_seq_event_output(seq_handle, &event);
                snd_seq_drain_output(seq_handle);
                msg++;
            }
            continue;
        case PITCH_BEND_CHANGE:
            while (msg<end && (*msg&0x80) == 0) {
                snd_seq_ev_clear(&event);
                snd_seq_ev_set_source(&event, port_out_id);
                snd_seq_ev_set_subs(&event);
                snd_seq_ev_set_direct(&event);
                event.type=SND_SEQ_EVENT_PITCHBEND;
                event.data.control.channel=channel;
                event.data.control.value=(UWORD)msg[0]|(((UWORD)msg[1])<<8);
                snd_seq_event_output(seq_handle, &event);
                snd_seq_drain_output(seq_handle);
                msg+=2; }
            continue;
        case CONTROL_CHANGE:
            while (msg<end && (*msg&0x80) == 0) {
                snd_seq_ev_clear(&event);
                snd_seq_ev_set_source(&event, port_out_id);
                snd_seq_ev_set_subs(&event);
                snd_seq_ev_set_direct(&event);
                event.type=SND_SEQ_EVENT_CONTROLLER;
                event.data.control.channel=channel;
                event.data.control.param=*msg;
                event.data.control.value=msg[1];
                snd_seq_event_output(seq_handle, &event);
                snd_seq_drain_output(seq_handle);
                msg+=2; }
            continue;
        case SYSTEM_EXCLUSIVE:
            while (msg<end && (*msg&0x80) == 0) msg++;
            continue;
        default:
            while (msg<end && (*msg&0x80) == 0) msg+=2;
        } }
}
#endif

void set_key_dims(SEQSHARE *seqshare)
{
    seqshare->bh = (short)(0.7 * (float)seqshare->wh);
    seqshare->bw = (short)(0.57 * (float)seqshare->ww);
    seqshare->bw1 = seqshare->bw / 2 - 1;
    seqshare->bw2 = seqshare->bw / 2;
    seqshare->bw3 = seqshare->bw / 2 + 1;
    seqshare->dd0 = seqshare->ww + 2;
    seqshare->dd1 = seqshare->ww - seqshare->bw1 + 1;
    seqshare->dd2 = seqshare->ww - seqshare->bw2 + 1;
    seqshare->dd3 = seqshare->ww - seqshare->bw3 + 1;
}

#ifdef AMIGA
#define setkeycolor(n1, n2) SetAPen(kwrp, b?pennum[n1]:pennum[n2])
#define drawkeyrect(x1, y1, x2, y2) RectFill(kwrp, x1, y1, x2, y2)
#elif defined(_WINDOWS)
#define setkeycolor(n1, n2) SelectObject(seqshare->key_dc, b ? seqshare->pens[n1] : seqshare->pens[n2]); \
    SelectObject(seqshare->key_dc, b ? seqshare->brushes[n1] : seqshare->brushes[n2])
#define drawkeyrect(x1, y1, x2, y2) Rectangle(seqshare->key_dc, x1, y1, x2, y2)
#elif defined(XLIB)
#define setkeycolor(n1, n2) XSetForeground(display, gck, b?pennum[n1]:pennum[n2])
#define drawkeyrect(x1, y1, x2, y2) XFillRectangle(display, kwnd, gck, x1, y1, x2-(x1), y2-(y1))
#else
#define setkeycolor(n1, n2) setcolor(b?pennum[n1]:pennum[n2])
#define drawkeyrect(x1, y1, x2, y2) bar(x1, y1, x2, y2)
#endif

static UBYTE key_inc[] = {0, 0, 1, 1, 2, 3, 3, 4, 4, 5, 5, 6};

#ifdef _WINDOWS
void draw_key(SEQSHARE *seqshare, int b, int n)
#else
    void draw_key(int b, int n)
#endif
{
    short a, s, d;
    a = (short)(n % 12);
    s = (short)(n / 12 * 7 + key_inc[a]);
    d = (short)(s * seqshare->dd0 + KEY_LEFT);
    switch (a) {
    case C_:
        setkeycolor(2, 0);
        if (n < N_KEYS - 1) {
            drawkeyrect(d, KEY_TOP, d + seqshare->ww - seqshare->bw3, KEY_TOP + seqshare->bh + 1);
            drawkeyrect(d, KEY_TOP + seqshare->bh + 1, d + seqshare->ww, KEY_TOP + seqshare->wh);
        }
        else drawkeyrect(d, KEY_TOP, d + seqshare->ww, KEY_TOP + seqshare->wh);
        break;
    case CIS_:
        setkeycolor(2, 1);
        drawkeyrect(d + seqshare->dd3, KEY_TOP, d + seqshare->dd3 + seqshare->bw, KEY_TOP + seqshare->bh);
        break;
    case D_:
        setkeycolor(2, 0);
        drawkeyrect(d + seqshare->bw1, KEY_TOP, d + seqshare->ww-seqshare->bw1, KEY_TOP + seqshare->bh + 1);
        drawkeyrect(d, KEY_TOP + seqshare->bh + 1, d + seqshare->ww, KEY_TOP + seqshare->wh);
        break;
    case DIS_:
        setkeycolor(2, 1);
        drawkeyrect(d + seqshare->dd1, KEY_TOP, d + seqshare->dd1 + seqshare->bw, KEY_TOP + seqshare->bh);
        break;
    case E_:
        setkeycolor(2, 0);
        drawkeyrect(d + seqshare->bw3, KEY_TOP, d + seqshare->ww, KEY_TOP + seqshare->bh + 1);
        drawkeyrect(d, KEY_TOP + seqshare->bh + 1, d + seqshare->ww, KEY_TOP + seqshare->wh);
        break;
    case F_:
        setkeycolor(2, 0);
        if (n < N_KEYS - 1) {
            drawkeyrect(d, KEY_TOP, d + seqshare->ww - seqshare->bw3, KEY_TOP + seqshare->bh + 1);
            drawkeyrect(d, KEY_TOP + seqshare->bh + 1, d + seqshare->ww, KEY_TOP + seqshare->wh);
        }
        else drawkeyrect(d, KEY_TOP, d + seqshare->ww, KEY_TOP + seqshare->wh);
        break;
    case FIS_:
        setkeycolor(2, 1);
        drawkeyrect(d + seqshare->dd3, KEY_TOP, d + seqshare->dd3 + seqshare->bw, KEY_TOP + seqshare->bh);
        break;
    case G_:
        setkeycolor(2, 0);
        if (n < N_KEYS - 1) {
            drawkeyrect(d + seqshare->bw1, KEY_TOP, d + seqshare->ww - seqshare->bw2, KEY_TOP + seqshare->bh + 1);
            drawkeyrect(d, KEY_TOP + seqshare->bh + 1, d + seqshare->ww, KEY_TOP + seqshare->wh);
        }
        else {
            drawkeyrect(d + seqshare->bw1, KEY_TOP, d + seqshare->ww, KEY_TOP + seqshare->bh + 1);
            drawkeyrect(d, KEY_TOP + seqshare->bh + 1, d + seqshare->ww, KEY_TOP + seqshare->wh);
        }
        break;
    case GIS_:
        setkeycolor(2, 1);
        drawkeyrect(d + seqshare->dd2, KEY_TOP, d + seqshare->dd2 + seqshare->bw, KEY_TOP + seqshare->bh);
        break;
    case A_:
        setkeycolor(2, 0);
        if (n == 0) drawkeyrect(d, KEY_TOP, d + seqshare->ww - seqshare->bw1, KEY_TOP + seqshare->bh + 1);
        else drawkeyrect(d + seqshare->bw2, KEY_TOP, d + seqshare->ww - seqshare->bw2, KEY_TOP + seqshare->bh + 1);
        drawkeyrect(d, KEY_TOP + seqshare->bh + 1, d + seqshare->ww, KEY_TOP + seqshare->wh);
        break;
    case AIS_:
        setkeycolor(2, 1);
        drawkeyrect(d + seqshare->dd1, KEY_TOP, d + seqshare->dd1 + seqshare->bw, KEY_TOP + seqshare->bh);
        break;
    case B_:
        setkeycolor(2, 0);
        drawkeyrect(d + seqshare->bw3, KEY_TOP, d + seqshare->ww, KEY_TOP + seqshare->bh + 1);
        drawkeyrect(d, KEY_TOP + seqshare->bh + 1, d + seqshare->ww, KEY_TOP + seqshare->wh);
        break;
    }
}

static UBYTE mouse_inc[] = {0, 1, 2, 2, 3, 4, 5};

#ifdef _WINDOWS
UBYTE note_mouse(SEQSHARE *seqshare, int x, int y)
#elif defined(XLIB)
    UBYTE note_mouse(int x, int y)
#else
    UBYTE note_mouse(void)
#endif
{
    short d;
    UBYTE a, b;
#ifdef AMIGA
    LONG n;
    SHORT x = key_win->MouseX, y = key_win->MouseY;
#elif defined(VGALIB)
    int n, x, y;
#elif defined(_WINDOWS)
    COLORREF c;
#elif defined(XLIB)
    unsigned long n;
#else
    int n, x = getx(), y = gety();
#endif
    if (x < KEY_LEFT || x >= N_WHITES * seqshare->dd0 + KEY_LEFT || y < KEY_TOP || y >= KEY_TOP + seqshare->wh) {
        if (seqshare->mouse_note == 255) return 0;
        b = 255;
    }
    else {
        a = (UBYTE)((b = (UBYTE)((d = (short)(x - KEY_LEFT)) / seqshare->dd0)) % 7);
        b = (UBYTE)(b / 7 * 12 + a + mouse_inc[a]);
        if (b >= N_KEYS) b = N_KEYS - 1;
#ifdef AMIGA
        n = ReadPixel(kwrp, x, y);
        if (n == pennum[2]) return 0;
#elif defined(VGALIB)
#elif defined(_WINDOWS)
        c = GetPixel(seqshare->key_dc, x, y);
        if (c == seqshare->colors[2]) return 0;
#elif defined(XLIB)
        if (!kimage) return 0;
        n = XGetPixel(kimage, x, y);
        if (n == pennum[2]) return 0;
#else
        n=getpixel(x, y);
        if (n == pennum[2]) return 0;
#endif
#ifdef _WINDOWS
        if (c == seqshare->colors[1])
#else
            if (n == pennum[1])
#endif
                {
                    if (y > KEY_TOP + seqshare->bh) return 0;
                    else if (d % seqshare->dd0 < seqshare->dd0 / 2) {
                        if (b > 0) b--;
                    }
                    else if (b < N_KEYS-1) b++;
                }
#ifdef _WINDOWS
            else if (c != seqshare->colors[0]) return 0;
#else
            else if (n != pennum[0]) return 0;
#endif
        if (b == seqshare->mouse_note) return 0;
    }
#ifdef AMIGA
    if (seqshare->emulation && seqshare->voice_map[seqshare->key_chan]) {
        if (seqshare->mou_note != 255)
            clear_note(seqshare->voice_map[seqshare->key_chan], seqshare->mou_note);
        seqshare->mou_note= b;
        if (b != 255)
            if (start_note(seqshare->voice_map[seqshare->key_chan], b, 
                           seqshare->key_volume)) seqshare->mou_note = b;
            else seqshare->mou_note = 255;
    }
#endif
    if (seqshare->mouse_note != 255) {
        *seqshare->key_str = (UBYTE)(NOTE_ON | seqshare->key_chan);
        seqshare->key_str[1] = seqshare->mouse_note;
        seqshare->key_str[2] = 0;
        if (b == 255) {
            seqshare->mouse_note = 255;
            return 3;
        }
        seqshare->key_str[3] = seqshare->mouse_note = b;
#ifdef AMIGA
        seqshare->key_str[4] = (seqshare->key_volume << 1) - 1;
#else
        seqshare->key_str[4] = seqshare->key_volume;
#endif
        return 5;
    }
    *seqshare->key_str = (UBYTE)(NOTE_ON | seqshare->key_chan);
    seqshare->key_str[1] = seqshare->mouse_note = b;
#ifdef AMIGA
    seqshare->key_str[2] = (seqshare->key_volume << 1) - 1;
#else
    seqshare->key_str[2] = seqshare->key_volume;
#endif
    return 3;
}

#ifdef AMIGA
int trans_finnish(char *char_string, char *phon_buffer, int buffer_len)
{
    int l=0;
    UBYTE c, *s;
    while (c = *char_string) {
        if ((isupper(c) || (s=strchr(sc+3, c)))&&
            strchr("\t .", char_string[1])) {
            if (!isupper(c)) c=cs[s-sc];
            s=finn_alph[c-'A'];
            if (l+strlen(s)+1>=buffer_len) return -1;
            while (*s) phon_buffer[l++]= *s++;
            phon_buffer[l++]=' ';
            char_string+=2;
            continue;
        }
        if (s=strchr(sc, c)) c=cs[s-sc];
        else c=toupper(c);
        if (c == 'C' && toupper(char_string[1]) == 'H') {
            if (l+2>=buffer_len) return -1;
            strcpy(phon_buffer+l, "CH");
            char_string+=2;
            l+=2;
            continue;
        }
        if (c=='N' && toupper(char_string[1])=='G') {
            if (l+2>=buffer_len) return -1;
            strcpy(phon_buffer+l, "NX");
            char_string+=2;
            l+=2;
            continue;
        }
        if (c>=' ' && c<=']') {
            if (l+1>=buffer_len) return -1;
            s=finn_phon[c-' '];
            while (*s) phon_buffer[l++]= *s++;
        }
        char_string++;
    }
    phon_buffer[l]='\0';
    return 0;
}
#endif

void set_stamp(void)
{
    if (!(seqshare->gflag & STARTED)) {
        seqshare->dstamp = 0L;
        seqshare->gflag |= STARTED;
    }
    seqshare->rec_seq_pos = putvarnum(seqshare->rec_seq_pos, 
                                      seqshare->dstamp);
    seqshare->pstamp = seqshare->cstamp;
    seqshare->len_pos = seqshare->rec_seq_pos++;
    *seqshare->len_pos = 0;
}

void complete(void)
{
    seqshare->gflag ^= RECORDING;
#ifdef _WINDOWS
    if (midiin) midiInReset(midiin);
#endif
    set_stamp();
    *seqshare->len_pos = 1;
    *seqshare->rec_seq_pos = SPECIAL;
}

#ifdef AMIGA
void allocate(void)
{
    UBYTE chan_comb[16], n, m;
    for (n=0; n<16; n++) chan_comb[n]=15-n;
    IOAudio[0][0]->ioa_AllocKey=0;
    IOAudio[0][0]->ioa_Data=chan_comb;
    IOAudio[0][0]->ioa_Length=sizeof chan_comb;
    IOAudio[0][0]->ioa_Request.io_Flags=IOF_QUICK|ADIOF_NOWAIT;
    IOAudio[0][0]->ioa_Request.io_Command=ADCMD_ALLOCATE;
    BeginIO((struct IORequest *)IOAudio[0][0]);
    for (n=0; n<4; n++)
        for (m=0; m<3; m++)
            if (n || m) IOAudio[n][m]->ioa_AllocKey=IOAudio[0][0]->ioa_AllocKey;
}

void free_audio(void)
{
    IOAudio[0][0]->ioa_Request.io_Unit=(struct Unit *)0xf;
    IOAudio[0][0]->ioa_Request.io_Flags=IOF_QUICK;
    IOAudio[0][0]->ioa_Request.io_Command=ADCMD_FREE;
    BeginIO((struct IORequest *)IOAudio[0][0]);
}

void midi_out(UBYTE *data, ULONG length)
{
    if (!CheckIO((struct IORequest *)IOExtSer[ser_ion]))
        WaitIO((struct IORequest *)IOExtSer[ser_ion]);
    while (GetMsg(IOExtSer[ser_ion]->IOSer.io_Message.mn_ReplyPort));
    IOExtSer[ser_ion]->IOSer.io_Command=CMD_WRITE;
    IOExtSer[ser_ion]->IOSer.io_Data=data;
    IOExtSer[ser_ion]->IOSer.io_Length=length;
    IOExtSer[ser_ion]->IOSer.io_Flags=0;
    SendIO((struct IORequest *)IOExtSer[ser_ion]);
    ser_ion^=1;
}
#elif defined(__APPLE__)
static void	midiInputReadProc(const MIDIPacketList *pktlist, void *refCon, void *connRefCon)
{
    unsigned int index;
    MIDIPacket *packet = NULL;
    if (seqshare->gflag & RECORDING) {
        packet = (MIDIPacket *)pktlist->packet;
		for (index = 0; index < pktlist->numPackets; index++) {
			packet = MIDIPacketNext(packet);
		}
	}
}

int midi_in(void)
{
    return 0;
}

void midi_out(UBYTE *data, ULONG length)
{
}

void get_stamp()
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    seqshare->cstamp = (ULONG)((tv.tv_sec - seqshare->stv.tv_sec) * 1000L +
                               (tv.tv_usec - seqshare->stv.tv_usec + 500L) / 1000L);
}
#elif defined(__linux__)
int midi_in(void)
{
    return snd_rawmidi_read(rawmidi_in_handle, (char *)seqshare->cmd_buf, CMD_BUF_SIZE);
}

void midi_out(UBYTE *data, ULONG length)
{
    snd_rawmidi_write(rawmidi_out_handle, data, length);
    snd_rawmidi_drain(rawmidi_out_handle);
}

void get_stamp()
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    seqshare->cstamp = (ULONG)((tv.tv_sec - seqshare->stv.tv_sec) * 1000L +
                               (tv.tv_usec - seqshare->stv.tv_usec + 500L) / 1000L);
}
#elif defined(_WINDOWS)
void midi_in(void)
{
    midiinhdr->lpData=(char *)seqshare->cmd_buf;
    midiinhdr->dwBufferLength=255;
    midiinhdr->dwFlags=0;
    midiInPrepareHeader(midiin, midiinhdr, sizeof(MIDIHDR));
    midiInAddBuffer(midiin, midiinhdr, sizeof(MIDIHDR));
}

void wait_midi(int i)
{
    int j = seqshare->midihdr_n[i] ^ 1;
    if (!seqshare->midihdr[i][j]->lpData) return;
    while (!(seqshare->midihdr[i][j]->dwFlags & MHDR_DONE)) WaitForSingleObject(seqshare->events[i], INFINITE);
    midiOutUnprepareHeader(seqshare->midiout[i], seqshare->midihdr[i][j], sizeof(MIDIHDR));
    seqshare->midihdr[i][j]->lpData = NULL;
}

void midi_out(SEQSHARE *seqshare, UBYTE *data, ULONG length, int i)
{
    UBYTE channel, command, n, MQ *pos = data, MQ *end = (UBYTE MQ *)data + length;
    while (pos < end && (*pos & 0x80) == 0) pos++;
    while (pos < end) {
        command=(UBYTE)(*pos & 0xf0);
        channel=(UBYTE)(*pos & 0xf);
        switch (command) {
        case PROGRAM_CHANGE:
        case CHANNEL_PRESSURE:
            if (++pos < end && (*pos & 0x80) == 0) {
                midiOutShortMsg(seqshare->midiout[i], (DWORD)pos[-1] | (DWORD)*pos << 8);
                while (++pos < end && (*pos & 0x80) == 0) midiOutShortMsg(seqshare->midiout[i], *pos);
            }
            continue;
        case SYSTEM_EXCLUSIVE:
            for (n = 1; pos + n < end && (pos[n] & 0x80) == 0; n++);
            if (n > 1) {
                seqshare->midihdr[i][seqshare->midihdr_n[i]]->lpData = (char *)pos;
                seqshare->midihdr[i][seqshare->midihdr_n[i]]->dwBufferLength = n;
                seqshare->midihdr[i][seqshare->midihdr_n[i]]->dwFlags = 0;
                midiOutPrepareHeader(seqshare->midiout[i], seqshare->midihdr[i][seqshare->midihdr_n[i]], sizeof(MIDIHDR));
                wait_midi(i);
                midiOutLongMsg(seqshare->midiout[i], seqshare->midihdr[i][seqshare->midihdr_n[i]], sizeof(MIDIHDR));
                ResetEvent(seqshare->events[i]);
                seqshare->midihdr_n[i] ^= 1;
            }
            pos += n;
            continue;
        default:
            if (++pos < end && (*pos & 0x80) == 0) {
                midiOutShortMsg(seqshare->midiout[i], (DWORD)pos[-1] | (DWORD)*pos << 8 | (DWORD)pos[1] << 16);
                while ((pos += 2) < end && (*pos & 0x80) == 0) midiOutShortMsg(seqshare->midiout[i], (DWORD)*pos | (DWORD)pos[1] << 8);
            }
        }
    }
}
#endif

void halt_seq(void)
{
    UBYTE i, n;
#ifdef AMIGA
    if (seqshare->gflag & PROCEEDING) {
        Signal(procTask, SIGBREAKF_CTRL_C);
        Wait(1L);
        Forbid();
        DeleteTask(procTask);
        Permit();
        procTask=NULL;
        allocate();
    }
    if ((seqshare->gflag & KEYBOARD) && key_win) {
        CloseWindow(key_win);
        key_win=NULL;
    }
#elif defined(_WINDOWS)
    if (seqshare->gflag & PROCEEDING) {
        timeKillEvent(timerid);
        timeEndPeriod(1);
        if (seqshare->gflag & RECORDING) {
            seqshare->cstamp = timeGetTime() - seqshare->stime;
            seqshare->dstamp = seqshare->cstamp - seqshare->pstamp;
            complete(); }
    }
    if (seqshare->gflag & KEYBOARD && seqshare->key_wnd) DestroyWindow(seqshare->key_wnd);
#elif defined(__linux__) || defined(__APPLE__)
    if ((seqshare->gflag & PROCEEDING) && taskproc_id != (pthread_t)0) {
        pthread_cancel(taskproc_id);
        pthread_join(taskproc_id, NULL);
    }
    /*
      kill(taskproc_pid, SIGINT);
    */
#endif
#ifdef XLIB
    if ((seqshare->gflag & KEYBOARD) && kwnd) {
        if (kimage) {
            XDestroyImage(kimage);
            kimage = NULL;
        }
        XAutoRepeatOn(display);
        XDestroyWindow(display, kwnd);
        XFreeGC(display, gck);
        kwnd = (Window)NULL;
    }
#endif
    for (n=0; n<16; n++) {
        i = (UBYTE)(n*3);
        seqshare->cmd_buf[i] = (UBYTE)(CONTROL_CHANGE|n);
        seqshare->cmd_buf[i+1] = ALL_NOTES_OFF;
        seqshare->cmd_buf[i+2] = 0;
    }
#ifdef AMIGA
    if (seqshare->emulation != ONLYEMULATION && IOExtSer[0]->IOSer.io_Device)
        midi_out(seqshare->cmd_buf, 16*3);
#elif defined(__linux__) || defined(__APPLE__)
    if ((seqshare->gflag & PROCEEDING) && (seqshare->gflag & RECORDING)) {
        get_stamp();
        seqshare->dstamp = seqshare->cstamp - seqshare->pstamp;
        complete();
    }
#ifdef __linux__
    if (seqshare->emulation != ONLYEMULATION && rawmidi_out_handle) midi_out(seqshare->cmd_buf, 16 * 3);
    if (seqshare->emulation != NOEMULATION && seq_handle) emulate(seqshare->cmd_buf, 16 * 3);
#else
    if (seqshare->emulation != ONLYEMULATION && midiOutEndpointRef) midi_out(seqshare->cmd_buf, 16 * 3);
    if (seqshare->emulation != NOEMULATION && audioUnit) emulate(seqshare->cmd_buf, 16 * 3);
#endif
#elif defined(_WINDOWS)
    if (seqshare->emulation != ONLYEMULATION && seqshare->midiout[0]) {
        midi_out(seqshare, seqshare->cmd_buf, 16*3, 0);
        midiOutReset(seqshare->midiout[0]);
    }
    if (seqshare->emulation != NOEMULATION && seqshare->midiout[1]) {
        midi_out(seqshare, seqshare->cmd_buf, 16*3, 1);
        midiOutReset(seqshare->midiout[1]);
    }
#endif
    if (seqshare->rate == 256) seqshare->gflag &= 0xffff ^ RATING;
    seqshare->gflag &= 0xffff ^ PLAYING ^ PROCEEDING ^ READY;
}

int seq_brk(void)
{
    if (seqshare->gflag&PROCEEDING) halt_seq();
    out_level=0;
    if (stop_level) wk_stop();
    wk_quit();
    return 0;
}

#ifdef _WINDOWS
int handle_midiin(UINT message, LPARAM lParam)
{
    ULONG n;
    switch (message) {
    case WM_USER:
        BitBlt(seqshare->dc, 0, seqshare->row_num, 
               seqshare->width, 1, seqshare->dcmem, 0, 0, SRCCOPY);
        if (seqshare->row_num==seqshare->height-1) {
            seqshare->row_num=0;
            seqshare->gflag^=INVERTING;
            PatBlt(seqshare->dcmem, 0, 0, 
                   seqshare->width, 1, DSTINVERT);
        }
        else seqshare->row_num++;
        return 0;
    case MM_MIM_DATA: {
        UWORD lw;
        UBYTE sta;
        if (!(seqshare->gflag & RECORDING)) return 0;
        lw=LOWORD(lParam);
        sta=LOBYTE(lw);
        switch (sta&0xf0) {
        case PROGRAM_CHANGE:
        case CHANNEL_PRESSURE:
            n=1;
            break;
        case SYSTEM_EXCLUSIVE:
            return 0;
        default:
            n=2; }
        seqshare->cstamp=timeGetTime()-seqshare->stime;
        seqshare->dstamp=seqshare->cstamp-seqshare->pstamp;
        switch (seqshare->dstamp) {
        default:
            if (seqshare->rec_seq_pos+2*sizeof(ULONG)+4+n<=
                seqshare->rec_seq_end) {
                set_stamp();
                status=0;
            } else {
                complete();
                return 0;
            }
        case 0:
            if (seqshare->rec_seq_pos+sizeof(ULONG)+3+n<=
                seqshare->rec_seq_end) {
                if (sta != status) {
                    *seqshare->rec_seq_pos++=sta;
                    *seqshare->len_pos=(UBYTE)(*seqshare->len_pos+n+1);
                    status=sta;
                }
                else *seqshare->len_pos=(UBYTE)(*seqshare->len_pos+n);
                *seqshare->rec_seq_pos++=HIBYTE(lw);
                if (n == 2) *seqshare->rec_seq_pos++=
                                LOBYTE(HIWORD(lParam));
            } else complete();
        }
        return 0;
    }
    case MM_MIM_LONGDATA:
        if (!(seqshare->gflag & RECORDING)) {
            while (!(midiinhdr->dwFlags&MHDR_DONE));
            midiInUnprepareHeader(midiin, midiinhdr, sizeof(MIDIHDR));
            return 0;
        }
        if ((n=((MIDIHDR *)lParam)->dwBytesRecorded)>0) {
            seqshare->cstamp=timeGetTime()-seqshare->stime;
            seqshare->dstamp=seqshare->cstamp-seqshare->pstamp;
            switch (seqshare->dstamp) {
            default:
                if (seqshare->rec_seq_pos+2*sizeof(ULONG)+3+n<=
                    seqshare->rec_seq_end) set_stamp();
                else {
                    complete();
                    return 0;
                }
            case 0:
                if (seqshare->rec_seq_pos+sizeof(ULONG)+2+n<=
                    seqshare->rec_seq_end) {
                    memcpy(seqshare->rec_seq_pos, 
                           ((MIDIHDR *)lParam)->lpData, n);
                    seqshare->rec_seq_pos+=n;
                    *seqshare->len_pos=(UBYTE)(*seqshare->len_pos+n);
                    status=0;
                }
                else {
                    complete();
                    return 0;
                }
            } }
        while (!(midiinhdr->dwFlags&MHDR_DONE));
        midiInUnprepareHeader(midiin, midiinhdr, 
                              sizeof(MIDIHDR));
        midi_in();
        return 0;
    }
    return -1;
}

LONG EXPORT FAR PASCAL KeyProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    UBYTE n, channel, command, MQ *pos, MQ *end;
    switch (message) {
    case WM_USER:
        draw_key(seqshare, wParam & 0x80, wParam & 0x7f);
        return 0;
    case WM_KEYDOWN:
        if (HIWORD(lParam) & KF_REPEAT) return 0;
        switch (wParam) {
        case VK_UP:
            *seqshare->key_str = (UBYTE)(PITCH_BEND_CHANGE | seqshare->key_chan);
            seqshare->key_str[1] = seqshare->key_str[2] = 127;
            break;
        case VK_DOWN:
            *seqshare->key_str = (UBYTE)(PITCH_BEND_CHANGE | seqshare->key_chan);
            seqshare->key_str[1] = seqshare->key_str[2] = 0;
            break;
        case VK_LEFT:
            if (seqshare->key_octave > 0) seqshare->key_octave -= 3 * OCTAVE;
            else seqshare->key_octave = 6 * OCTAVE;
            return 0;
        case VK_RIGHT:
            if (seqshare->key_octave < 6 * OCTAVE) seqshare->key_octave += 3 * OCTAVE;
            else seqshare->key_octave = 0;
            return 0;
        default:
            if (wParam >= 112 && wParam <= 121) {
                seqshare->key_volume = (UBYTE)(1 + (wParam - 112) * 14);
                return 0;
            }
            if (wParam >= (WPARAM)seqshare->n_key_elems || !seqshare->key_table[wParam]) return 0;
            *seqshare->key_str = (UBYTE)(NOTE_ON | seqshare->key_chan);
            seqshare->key_str[1] = seqshare->key_table[wParam];
            seqshare->oct_key[seqshare->key_str[1] - A_] = seqshare->key_octave;
            seqshare->key_str[1] = (UBYTE)(seqshare->key_str[1] + seqshare->key_octave);
            seqshare->key_str[2] = seqshare->key_volume;
            break;
        }
        n=3;
        break;
    case WM_KEYUP:
        if (wParam == VK_UP || wParam == VK_DOWN) {
            *seqshare->key_str = (UBYTE)(PITCH_BEND_CHANGE | seqshare->key_chan);
            seqshare->key_str[1] = seqshare->key_str[2] = 63;
            n = 3;
            break;
        }
        if (wParam >= (WPARAM)seqshare->n_key_elems || !seqshare->key_table[wParam]) return 0;
        *seqshare->key_str = (UBYTE)(NOTE_ON | seqshare->key_chan);
        seqshare->key_str[1] = seqshare->key_table[wParam];
        if (seqshare->oct_key[seqshare->key_str[1] - A_])
            seqshare->key_str[1] = (UBYTE)(seqshare->key_str[1] + seqshare->oct_key[seqshare->key_str[1] - A_]);
        seqshare->key_str[2]=0;
        n=3;
        break;
    case WM_LBUTTONDOWN:
        if ((n = note_mouse(seqshare, LOWORD(lParam), HIWORD(lParam))) !=0 ) break;
        return 0;
    case WM_LBUTTONUP:
        seqshare->key_str[1] = seqshare->mouse_note;
        seqshare->mouse_note = seqshare->mou_note = 255;
        *seqshare->key_str = (UBYTE)(NOTE_ON | seqshare->key_chan);
        seqshare->key_str[2] = 0;
        n = 3;
        break;
    case WM_MOUSEMOVE:
        if (seqshare->mouse_note != 255 && (n = note_mouse(seqshare, LOWORD(lParam), HIWORD(lParam))) != 0) break;
        return 0;
    case WM_SIZE:
        if (!seqshare) return 0;
        seqshare->wh = (short)(HIWORD(lParam) - 26);
        seqshare->ww = (short)(LOWORD(lParam) - 36 - N_WHITES) / N_WHITES;
        set_key_dims(seqshare);
        return 0;
    case WM_PAINT: {
        RECT r;
        GetClientRect(hwnd, &r);
        for (n = 0; n < N_KEYS; n++) draw_key(seqshare, FALSE, n);
        ValidateRect(hwnd, &r);
        return 0;
    }
    case WM_CREATE:
        seqshare->key_dc = GetDC(hwnd);
        return 0;
    case WM_CLOSE:
        DestroyWindow(hwnd);
        return 0;
    case WM_DESTROY:
        ReleaseDC(hwnd, seqshare->key_dc);
        seqshare->key_wnd = (HWND)NULL;
        seqshare->gflag ^= KEYBOARD;
        return 0;
    default:
        return DefWindowProc(hwnd, message, wParam, lParam);
    }
    if (seqshare->gflag & RECORDING) {
        seqshare->cstamp = timeGetTime() - seqshare->stime;
        seqshare->dstamp = seqshare->cstamp - seqshare->pstamp;
        switch (seqshare->dstamp) {
        default:
            if (seqshare->rec_seq_pos + 2 * sizeof(ULONG) + 3 + n <= seqshare->rec_seq_end) {
                set_stamp();
                status=0;
            }
            else {
                complete();
                break;
            }
        case 0:
            if (seqshare->rec_seq_pos + sizeof(ULONG) + 2 + n <= seqshare->rec_seq_end) {
                if (status== *seqshare->key_str)
                    memcpy(seqshare->rec_seq_pos, seqshare->key_str + 1, --n);
                else memcpy(seqshare->rec_seq_pos, seqshare->key_str, n);
                status= *seqshare->key_str;
                seqshare->rec_seq_pos += n;
                *seqshare->len_pos = (UBYTE)(*seqshare->len_pos + n);
            }
            else complete();
        }
    }
    if ((seqshare->emulation != ONLYEMULATION && seqshare->midiout[0]) ||
        (seqshare->emulation != NOEMULATION && seqshare->midiout[1])) {
        MSG msg;
        seqshare->key_msg = seqshare->key_str;
        seqshare->key_len = n;
        seqshare->gflag |= RAISED;
        while (seqshare->gflag & RAISED) PeekMessage(&msg, (HWND)NULL, 0, 0, PM_NOREMOVE);
    }
    pos = seqshare->key_str;
    end = pos + n;
    while (pos<end && (*pos&0x80) == 0) pos++;
    while (pos<end) {
        command = (UBYTE)(*pos & 0xf0);
        channel = (UBYTE)(*pos & 0xf);
        pos++;
        switch (command) {
        case NOTE_ON:
        case NOTE_OFF:
            while (pos < end && (*pos & 0x80) == 0) {
                draw_key(seqshare, command == NOTE_ON && pos[1], *pos);
                if (seqshare->gflag & DISPLAYING && (channel < seqshare->num_chans || channel == 9))
                    show_note(seqshare, channel, *pos, command == NOTE_ON && pos[1]);
                pos += 2;
            }
            continue;
        case PROGRAM_CHANGE:
        case CHANNEL_PRESSURE:
            while (pos < end && (*pos & 0x80) == 0) pos++;
            continue;
        case SYSTEM_EXCLUSIVE:
            while (pos < end && (*pos & 0x80) == 0) pos++;
            continue;
        default:
            while (pos < end && (*pos & 0x80) == 0) pos += 2;
        }
    }
    return 0;
}
#elif defined(XLIB)
int do_key_event(XEvent *event)
{
    UBYTE n = 0, channel, command, *pos, *end;
    switch (event->type) {
    case KeyPress: {
        char c;
        KeySym ks;
        XLookupString(&event->xkey, &c, 1, &ks, NULL);
        switch (ks) {
        case XK_Left:
            if (seqshare->key_octave>0)
                seqshare->key_octave-=OCTAVE;
            else seqshare->key_octave = 9 * OCTAVE;
            return 0;
        case XK_Up:
            if (event->xkey.state & ControlMask) {
                if (seqshare->program_number > 0)
                    seqshare->program_number--;
                else seqshare->program_number = 127;
                MusicDeviceMIDIEvent(audioUnit,
                                     PROGRAM_CHANGE | seqshare->key_chan,
                                     seqshare->program_number, 0, 0);
                break;
            }
            *seqshare->key_str=PITCH_BEND_CHANGE|
                seqshare->key_chan;
            seqshare->key_str[1]=seqshare->key_str[2]=127;
            break;
        case XK_Right:
            if (seqshare->key_octave<9*OCTAVE)
                seqshare->key_octave+=OCTAVE;
            else seqshare->key_octave=0;
            return 0;
        case XK_Down:
            if (event->xkey.state & ControlMask) {
                if (seqshare->program_number < 127)
                    seqshare->program_number++;
                else seqshare->program_number = 0;
                MusicDeviceMIDIEvent(audioUnit,
                                     PROGRAM_CHANGE | seqshare->key_chan,
                                     seqshare->program_number, 0, 0);
                break;
            }
            *seqshare->key_str=PITCH_BEND_CHANGE|
                seqshare->key_chan;
            seqshare->key_str[1]=seqshare->key_str[2]=0;
            break;
        default:
#ifdef __APPLE__
            if (((event->xkey.state & ShiftMask) || (event->xkey.state & ControlMask)) &&
                ((event->xkey.keycode>=26 && event->xkey.keycode<=31) ||
                 (event->xkey.keycode>=33 && event->xkey.keycode<=34) ||
                 (event->xkey.keycode>=36 && event->xkey.keycode<=37))) {
                if (event->xkey.state & ShiftMask)
                    seqshare->key_volume = key_volume_table[event->xkey.keycode-26];
                else seqshare->key_chan = key_chan_table[event->xkey.keycode-26];
                return 0;
            }
#else
            if (event->xkey.keycode>=67 && event->xkey.keycode<=76) {
                seqshare->key_volume=
                    1+(event->xkey.keycode-67)*14;
                return 0;
            }
#endif
            if (event->xkey.keycode>=seqshare->n_key_elems||
                !seqshare->key_table[event->xkey.keycode]) return 0;
            *seqshare->key_str=NOTE_ON|seqshare->key_chan;
            seqshare->key_str[1]=
                seqshare->key_table[event->xkey.keycode];
            seqshare->oct_key[seqshare->key_str[1]-A_]=
                seqshare->key_octave;
            seqshare->key_str[1]+=seqshare->key_octave;
            seqshare->key_str[2]=seqshare->key_volume;
            break;
        }
        n=3;
        break;
    }
    case KeyRelease: {
        char c;
        KeySym ks;
        XLookupString(&event->xkey, &c, 1, &ks, NULL);
        if (ks == XK_Up || ks == XK_Down) {
            *seqshare->key_str = PITCH_BEND_CHANGE | seqshare->key_chan;
            seqshare->key_str[1] = seqshare->key_str[2] = 63;
            n = 3;
            break;
        }
        if (event->xkey.keycode >= seqshare->n_key_elems || !seqshare->key_table[event->xkey.keycode]) return 0;
        *seqshare->key_str = NOTE_ON | seqshare->key_chan;
        seqshare->key_str[1] = seqshare->key_table[event->xkey.keycode];
        if (seqshare->oct_key[seqshare->key_str[1] - A_])
            seqshare->key_str[1] += seqshare->oct_key[seqshare->key_str[1] - A_];
        seqshare->key_str[2] = 0;
        n=3;
        break;
    }
    case ButtonPress:
        if ((n = note_mouse(event->xbutton.x, event->xbutton.y))) break;
        return 0;
    case ButtonRelease:
        seqshare->key_str[1]=seqshare->mouse_note;
        seqshare->mouse_note=seqshare->mou_note=255;
        *seqshare->key_str=NOTE_ON|seqshare->key_chan;
        seqshare->key_str[2]=0;
        n=3;
        break;
    case MotionNotify:
        if (seqshare->mouse_note != 255 && (n = note_mouse(event->xbutton.x, event->xbutton.y))) break;
        return 0;
    case Expose: {
        for (n = 0; n < N_KEYS; n++) draw_key(FALSE, n);
        return 0;
    }
    case ConfigureNotify: {
        Status status;
        Window root;
        XImage *image;
        int x, y;
        unsigned width, height, border_width, depth;
        seqshare->wh = event->xconfigure.height - 26;
        seqshare->ww = (event->xconfigure.width - 36 - N_WHITES) / N_WHITES;
        set_key_dims(seqshare);
        status = XGetGeometry(display, kwnd, &root, &x, &y, &width, &height, &border_width, &depth);
        if (status) {
            image = XGetImage(display, kwnd, 0, 0, width, height, AllPlanes, XYPixmap);
            if (image) {
                if (kimage) {
                    XDestroyImage(kimage);
                }
                kimage = image;
            }
        }
        return 0;
    }
    }
    if (seqshare->gflag & RECORDING) {
        get_stamp();
        seqshare->dstamp = seqshare->cstamp - seqshare->pstamp;
        switch (seqshare->dstamp) {
        default:
            if (seqshare->rec_seq_pos+2*sizeof(ULONG)+3+n<=
                seqshare->rec_seq_end) {
                set_stamp();
                status=0;
            }
            else {
                complete();
                break;
            }
        case 0:
            if (seqshare->rec_seq_pos+sizeof(ULONG)+2+n<=
                seqshare->rec_seq_end) {
                if (status== *seqshare->key_str)
                    memcpy(seqshare->rec_seq_pos, seqshare->key_str+1, --n);
                else
                    memcpy(seqshare->rec_seq_pos, seqshare->key_str, n);
                status= *seqshare->key_str;
                seqshare->rec_seq_pos+=n;
                *seqshare->len_pos+=n;
            }
            else complete();
        } }
    pos=seqshare->key_str;
    end=pos+n;
#ifdef __linux__
    if (seqshare->emulation != ONLYEMULATION && rawmidi_out_handle) midi_out(pos, n);
    if (seqshare->emulation != NOEMULATION && seq_handle) emulate(pos, n);
#else
    if (seqshare->emulation != ONLYEMULATION && midiOutEndpointRef) midi_out(pos, n);
    if (seqshare->emulation != NOEMULATION && audioUnit) emulate(pos, n);
#endif
    while (pos<end&&(*pos&0x80) == 0) pos++;
    while (pos<end) {
        command= *pos&0xf0;
        channel= *pos&0xf;
        pos++;
        switch (command) {
        case NOTE_ON:
        case NOTE_OFF:
            while (pos<end && (*pos&0x80) == 0) {
                draw_key(command == NOTE_ON && pos[1], *pos);
                if (seqshare->gflag&DISPLAYING&&
                    (channel<seqshare->num_chans || channel == 9))
                    show_note(channel, *pos, command == NOTE_ON && pos[1]);
                pos+=2; }
            continue;
        case PROGRAM_CHANGE:
        case CHANNEL_PRESSURE:
            while (pos<end && (*pos&0x80) == 0) pos++;
            continue;
        case SYSTEM_EXCLUSIVE:
            while (pos<end && (*pos&0x80) == 0) pos++;
            continue;
        default:
            while (pos<end && (*pos&0x80) == 0) pos+=2;
        } }
    return 0;
}
#endif

void taskproc_clean(void)
{
#ifdef AMIGA
    if (tmrmp) {
        DeletePort(tmrmp);
        tmrmp=NULL;
    }
    if (sermp) {
        DeletePort(sermp);
        sermp=NULL;
    }
    if (audmp) {
        DeletePort(audmp);
        audmp=NULL;
    }
    if (key_win) {
        CloseWindow(key_win);
        key_win=NULL;
    }
#endif
}

int taskproc_init(void)
{
    UBYTE n;
    for (n=0; n<128; n++) seqshare->cnv_old[0][n]=n;
    for (n=1; n<16; n++) memcpy(seqshare->cnv_old[n], *seqshare->cnv_old, 128);
#ifdef AMIGA
    {
        UBYTE m;
        if (IOExtSer[0]->IOSer.io_Device) {
            if (!(sermp=(struct MsgPort *)CreatePort(NULL, 127))||
                !(tmrmp=(struct MsgPort *)CreatePort(NULL, 127))||
                !(audmp=(struct MsgPort *)CreatePort(NULL, 127))) return -1;
            for (n=0; n<3; n++)
                IOExtSer[n]->IOSer.io_Message.mn_ReplyPort=sermp; }
        for (n=0; n<3; n++)
            timerequest[n]->tr_node.io_Message.mn_ReplyPort=tmrmp;
        for (n=0; n<4; n++)
            for (m=0; m<3; m++)
                IOAudio[n][m]->ioa_Request.io_Message.mn_ReplyPort=audmp;
        allocate();
        if (!(seqshare->gflag & RECORDING)) seqshare->gflag &= 0xffff ^ ECHOING;
    }
#endif
    if (seqshare->gflag&KEYBOARD) {
#ifdef AMIGA
        struct NewWindow *nw;
        if (!(nw=(struct NewWindow *)AllocMem(
                                              sizeof(struct NewWindow), MEMF_PUBLIC|MEMF_CLEAR)))
            return -1;
        if (GfxBase->ActiView->ViewPort->Modes&LACE) {
            seqshare->wh = WH * 2;
            seqshare->bh = BH * 2;
        }
        else {
            seqshare->wh = WH;
            seqshare->bh = BH;
        }
        set_key_dims(seqshare);
        if (GfxBase->ActiView->ViewPort->ColorMap) {
            pennum[0]=2;
            pennum[1]=1;
            pennum[2]=3;
        }
        else {
            pennum[0]=0;
            pennum[1]=1;
            pennum[2]=2;
        }
        nw->MinWidth=nw->MaxWidth=nw->Width=KW+24;
        nw->MinHeight=nw->Height=seqshare->wh+26;
        nw->MaxHeight=GfxBase->ActiView->ViewPort->DHeight;
        nw->LeftEdge=(GfxBase->ActiView->ViewPort->DWidth-nw->Width)/2;
        nw->TopEdge=(GfxBase->ActiView->ViewPort->DHeight-nw->Height)/2;
        if (nw->LeftEdge < 0) nw->LeftEdge = 0;
        if (nw->TopEdge < 0) nw->TopEdge = 0;
        nw->BlockPen=1;
        nw->IDCMPFlags=NEWSIZE|MOUSEBUTTONS|MOUSEMOVE|CLOSEWINDOW|RAWKEY;
        nw->Flags=WINDOWSIZING|WINDOWDRAG|WINDOWDEPTH|WINDOWCLOSE|
            REPORTMOUSE|ACTIVATE;
        nw->Title=wake_key_name;
        nw->Type=WBENCHSCREEN;
        key_win=(struct Window *)OpenWindow(nw);
        FreeMem(nw, sizeof(struct NewWindow));
        if (!key_win) return -1;
        kwrp=key_win->RPort;
        SetDrMd(kwrp, JAM1);
        SetBPen(kwrp, 1);
        for (n=0; n<N_KEYS; n++) draw_key(FALSE, n);
#elif defined(VGALIB)
#elif defined(_WINDOWS)
        int x = (seqshare->width - KW - 24) / 2, y = (seqshare->height - WH - 26) / 2;
        if (x < 0) x = 0;
        if (y < 0) y = 0;
        seqshare->key_wnd = CreateWindow(seqshare->key_name, 
                                         seqshare->key_name, WS_OVERLAPPEDWINDOW|WS_VISIBLE, x, y, 
                                         KW + 24, WH + 56, (HWND)NULL, (HMENU)NULL, seqshare->instance, &seqshare);
        for (n = 0; n < 2; n++) draw_key(seqshare, FALSE, n);
        seqshare->colors[0] = GetPixel(seqshare->key_dc, KEY_LEFT + seqshare->ww / 2, KEY_TOP + seqshare->wh / 2);
        seqshare->colors[1] = GetPixel(seqshare->key_dc, KEY_LEFT + (seqshare->ww - seqshare->bw1 + 1) + 1, KEY_TOP + seqshare->bh / 2);
        draw_key(seqshare, TRUE, 0);
        seqshare->colors[2] = GetPixel(seqshare->key_dc, KEY_LEFT + seqshare->ww / 2, KEY_TOP + seqshare->wh / 2);
        if (!seqshare->key_wnd) return -1;
        ShowWindow(seqshare->key_wnd, SW_SHOW);
#elif defined(XLIB)
        XGCValues gcv;
        XSetWindowAttributes swa;
        XSizeHints *sizeHints;
        XWMHints *wmHints;
        XClassHint *classHint;
        XTextProperty windowName, iconName;
        int wxsize = KW + 24, wysize = WH + 26, 
            x = (width - wxsize) / 2, y = (height - wysize) / 2;
        if (x < 0) x = 0;
        if (y < 0) y = 0;
        if (!(sizeHints = XAllocSizeHints()) ||
            !(wmHints = XAllocWMHints()) ||
            !(classHint = XAllocClassHint()) ||
            !XStringListToTextProperty(&wake_key_name, 1, &windowName) ||
            !XStringListToTextProperty(&wake_key_name, 1, &iconName)) return -1;
        sizeHints->flags = PPosition | PSize | PMinSize;
        sizeHints->min_width=wxsize;
        sizeHints->min_height=wysize/2;
        wmHints->flags = StateHint | InputHint;
        wmHints->initial_state = NormalState;
        wmHints->input = True;
        classHint->res_name = wake_key_name;
        classHint->res_class = wake_key_name;
        swa.event_mask=KeyPressMask|KeyReleaseMask|ButtonPressMask|
            ButtonReleaseMask|PointerMotionMask|ExposureMask|
            StructureNotifyMask;
        swa.background_pixel=pennum[3];
        serial=XNextRequest(display);
        kwnd=XCreateWindow(display, XDefaultRootWindow(display), x, y, wxsize, wysize, 0, XDefaultDepth(display, screen), InputOutput, XDefaultVisual(display, screen), CWBackPixel|CWEventMask, &swa);
        XSync(display, FALSE);
        if (xerror) return -1;
        serial=XNextRequest(display);
        XSetWMProperties(display, kwnd, &windowName, &iconName, (char **)NULL, 0, sizeHints, wmHints, classHint);
        XSync(display, FALSE);
        if (xerror) return -1;
        XStoreName(display, kwnd, wake_key_name);
        gcv.background=pennum[3];
        serial=XNextRequest(display);
        gck=XCreateGC(display, kwnd, GCBackground, &gcv);
        XSync(display, FALSE);
        if (xerror) {
            XDestroyWindow(display, kwnd);
            return -1;
        }
        serial=XNextRequest(display);
        XMapRaised(display, kwnd);
        XSync(display, FALSE);
        if (xerror) {
            XDestroyWindow(display, kwnd);
            return -1;
        }
        XAutoRepeatOff(display);
        XFree(sizeHints);
        XFree(wmHints);
        XFree(classHint);
#endif
        seqshare->mouse_note=seqshare->mou_note=255;
    }
    seqshare->seq_pos=seqshare->sequence;
    seqshare->rec_seq_pos=seqshare->rec_sequence;
    if (seqshare->gflag&DISPLAYING) {
#ifdef AMIGA
        UWORD t;
        LoadRGB4(viewport, palette, n_colors);
        BNDRYOFF(rastport);
        BNDRYOFF(rastport2);
        for (n=0; n<=seqshare->num_chans; n++) {
            t=seqshare->disp_pos+(n<<7);
            SetAPen(rastport, (n&1)?0:15);
            RectFill(rastport, t, 0, t+127, height-1);
            SetAPen(rastport2, (n&1)?15:0);
            RectFill(rastport2, t, 0, t+127, 0); }
        if (seqshare->disp_pos) {
            t=seqshare->disp_pos+(n<<7);
            SetAPen(rastport, 0);
            RectFill(rastport, 0, 0, seqshare->disp_pos-1, height-1);
            SetAPen(rastport2, 15);
            RectFill(rastport2, 0, 0, seqshare->disp_pos-1, 0);
            SetAPen(rastport, (seqshare->num_chans&1)?15:0);
            RectFill(rastport, t, 0, width-1, height-1);
            SetAPen(rastport2, (seqshare->num_chans&1)?0:15);
            RectFill(rastport2, t, 0, width-1, 0); }
#elif defined(VGALIB)
#elif defined (_WINDOWS)
        UWORD t;
        seqshare->dc=dc;
        seqshare->dcmem=dcmem;
        seqshare->bitmap=bitmap;
        SelectObject(seqshare->dcmem, seqshare->bitmap);
        for (n=0; n<=seqshare->num_chans; n++) {
            t=(UWORD)(seqshare->disp_pos+(n<<7));
            SelectObject(seqshare->dc, seqshare->brushes[n&1]);
            PatBlt(seqshare->dc, t, 0, 128, height, PATCOPY);
            SelectObject(seqshare->dcmem, seqshare->brushes[(n&1)^1]);
            PatBlt(seqshare->dcmem, t, 0, 128, 1, PATCOPY); }
        if (seqshare->disp_pos) {
            t=(UWORD)(seqshare->disp_pos+(n<<7));
            SelectObject(seqshare->dc, seqshare->brushes[1]);
            PatBlt(seqshare->dc, 0, 0, seqshare->disp_pos, height, PATCOPY);
            SelectObject(seqshare->dcmem, seqshare->brushes[0]);
            PatBlt(seqshare->dcmem, 0, 0, seqshare->disp_pos, 1, PATCOPY);
            SelectObject(seqshare->dc, 
                         seqshare->brushes[(seqshare->num_chans&1)^1]);
            PatBlt(seqshare->dc, t, 0, width-t, height, PATCOPY);
            SelectObject(seqshare->dcmem, 
                         seqshare->brushes[seqshare->num_chans&1]);
            PatBlt(seqshare->dcmem, t, 0, width-t, 1, PATCOPY); }
        SelectObject(seqshare->dc, brush?brush:dbrush);
        SetBkMode(seqshare->dcmem, TRANSPARENT);
        SetROP2(seqshare->dcmem, R2_COPYPEN);
#elif defined(XLIB)
        int t;
        for (n=0; n<=seqshare->num_chans; n++) {
            t=seqshare->disp_pos+(n<<7);
            XSetForeground(display, gc, pennum[n&1]);
            XFillRectangle(display, window, gc, t, 0, 128, height);
            XSetForeground(display, gc, pennum[(n&1)^1]);
            XFillRectangle(display, pixmap, gc, t, 0, 128, 1); }
        if (seqshare->disp_pos) {
            t=seqshare->disp_pos+(n<<7);
            XSetForeground(display, gc, pennum[1]);
            XFillRectangle(display, window, gc, 
                           0, 0, seqshare->disp_pos, height);
            XSetForeground(display, gc, pennum[0]);
            XFillRectangle(display, window, gc, 0, 0, seqshare->disp_pos, 1);
            XSetForeground(display, gc, pennum[(seqshare->num_chans&1)^1]);
            XFillRectangle(display, window, gc, t, 0, width-t, height);
            XSetForeground(display, gc, pennum[seqshare->num_chans&1]);
            XFillRectangle(display, window, gc, t, 0, width-t, 1); }
#else
        memset(bitmap+4, 0, bitmap_size-4);
        cleardevice();
#endif
        seqshare->row_num=0;
        seqshare->gflag&=0xffff^INVERTING;
    }
    seqshare->cstamp = seqshare->lstamp = seqshare->nstamp = seqshare->pstamp = 0L;
    if (seqshare->gflag&PLAYING) seqshare->gflag|=STARTED;
    else seqshare->gflag&=0xffff^STARTED;
#ifdef AMIGA
    if (IOExtSer[0]->IOSer.io_Device) {
        IOExtSer[0]->IOSer.io_Command=CMD_CLEAR;
        IOExtSer[0]->IOSer.io_Flags=0;
        DoIO((struct IORequest *)IOExtSer[0]);
        WaitIO((struct IORequest *)IOExtSer[0]);
    }
    Signal(thatTask, 1L);
    timerequest[0]->tr_node.io_Command=TR_GETSYSTIME;
    timerequest[0]->tr_node.io_Flags=IOF_QUICK;
    DoIO((struct IORequest *)timerequest[0]);
    timeval=timerequest[0]->tr_time;
#elif defined(_WINDOWS)
    status=0;
    if (midiin && (seqshare->gflag & RECORDING)) {
        midi_in();
        midiInStart(midiin);
    }
    seqshare->stime=timeGetTime();
#endif
    return 0;
}

#ifdef _DCC
__geta4 void taskproc(void)
#elif defined(LATTICE)
    __saveds void taskproc(void)
#elif defined(_WINDOWS)
    void EXPORT CALLBACK taskproc(UINT wTimerID, UINT wMsg, DWORD dwUser, 
                                  DWORD dw1, DWORD dw2)
#else
    void *taskproc(void *data)
#endif
{
    char pass;
    UBYTE channel = 0, command = 0, note = 0, number = 0, status = 0, MQ *msg_pos=NULL, msg_len=0, MQ *pos, MQ *end;
#ifdef AMIGA
    BYTE rec;
    ULONG bits, mask=SIGBREAKF_CTRL_C;
    struct IntuiMessage *imsg;
    if (taskproc_init()) {
        taskproc_clean();
        seqshare->gflag|=ALLOCERROR;
        Signal(thatTask, 1L);
        Wait(0L);
    }
    SetTaskPri(FindTask(NULL), 127);
    rec = ((seqshare->gflag & RECORDING) &&
           (seqshare->emulation != ONLYEMULATION) && IOExtSer[0]->IOSer.io_Device);
    if (seqshare->gflag&KEYBOARD)
        mask|=1<<key_win->UserPort->mp_SigBit;
    if (seqshare->gflag&PLAYING)
        mask|=1<<timerequest[1]->tr_node.io_Message.mn_ReplyPort->mp_SigBit;
    if (seqshare->emulation || (seqshare->gflag&DISPLAYING)) {
        mask|=1<<timerequest[2]->tr_node.io_Message.mn_ReplyPort->mp_SigBit;
        timerequest[2]->tr_node.io_Command=TR_ADDREQUEST;
        timerequest[2]->tr_node.io_Flags=0;
        timerequest[2]->tr_time.tv_secs=0;
        timerequest[2]->tr_time.tv_micro=20000;
        SendIO((struct IORequest *)timerequest[2]);
    }
#elif defined(_WINDOWS)
    SEQSHARE *seqshare=(SEQSHARE *)dwUser;
    if (seqshare->gflag&RAISED) {
        if (seqshare->emulation != ONLYEMULATION && seqshare->midiout[0])
            midi_out(seqshare, seqshare->key_msg, seqshare->key_len, 0);
        if (seqshare->emulation != NOEMULATION && seqshare->midiout[1])
            midi_out(seqshare, seqshare->key_msg, seqshare->key_len, 1);
        seqshare->gflag&=0xffff^RAISED;
    }
#elif defined(__linux__) || defined(__APPLE__)
    int n;
    gettimeofday(&seqshare->stv, NULL);
    status = 0;
#endif
    for (;;) {
        if ((seqshare->gflag & PLAYING) && !(seqshare->gflag & READY)) {
            if (seqshare->seq_pos >= seqshare->seq_end)
                switch (seqshare->pmode) {
                case NONSTOP:
                    seqshare->seq_pos = seqshare->sequence;
                    break;
                case AUTOSTOP:
#ifdef AMIGA
                    SetTaskPri(FindTask(NULL), 
                               thatTask->tc_Node.ln_Pri);
                    Signal(thatTask, SIGBREAKF_CTRL_C);
#elif defined(_WINDOWS)
                    PostMessage(seqshare->hwnd, WM_CHAR, (short)'\3', 0L);
#else
                    taskproc_id = (pthread_t)0;
                    halt_seq();
                    pthread_exit(NULL);
#endif
                default:
                    seqshare->gflag &= 0xffff ^ PLAYING;
#ifdef _WINDOWS
                    return;
#else
                    continue;
#endif
                }
            seqshare->seq_pos=getvarnum(seqshare->seq_pos, &seqshare->delta);
            if (seqshare->gflag&RATING) seqshare->delta=(seqshare->delta<<8)/seqshare->rate;
            seqshare->nstamp+=seqshare->delta;
            seqshare->gflag|=READY;
#ifdef AMIGA
            timerequest[1]->tr_node.io_Command=TR_ADDREQUEST;
            timerequest[1]->tr_node.io_Flags=0;
            timerequest[1]->tr_time.tv_secs=seqshare->delta/1000;
            timerequest[1]->tr_time.tv_micro=seqshare->delta%1000*1000;
            SendIO((struct IORequest *)timerequest[1]);
#endif
            seqshare->msg_len= *seqshare->seq_pos++;
            if (*seqshare->seq_pos == SPECIAL) {
                seqshare->seq_pos=seqshare->seq_end;
                continue;
            }
            if (seqshare->gflag&CONVERTING) {
                pos=seqshare->cmd_str;
                end=seqshare->seq_pos+seqshare->msg_len;
                if (seqshare->gflag&SYSEX)
                    while (seqshare->seq_pos<end&&
                           (*seqshare->seq_pos&0x80) == 0)
                        *pos++= *seqshare->seq_pos++;
                else while (seqshare->seq_pos<end&&
                            (*seqshare->seq_pos&0x80) == 0)
                         seqshare->seq_pos++;
                while (seqshare->seq_pos<end) {
                    if (*seqshare->seq_pos == 0xf0) {
                        if (seqshare->gflag&SYSEX) {
                            *pos++= *seqshare->seq_pos++;
                            while (seqshare->seq_pos<end&&
                                   (*seqshare->seq_pos&0x80) == 0)
                                *pos++= *seqshare->seq_pos++;
                        }
                        else {
                            seqshare->seq_pos++;
                            while (seqshare->seq_pos<end&&
                                   (*seqshare->seq_pos&0x80) == 0)
                                seqshare->seq_pos++;
                        }
                        continue;
                    }
                    command=(UBYTE)(*seqshare->seq_pos&0xf0);
                    channel=(UBYTE)(*seqshare->seq_pos&0xf);
                    pass=(char)(!(seqshare->conv_flag&CHANNELS) || !(seqshare->channels[channel]&0x80));
                    if (pass) {
                        if (seqshare->conv_flag&CHANNELS) *pos++=(UBYTE)(command|seqshare->channels[channel]);
                        else *pos++= *seqshare->seq_pos;
                    }
                    seqshare->seq_pos++;
                    switch (command) {
                    case NOTE_ON:
                    case NOTE_OFF:
                        while (seqshare->seq_pos<end && (*seqshare->seq_pos&0x80) == 0) {
                            if (command == NOTE_ON && seqshare->seq_pos[1])
                                if (pass)
                                    if (seqshare->note_flag&(1<<channel))
                                        if (seqshare->cnv_str[channel][*seqshare->seq_pos]&0x80) {
                                            seqshare->seq_pos+=2;
                                            continue;
                                        }
                                        else *pos++=seqshare->cnv_old[channel][*seqshare->seq_pos]=seqshare->cnv_str[channel][*seqshare->seq_pos];
                                    else *pos++=seqshare->cnv_old[channel][*seqshare->seq_pos]= *seqshare->seq_pos;
                                else {
                                    seqshare->seq_pos+=2;
                                    continue;
                                }
                            else if (seqshare->cnv_old[channel][*seqshare->seq_pos]&0x80) {
                                seqshare->seq_pos+=2;
                                continue;
                            }
                            else {
                                *pos++=seqshare->cnv_old[channel][*seqshare->seq_pos];
                                seqshare->cnv_old[channel][*seqshare->seq_pos]=0x80;
                            }
                            *pos++=(UBYTE)((seqshare->vel_flag&(1<<channel))?
                                           seqshare->vel_str[channel][seqshare->seq_pos[1]]:seqshare->seq_pos[1]);
                            seqshare->seq_pos+=2;
                        }
                        continue;
                    case PROGRAM_CHANGE:
                        while (seqshare->seq_pos<end && (*seqshare->seq_pos&0x80) == 0) {
                            if (pass) {
                                if (seqshare->conv_flag&PROGRAMS) *pos++=seqshare->programs[*seqshare->seq_pos];
                                else *pos++= *seqshare->seq_pos;
                            }
                            seqshare->seq_pos++;
                        }
                        continue;
                    case CHANNEL_PRESSURE:
                        while (seqshare->seq_pos<end && (*seqshare->seq_pos&0x80) == 0) {
                            if (pass) *pos++= *seqshare->seq_pos;
                            seqshare->seq_pos++;
                        }
                        continue;
                    default:
                        while (seqshare->seq_pos<end && (*seqshare->seq_pos&0x80) == 0) {
                            if (pass) {
                                *pos++= *seqshare->seq_pos++;
                                *pos++= *seqshare->seq_pos++;
                            }
                            else seqshare->seq_pos+=2;
                        }
                    }
                }
                seqshare->msg_len=(UBYTE)(pos-(UBYTE MQ *)seqshare->cmd_str);
                seqshare->msg_pos=seqshare->cmd_str;
            }
            else {
                seqshare->msg_pos=seqshare->seq_pos;
                seqshare->seq_pos+=seqshare->msg_len;
            }
        }
#if defined(__linux__) || defined(__APPLE__)
        get_stamp();
        pthread_testcancel();
#endif
        for (;;) {
#ifdef AMIGA
            UBYTE n, m, *pos, *end;
            if (rec) {
                for (;;) {
                    IOExtSer[2]->IOSer.io_Command=SDCMD_QUERY;
                    IOExtSer[2]->IOSer.io_Flags=0;
                    DoIO((struct IORequest *)IOExtSer[2]);
                    WaitIO((struct IORequest *)IOExtSer[2]);
                    if (IOExtSer[2]->IOSer.io_Actual == 0) break;
                    timerequest[0]->tr_node.io_Command=TR_GETSYSTIME;
                    timerequest[0]->tr_node.io_Flags=IOF_QUICK;
                    DoIO((struct IORequest *)timerequest[0]);
                    SubTime(&timerequest[0]->tr_time, &timeval);
                    seqshare->cstamp=(timerequest[0]->tr_time.tv_micro+500)/1000+
                        timerequest[0]->tr_time.tv_secs*1000;
                    seqshare->dstamp=seqshare->cstamp-seqshare->pstamp;
                    IOExtSer[2]->IOSer.io_Command=CMD_READ;
                    IOExtSer[2]->IOSer.io_Flags=IOF_QUICK;
                    IOExtSer[2]->IOSer.io_Data=seqshare->cmd_buf;
                    IOExtSer[2]->IOSer.io_Length=IOExtSer[2]->IOSer.io_Actual;
                    DoIO((struct IORequest *)IOExtSer[2]);
                    WaitIO((struct IORequest *)IOExtSer[2]);
                    pos=seqshare->cmd_buf;
                    end=pos+IOExtSer[2]->IOSer.io_Actual;
                    while (pos<end) {
                        if ((*pos&0xf0) == 0xf0 && *pos != 0xf0) {
                            pos++;
                            continue;
                        }
                        if (seqshare->dstamp)
                            if (seqshare->rec_seq_pos+2*sizeof(ULONG)+4<=
                                seqshare->rec_seq_end) {
                                set_stamp();
                                seqshare->dstamp=0L;
                                if (*pos&0x80) status= *pos;
                                *seqshare->rec_seq_pos++= *pos;
                                *seqshare->len_pos=1;
                            }
                            else {
                                complete();
                                break;
                            }
                        else if (seqshare->rec_seq_pos+sizeof(ULONG)+3<=
                                 seqshare->rec_seq_end) {
                            if (*pos&0x80) status= *pos;
                            *seqshare->rec_seq_pos++= *pos;
                            (*seqshare->len_pos)++;
                        }
                        else {
                            complete();
                            break;
                        }
                        pos++; } }
                while (GetMsg(IOExtSer[2]->IOSer.io_Message.mn_ReplyPort));
                timerequest[0]->tr_node.io_Command=TR_ADDREQUEST;
                timerequest[0]->tr_node.io_Flags=IOF_QUICK;
                timerequest[0]->tr_time.tv_secs=0;
                timerequest[0]->tr_time.tv_micro=1000;
                DoIO((struct IORequest *)timerequest[0]);
                bits=procTask->tc_SigRecvd; }
            else bits=Wait(mask);
            if (bits&SIGBREAKF_CTRL_C) {
                if (seqshare->gflag & RECORDING) {
                    timerequest[0]->tr_node.io_Command=TR_GETSYSTIME;
                    timerequest[0]->tr_node.io_Flags=IOF_QUICK;
                    DoIO((struct IORequest *)timerequest[0]);
                    SubTime(&timerequest[0]->tr_time, &timeval);
                    seqshare->cstamp=
                        (timerequest[0]->tr_time.tv_micro+500)/1000+
                        timerequest[0]->tr_time.tv_secs*1000;
                    seqshare->dstamp=seqshare->cstamp-seqshare->pstamp;
                    complete(); }
                SetTaskPri(FindTask(NULL), thatTask->tc_Node.ln_Pri);
                if (IOExtSer[0]->IOSer.io_Device) {
                    if (!CheckIO((struct IORequest *)IOExtSer[ser_ion]))
                        WaitIO((struct IORequest *)IOExtSer[ser_ion]);
                    while (GetMsg(IOExtSer[ser_ion]->IOSer.io_Message.mn_ReplyPort));
                    for (n=0; n<3; n++)
                        IOExtSer[n]->IOSer.io_Message.mn_ReplyPort=serMsgPort; }
                for (n=0; n<4; n++)
                    for (m=0; m<3; m++)
                        AbortIO((struct IORequest *)IOAudio[n][m]);
                aud_use=0;
                free_audio();
                for (n=0; n<4; n++)
                    for (m=0; m<3; m++)
                        IOAudio[n][m]->ioa_Request.io_Message.mn_ReplyPort=audMsgPort;
                for (n=0; n<3; n++) {
                    AbortIO((struct IORequest *)timerequest[n]);
                    timerequest[n]->tr_node.io_Message.mn_ReplyPort=tmrMsgPort; }
                taskproc_clean();
                Signal(thatTask, 1L);
                Wait(0L);
            }
            if ((seqshare->emulation || (seqshare->gflag&DISPLAYING))&&
                CheckIO((struct IORequest *)timerequest[2])) {
                if (n_pitchbends)
                    for (n=0; n<4; n++)
                        if (pitchbend[n]) {
                            if (pitchbend[n] == 1) {
                                if (IOAudio[n][2]->ioa_Period<period[n])
                                    IOAudio[n][2]->ioa_Period++;
                                else {
                                    pitchbend[n]=0;
                                    if (--n_pitchbends) continue;
                                    break;
                                }
                            }
                            else if (IOAudio[n][2]->ioa_Period>period[n])
                                IOAudio[n][2]->ioa_Period--;
                            else {
                                pitchbend[n]=0;
                                if (--n_pitchbends) continue;
                                break;
                            }
                            IOAudio[n][2]->ioa_Request.io_Unit=(struct Unit *)btms[n];
                            IOAudio[n][2]->ioa_Request.io_Command=ADCMD_PERVOL;
                            IOAudio[n][2]->ioa_Request.io_Flags=IOF_QUICK|ADIOF_SYNCCYCLE;
                            IOAudio[n][2]->ioa_Volume=IOAudio[n][1]->ioa_Volume;
                            IOAudio[n][2]->ioa_Data=IOAudio[n][1]->ioa_Data;
                            BeginIO((struct IORequest *)IOAudio[n][2]);
                        }
                if (seqshare->gflag&DISPLAYING) {
                    BltBitMap(bitmap, 0, 0, dbitmap, 0, seqshare->row_num, width, 1, 
                              0xcc, 0xf, NULL);
                    if (seqshare->row_num == height-1) {
                        seqshare->row_num=0;
                        seqshare->gflag^=INVERTING;
                        BltBitMap(NULL, 0, 0, bitmap, 0, 0, width, 1, 0x55, 0xf, NULL);
                    }
                    else seqshare->row_num++;
                }
                while (GetMsg(timerequest[2]->tr_node.io_Message.mn_ReplyPort));
                timerequest[2]->tr_node.io_Command=TR_ADDREQUEST;
                timerequest[2]->tr_node.io_Flags=0;
                timerequest[2]->tr_time.tv_secs=0;
                timerequest[2]->tr_time.tv_micro=20000;
                SendIO((struct IORequest *)timerequest[2]); }
            if (seqshare->gflag&KEYBOARD)
                while (imsg=(struct IntuiMessage *)GetMsg(key_win->UserPort)) {
                    switch (imsg->Class) {
                    case RAWKEY:
                        if (imsg->Qualifier&IEQUALIFIER_REPEAT) {
                            ReplyMsg((struct Message *)imsg);
                            continue;
                        }
                        if (imsg->Code&IECODE_UP_PREFIX) {
                            imsg->Code^=IECODE_UP_PREFIX;
                            pass=0;
                        }
                        else pass=1;
                        switch (imsg->Code) {
                        case 76:
                            if (pass) {
                                bend_pitch(0x7f7f, seqshare->key_chan);
                                *seqshare->key_str=PITCH_BEND_CHANGE|
                                    seqshare->key_chan;
                                seqshare->key_str[1]=seqshare->key_str[2]=127;
                            }
                            else {
                                bend_pitch(0x3f3f, seqshare->key_chan);
                                *seqshare->key_str=PITCH_BEND_CHANGE|
                                    seqshare->key_chan;
                                seqshare->key_str[1]=seqshare->key_str[2]=63;
                            }
                            break;
                        case 77:
                            if (pass) {
                                bend_pitch(0x0000, seqshare->key_chan);
                                *seqshare->key_str=PITCH_BEND_CHANGE|
                                    seqshare->key_chan;
                                seqshare->key_str[1]=seqshare->key_str[2]=0;
                            }
                            else {
                                bend_pitch(0x3f3f, seqshare->key_chan);
                                *seqshare->key_str=PITCH_BEND_CHANGE|
                                    seqshare->key_chan;
                                seqshare->key_str[1]=
                                    seqshare->key_str[2]=63;
                            }
                            break;
                        case 78:
                            if (!pass)
                                if (seqshare->key_octave<4*OCTAVE)
                                    seqshare->key_octave+=OCTAVE;
                                else seqshare->key_octave=2*OCTAVE;
                            ReplyMsg((struct Message *)imsg);
                            continue;
                        case 79:
                            if (!pass)
                                if (seqshare->key_octave>2*OCTAVE)
                                    seqshare->key_octave-=OCTAVE;
                                else seqshare->key_octave=4*OCTAVE;
                            ReplyMsg((struct Message *)imsg);
                            continue;
                        default:
                            if (imsg->Code>=80 && imsg->Code<=89) {
                                if (!pass)
                                    seqshare->key_volume=1+(imsg->Code-80)*7;
                                ReplyMsg((struct Message *)imsg);
                                continue;
                            }
                            if (imsg->Code>=seqshare->n_key_elems||
                                !seqshare->key_table[imsg->Code]) {
                                ReplyMsg((struct Message *)imsg);
                                continue;
                            }
                            *seqshare->key_str=NOTE_ON|seqshare->key_chan;
                            seqshare->key_str[1]=seqshare->key_table[imsg->Code];
                            if (pass) {
                                seqshare->oct_key[seqshare->key_str[1]-A_] = seqshare->key_octave;
                                seqshare->key_str[1] += seqshare->key_octave;
                                seqshare->key_str[2] = (seqshare->key_volume << 1) - 1;
                                if (seqshare->emulation && seqshare->voice_map[seqshare->key_chan])
                                    start_note(seqshare->voice_map[seqshare->key_chan], seqshare->key_str[1], seqshare->key_volume);
                            }
                            else {
                                seqshare->key_str[1] += seqshare->oct_key[seqshare->key_str[1]-A_];
                                seqshare->key_str[2] = 0;
                                if (seqshare->emulation && seqshare->voice_map[seqshare->key_chan])
                                    clear_note(seqshare->voice_map[seqshare->key_chan], seqshare->key_str[1]);
                            }
                        }
                        n = 3;
                        break;
                    case MOUSEBUTTONS:
                        if (imsg->Qualifier&IEQUALIFIER_RBUTTON) {
                            ReplyMsg((struct Message *)imsg);
                            while (!(imsg=(struct IntuiMessage *)
                                     GetMsg(key_win->UserPort)));
                        }
                        else if (imsg->Code&IECODE_UP_PREFIX) {
                            *seqshare->key_str=NOTE_ON|seqshare->key_chan;
                            seqshare->key_str[1]=seqshare->mouse_note;
                            seqshare->key_str[2]=0;
                            if (seqshare->emulation&&
                                seqshare->voice_map[seqshare->key_chan]&&
                                seqshare->mou_note != 255)
                                clear_note(seqshare->voice_map[seqshare->key_chan], 
                                           seqshare->mou_note);
                            seqshare->mouse_note=seqshare->mou_note=255;
                            n=3;
                            break;
                        }
                        else if (n = note_mouse()) break;
                        ReplyMsg((struct Message *)imsg);
                        continue;
                    case MOUSEMOVE:
                        if (seqshare->mouse_note != 255 && (n = note_mouse())) break;
                        ReplyMsg((struct Message *)imsg);
                        continue;
                    case NEWSIZE:
                        seqshare->wh = key_win->Height - 26;
                        seqshare->ww = (key_win->Width - 36 - N_WHITES) / N_WHITES;
                        set_key_dims(seqshare);
                        SetAPen(key_win->RPort, 0);
                        RectFill(key_win->RPort, 3, KEY_TOP, 
                                 key_win->Width-17, key_win->Height-2);
                        for (n=0; n<N_KEYS; n++)
                            draw_key(FALSE, n);
                        ReplyMsg((struct Message *)imsg);
                        continue;
                    case CLOSEWINDOW:
                        ReplyMsg((struct Message *)imsg);
                        CloseWindow(key_win);
                        seqshare->gflag&=0xffff^KEYBOARD;
                        key_win=NULL;
                        break; }
                    if (!key_win) break;
                    ReplyMsg((struct Message *)imsg);
                    if (seqshare->gflag & RECORDING) {
                        timerequest[0]->tr_node.io_Command=TR_GETSYSTIME;
                        timerequest[0]->tr_node.io_Flags=IOF_QUICK;
                        DoIO((struct IORequest *)timerequest[0]);
                        SubTime(&timerequest[0]->tr_time, &timeval);
                        seqshare->cstamp=
                            (timerequest[0]->tr_time.tv_micro+500)/1000+
                            timerequest[0]->tr_time.tv_secs*1000;
                        seqshare->dstamp=seqshare->cstamp-seqshare->pstamp;
                        switch (seqshare->dstamp) {
                        default:
                            if (seqshare->rec_seq_pos+2*sizeof(ULONG)+3+n<=
                                seqshare->rec_seq_end) {
                                set_stamp();
                                status=0;
                            }
                            else {
                                complete();
                                break;
                            }
                        case 0:
                            if (seqshare->rec_seq_pos+sizeof(ULONG)+2+n<=
                                seqshare->rec_seq_end) {
                                if (status ==  *seqshare->key_str)
                                    memcpy(seqshare->rec_seq_pos, 
                                           seqshare->key_str+1, --n);
                                else memcpy(seqshare->rec_seq_pos, 
                                            seqshare->key_str, n);
                                status= *seqshare->key_str;
                                seqshare->rec_seq_pos+=n;
                                *seqshare->len_pos+=n; }
                            else complete();
                        } }
                    pos=seqshare->key_str;
                    end=pos+n;
                    if (seqshare->emulation != ONLYEMULATION && IOExtSer[0]->IOSer.io_Device) midi_out(pos, n);
                    while (pos<end && (*pos&0x80) == 0) pos++;
                    while (pos<end) {
                        command= *pos&0xf0;
                        channel= *pos&0xf;
                        pos++;
                        switch (command) {
                        case NOTE_ON:
                        case NOTE_OFF:
                            while (pos<end && (*pos&0x80) == 0) {
                                draw_key(command == NOTE_ON && pos[1], *pos);
                                if (seqshare->gflag&DISPLAYING&&
                                    (channel<seqshare->num_chans || channel == 9))
                                    show_note(channel, *pos, command == NOTE_ON && pos[1]);
                                pos+=2; }
                            continue;
                        case PROGRAM_CHANGE:
                        case CHANNEL_PRESSURE:
                            while (pos<end && (*pos&0x80) == 0) pos++;
                            continue;
                        case SYSTEM_EXCLUSIVE:
                            while (pos<end && (*pos&0x80) == 0) pos++;
                            continue;
                        default:
                            while (pos<end && (*pos&0x80) == 0) pos+=2;
                        } } }
            if (seqshare->gflag&PLAYING&&
                CheckIO((struct IORequest *)timerequest[1])) break;
#elif defined(_WINDOWS)
            seqshare->cstamp=timeGetTime()-seqshare->stime;
            if (seqshare->gflag&DISPLAYING && seqshare->cstamp-seqshare->lstamp>10) {
                seqshare->lstamp=seqshare->cstamp;
                PostMessage(seqshare->hwnd, WM_USER, 0, 0L);
            }
            if (seqshare->gflag&PLAYING && seqshare->cstamp>=seqshare->nstamp)
                break;
            return;
#elif defined(XLIB)
            if (seqshare->gflag&DISPLAYING && seqshare->cstamp-seqshare->lstamp>10) {
                seqshare->lstamp=seqshare->cstamp;
                XCopyArea(display, pixmap, window, gc, 0, 0, width, 1, 
                          0, seqshare->row_num);
                if (seqshare->row_num == height-1) {
                    seqshare->row_num=0;
                    seqshare->gflag^=INVERTING;
                    XSetFunction(display, gcp, GXinvert);
                    XFillRectangle(display, pixmap, gcp, 0, 0, width, 1);
                    XSetFunction(display, gcp, GXcopy);
                }
                else seqshare->row_num++;
            }
#endif
#if defined(__linux__) || defined(__APPLE__)
#if defined(__linux__)
            if (rawmidi_in_handle && (seqshare->gflag & RECORDING)) {
#else
            if (midiInEndpointRef && (seqshare->gflag & RECORDING)) {
#endif
                n = midi_in();
                if (n <= 0) continue;
                seqshare->dstamp = seqshare->cstamp - seqshare->pstamp;
                pos = seqshare->cmd_buf;
                end = pos + n;
                while (pos < end) {
                    if ((*pos & 0xf0)  ==  0xf0 && *pos != 0xf0) {
                        pos++;
                        continue;
                    }
                    if (seqshare->dstamp && (number == 0 || number == 2))
                        if (seqshare->rec_seq_pos + 2 * sizeof(ULONG) + 4 + 1 <=
                            seqshare->rec_seq_end) {
                            set_stamp();
                            seqshare->dstamp = 0L;
                            if (*pos&0x80) {
                                status = *pos;
                                command=(UBYTE)(status & 0xf0);
                                channel=(UBYTE)(status & 0xf);
                            }
                            *seqshare->rec_seq_pos++ = *pos;
                            *seqshare->len_pos = 1;
                        }
                        else {
                            complete();
                            break;
                        }
                    else if (seqshare->rec_seq_pos + sizeof(ULONG) + 3 + 1 <=
                             seqshare->rec_seq_end) {
                        if (*pos & 0x80) {
                            status = *pos;
                            command=(UBYTE)(status & 0xf0);
                            channel=(UBYTE)(status & 0xf);
                        }
                        *seqshare->rec_seq_pos++ = *pos;
                        (*seqshare->len_pos)++;
                    }
                    else {
                        complete();
                        break;
                    }
                    if ((*pos & 0x80) == 0 && status != 0 &&
                        (seqshare->gflag & DISPLAYING || seqshare->gflag & KEYBOARD)) {
                        switch (command) {
                        case NOTE_ON:
                        case NOTE_OFF:
                            if (++number == 2) {
                                number = 0;
                                if ((seqshare->gflag & DISPLAYING) &&
                                    (channel < seqshare->num_chans || channel == 9))
                                    show_note(channel, note, 
                                              command == NOTE_ON && *pos);
                                if ((seqshare->gflag & KEYBOARD) &&
                                    (channel == seqshare->key_chan) &&
                                    note < (UBYTE)N_KEYS)
                                    draw_key(command == NOTE_ON && *pos, note);
                            } else note = *pos;
                        }
                    }
                    pos++;
                }
            }
            break;
#endif
        }
#if defined(__linux__) || defined(__APPLE__)
        if (!(seqshare->gflag & PLAYING) || seqshare->cstamp < seqshare->nstamp) {
            int millis = (seqshare->gflag & PLAYING) ? min(10, seqshare->nstamp - seqshare->cstamp) : 10;
            usleep(millis * 1000);
            continue;
        }
#endif
        seqshare->gflag &= 0xffff ^ READY;
        msg_pos = seqshare->msg_pos;
        msg_len = seqshare->msg_len;
        if (seqshare->gflag & ECHOING) {
#ifdef AMIGA
            timerequest[0]->tr_node.io_Command=TR_GETSYSTIME;
            timerequest[0]->tr_node.io_Flags=IOF_QUICK;
            DoIO((struct IORequest *)timerequest[0]);
            SubTime(&timerequest[0]->tr_time, &timeval);
            seqshare->cstamp=
                (timerequest[0]->tr_time.tv_micro+500)/1000+
                timerequest[0]->tr_time.tv_secs*1000;
#elif defined(_WINDOWS)
            seqshare->cstamp=timeGetTime()-seqshare->stime;
#else
            get_stamp();
#endif
            seqshare->dstamp = seqshare->cstamp - seqshare->pstamp;
            switch (seqshare->dstamp) {
            default:
                if (seqshare->rec_seq_pos+2*sizeof(ULONG)+3+msg_len<=
                    seqshare->rec_seq_end) set_stamp();
                else {
                    complete();
                    break;
                }
            case 0:
                if (seqshare->rec_seq_pos+sizeof(ULONG)+2+msg_len<=
                    seqshare->rec_seq_end) {
                    memcpy(UN seqshare->rec_seq_pos, UN msg_pos, msg_len);
                    seqshare->rec_seq_pos+=msg_len;
                    *seqshare->len_pos=(UBYTE)(*seqshare->len_pos+msg_len);
                    status=0;
                }
                else complete();
            } }
#ifdef AMIGA
        if (seqshare->emulation != ONLYEMULATION && IOExtSer[0]->IOSer.io_Device) midi_out(msg_pos, msg_len);
        if (seqshare->emulation != NOEMULATION) emulate(msg_pos, msg_len);
#elif defined(__APPLE__)
        if (seqshare->emulation != ONLYEMULATION && midiOutEndpointRef) midi_out(seqshare->msg_pos, seqshare->msg_len);
        if (seqshare->emulation != NOEMULATION && audioUnit) emulate(seqshare->msg_pos, seqshare->msg_len);
#elif defined(_WINDOWS)
        if (seqshare->emulation != ONLYEMULATION && seqshare->midiout[0]) midi_out(seqshare, msg_pos, msg_len, 0);
        if (seqshare->emulation != NOEMULATION && seqshare->midiout[1]) midi_out(seqshare, msg_pos, msg_len, 1);
#elif defined(__linux__)
        if (seqshare->emulation != ONLYEMULATION && rawmidi_out_handle) midi_out(seqshare->msg_pos, seqshare->msg_len);
        if (seqshare->emulation != NOEMULATION && seq_handle) emulate(seqshare->msg_pos, seqshare->msg_len);
#endif
        if (seqshare->gflag & DISPLAYING || seqshare->gflag & KEYBOARD) {
            UBYTE MQ *end=msg_pos+msg_len;
            while (msg_pos<end && (*msg_pos&0x80) == 0) msg_pos++;
            while (msg_pos<end) {
                command=(UBYTE)(*msg_pos&0xf0);
                channel=(UBYTE)(*msg_pos&0xf);
                msg_pos++;
                switch (command) {
                case NOTE_ON:
                case NOTE_OFF:
                    while (msg_pos<end && (*msg_pos&0x80) == 0) {
                        if ((seqshare->gflag & DISPLAYING) &&
                            (channel<seqshare->num_chans || channel == 9))
#ifdef _WINDOWS
                            show_note(seqshare, channel, *msg_pos, 
                                      command == NOTE_ON && msg_pos[1]);
#else
                        show_note(channel, *msg_pos, 
                                  command == NOTE_ON && msg_pos[1]);
#endif
                        if ((seqshare->gflag & KEYBOARD) &&
                            (channel == seqshare->key_chan)&&
                            *msg_pos<(UBYTE)N_KEYS)
#ifdef _WINDOWS
                            PostMessage(seqshare->key_wnd, WM_USER, 
                                        ((WORD)(command == NOTE_ON && msg_pos[1])<<7)|
                                        ((WORD)(*msg_pos)), 0L);
#else
                        draw_key(command == NOTE_ON && msg_pos[1], *msg_pos);
#endif
                        msg_pos+=2; }
                    continue;
                case PROGRAM_CHANGE:
                case CHANNEL_PRESSURE:
                    while (msg_pos<end && (*msg_pos&0x80) == 0) msg_pos++;
                    continue;
                case SYSTEM_EXCLUSIVE:
                    while (msg_pos<end && (*msg_pos&0x80) == 0) msg_pos++;
                    continue;
                default:
                    while (msg_pos<end && (*msg_pos&0x80) == 0) msg_pos+=2;
                } }
        }
#ifdef AMIGA
        while (GetMsg(timerequest[1]->tr_node.io_Message.mn_ReplyPort));
#elif defined(_WINDOWS)
        return;
#endif
    }
#if defined(__linux__) || defined(__APPLE__)
    pthread_exit(NULL);
#endif
}

int init_seq(void)
{
    ENTRY *entry;
    /*
      #ifdef __linux__
      if ((shmid = shmget(ftok(".", 'S'), sizeof(SEQSHARE), IPC_CREAT|0666)) == -1||
      (seqshare=(SEQSHARE *)shmat(shmid, 0, 0)) == (SEQSHARE *)-1) return -1;
      memset(seqshare, 0, sizeof(SEQSHARE));
      #else
    */
    if (!(seqshare=(SEQSHARE *)memcalloc(sizeof(SEQSHARE)))) return -1;
    /*
      #endif
    */
    if (init_seq2() || (entry=enter("Frequences", system_nomen)) == NULL) return -1;
    entry->flag=BOUND|DETERMINED;
    entry->element.flag=INDEXED;
    entry->element.type=SINGLEARRAY_TYPE;
    entry->element.unit.singlearray=seqshare->note_freq;
    entry->element.unit.line.length=128;
    seqshare->wh = WH;
    seqshare->ww = WW;
    set_key_dims(seqshare);
    seqshare->key_octave = 3 * OCTAVE;
    seqshare->key_table=key_table;
    seqshare->n_key_elems=ELEMS(key_table);
    seqshare->num_chans=(width>>7)-1;
    seqshare->drum_pos=seqshare->num_chans<<7;
    seqshare->disp_width=(seqshare->num_chans+1)<<7;
    seqshare->disp_pos=(width-seqshare->disp_width)>>1;
#ifdef AMIGA
    {
        UBYTE i, j;
        UBYTE chan_comb[16];
        if (!(tmrMsgPort=(struct MsgPort *)CreatePort(NULL, 0))) return 500;
        if (!(timerequest[0]=(struct timerequest *)CreateExtIO(tmrMsgPort, 
                                                               sizeof(struct timerequest)))) return 501;
        if (OpenDevice(TIMERNAME, UNIT_MICROHZ, 
                       (struct IORequest *)timerequest[0], 0)) return 502;
        TimerBase=(struct Library *)timerequest[0]->tr_node.io_Device;
        for (i = 1; i < 3; i++) {
            if (!(timerequest[i]=(struct timerequest *)CreateExtIO(tmrMsgPort, 
                                                                   sizeof(struct timerequest)))) return 503;
            *timerequest[i]=*timerequest[0]; }
        if (!(serMsgPort=(struct MsgPort *)CreatePort(NULL, 0)))
            return 504;
        if (!(IOExtSer[0]=(struct IOExtSer *)CreateExtIO(serMsgPort, 
                                                         sizeof(struct IOExtSer)))) return 505;
        if (!OpenDevice(SERIALNAME, 0, (struct IORequest *)IOExtSer[0], 0)) {
            IOExtSer[0]->io_Baud=31250;
            IOExtSer[0]->io_ReadLen=8;
            IOExtSer[0]->io_WriteLen=8;
            IOExtSer[0]->io_StopBits=1;
            IOExtSer[0]->io_SerFlags=SERF_XDISABLED|SERF_RAD_BOOGIE;
            IOExtSer[0]->IOSer.io_Command=SDCMD_SETPARAMS;
            IOExtSer[0]->IOSer.io_Flags=IOF_QUICK;
            if (DoIO((struct IORequest *)IOExtSer[0])) return 506;
            WaitIO((struct IORequest *)IOExtSer[0]);
            for (i=1; i<3; i++) {
                if (!(IOExtSer[i]=(struct IOExtSer *)CreateExtIO(serMsgPort, 
                                                                 sizeof(struct IOExtSer)))) return 507;
                *IOExtSer[i]= *IOExtSer[0]; }
        }
        if (!(audMsgPort=(struct MsgPort *)CreatePort(NULL, 0))) return 508;
        if (!(IOAudio[0][0]=(struct IOAudio *)CreateExtIO(audMsgPort, 
                                                          sizeof(struct IOAudio)))) return 509;
        for (i=0; i<16; i++) chan_comb[i]=15-i;
        IOAudio[0][0]->ioa_Data=chan_comb;
        IOAudio[0][0]->ioa_Length=sizeof chan_comb;
        IOAudio[0][0]->ioa_Request.io_Flags=IOF_QUICK|ADIOF_NOWAIT;
        if (OpenDevice(AUDIONAME, 0, (struct IORequest *)IOAudio[0][0], 0))
            return 510;
        if (IOAudio[0][0]->ioa_Request.io_Error) return 511;
        for (i=0; i<4; i++)
            for (j=0; j<3; j++)
                if (i || j) {
                    if (!(IOAudio[i][j]=(struct IOAudio *)CreateExtIO(
                                                                      audMsgPort, sizeof(struct IOAudio)))) return 512;
                    *IOAudio[i][j]=*IOAudio[0][0];
                }
        if (!(TranslatorBase=OpenLibrary("translator.library", 0))) return 513;
        if (!(narMsgPort=(struct MsgPort *)CreatePort(NULL, 0))) return 514;
        if (!(narrator_rb=(struct narrator_rb *)CreateExtIO(
                                                            narMsgPort, sizeof(struct narrator_rb)))) return 515;
        narrator_rb->ch_masks=chan_comb;
        narrator_rb->nm_masks=sizeof chan_comb;
        if (OpenDevice("narrator.device", 0, (struct IORequest *)narrator_rb, 0))
            return 516;
        if (narrator_rb->message.io_Error) return 517;
        narrator_rb->message.io_Command=CMD_WRITE;
        thatTask=FindTask(NULL);
        if (GfxBase->DisplayFlags&PAL) clock_value=3546895;
        else clock_value=3579545;
        seqshare->key_volume=64;
    }
#elif defined(__APPLE__)
    {
        OSStatus result;
        AUNode synthNode, limiterNode, outNode;
        AudioComponentDescription audioComponentDescription;
		MIDIDeviceRef device;
        CFStringRef deviceNameRef, manufacturerNameRef, modelNameRef, sourceNameRef, destinationNameRef;
        int channel, numberOfDevices, deviceNumber, numberOfSources, sourceNumber, numberOfDestinations, destinationNumber;
        const char *deviceName, *manufacturerName, *modelName, *sourceName, *destinationName;
        result = MIDIClientCreate(CFSTR("Wake"), NULL, NULL, &midiClientRef);
        if (result != noErr) return 500;
        result = MIDIInputPortCreate(midiClientRef, CFSTR("Wake input port"), midiInputReadProc, NULL, &midiInPortRef);
        if (result != noErr) return 501;
        result = MIDIOutputPortCreate(midiClientRef, CFSTR("Wake output port"), &midiOutPortRef);
        if (result != noErr) return 502;
        numberOfDevices = MIDIGetNumberOfDevices();
        for (deviceNumber = 0; deviceNumber < numberOfDevices; deviceNumber++) {
            device = MIDIGetDevice(deviceNumber);
            MIDIObjectGetStringProperty(device, kMIDIPropertyName, &deviceNameRef);
            MIDIObjectGetStringProperty(device, kMIDIPropertyManufacturer, &manufacturerNameRef);
            MIDIObjectGetStringProperty(device, kMIDIPropertyModel, &modelNameRef);
            deviceName = CFStringGetCStringPtr(deviceNameRef, 0);
            manufacturerName = CFStringGetCStringPtr(manufacturerNameRef, 0);
            modelName = CFStringGetCStringPtr(modelNameRef, 0);
            printf("MIDI device=%s, manufacturer=%s, model=%s\n", deviceName, manufacturerName, modelName);
            CFRelease(deviceNameRef);
            CFRelease(manufacturerNameRef);
            CFRelease(modelNameRef);
        }
        numberOfSources = MIDIGetNumberOfSources();
        for (sourceNumber = 0; sourceNumber < numberOfSources; sourceNumber++) {
            midiInEndpointRef = MIDIGetSource(sourceNumber);
            MIDIObjectGetStringProperty(midiInEndpointRef, kMIDIPropertyName, &sourceNameRef);
            sourceName = CFStringGetCStringPtr(sourceNameRef, 0);
            if (sourceName) {
                if (!strcmp(sourceName, midioutdev[0])) {
                    CFRelease(sourceNameRef);
                    MIDIPortConnectSource(midiInPortRef, midiInEndpointRef, NULL);
                    break;
                }
            }
            MIDIEndpointDispose(midiInEndpointRef);
        }
        numberOfDestinations = MIDIGetNumberOfDestinations();
        for (destinationNumber = 0; destinationNumber < numberOfDestinations; destinationNumber++) {
            midiOutEndpointRef = MIDIGetDestination(destinationNumber);
            MIDIObjectGetStringProperty(midiOutEndpointRef, kMIDIPropertyName, &destinationNameRef);
            destinationName = CFStringGetCStringPtr(destinationNameRef, 0);
            if (destinationName) {
                if (!strcmp(destinationName, midioutdev[0])) {
                    CFRelease(destinationNameRef);
                    break;
                }
            }
            CFRelease(destinationNameRef);
            MIDIEndpointDispose(midiOutEndpointRef);
        }
        memset(&audioComponentDescription, 0, sizeof audioComponentDescription);
        result = NewAUGraph(&auGraph);
        if (result != noErr) return 503;
        audioComponentDescription.componentManufacturer = kAudioUnitManufacturer_Apple;
        audioComponentDescription.componentFlags = 0;
        audioComponentDescription.componentFlagsMask = 0;
        audioComponentDescription.componentType = kAudioUnitType_MusicDevice;
        audioComponentDescription.componentSubType = kAudioUnitSubType_DLSSynth;
        result = AUGraphAddNode(auGraph, &audioComponentDescription, &synthNode);
        if (result != noErr) return 504;
        audioComponentDescription.componentType = kAudioUnitType_Effect;
        audioComponentDescription.componentSubType = kAudioUnitSubType_PeakLimiter;  
        result = AUGraphAddNode(auGraph, &audioComponentDescription, &limiterNode);
        if (result != noErr) return 505;
        audioComponentDescription.componentType = kAudioUnitType_Output;
        audioComponentDescription.componentSubType = kAudioUnitSubType_DefaultOutput;  
        result = AUGraphAddNode(auGraph, &audioComponentDescription, &outNode);
        if (result != noErr) return 506;
        result = AUGraphOpen(auGraph);
        if (result != noErr) return 507;
        result = AUGraphConnectNodeInput(auGraph, synthNode, 0, limiterNode, 0);
        if (result != noErr) return 508;
        result = AUGraphConnectNodeInput(auGraph, limiterNode, 0, outNode, 0);
        if (result != noErr) return 509;
        result = AUGraphNodeInfo(auGraph, synthNode, 0, &audioUnit);
        if (result != noErr) return 510;
        result = AUGraphInitialize(auGraph);
        if (result != noErr) return 511;
        for (channel = 0; channel <= 15; channel++) {
            result = MusicDeviceMIDIEvent(audioUnit,
                                          CONTROL_CHANGE | channel,
                                          0, 0, channel);
            if (result != noErr) return 512;
            result = MusicDeviceMIDIEvent(audioUnit,
                                          PROGRAM_CHANGE | channel,
                                          channel, 0, 0);
            if (result != noErr) return 513;
        }
        result = AUGraphStart(auGraph);
        if (result != noErr) return 514;
        seqshare->key_volume=127;
    }
#elif defined(__linux__)
    {
        snd_rawmidi_params_t *rawmidiParams;
        int err;
        if (!midiindev) midiindev = "hw:0, 0";
        err = snd_rawmidi_open(&rawmidi_in_handle, NULL, midiindev, 0);
        if (!midioutdev[0]) midioutdev[0] = "hw:0, 0";
        err = snd_rawmidi_open(NULL, &rawmidi_out_handle, midioutdev[0], SND_RAWMIDI_SYNC);
        if (snd_rawmidi_params_malloc(&rawmidiParams)) return 500;
        if (rawmidi_in_handle && snd_rawmidi_params_set_buffer_size(rawmidi_in_handle, rawmidiParams, CMD_BUF_SIZE)) return 501;
        if (rawmidi_out_handle && snd_rawmidi_params_set_buffer_size(rawmidi_out_handle, rawmidiParams, CMD_BUF_SIZE)) return 502;
        if (snd_seq_open(&seq_handle, "hw", SND_SEQ_OPEN_DUPLEX, 0) < 0) return 503;
        if (snd_seq_set_client_name(seq_handle, "Wake") < 0) return 504;
        if ((port_out_id = snd_seq_create_simple_port(seq_handle, "Wake out", SND_SEQ_PORT_CAP_READ | SND_SEQ_PORT_CAP_SUBS_READ, SND_SEQ_PORT_TYPE_MIDI_GENERIC)) < 0) return 505;
        if ((port_in_id = snd_seq_create_simple_port(seq_handle, "Wake in", SND_SEQ_PORT_CAP_WRITE | SND_SEQ_PORT_CAP_SUBS_WRITE, SND_SEQ_PORT_TYPE_MIDI_GENERIC)) < 0) return 506;
        if ((client_id = snd_seq_client_id(seq_handle)) < 0) return 507;
        if (snd_seq_connect_to(seq_handle, port_out_id, 128, 0) < 0) return 508;
        seqshare->key_volume=127;
    }
#elif defined(_WINDOWS)
    {
        UBYTE i, j;
        WAVEOUTCAPS woc;
        seqshare->hwnd=hwnd;
        seqshare->key_name=key_name;
        seqshare->instance=instance;
        seqshare->width=width;
        seqshare->height=height;
        if (!(seqshare->red_pen=CreatePen(PS_SOLID, 1, RGB(255, 0, 0)))) {
            report(message(GetLastError()));
            return 500;
        }
        if (!(seqshare->red_brush=CreateSolidBrush(RGB(255, 0, 0)))) {
            report(message(GetLastError()));
            return 501;
        }
        seqshare->colors[0]=RGB(255, 255, 255);
        seqshare->colors[1]=RGB(0, 0, 0);
        seqshare->colors[2]=RGB(255, 0, 0);
        seqshare->brushes[0]=GetStockObject(WHITE_BRUSH);
        seqshare->brushes[1]=GetStockObject(BLACK_BRUSH);
        seqshare->brushes[2]=seqshare->red_brush;
        seqshare->pens[0]=GetStockObject(WHITE_PEN);
        seqshare->pens[1]=GetStockObject(BLACK_PEN);
        seqshare->pens[2]=seqshare->red_pen;
        if (midiindev != -1 && midiInGetNumDevs()>midiindev)
            if (!midiInOpen(&midiin, midiindev, (DWORD)hwnd, 0, CALLBACK_WINDOW))
                if (!(midiinhdr=(MIDIHDR *)memcalloc(sizeof(MIDIHDR)))) {
                    report(message(GetLastError()));
                    return 502;
                }
        for (i=0; i<2; i++)
            if (midioutdev[i] != -1 && midiOutGetNumDevs()>midioutdev[i]) {
                seqshare->events[i] = CreateEvent(NULL, TRUE, FALSE, NULL);
                if (!midiOutOpen(&seqshare->midiout[i], midioutdev[i], (DWORD)seqshare->events[i], 0, CALLBACK_EVENT))
                    for (j=0; j<2; j++)
                        if (!(seqshare->midihdr[i][j]=(MIDIHDR *)memcalloc(sizeof(MIDIHDR)))) {
                            report(message(GetLastError()));
                            return 503;
                        }
            }
        if (waveOutGetNumDevs() && !waveOutGetDevCaps(0, &woc, sizeof woc)) {
            waveformat.wFormatTag=WAVE_FORMAT_PCM;
            if (woc.dwFormats&WAVE_FORMAT_4S16) {
                waveformat.nChannels=2;
                waveformat.nSamplesPerSec=44100L;
                waveformat.nBlockAlign=4;
                waveformat.wBitsPerSample=16;
                sample_type=INTARRAY_TYPE;
            }
            else if (woc.dwFormats&WAVE_FORMAT_4M16) {
                waveformat.nChannels=1;
                waveformat.nSamplesPerSec=44100L;
                waveformat.nBlockAlign=2;
                waveformat.wBitsPerSample=16;
                sample_type=SHORTARRAY_TYPE;
            }
            else if (woc.dwFormats&WAVE_FORMAT_4S08) {
                waveformat.nChannels=2;
                waveformat.nSamplesPerSec=44100L;
                waveformat.nBlockAlign=2;
                waveformat.wBitsPerSample=8;
                sample_type=SHORTARRAY_TYPE;
            }
            else if (woc.dwFormats&WAVE_FORMAT_4M08) {
                waveformat.nChannels=1;
                waveformat.nSamplesPerSec=44100L;
                waveformat.nBlockAlign=1;
                waveformat.wBitsPerSample=8;
                sample_type=STRING_TYPE;
            }
            else if (woc.dwFormats&WAVE_FORMAT_2S16) {
                waveformat.nChannels=2;
                waveformat.nSamplesPerSec=22050L;
                waveformat.nBlockAlign=4;
                waveformat.wBitsPerSample=16;
                sample_type=INTARRAY_TYPE;
            }
            else if (woc.dwFormats&WAVE_FORMAT_2M16) {
                waveformat.nChannels=1;
                waveformat.nSamplesPerSec=22050L;
                waveformat.nBlockAlign=2;
                waveformat.wBitsPerSample=16;
                sample_type=SHORTARRAY_TYPE;
            }
            else if (woc.dwFormats&WAVE_FORMAT_2S08) {
                waveformat.nChannels=2;
                waveformat.nSamplesPerSec=22050;
                waveformat.nBlockAlign=2;
                waveformat.wBitsPerSample=8;
                sample_type=SHORTARRAY_TYPE;
            }
            else if (woc.dwFormats&WAVE_FORMAT_2M08) {
                waveformat.nChannels=1;
                waveformat.nSamplesPerSec=22050L;
                waveformat.nBlockAlign=1;
                waveformat.wBitsPerSample=8;
                sample_type=STRING_TYPE;
            }
            else if (woc.dwFormats&WAVE_FORMAT_1S16) {
                waveformat.nChannels=2;
                waveformat.nSamplesPerSec=11025L;
                waveformat.nBlockAlign=4;
                waveformat.wBitsPerSample=16;
                sample_type=INTARRAY_TYPE;
            }
            else if (woc.dwFormats&WAVE_FORMAT_1M16) {
                waveformat.nChannels=1;
                waveformat.nSamplesPerSec=11025L;
                waveformat.nBlockAlign=2;
                waveformat.wBitsPerSample=16;
                sample_type=SHORTARRAY_TYPE;
            }
            else if (woc.dwFormats&WAVE_FORMAT_1S08) {
                waveformat.nChannels=2;
                waveformat.nSamplesPerSec=11025L;
                waveformat.nBlockAlign=2;
                waveformat.wBitsPerSample=8;
                sample_type=SHORTARRAY_TYPE;
            }
            else if (woc.dwFormats&WAVE_FORMAT_1M08) {
                waveformat.nChannels=1;
                waveformat.nSamplesPerSec=11025L;
                waveformat.nBlockAlign=1;
                waveformat.wBitsPerSample=8;
                sample_type=STRING_TYPE;
            }
            waveformat.nAvgBytesPerSec=waveformat.nSamplesPerSec*waveformat.nBlockAlign;
            if (waveformat.nChannels&&
                !waveOutOpen(&seqshare->waveout, WAVE_MAPPER, &waveformat, 0, 0, 0))
                if (!(seqshare->wavehdr=(WAVEHDR *)memcalloc(sizeof(WAVEHDR)))) {
                    report(message(GetLastError()));
                    return 504;
                }
        }
        seqshare->key_volume=127;
    }
#endif
#ifdef XLIB
    if (!(oflag&TEXTONLY)) {
        pennum[0]=WhitePixel(display, screen);
        pennum[1]=BlackPixel(display, screen);
        pennum[2]=alloc_color(0xffff, 0, 0);
        pennum[3]=alloc_color(0x7777, 0x7777, 0x7777);
    }
#endif
    wk_reset();
    return 0;
}
