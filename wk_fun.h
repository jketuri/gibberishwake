
/* wk_fun.h */

#ifdef MAIN
#define VARIABLE
#else
#define VARIABLE extern
#endif

#ifdef VAXC
#include <stat.h>
#elif defined(_MSC_VER)
#include <sys/stat.h>
#include <sys/timeb.h>
#include <sys/types.h>
#include <direct.h>
#define access _access
#define chdir _chdir
#define stat _stat
#elif defined(__TURBOC__)
#include <sys/stat.h>
#include <dir.h>
#else
#define __USE_XOPEN_EXTENDED
#include <unistd.h>
#include <sys/stat.h>
#endif
#ifndef _MSC_VER
#include <sys/time.h>
#endif
#include <time.h>
#ifdef AMIGA
#ifdef _DCC
#include <fcntl.h>
#endif
#include <exec/memory.h>
#include <devices/audio.h>
#include <dos/dostags.h>
#include <proto/dos.h>
#include <proto/exec.h>
#include <proto/intuition.h>
#include <clib/alib_protos.h>
#elif defined(__APPLE__) || defined(__linux__)
#include <dirent.h>
#include <unistd.h>
#elif defined(_MSDOS)
#include <dos.h>
#include <io.h>
#elif defined(_WINDOWS)
#include <io.h>
#endif
#include "wake_msg.h"
#ifdef VAXC
#include <signal.h>
#include <unixio.h>
#include <rms.h>
#else
#include "file_acs.h"
#endif

#ifdef LATTICE
#define size_t int
#endif
#define BASE_KEY_SIZE (sizeof(long)+30)
#define HEX_CHR(bt) (bt<10?bt+'0':bt+'7')
#define HEX_VAL(ch) ((ch>='0'&&ch<='9')?ch-'0':toupper(ch)-'7')

extern int init_fun2(void);
extern int init_gra(void);
extern int init_pic(void);
extern int init_log(void);
extern int init_mat(void);
extern int init_seq(void);
extern void shut_gra(void);
extern void shut_seq(void);
extern void wk_close(void);
extern void free_image(void *image);
extern void free_state(STATE *state);
#ifndef VAXC
extern void free_voice(void *voice);
extern void free_wave(void *wave);
#endif

extern void wk_quit(void);
extern void wk_stop(void);

#ifdef AMIGA
VARIABLE struct MsgPort *wakePort;
VARIABLE struct GfxBase *GfxBase;
VARIABLE struct IntuitionBase *IntuitionBase;
VARIABLE struct Library *DiskfontBase;
VARIABLE struct Library *LayersBase;
VARIABLE struct Library *MathBase;
VARIABLE struct Library *MathTransBase;
VARIABLE struct Library *MathIeeeDoubBasBase;
VARIABLE struct Library *MathIeeeDoubTransBase;
#elif defined(_WINDOWS)
extern HWND hwnd;
#endif
#if defined(_MSC_VER)
VARIABLE struct timeb rtimeb;
#elif defined(LATTICE)
VARIABLE time_t rtime;
#else
VARIABLE struct timeval rtimeval;
#endif
VARIABLE unsigned nomen_stack_size;
VARIABLE NOMEN *system_nomen;
#ifdef VAXC
VARIABLE struct FAB fab;
VARIABLE struct RAB rab;
VARIABLE struct XABKEY xabkey;
VARIABLE char base_open;
#else
VARIABLE INDEX_FILE base_index;
#endif
