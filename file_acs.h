
#ifndef STDIO_H
#include <stdio.h>
#endif

#ifdef __MSDOS__
#define _MSDOS
#endif

#define MAX_KEY_LEN 34
#define PAGE_SIZE 24
#define ORDER 12
#define PAGE_STACK_SIZE 10
#define MAX_HEIGHT 4
#define FIL_STR_LEN 64

#ifndef __GNUC__
#define SEEK_SET 0
#define SEEK_CUR 1
#define SEEK_END 2
#endif

typedef char FIL_STR[FIL_STR_LEN+1];
typedef struct {
	FILE *f;
	int rec_len;
	char changed;
	char pass_str[9];
	long first_free,number_free,num,num_rec;
	FIL_STR fa_name;
} DATA_FILE;
typedef char KEY_STR[MAX_KEY_LEN+1];
typedef struct {
	long data_ref,page_ref;
	KEY_STR key;
} FA_ITEM;
typedef struct {
	short items_on_page;
	long bckw_page_ref;
	FA_ITEM item_array[PAGE_SIZE];
} FA_PAGE;
typedef FA_PAGE *FA_PAGE_PTR;
typedef struct {
	long page_ref;
	short item_arr_index;
} FA_SEARCH_STEP;
typedef FA_SEARCH_STEP FA_PATH[MAX_HEIGHT];
typedef struct {
	DATA_FILE data_f;
	int allow_dupl_keys,key_l,pp;
	long rr;
	FA_PATH path;
} INDEX_FILE;
typedef INDEX_FILE *INDEX_FILE_PTR;
typedef struct {
	FA_PAGE page;
	INDEX_FILE_PTR index_f_ptr;
	long page_ref;
	short updated;
} FA_STACK_REC;
typedef FA_STACK_REC *FA_STACK_REC_PTR;
typedef FA_STACK_REC FA_PAGE_STACK[PAGE_STACK_SIZE];
typedef short FA_PAGE_MAP[PAGE_STACK_SIZE];
typedef enum {
	dt_byte,dt_sbyte,dt_uchar,dt_char,dt_ushort,dt_short,dt_ushort_code,
	dt_ulong,dt_long,dt_date,dt_time,dt_string,dt_ref,dt_float,dt_double,
	dt_tval,dt_stamp
} DATA_TYPE;
typedef struct {
	DATA_TYPE dty;
	void *ptr;
	int len;
} DATA_DEF;
typedef struct {
	short year_since_0;
	char month_of_year;
	char day_of_month;
	char hour_of_day;
	char minute_of_hour;
	char second_of_minute;
	char hundreths_of_second;
} STAMP;

extern FA_PAGE_STACK fa_page_stk;
extern FA_PAGE_MAP fa_pg_map;

extern long fa_filelen(FILE *fp);
extern void fa_seekrec(DATA_FILE *,long);
extern void fa_pack(FA_PAGE *,int);
extern void fa_unpack(FA_PAGE *,int);
extern void fa_last(int);
extern void fa_get_page(INDEX_FILE *,long,FA_PAGE_PTR *);
extern void fa_new_page(INDEX_FILE *,long *,FA_PAGE_PTR *);
extern void fa_update_page(FA_PAGE_PTR);
extern void fa_return_page(FA_PAGE_PTR *);
extern long fa_comp_keys(char *,char *,long,long,int,int);
extern int fa_insert(INDEX_FILE *,long,long *,
FA_PAGE_PTR *,FA_PAGE_PTR *,FA_ITEM *,FA_ITEM *,char *,long *,int *);
extern int fa_find_key(INDEX_FILE *,long *,char *);
extern void fa_underflow(INDEX_FILE *,long,long,int,int *);
extern void fa_del_a(INDEX_FILE *,long,long,FA_PAGE_PTR *,int *,int *);
extern int fa_del_b(INDEX_FILE *,long,long *,int *,char *);

extern char toupp(char);
extern char *upcase(char *);
extern char *zwab(char *,char *,int);
extern void get_rec(DATA_FILE *,long,void *);
extern void put_rec(DATA_FILE *,long,void *);
extern void make_file(DATA_FILE *,char *,int);
extern int open_file(DATA_FILE *,char *,int);
extern void update_file(DATA_FILE *);
extern void close_file(DATA_FILE *);
extern void found_file(DATA_FILE *);
extern void set_pass_str(DATA_FILE *,char *);
extern int test_pass_str(DATA_FILE *,char *);
extern void new_rec(DATA_FILE *,long *);
extern void add_rec(DATA_FILE *,long *,void *);
extern void delete_rec(DATA_FILE *,long);
extern long file_len(DATA_FILE *);
extern long used_recs(DATA_FILE *);
extern void init_index(void);
extern void make_index(INDEX_FILE *,char *,int,int);
extern int open_index(INDEX_FILE *,char *,int,int);
extern void update_index(INDEX_FILE *);
extern void close_index(INDEX_FILE *);
extern void found_index(INDEX_FILE *);
extern void clear_key(INDEX_FILE *);
extern int add_key(INDEX_FILE *,long *,char *);
extern int next_key(INDEX_FILE *,long *,char *);
extern int prev_key(INDEX_FILE *,long *,char *);
extern int find_key(INDEX_FILE *,long *,char *);
extern int search_key(INDEX_FILE *,long *,char *);
extern int search_the_key(INDEX_FILE *,long *,char *,int);
extern int get_point(INDEX_FILE *,long *,char *);
extern int first_key(INDEX_FILE *,long *,char *);
extern int last_key(INDEX_FILE *,long *,char *);
extern int set_point_to_end(INDEX_FILE *);
extern int set_point_to_beginning(INDEX_FILE *);
extern int delete_key(INDEX_FILE *,long *,char *);
extern int index_key(char *,char *,DATA_DEF *,long);
extern char *build_key(char *,DATA_DEF *,int,long);
extern void recover_index(INDEX_FILE *,char *,int,int,
void *,DATA_FILE *,DATA_DEF *,int);
extern void open_file_index(char *,DATA_FILE *,int,INDEX_FILE *,int,int,
void *,DATA_DEF *,int);
extern int reduce_file(DATA_FILE *,char *,int,void *);
extern int merge_file(DATA_FILE *,char *,int,void *);
extern void rescue_file(DATA_FILE *);

extern void access_error(char *);

