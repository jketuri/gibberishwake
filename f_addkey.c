
/* File Access routines */
/* Add Key module */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "file_acs.h"

extern FA_PAGE_STACK fa_page_stk;
extern FA_PAGE_MAP fa_pg_map;

int fa_insert(INDEX_FILE *idx_f,long pr_pg_ref1,long *pr_pg_ref2,
FA_PAGE_PTR *page_ptr1,FA_PAGE_PTR *page_ptr2,FA_ITEM *proc_item1,
FA_ITEM *proc_item2,char *proc_key,long *proc_dat_ref,int *pass_up)
{
	long c;
	int i,k,l,r;
	if (pr_pg_ref1==0)
	{
		memcpy(proc_item1->key,proc_key,idx_f->key_l);
		proc_item1->data_ref= *proc_dat_ref;
		proc_item1->page_ref=0;
		*pass_up=1;
		return 0;
	}
	fa_get_page(idx_f,pr_pg_ref1,page_ptr1);
	l=0;
	r=(*page_ptr1)->items_on_page-1;
	do
	{
		k=(l+r)/2;
		c=fa_comp_keys(proc_key,
		(*page_ptr1)->item_array[k].key,
		*proc_dat_ref,(*page_ptr1)->item_array[k].data_ref,
		idx_f->allow_dupl_keys,idx_f->key_l);
		if (c<=0) r=k-1;
		if (c>=0) l=k+1;
	}
	while (r>=l);
	if (l-r>1)
	{
		*pass_up=0;
		return 1;
	}
	if (r== -1)
	{
		if (fa_insert(idx_f,
		(*page_ptr1)->bckw_page_ref,pr_pg_ref2,
		page_ptr1,page_ptr2,proc_item1,proc_item2,
		proc_key,proc_dat_ref,pass_up)) return 1;
	}
	else if (fa_insert(idx_f,
	(*page_ptr1)->item_array[r].page_ref,pr_pg_ref2,
	page_ptr1,page_ptr2,proc_item1,proc_item2,
	proc_key,proc_dat_ref,pass_up)) return 1;
	if (!*pass_up) return 0;
	fa_get_page(idx_f,pr_pg_ref1,page_ptr1);
	if ((*page_ptr1)->items_on_page<PAGE_SIZE)
	{
		(*page_ptr1)->items_on_page++;
		for (i=(*page_ptr1)->items_on_page-1; i>=r+2; i--)
		(*page_ptr1)->item_array[i]=(*page_ptr1)->item_array[i-1];
		(*page_ptr1)->item_array[r+1]= *proc_item1;
		fa_update_page(*page_ptr1);
		*pass_up=0;
		return 0;
	}
	fa_new_page(idx_f,pr_pg_ref2,page_ptr2);
	if (r<ORDER)
	{
		if (r==ORDER-1) *proc_item2= *proc_item1;
		else
		{
			*proc_item2=
			(*page_ptr1)->item_array[ORDER-1];
			for (i=ORDER-1; i>=r+2; i--)
			(*page_ptr1)->item_array[i]=
			(*page_ptr1)->item_array[i-1];
			(*page_ptr1)->item_array[r+1]= *proc_item1;
		}
		for (i=0; i<ORDER; i++) (*page_ptr2)->item_array[i]=
		(*page_ptr1)->item_array[i+ORDER];
	}
	else
	{
		r-=ORDER;
		*proc_item2=(*page_ptr1)->item_array[ORDER];
		for (i=0; i<r; i++) (*page_ptr2)->item_array[i]=
		(*page_ptr1)->item_array[i+ORDER+1];
		(*page_ptr2)->item_array[r]= *proc_item1;
		for (i=r+1; i<ORDER; i++)
		(*page_ptr2)->item_array[i]=
		(*page_ptr1)->item_array[i+ORDER];
	}
	(*page_ptr1)->items_on_page=ORDER;
	(*page_ptr2)->items_on_page=ORDER;
	(*page_ptr2)->bckw_page_ref=proc_item2->page_ref;
	proc_item2->page_ref= *pr_pg_ref2;
	*proc_item1= *proc_item2;
	fa_update_page(*page_ptr2);
	fa_update_page(*page_ptr1);
	return 0;
}

int add_key(INDEX_FILE *idx_f,long *proc_dat_ref,char *proc_key)
{
	int pass_up;
	long pr_pg_ref1,pr_pg_ref2;
	FA_PAGE_PTR page_ptr1,page_ptr2;
	FA_ITEM proc_item1,proc_item2;
	if (fa_insert(idx_f,idx_f->rr,&pr_pg_ref2,&page_ptr1,&page_ptr2,
	&proc_item1,&proc_item2,proc_key,proc_dat_ref,&pass_up))
	{
		idx_f->pp= -1;
		return 1;
	}
	if (pass_up)
	{
		pr_pg_ref1=idx_f->rr;
		fa_new_page(idx_f,&idx_f->rr,&page_ptr1);
		page_ptr1->items_on_page=1;
		page_ptr1->bckw_page_ref=pr_pg_ref1;
		page_ptr1->item_array[0]=proc_item1;
		fa_update_page(page_ptr1);
	}
	idx_f->pp= -1;
	return 0;
}

