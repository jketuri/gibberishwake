
/* File Access routines */
/* Access module */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "file_acs.h"

FA_PAGE_STACK fa_page_stk;
FA_PAGE_MAP fa_pg_map;

char toupp(char c)
{
#ifdef AMIGA
	static char *s0="\344\345\366";
	static char *s1="\304\305\326";
#elif defined(VAXC)
	static char *s0="\173\175\174";
	static char *s1="\133\135\134";
#else
	static char *s0="\204\206\224";
	static char *s1="\216\217\231";
#endif
	char *p=strchr(s0,c);
	if (p) c=s1[p-s0];
	else c=(char)toupper(c);
	return c;
}

char *upcase(char *s)
{
	char *p=s;
	while (*p)
	{
		*p=toupp(*p);
		p++;
	}
	return s;
}

char *zwab(char *from,char *to,int n)
{
	int i;
	for (i=0; i<n; i++) to[n-i-1]=from[i];
	return to;
}

long fa_filelen(FILE *fp)
{
	fseek(fp,0,SEEK_END);
	return ftell(fp);
}

void fa_seekrec(DATA_FILE *dat_f,long r)
{
	if (fseek(dat_f->f,
	sizeof(char[9])+sizeof(long[3])+(r-1)*(long)dat_f->rec_len,SEEK_SET))
	access_error(dat_f->fa_name);
}

void get_rec(DATA_FILE *dat_f,long r,void *buffer)
{
	fa_seekrec(dat_f,r);
	if (fread(buffer,dat_f->rec_len,1,dat_f->f)!=1)
	access_error(dat_f->fa_name);
}

void put_rec(DATA_FILE *dat_f,long r,void *buffer)
{
	fa_seekrec(dat_f,r);
	if (fwrite(buffer,dat_f->rec_len,1,dat_f->f)!=1)
	access_error(dat_f->fa_name);
	dat_f->changed= -1;
}

void make_file(DATA_FILE *dat_f,char *f_name,int rec_len)
{
	strcpy(dat_f->fa_name,f_name);
	dat_f->f=fopen(f_name,"w+b");
	if (dat_f->f==NULL) access_error(dat_f->fa_name);
	dat_f->changed=0;
	dat_f->rec_len=rec_len;
	dat_f->first_free= -1;
	dat_f->number_free=0;
	dat_f->num=0;
	if (fseek(dat_f->f,0,SEEK_SET)||
	fwrite(dat_f->pass_str,sizeof(char[9])+sizeof(long[3]),1,
	dat_f->f)!=1||fflush(dat_f->f)) access_error(dat_f->fa_name);
	found_file(dat_f);
	dat_f->num_rec=1;
}

int open_file(DATA_FILE *dat_f,char *f_name,int rec_len)
{
	strcpy(dat_f->fa_name,f_name);
	dat_f->f=fopen(f_name,"r+b");
	if (dat_f->f==NULL) return 1;
	if (fseek(dat_f->f,0,SEEK_SET)||fread(dat_f->pass_str,sizeof(char[9])+
	sizeof(long[3]),1,dat_f->f)!=1) access_error(dat_f->fa_name);
	dat_f->changed=0;
	dat_f->rec_len=rec_len;
	dat_f->num_rec=(fa_filelen(dat_f->f)-
	sizeof(char[9])-sizeof(long[3]))/rec_len+1;
	return 0;
}

void update_file(DATA_FILE *dat_f)
{
	if (fseek(dat_f->f,0,SEEK_SET)||fwrite(dat_f->pass_str,
	sizeof(char[9])+sizeof(long[3]),1,dat_f->f)!=1||fflush(dat_f->f))
	access_error(dat_f->fa_name);
	dat_f->changed=0;
}

void close_file(DATA_FILE *dat_f)
{
	if (dat_f->changed) update_file(dat_f);
	if (fclose(dat_f->f)) access_error(dat_f->fa_name);
	dat_f->f=NULL;
}

void found_file(DATA_FILE *dat_f)
{
	close_file(dat_f);
	dat_f->f=fopen(dat_f->fa_name,"r+b");
	if (dat_f->f==NULL) access_error(dat_f->fa_name);
}

void set_pass_str(DATA_FILE *dat_f,char *pass_str)
{
	strcpy(dat_f->pass_str,pass_str);
}

int test_pass_str(DATA_FILE *dat_f,char *pass_str)
{
	return (strcmp(dat_f->pass_str,pass_str));
}

void new_rec(DATA_FILE *dat_f,long *r)
{
	long l;
	if (dat_f->first_free== -1)
	{
		*r=dat_f->num_rec;
		dat_f->num_rec++;
	}
	else
	{
		*r=dat_f->first_free;
		fa_seekrec(dat_f,*r);
		if (fread((void *)&l,sizeof l,1,dat_f->f)!=1)
		access_error(dat_f->fa_name);
		dat_f->first_free=l;
		dat_f->number_free--;
	}
	dat_f->changed= -1;
}

void add_rec(DATA_FILE *dat_f,long *r,void *buffer)
{
	new_rec(dat_f,r);
	put_rec(dat_f,*r,buffer);
}

void delete_rec(DATA_FILE *dat_f,long r)
{
	fa_seekrec(dat_f,r);
	if (fwrite((void *)&dat_f->first_free,sizeof(long),1,dat_f->f)!=1)
	access_error(dat_f->fa_name);
	dat_f->first_free=r;
	dat_f->number_free++;
	dat_f->changed= -1;
}

long file_len(DATA_FILE *dat_f)
{
	return dat_f->num_rec;
}

long used_recs(DATA_FILE *dat_f)
{
	return dat_f->num_rec-dat_f->number_free-1;
}

void init_index(void)
{
	int i;
	for (i=0; i<PAGE_STACK_SIZE; i++) fa_pg_map[i]=(short)i;
}

void fa_pack(FA_PAGE *page,int key_l)
{
	int i;
	if (key_l!=MAX_KEY_LEN)
	for (i=0; i<PAGE_SIZE; i++)
	memcpy((char *)page+
	i*(key_l+sizeof(long[2]))+sizeof(short)+sizeof(long),
	&page->item_array[i],key_l+sizeof(long[2]));
}

void fa_unpack(FA_PAGE *page,int key_l)
{
	int i;
	if (key_l!=MAX_KEY_LEN)
	for (i=PAGE_SIZE-1; i>=0; i--)
	memcpy(&page->item_array[i],(char *)page+
	i*(key_l+sizeof(long[2]))+sizeof(short)+sizeof(long),
	key_l+sizeof(long[2]));
}

void make_index(INDEX_FILE *idx_f,char *f_name,int key_len,int s)
{
	int k=(key_len+sizeof(long[2]))*PAGE_SIZE+sizeof(short)+sizeof(long);
	make_file(&idx_f->data_f,f_name,k);
	idx_f->allow_dupl_keys=s;
	idx_f->key_l=key_len;
	idx_f->rr=0;
	idx_f->pp= -1;
}

int open_index(INDEX_FILE *idx_f,char *f_name,int key_len,int s)
{
	int k=(key_len+sizeof(long[2]))*PAGE_SIZE+sizeof(short)+sizeof(long);
	if (open_file(&idx_f->data_f,f_name,k)) return 1;
	idx_f->allow_dupl_keys=s;
	idx_f->key_l=key_len;
	idx_f->rr=idx_f->data_f.num;
	idx_f->pp= -1;
	return 0;
}

void update_index(INDEX_FILE *idx_f)
{
	int i;
	for (i=0; i<PAGE_STACK_SIZE; i++)
	if (fa_page_stk[i].index_f_ptr==idx_f)
	{
		fa_page_stk[i].index_f_ptr=NULL;
		if (fa_page_stk[i].updated)
		{
			fa_pack(&fa_page_stk[i].page,idx_f->key_l);
			put_rec(&idx_f->data_f,fa_page_stk[i].page_ref,
			&fa_page_stk[i].page);
			fa_page_stk[i].updated=0;
		}
	}
	idx_f->data_f.num=idx_f->rr;
	if (idx_f->data_f.changed) update_file(&idx_f->data_f);
}

void close_index(INDEX_FILE *idx_f)
{
	update_index(idx_f);
	if (fclose(idx_f->data_f.f)) access_error(idx_f->data_f.fa_name);
	idx_f->data_f.f=NULL;
}

void found_index(INDEX_FILE *idx_f)
{
	close_index(idx_f);
	idx_f->data_f.f=fopen(idx_f->data_f.fa_name,"r+b");
	if (idx_f->data_f.f==NULL) access_error(idx_f->data_f.fa_name);
}

void fa_last(int i)
{
	int j=0,k;
	while (fa_pg_map[j]!=i&&j<PAGE_STACK_SIZE-1) j++;
	for (k=j; k<PAGE_STACK_SIZE-1; k++) fa_pg_map[k]=fa_pg_map[k+1];
	fa_pg_map[PAGE_STACK_SIZE-1]=(short)i;
}

void fa_get_page(INDEX_FILE *idx_f,long r,FA_PAGE_PTR *pg_ptr)
{
	int i= -1,found;
	do
	{
		i++;
		found=((fa_page_stk[i].index_f_ptr==idx_f)&&
		(fa_page_stk[i].page_ref==r));
	}
	while (!found&&i!=PAGE_STACK_SIZE-1);
	if (!found)
	{
		i=fa_pg_map[0];
		if (fa_page_stk[i].updated)
		{
			fa_pack(&fa_page_stk[i].page,
			fa_page_stk[i].index_f_ptr->key_l);
			put_rec(&fa_page_stk[i].index_f_ptr->data_f,
			fa_page_stk[i].page_ref,&fa_page_stk[i].page);
		}
		get_rec(&idx_f->data_f,r,&fa_page_stk[i].page);
		fa_unpack(&fa_page_stk[i].page,idx_f->key_l);
		fa_page_stk[i].index_f_ptr=idx_f;
		fa_page_stk[i].page_ref=r;
		fa_page_stk[i].updated=0;
	}
	fa_last(i);
	*pg_ptr=(FA_PAGE_PTR)&fa_page_stk[i];
}

void fa_new_page(INDEX_FILE *idx_f,long *r,FA_PAGE_PTR *pg_ptr)
{
	int i=fa_pg_map[0];
	if (fa_page_stk[i].updated)
	{
		fa_pack(&fa_page_stk[i].page,
		fa_page_stk[i].index_f_ptr->key_l);
		put_rec(&fa_page_stk[i].index_f_ptr->data_f,
		fa_page_stk[i].page_ref,&fa_page_stk[i].page);
	}
	new_rec(&idx_f->data_f,r);
	fa_page_stk[i].index_f_ptr=idx_f;
	fa_page_stk[i].page_ref= *r;
	fa_page_stk[i].updated=0;
	fa_last(i);
	*pg_ptr=(FA_PAGE_PTR)&fa_page_stk[i];
	put_rec(&idx_f->data_f,*r,*pg_ptr);
}

void fa_update_page(FA_PAGE_PTR pg_ptr)
{
	((FA_STACK_REC_PTR)pg_ptr)->updated=1;
}

void fa_return_page(FA_PAGE_PTR *pg_ptr)
{
	FA_STACK_REC_PTR p;
	p=(FA_STACK_REC_PTR)*pg_ptr;
	delete_rec(&p->index_f_ptr->data_f,p->page_ref);
	p->index_f_ptr=NULL;
	p->updated=0;
}

long fa_comp_keys(char *k1,char *k2,long dr1,long dr2,
int dup0,int key_l)
{
	int d=memcmp(k1,k2,key_l);
	if (d) return (long)d;
	if (dup0) return dr1-dr2;
	return 0;
}

void clear_key(INDEX_FILE *idx_f)
{
	idx_f->pp= -1;
}

