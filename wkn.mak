
# makefile
# MSC, D=Debug

!IFDEF D
DEBUG=/debug
DEBUG_C=/Zi
!ELSE
DEBUG=
DEBUG_C=
!ENDIF

OBJS = obj\wk.obj obj\wake.obj obj\wk_fun.obj obj\wk_fun2.obj\
 obj\wk_gra.obj obj\wk_gra2.obj obj\wk_log.obj obj\wk_mat.obj\
 obj\wk_mat2.obj obj\wk_pic.obj obj\wk_seq.obj obj\wk_seq2.obj\
 obj\file_acs.lib obj\wk.res
F_OBJS = obj\f_access.obj obj\f_addkey.obj obj\f_getkey.obj\
 obj\f_delkey.obj obj\f_setkey.obj

bin-win32\wk.exe: $(OBJS)
	link $(DEBUG) /out:bin-win32\wk.exe /subsystem:windows $(OBJS)\
 advapi32.lib gdi32.lib kernel32.lib user32.lib winmm.lib
	dir bin-win32\wk.exe

obj\file_acs.lib: $(F_OBJS)
 	lib /out:obj\file_acs.lib $(F_OBJS)

{}.c{obj\}.obj:
	cl /DWIN32 /Fdobj\wk /Foobj\ /Gr /Gs /Gy /MD /Ox /WX $(DEBUG_C) /c $<

{}.rc{obj\}.res:
	rc -foobj\$(@B).res -r $<
