
COPTS=-O3
OD=o
FLIB=./lib/libfileacs.a
OBJS=wk.o wake.o wk_fun.o wk_fun2.o wk_gra.o wk_gra2.o wk_log.o wk_mat.o \
wk_mat2.o wk_pic.o wk_seq.o wk_seq2.o
FOBJS=f_access.o f_addkey.o f_getkey.o f_delkey.o f_setkey.o
C=gcc $(COPTS) -D_POSIX_SOURCE -D_SVID_SOURCE -DXLIB -Wall -ansi -pedantic -c -o $@ $<
ifeq ($(OSTYPE),darwin10.0)
BIN=./bin-mac/wk
LIBS=-framework CoreFoundation -framework AudioToolbox -framework AudioUnit -framework CoreMidi
else
BIN=./bin-linux/wk
LIBS=-lasound
endif

vpath %.o $(OD)

$(BIN): $(OBJS) $(FLIB)
	(cd $(OD); gcc $(OBJS) -L../lib -L/usr/X11R6/lib -lm -lpthread -lX11 -lfileacs ${LIBS} -o ../$(BIN); ls -l ../$(BIN); cd -)

$(FLIB): $(FOBJS)
	(cd $(OD); ar r ../$(FLIB) $(FOBJS); cd -)

$(OD)/wk.o: wk.c
	$(C)

$(OD)/wake.o: wake.c
	$(C)

$(OD)/wk_fun.o: wk_fun.c
	$(C)

$(OD)/wk_fun2.o: wk_fun2.c
	$(C)

$(OD)/wk_gra.o: wk_gra.c
	$(C)

$(OD)/wk_gra2.o: wk_gra2.c
	$(C)

$(OD)/wk_log.o: wk_log.c
	$(C)

$(OD)/wk_mat.o: wk_mat.c
	$(C)

$(OD)/wk_mat2.o: wk_mat2.c
	$(C)

$(OD)/wk_pic.o: wk_pic.c
	$(C)

$(OD)/wk_seq.o: wk_seq.c
	$(C)

$(OD)/wk_seq2.o: wk_seq2.c
	$(C)

$(OD)/f_access.o: f_access.c
	$(C)

$(OD)/f_addkey.o: f_addkey.c
	$(C)

$(OD)/f_getkey.o: f_getkey.c
	$(C)

$(OD)/f_delkey.o: f_delkey.c
	$(C)

$(OD)/f_setkey.o: f_setkey.c
	$(C)
