
/* wake.c */

#define MAIN
#include "wake.h"
#include <ctype.h>
#ifdef AMIGA
#include <exec/memory.h>
#include <proto/dos.h>
#include <proto/exec.h>
#include <errno.h>
#endif
#include <math.h>

#ifdef _DCC
#define strtoul (ULONG)strtol
#endif

#ifdef AMIGA
#define IMAGESIZE 8
#define VOICESIZE sizeof(VOICE)
#else
#define IMAGESIZE 4
#define VOICESIZE 4
#endif

extern NOMEN *system_nomen;

#ifdef _WINDOWS
extern LPCSTR wk_name;
extern HWND hwnd;
#elif defined(XLIB)
extern char xerror;
#endif

extern void wk_exec(void);
extern void wk_quit(void);

char *whites=" \11\12\15";
char *delims=" \11\12\15%()/<>@[]{}";
UBYTE type_size[NUMTYPES]={0,0,sizeof(ELEMENT),sizeof(ELEMENT),0,0,0,0,0,0,0,
                           sizeof(double),sizeof(long),sizeof(ULONG),sizeof(float),sizeof(short),
                           sizeof(UWORD),sizeof(UBYTE),0,0,sizeof(NOMEN),0,IMAGESIZE,VOICESIZE,0,
                           sizeof(STATE),0,8};

static int level_number;
static ENTRY *comment_entry;
static NOMENP comment_nomen;
static int (_CDECL *gc)(FILE *);
static int (_CDECL *nc)(int,FILE *);

#ifdef AMIGA
int test(LONG file,char *s)
{
    char c,*p=s;
    while (*p&&Read(file,&c,1)==1&&c== *p) p++;
    if (*p&&Seek(file,strlen(p+1),OFFSET_CURRENT)==-1) return -1;
    return (int)*p;
}

int skip(LONG file)
{
    ULONG chunk_size;
    if (Read(file,&chunk_size,sizeof chunk_size)!=sizeof chunk_size||
        Seek(file,chunk_size+chunk_size%2,OFFSET_CURRENT)==-1) return -1;
    return 0;
}
#endif

void memory_overflow(void)
{
    term_wake();
#ifdef _WINDOWS
    MessageBox(hwnd,"memory overflow, terminated wake",wk_name,
               MB_ICONEXCLAMATION|MB_OK);
    DestroyWindow(hwnd);
#else
    fpts("memory overflow, terminated wake",stdout);
#endif
    exit(-1);
}

REGQUAL ENTRY *ssearch(register char *name,register ENTRY *entry)
{
    int d;
    while (entry) {
        if (!(d=strcmp(name,entry->name))) return entry;
        if (d<0) entry=entry->l;
        else entry=entry->r;
    }
    return NULL;
}

REGQUAL ENTRY *search(register char *name,register NOMENP *nomenp)
{
    int d;
    ENTRY *entry;
    NOMENP *np=nsp;
    while (np>nomen_stack) {
        entry=(*--np)->entry;
        while (entry) {
            if (!(d=strcmp(name,entry->name))) {
                *nomenp= *np;
                return entry;
            }
            if (d<0) entry=entry->l;
            else entry=entry->r;
        }
    }
    *nomenp=nsp[-1];
    return NULL;
}

REGQUAL ENTRY *enter(register char *name,register NOMENP nomen)
{
    int d;
    char a;
    ENTRY *p,*q,*r,*s,*t=NULL;
    s=p=nomen->entry;
    if (!s) {
        nomen->number++;
        nomen->entry=q=(ENTRY *)memcalloc(sizeof(ENTRY));
        if (!q) return NULL;
        q->name=name;
        return q;
    }
    for (;;) {
        d=strcmp(name,p->name);
        if (!d) return p;
        if (d<0) {
            if (!(q=p->l)) {
                p->l=q=(ENTRY *)memcalloc(sizeof(ENTRY));
                if (!q) return NULL;
                break;
            }
        }
        else if (!(q=p->r)) {
            p->r=q=(ENTRY *)memcalloc(sizeof(ENTRY));
            if (!q) return NULL;
            break;
        }
        if (q->b) {
            t=p;
            s=q;
        }
        p=q;
    }
    nomen->number++;
    q->name=name;
    if (strcmp(name,s->name)<0) r=p=s->l;
    else r=p=s->r;
    while (p!=q) {
        if (strcmp(name,p->name)<0) {
            p->b= -1;
            p=p->l;
        }
        else {
            p->b=1;
            p=p->r;
        }
    }
    if (strcmp(name,s->name)<0) a= -1;
    else a=1;
    if (!s->b) {
        s->b=a;
        return q;
    }
    if (s->b== -a) {
        s->b=0;
        return q;
    }
    if (r->b==a) {
        p=r;
        if (a== -1) {
            s->l=r->r;
            r->r=s;
        }
        else {
            s->r=r->l;
            r->l=s;
        }
        s->b=r->b=0;
    }
    else if (r->b== -a) {
        if (a== -1) {
            p=r->r;
            r->r=p->l;
            p->l=r;
            s->l=p->r;
            p->r=s;
        }
        else {
            p=r->l;
            r->l=p->r;
            p->r=r;
            s->r=p->l;
            p->l=s;
        }
        if (!p->b) s->b=r->b=0;
        else if (p->b==a) {
            s->b=(char)-a;
            r->b=0;
        }
        else {
            s->b=0;
            r->b=a;
        }
        p->b=0;
    }
    if (t) {
        if (s==t->r) t->r=p;
        else t->l=p;
    }
    else nomen->entry=p;
    return q;
}

REGQUAL void stack(register unsigned number)
{
    register unsigned top = wsp - wake_stack;
    if (top + number > wake_stack_size) {
        ELEMENT *new=(ELEMENT *)realloc((void *)wake_stack,
                                        (wake_stack_size+=number+WAKE_STACK_INCR)*sizeof(ELEMENT));
        if (!new) {
            free((void *)wake_stack);
            wsp=wake_stack=(ELEMENT *)
                malloc(WAKE_STACK_SIZE*sizeof(ELEMENT));
            if (!wake_stack) memory_overflow();
            fpts("memory overflow, stack restored\n",stdout);
            wk_quit();
        }
        wake_stack=new;
    }
    wsp=wake_stack+top;
}

REGQUAL void push(register ELEMENT *e)
{
    if (++est>exec_stack_size) {
        EXECUTE *new=(EXECUTE *)realloc((void *)exec_stack,
                                        (exec_stack_size+=EXEC_STACK_INCR)*sizeof(EXECUTE));
        if (!new) {
            free((void *)exec_stack);
            est=0;
            exec_stack=(EXECUTE *)
                malloc(EXEC_STACK_SIZE*sizeof(EXECUTE));
            if (!exec_stack) memory_overflow();
            fpts("memory overflow, exec stack restored\n",stdout);
            wk_quit();
        }
        exec_stack=new;
    }
    eel= &exec_stack[est-1].exec_elem;
    exec_stack[est-1].element= *e;
}

REGQUAL void check_complex(register ELEMENT *e,register COMPLEX *c)
{
    switch (e->type) {
    case COMPLEX_TYPE:
        *c=e->unit.compl;
        return;
    case FLOATING_TYPE:
        c->real=(float)e->unit.floating;
        break;
    case FRACTION_TYPE:
        c->real=(float)((double)e->unit.fract.num/
                        (double)e->unit.fract.den);
        break;
    case INTEGER_TYPE:
        c->real=(float)e->unit.integer;
        break;
    default:
        error(WAKE_INVALID_TYPE);
    }
    c->imag=(float)0.0;
}

REGQUAL double check_double(register ELEMENT *e)
{
    switch (e->type) {
    case FLOATING_TYPE:
        return e->unit.floating;
    case FRACTION_TYPE:
        return (double)e->unit.fract.num/
            (double)e->unit.fract.den;
    case INTEGER_TYPE:
        return (double)e->unit.integer;
    case UINTEGER_TYPE:
        return (double)e->unit.uinteger;
    default:
        error(WAKE_INVALID_TYPE);
    }
    return 0.0;
}

REGQUAL long check_integer(register ELEMENT *e)
{
    switch (e->type) {
    case INTEGER_TYPE:
        return e->unit.integer;
    case UINTEGER_TYPE:
        return (long)e->unit.uinteger;
    default:
        error(WAKE_INVALID_TYPE);
    }
    return 0;
}

REGQUAL ULONG check_uinteger(register ELEMENT *e)
{
    switch (e->type) {
    case UINTEGER_TYPE:
        return e->unit.uinteger;
    case INTEGER_TYPE:
        return (ULONG)e->unit.integer;
    default:
        error(WAKE_INVALID_TYPE);
    }
    return 0;
}

REGQUAL void *get_mem(register ULONG num,register UBYTE type,
                      register char quit)
{
    HEAD *head;
    if (!(head=(HEAD *)memcalloc(sizeof(HEAD)+num*(ULONG)type_size[type]))) {
        if (quit) error(WAKE_CANNOT_ALLOCATE);
        else return NULL;
    }
    head->type=type;
    head->last=last_save;
    last_save=head;
    return (void *)(head+1);
}

static int _CDECL bgetc(FILE *file)
{
    if (**(UBYTE **)file) return (int)*(*(UBYTE **)file)++;
    return EOF;
}

static int _CDECL bungetc(int c,FILE *file)
{
    if (c==EOF) c=0;
    *--(*(UBYTE **)file)=(UBYTE)c;
    return c;
}

static char get_num(FILE *file,int base,int len,int c)
{
    char b[9],*p;
    p=b;
    for (;;) {
        if (c==EOF) error(WAKE_SYNTAX_ERROR);
        *p++=(char)c;
        if (len--) c=(*gc)(file);
        else break;
    }
    *p='\0';
    *b=(char)strtol(b,&p,base);
    if (*p) error(WAKE_SYNTAX_ERROR);
    return *b;
}

static void set_pointers(ELEMENT *elem,HEAD *head)
{
    ULONG i;
    ELEMENT *e = elem->unit.area;
    for (i=0; i<elem->unit.line.length; i++) {
        if (e->flag & COMPOSITE) e->unit.head[-1].head = head;
        if (e->type == ARRAY_TYPE || e->type == BLOCK_TYPE)
            set_pointers(UN e, head);
        e++;
    }
}

static void unknown_name_error(char *name) {
    err_elem.flag=0;
    err_elem.type=STRING_TYPE;
    err_elem.unit.sstring=name;
    err_elem.unit.line.length=strlen(name);
    error(WAKE_UNKNOWN_NAME);
}

REGQUAL int token(register ELEMENT *elem)
{
    int c;
    char *p;
    FILE *file;
    memset(elem, 0, sizeof(ELEMENT));
    if (exec_stack[est-1].element.type==FILE_TYPE) {
        file=exec_stack[est-1].element.unit.file;
        gc=file==stdin?fgtc:fgetc;
        nc=file==stdin?ngtc:ungetc;
    }
    else {
        file=(FILE *)&exec_stack[est-1].element.unit.string;
        gc=bgetc;
        nc=bungetc;
    }
    while ((c=(*gc)(file))!=EOF) {
        if (strchr(whites,c)) continue;
        if (isdigit(c)||c=='+'||c=='-'||c=='.') {
            *strbuf=(char)c;
            p=strbuf+1;
            while ((c=(*gc)(file))!=EOF&&
                   !strchr(delims,c)) *p++=(char)c;
            (*nc)(c,file);
            *p='\0';
            p=strpbrk(strbuf,":#;.Ee");
            elem->flag=NUMBER;
            if (p)
                switch (*p) {
                case ':':
                    elem->unit.fract.num=atol(strbuf);
                    elem->unit.fract.den=atol(p+1);
                    return canon(elem);
                case '#':
                    elem->unit.integer=
                        strtoul(p+1,&p,atoi(strbuf));
                    if (*p) error(WAKE_SYNTAX_ERROR);
                    elem->flag|=INTEGER;
                    elem->type=INTEGER_TYPE;
                    return INTEGER_TYPE;
                case ';':
                    elem->unit.compl.imag=
                        (float)atof(p+1);
                    if (elem->unit.compl.imag!=
                        (float)0.0)
                        {
                            elem->type=COMPLEX_TYPE;
                            elem->unit.compl.real=
                                (float)atof(strbuf);
                            return COMPLEX_TYPE;
                        }
                default:
                    elem->unit.floating=atof(strbuf);
                    elem->type=FLOATING_TYPE;
                    return FLOATING_TYPE;
                }
            elem->flag|=INTEGER;
            if (*strbuf=='+') {
                elem->unit.uinteger=strtoul(strbuf,&p,10);
                if (*p) error(WAKE_SYNTAX_ERROR);
                elem->type=UINTEGER_TYPE;
                return UINTEGER_TYPE;
            }
            elem->unit.integer=strtol(strbuf,&p,10);
            if (*p) error(WAKE_SYNTAX_ERROR);
            elem->type=INTEGER_TYPE;
            return INTEGER_TYPE;
        }
        switch (c) {
        case '/':
            if ((c=(*gc)(file))=='/') {
                ENTRY *en;
                NOMENP np;
                p=strbuf;
                while ((c=(*gc)(file))!=EOF&&
                       !strchr(delims,c)) *p++=(char)c;
                *p='\0';
                if (c=='@') {
                    char *q= ++p;
                    while ((c=(*gc)(file))!=EOF&&
                           !strchr(delims,c)) *p++=(char)c;
                    (*nc)(c,file);
                    *p='\0';
                    if (*q) {
                        if (!(en=search(q,&np)))
                            unknown_name_error(q);
                        if (!(en->flag&BOUND)) {
                            err_elem.flag=0;
                            err_elem.type=ENTRY_TYPE;
                            err_elem.unit.entry=en;
                            err_elem.unit.site.nomen=np;
                            error(WAKE_ENTRY_NOT_BOUND); }
                        if (en->element.type!=
                            NOMEN_TYPE)
                            error(WAKE_ILLEGAL_USE);
                        np=en->element.unit.nomen;
                        if (!(en=ssearch(strbuf,
                                         np->entry)))
                            unknown_name_error(strbuf);
                    }
                    else {
                        np=nsp[-1];
                        if (!(en=ssearch(strbuf,
                                         np->entry)))
                            unknown_name_error(strbuf);
                    }
                }
                else {
                    (*nc)(c,file);
                    if (!(en=search(strbuf,&np)))
                        unknown_name_error(strbuf);
                }
                if (!(en->flag&BOUND)) {
                    err_elem.flag=0;
                    err_elem.type=ENTRY_TYPE;
                    err_elem.unit.entry=en;
                    err_elem.unit.site.nomen=np;
                    error(WAKE_ENTRY_NOT_BOUND); }
                *elem=en->element;
                elem->flag&=0xff^COMPOSITE;
                return (int)elem->type;
            }
            p=strbuf;
            (*nc)(c,file);
            while ((c=(*gc)(file))!=EOF&&
                   !strchr(delims,c)) *p++=(char)c;
            *p='\0';
            if (c=='@') {
                ENTRY *en;
                NOMENP np;
                char *q= ++p;
                while ((c=(*gc)(file))!=EOF&&
                       !strchr(delims,c)) *p++=(char)c;
                (*nc)(c,file);
                *p='\0';
                if (*q) {
                    if (!(en=search(q,&np)))
                        unknown_name_error(q);
                    if (!(en->flag&BOUND)) {
                        err_elem.flag=0;
                        err_elem.type=ENTRY_TYPE;
                        err_elem.unit.entry=en;
                        err_elem.unit.site.nomen=np;
                        error(WAKE_ENTRY_NOT_BOUND); }
                    if (en->element.type!=NOMEN_TYPE)
                        error(WAKE_ILLEGAL_USE);
                    if (!(elem->unit.name=ssearch(strbuf,
                                                  en->element.unit.nomen->entry))) {
                        if (!(p=(char *)memalloc(
                                                 strlen(strbuf)+1)))
                            error(WAKE_CANNOT_ALLOCATE);
                        strcpy(p,strbuf);
                        if (!(elem->unit.name=
                              enter(p,
                                    en->element.unit.nomen))) {
                            memfree(p);
                            error(WAKE_CANNOT_ALLOCATE); }
                    }
                    elem->unit.site.nomen=
                        en->element.unit.nomen;
                }
                else if (!(elem->unit.name=ssearch(strbuf,
                                                   nsp[-1]->entry))) {
                    if (!(p=(char *)memalloc(
                                             strlen(strbuf)+1)))
                        error(WAKE_CANNOT_ALLOCATE);
                    strcpy(p,strbuf);
                    if (!(elem->unit.name=
                          enter(p,nsp[-1]))) {
                        memfree(p);
                        error(WAKE_CANNOT_ALLOCATE); }
                    elem->unit.site.nomen=nsp[-1];
                }
            }
            else {
                (*nc)(c,file);
                if (!(elem->unit.name=search(strbuf,
                                             &elem->unit.site.nomen))) {
                    if (!(p=(char *)memalloc(
                                             strlen(strbuf)+1)))
                        error(WAKE_CANNOT_ALLOCATE);
                    strcpy(p,strbuf);
                    if (!(elem->unit.name=
                          enter(p,nsp[-1]))) {
                        memfree(p);
                        error(WAKE_CANNOT_ALLOCATE); }
                }
            }
            elem->flag=0;
            elem->type=NAME_TYPE;
            return NAME_TYPE;
        case '(':
        case '<': {
            int l=0;
            char e=(char)(c=='<'?'>':')');
            void *mem;
            p=strbuf;
            temp_mem=NULL;
            elem->unit.line.length=0;
            for (;;) {
                if ((c=(*gc)(file))==EOF)
                    error(WAKE_SYNTAX_ERROR);
                if (c=='\n') continue;
                if (c==e||l>=WAKE_STRBUF_SIZE-1) {
                    elem->unit.line.length+=l;
                    if (temp_mem)
                        mem=(void *)memrealloc(
                                               temp_mem,sizeof(HEAD)+
                                               elem->unit.line.length+1);
                    else mem=(void *)memcalloc(
                                               sizeof(HEAD)+
                                               elem->unit.line.length+1);
                    if (!mem) {
                        if (temp_mem)
                            memfree(temp_mem);
                        error(WAKE_CANNOT_ALLOCATE);
                    }
                    else temp_mem=mem;
                    memcpy((UBYTE *)temp_mem+sizeof(HEAD)+
                           elem->unit.line.length-l,strbuf,l);
                    if (c==e) {
                        elem->unit.head=
                            (HEAD *)temp_mem;
                        temp_mem=NULL;
                        elem->unit.head->type=
                            STRING_TYPE;
                        elem->unit.head->last=
                            last_save;
                        last_save=elem->unit.head;
                        elem->unit.head++;
                        elem->unit.string[
                                          elem->unit.line.length]='\0';
                        elem->flag=COMPOSITE|INDEXED;
                        elem->type=STRING_TYPE;
                        return STRING_TYPE;
                    }
                    l=0;
                    p=strbuf;
                }
                if (e=='>') {
                    if (strchr(whites,c)) continue;
                    *p=get_num(file,16,1,c);
                }
                else if (c=='\\') {
                    if ((c=(*gc)(file))==EOF) break;
                    switch (c) {
                    case 'a':
                        *p='\a';
                        break;
                    case 'f':
                        *p='\f';
                        break;
                    case 'n':
                        *p='\n';
                        break;
                    case 'r':
                        *p='\r';
                        break;
                    case 't':
                        *p='\t';
                        break;
                    case 'v':
                        *p='\v';
                        break;
                    case 'x':
                        *p=get_num(file,16,1,
                                   (*gc)(file));
                        break;
                    default:
                        if (isdigit(c))
                            *p=get_num(file,8,2,c);
                        else *p=(char)c;
                    }
                }
                else *p=(char)c;
                p++;
                l++;
            }
        }
        case '[':
        case '{': {
            char e;
            ELEMENT el,*q;
            unsigned n, top = wsp - wake_stack;
            level_number++;
            e=(char)(c=='['?']':'}');
            elem->type=0;
            if (e==']')
                switch (c=(*gc)(file)) {
                case 0:
                case EOF:
                    error(WAKE_SYNTAX_ERROR);
                case '@':
                    elem->type=FLOATARRAY_TYPE;
                    break;
                case '#':
                    if ((c=(*gc)(file))==EOF)
                        error(WAKE_SYNTAX_ERROR);
                    if (c!='+') {
                        elem->type=INTARRAY_TYPE;
                        (*nc)(c,file);
                    }
                    else elem->type=UINTARRAY_TYPE;
                    break;
                case '&':
                    elem->type=SINGLEARRAY_TYPE;
                    break;
                case '$':
                    if ((c=(*gc)(file))==EOF)
                        error(WAKE_SYNTAX_ERROR);
                    if (c!='+') {
                        elem->type=SHORTARRAY_TYPE;
                        (*nc)(c,file);
                    }
                    else elem->type=USHORTARRAY_TYPE;
                    break;
                default:
                    (*nc)(c,file);
                }
            elem->unit.line.length=0;
            while ((c=(*gc)(file))!=e) {
                if (c==EOF) error(WAKE_SYNTAX_ERROR);
                if (strchr(whites,c)) continue;
                if (c=='%') {
                    while ((c=(*gc)(file))!=EOF&&c!='\n');
                    (*nc)(c,file);
                    continue;
                }
                (*nc)(c,file);
                token(&el);
                if (elem->type&&!(el.flag&NUMBER))
                    error(WAKE_INVALID_TYPE);
                if (el.type==ENTRY_TYPE) {
                    if (!el.unit.entry) {
                        p=(char *)memalloc(strlen(strbuf)+1);
                        if (!p) error(WAKE_CANNOT_ALLOCATE);
                        strcpy(p,strbuf);
                        el.unit.entry=enter(p,el.unit.site.nomen);
                        if (!el.unit.entry) {
                            memfree(p);
                            error(WAKE_CANNOT_ALLOCATE);
                        }
                    }
                    else if (el.unit.entry->flag&DETERMINED) {
                        ENTRY *en=el.unit.entry;
                        if (en->element.type==BLOCK_TYPE) {
                            el.type=QUICK_TYPE;
                            el.unit.block=
                                en->element.unit.block;
                            el.unit.pair.name=en;
                        }
                        else el=en->element;
                        el.flag|=FIXED;
                        el.flag&=0xff^COMPOSITE;
                    }
                }
                n = wsp - wake_stack;
                stack(1);
                *wsp++ = el;
                if (e == ']' && (wsp[-1].type == FUNCTION_TYPE || wsp[-1].type == ENTRY_TYPE)) {
                    if (level_number == 1) wk_exec();
                    else elem->flag|=TEMPORARY;
                }
            }
            level_number--;
            elem->flag|=COMPOSITE|INDEXED;
            elem->unit.line.length = (wsp - wake_stack) - top;
            q = wsp - elem->unit.line.length;
            if (!elem->type) {
                elem->type=(UBYTE)(e==']'?ARRAY_TYPE:BLOCK_TYPE);
                elem->unit.area=(ELEMENT *)
                    get_mem(elem->unit.line.length+1, elem->type, TRUE);
            }
            else elem->unit.data=get_mem(elem->unit.line.length,elem->type,TRUE);
            switch (elem->type) {
            case FLOATARRAY_TYPE: {
                ULONG i;
                for (i=0; i<elem->unit.line.length;
                     i++) elem->unit.floatarray[i]=
                              check_double(q+i);
                break;
            }
            case INTARRAY_TYPE: {
                ULONG i;
                for (i=0; i<elem->unit.line.length;
                     i++) elem->unit.intarray[i]=
                              check_integer(q+i);
                break;
            }
            case UINTARRAY_TYPE: {
                ULONG i;
                for (i=0; i<elem->unit.line.length;
                     i++) elem->unit.uintarray[i]=
                              check_uinteger(q+i);
                break;
            }
            case SINGLEARRAY_TYPE: {
                ULONG i;
                for (i=0; i<elem->unit.line.length;
                     i++) elem->unit.singlearray[i]=
                              (float)check_double(q+i);
                break;
            }
            case SHORTARRAY_TYPE: {
                ULONG i;
                for (i=0; i<elem->unit.line.length;
                     i++) elem->unit.shortarray[i]=
                              (short)check_integer(q+i);
                break;
            }
            case USHORTARRAY_TYPE: {
                ULONG i;
                for (i=0; i<elem->unit.line.length;
                     i++) elem->unit.ushortarray[i]=
                              (UWORD)check_uinteger(q+i);
                break;
            }
            default:
                memcpy(elem->unit.area,q,
                       (int)elem->unit.line.length*
                       sizeof(ELEMENT));
                if (!level_number)
                    set_pointers(elem,elem->unit.head-1);
            }
            wsp = q;
            return (int)elem->type;
        }
        case '%':
            if (comment_entry) {
                exec_entry=comment_entry;
                exec_nomen=comment_nomen;
                entry_exec();
            }
            else {
                while ((c=(*gc)(file))!=EOF&&c!='\n');
                (*nc)(c,file);
            }
            break;
        default:
            *strbuf=(char)c;
            p=strbuf+1;
            while ((c=(*gc)(file))!=EOF&&!strchr(delims,c))
                *p++=(char)c;
            *p='\0';
            if (c=='@') {
                ENTRY *en;
                NOMENP np;
                char *q= ++p;
                while ((c=(*gc)(file))!=EOF&&
                       !strchr(delims,c)) *p++=(char)c;
                (*nc)(c,file);
                *p='\0';
                if (*q) {
                    if (!(en=search(q,&np)))
                        unknown_name_error(q);
                    if (en->element.type!=NOMEN_TYPE)
                        error(WAKE_ILLEGAL_USE);
                    elem->unit.entry=ssearch(strbuf,
                                             en->element.unit.nomen->entry);
                    elem->unit.site.nomen=
                        en->element.unit.nomen;
                }
                else {
                    elem->unit.entry=ssearch(strbuf,
                                             nsp[-1]->entry);
                    elem->unit.site.nomen=nsp[-1];
                }
            }
            else {
                (*nc)(c,file);
                elem->unit.entry=search(strbuf,
                                        &elem->unit.site.nomen);
            }
            elem->flag=0;
            elem->type=ENTRY_TYPE;
            return ENTRY_TYPE;
        }
    }
    return EOF;
}

int enter_funcs(WAKE_FUNC funcs[],int n_funcs)
{
    int i;
    ENTRY *entry;
    for (i=0; i<n_funcs; i++) {
        entry=enter(funcs[i].name,system_nomen);
        if (!entry) return -1;
        entry->flag=BOUND|DETERMINED;
        entry->element.type=FUNCTION_TYPE;
        entry->element.unit.function=funcs[i].function;
        entry->element.unit.pair.name=entry;
    }
    return 0;
}

void out_element(ELEMENT *elem,int typ,FILE *f)
{
    switch (elem->type) {
    case NULL_TYPE:
        fpts("null",f);
        break;
    case TRUE_TYPE:
        fpts("true",f);
        break;
    case ARRAY_TYPE: {
        int n;
        fpts("[ ",f);
        n=elem->unit.line.length;
        elem=elem->unit.array;
        while (n--) {
            out_element(elem++,typ,f);
            fptc(' ',f);
        }
        fptc(']',f);
        break;
    }
    case BLOCK_TYPE:
        elem=elem->unit.block;
        if (typ) {
            char going=0;
            int n=out_level;
            while (n--) fptc('\t',f);
            fpts("{\n",f);
            while (elem->type) {
                switch (elem->type) {
                case BLOCK_TYPE:
                    out_level++;
                    if (going) {
                        fptc('\n',f);
                        going=0;
                    }
                    out_element(elem,typ,f);
                    out_level--;
                    break;
                case QUICK_TYPE:
                case FUNCTION_TYPE:
                    if (going) {
                        fptc(' ',f);
                        going=0;
                    }
                    else {
                        n=out_level+1;
                        while (n--) fptc('\t',f);
                    }
                    out_element(elem,typ,f);
                    fptc('\n',f);
                    break;
                default:
                    if (!going) {
                        n=out_level+1;
                        while (n--) fptc('\t',f);
                        going=1;
                    }
                    else fptc(' ',f);
                    out_element(elem,typ,f);
                }
                elem++;
            }
            if (going) fptc('\n',f);
            n=out_level;
            while (n--) fptc('\t',f);
            if (out_level) fpts("}\n",f);
            else fptc('}',f);
        }
        else {
            fpts("{ ",f);
            while (elem->type) {
                out_element(elem++,typ,f);
                fptc(' ',f);
            }
            fptc('}',f);
        }
        break;
    case QUICK_TYPE:
        fprntf(f,"%s",elem->unit.pair.name->name);
        break;
    case FUNCTION_TYPE:
        fprntf(f,"%s",elem->unit.pair.name->name);
        break;
    case FLOATING_TYPE:
        fprntf(f,"%g",elem->unit.floating);
        break;
    case FRACTION_TYPE:
        fprntf(f,"%ld:%ld",elem->unit.fract.num,
               elem->unit.fract.den);
        break;
    case INTEGER_TYPE:
        fprntf(f,"%ld",elem->unit.integer);
        break;
    case UINTEGER_TYPE:
        fprntf(f,"%lu",elem->unit.uinteger);
        break;
    case COMPLEX_TYPE:
        fprntf(f,"%g;%g",elem->unit.compl.real,
               elem->unit.compl.imag);
        break;
    case FLOATARRAY_TYPE: {
        ULONG i;
        fpts("[@ ",f);
        for (i=0; i<elem->unit.line.length; i++)
            fprntf(f,"%g ",elem->unit.floatarray[i]);
        fptc(']',f);
        break;
    }
    case INTARRAY_TYPE: {
        ULONG i;
        fpts("[# ",f);
        for (i=0; i<elem->unit.line.length; i++)
            fprntf(f,"%ld ",elem->unit.intarray[i]);
        fptc(']',f);
        break;
    }
    case UINTARRAY_TYPE: {
        ULONG i;
        fpts("[#+ ",f);
        for (i=0; i<elem->unit.line.length; i++)
            fprntf(f,"%lu ",elem->unit.uintarray[i]);
        fptc(']',f);
        break;
    }
    case SINGLEARRAY_TYPE: {
        ULONG i;
        fpts("[& ",f);
        for (i=0; i<elem->unit.line.length; i++)
            fprntf(f,"%g ",elem->unit.singlearray[i]);
        fptc(']',f);
        break;
    }
    case SHORTARRAY_TYPE: {
        ULONG i;
        fpts("[$ ",f);
        for (i=0; i<elem->unit.line.length; i++)
            fprntf(f,"%d ",elem->unit.shortarray[i]);
        fptc(']',f);
        break;
    }
    case USHORTARRAY_TYPE: {
        ULONG i;
        fpts("[$+ ",f);
        for (i=0; i<elem->unit.line.length; i++)
            fprntf(f,"%u ",elem->unit.ushortarray[i]);
        fptc(']',f);
        break;
    }
    case STRING_TYPE: {
        ULONG l=elem->unit.line.length;
        UBYTE MQ *p=elem->unit.string;
        if (typ) {
            fptc('(',f);
            while (l>0&&*p) {
                if (*p < 32) fprntf(f,"\\x%02d",*p);
                else fptc(*p,f);
                l--;
                p++;
            }
            fptc(')',f);
        }
        else
            while (l>0&&*p) {
                fptc(*p,f);
                l--;
                p++;
            }
        break;
    }
    case ENTRY_TYPE:
        fpts(elem->unit.entry->name,f);
        break;
    case NAME_TYPE:
        fprntf(f,"/%s",elem->unit.name->name);
        break;
    case NOMEN_TYPE:
        fprntf(f,"@%p@",elem->unit.nomen);
        break;
    case FILE_TYPE:
        fprntf(f,"*%p*",elem->unit.file);
        break;
    case IMAGE_TYPE:
        fprntf(f,"#%p#",elem->unit.data);
        break;
    case VOICE_TYPE:
        fprntf(f,"&%p&",elem->unit.data);
        break;
    case SAVE_TYPE:
        fprntf(f,"$%p$",elem->unit.save);
        break;
    case STATE_TYPE:
        fprntf(f,"?%p?",elem->unit.state);
        break;
    case MARK_TYPE:
        fpts("mark",f);
        break;
    }
}

void error(int err_num)
{
    NOMENP np=NULL;
#ifdef XLIB
    xerror=0;
#endif
    level_number = 0;
    if (temp_mem) {
        memfree(temp_mem);
        temp_mem=NULL;
    }
    if (exec_stack[est-1].element.type==FILE_TYPE)
        file_pos=ftll(exec_stack[est-1].element.unit.file);
    if (!stop_level) {
        exec_entry=search("error",&np);
        if (!exec_entry) {
            fprntf(stdout,"wake error %d",err_num);
            if (err_elem.type) {
                fptc(' ',stdout);
                out_element(&err_elem,0,stdout);
            }
            if (file_pos!= -1)
                fprntf(stdout," %ld",file_pos);
            if (file_name) fprntf(stdout," %s",file_name);
            fprntf(stdout,"\n");
            err_elem.type=0;
            wk_quit();
        }
    }
    if (file_name) {
        errfnameent->element.flag=INDEXED;
        errfnameent->element.type=STRING_TYPE;
        errfnameent->element.unit.sstring=file_name;
        errfnameent->element.unit.line.length=strlen(file_name);
    }
    else {
        errfnameent->element.flag=0;
        errfnameent->element.type=NULL_TYPE;
    }
    if (file_pos!= -1) {
        errfposent->element.flag=NUMBER;
        errfposent->element.type=INTEGER_TYPE;
        errfposent->element.unit.integer=file_pos;
    }
    else {
        errfposent->element.flag=0;
        errfposent->element.type=NULL_TYPE;
    }
    if (!err_elem.type) {
        if (eel&&(*eel)->type==FUNCTION_TYPE) {
            err_elem.flag=0;
            err_elem.type=NAME_TYPE;
            err_elem.unit.name=(*eel)->unit.pair.name;
            err_elem.unit.site.nomen=system_nomen;
        }
    }
    errelement->element=err_elem;
    errnument->element.flag=INTEGER;
    errnument->element.type=INTEGER_TYPE;
    errnument->element.unit.integer=(long)err_num;
    err_elem.type=0;
    if (stop_level) {
        stack(1);
        wsp->flag=0;
        wsp->type=TRUE_TYPE;
        wsp++;
        jump_exec(stop_est);
        longjmp(stop_jump,WAKE_STOP);
    }
    exec_nomen=np;
    entry_exec();
    wk_quit();
}

void entry_exec(void)
{
    if (exec_entry->flag&BOUND)
        switch (exec_entry->element.type) {
        case FUNCTION_TYPE:
            (*exec_entry->element.unit.function)();
            return;
        case BLOCK_TYPE:
        case QUICK_TYPE:
            push(&exec_entry->element);
            exec_stack[est-1].element.unit.head[-1].prot++;
            execute();
            exec_stack[est-1].element.unit.head[-1].prot--;
            eel= &exec_stack[--est-1].exec_elem;
            return;
        default:
            stack(1);
            *wsp++=exec_entry->element;
            return;
        }
    else if (nsp[-1]!=exec_nomen) {
        ENTRY *en=ssearch(exec_entry->name,nsp[-1]->entry);
        if (en) {
            exec_entry=en;
            exec_nomen=nsp[-1];
            entry_exec();
            return;
        }
    }
    err_elem.flag=0;
    err_elem.type=ENTRY_TYPE;
    err_elem.unit.name=exec_entry;
    err_elem.unit.site.nomen=exec_nomen;
    error(WAKE_ENTRY_NOT_BOUND);
}

void execute(void)
{
    *eel=exec_stack[est-1].element.unit.block;
    while ((*eel)->type) {
        switch ((*eel)->type) {
        case FUNCTION_TYPE:
            (*(*eel)->unit.function)();
            break;
        case QUICK_TYPE:
            push(*eel);
            exec_stack[est-1].element.unit.head[-1].prot++;
            execute();
            exec_stack[est-1].element.unit.head[-1].prot--;
            eel = &exec_stack[--est-1].exec_elem;
            break;
        case ENTRY_TYPE:
            exec_entry=(*eel)->unit.entry;
            exec_nomen=(*eel)->unit.site.nomen;
            entry_exec();
            break;
        case ARRAY_TYPE:
            if (!((*eel)->flag & FIXED) && (*eel)->flag & TEMPORARY) {
                ELEMENT *array, *q;
                unsigned n, top = wsp - wake_stack;
                push(*eel);
                exec_stack[est-1].element.unit.head[-1].prot++;
                execute();
                exec_stack[est-1].element.unit.head[-1].prot--;
                eel = &exec_stack[--est-1].exec_elem;
                n = (wsp - wake_stack) - top;
                q = wsp - n;
                array = (ELEMENT *)get_mem(n + 1, ARRAY_TYPE, TRUE);
                memcpy(array, q, (int)n * sizeof(ELEMENT));
                wsp = q;
                stack(1);
                wsp->unit.line.length = n;
                wsp->type = ARRAY_TYPE;
                wsp->flag = COMPOSITE|INDEXED;
                wsp->unit.array = array;
                wsp++;
                break;
            }
        default:
            stack(1);
            *wsp++= **eel;
        }
        (*eel)++;
    }
}

void wake(void)
{
    ELEMENT elem;
    comment_entry=search("comment",&comment_nomen);
    while (token(&elem)!=EOF)
        if (elem.type==ENTRY_TYPE) {
            eel=NULL;
            if (!elem.unit.entry) unknown_name_error(strbuf);
            exec_entry=elem.unit.entry;
            exec_nomen=elem.unit.site.nomen;
            entry_exec();
        }
        else {
            stack(1);
            *wsp=elem;
            wsp++;
        }
}
