
/* File Access routines */
/* Set Key module */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "file_acs.h"

extern FA_PAGE_STACK fa_page_stk;
extern FA_PAGE_MAP fa_pg_map;

int index_key(char *key,char *area,DATA_DEF *data_def,long ref)
{
	int i;
	switch (data_def->dty)
	{
		case dt_byte:
		case dt_sbyte:
		case dt_tval:
			*key= *area;
			return sizeof(char);
		case dt_char:
		case dt_uchar:
			*key=toupp(*area);
			return sizeof(char);
		case dt_short:
		case dt_ushort:
		case dt_ushort_code:
#ifdef _MSDOS
			zwab(area,key,sizeof(short));
			return sizeof(short);
		case dt_long:
		case dt_ulong:
		case dt_date:
		case dt_time:
			zwab(area,key,sizeof(long));
			return sizeof(long);
#else
			memcpy(key,area,sizeof(short));
			return sizeof(short);
		case dt_long:
		case dt_ulong:
		case dt_date:
		case dt_time:
			memcpy(key,area,sizeof(long));
			return sizeof(long);
#endif
		case dt_string:
			for (i=0; i<data_def->len&&area[i]; i++)
				key[i]=toupp(area[i]);
			if (i<data_def->len) memset(key+i,0,data_def->len-i);
			return data_def->len;
		case dt_ref:
#ifdef _MSDOS
			zwab((char *)&ref,key,sizeof(long));
#else
			memcpy(key,&ref,sizeof(long));
#endif
			return sizeof(long);
		case dt_stamp:
#ifdef _MSDOS
			swab(area,key,sizeof(short));
			memcpy(key+sizeof(short),area+sizeof(short),
			sizeof(STAMP)-sizeof(short));
#else
			memcpy(key,area,sizeof(STAMP));
#endif
			return sizeof(STAMP);
		case dt_float:
			memcpy(key,area,sizeof(float));
			return sizeof(float);
		case dt_double:
			memcpy(key,area,sizeof(double));
			return sizeof(double);
	}
	return -1;
}

char *build_key(char *key,DATA_DEF ddefs[],int num_of_ddefs,long ref)
{
	int n,x=0;
	for (n=0; n<num_of_ddefs; n++)
	x+=index_key(key+x,ddefs[n].ptr,ddefs+n,ref);
	return key;
}

void recover_index(INDEX_FILE *idx_f,char *f_name,int key_len,int s,void *rec,
DATA_FILE *dat_f,DATA_DEF ddefs[],int num_of_ddefs)
{
	long ref;
	KEY_STR key;
	make_index(idx_f,f_name,key_len,s);
	for (ref=1; ref<file_len(dat_f); ref++)
	{
		get_rec(dat_f,ref,rec);
		if (!*(long *)rec) add_key(idx_f,&ref,
		build_key(key,ddefs,num_of_ddefs,ref));
	}
	update_file(&idx_f->data_f);
}

void open_file_index(char *name,DATA_FILE *dat_f,int rec_len,
INDEX_FILE *idx_f,int key_len,int s,void *rec,DATA_DEF *ddefs,int n_ddefs)
{
	FIL_STR f_name;
	strcat(strcpy(f_name,name),".DTA");
	if (open_file(dat_f,f_name,rec_len))
	{
		make_file(dat_f,f_name,rec_len);
		strcat(strcpy(f_name,name),".IDX");
		make_index(idx_f,f_name,key_len,s);
	}
	else
	{
		strcat(strcpy(f_name,name),".IDX");
		if (open_index(idx_f,f_name,key_len,s))
		recover_index(idx_f,f_name,key_len,s,rec,dat_f,ddefs,n_ddefs);
	}
}

int reduce_file(DATA_FILE *dat_f,char *f_name,int rec_len,void *rec)
{
	long ref,s_ref;
	FIL_STR s_f_name;
	DATA_FILE s_dat_f;
	strcpy(s_f_name,f_name);
	strcpy(s_f_name+strspn(s_f_name,"."),".$$$");
	if (rename(f_name,s_f_name)) access_error(dat_f->fa_name);
	if (open_file(&s_dat_f,s_f_name,rec_len)) return 1;
	make_file(dat_f,f_name,rec_len);
	for (s_ref=1; s_ref<file_len(&s_dat_f); s_ref++)
	{
		get_rec(&s_dat_f,s_ref,&rec);
		if (!*(long *)rec) add_rec(dat_f,&ref,rec);
	}
	close_file(&s_dat_f);
	if (remove(s_f_name)) access_error(dat_f->fa_name);
	return 0;
}

int merge_file(DATA_FILE *dat_f,char *f_name,int rec_len,void *rec)
{
	long ref,s_ref;
	DATA_FILE s_dat_f;
	if (open_file(&s_dat_f,f_name,rec_len)) return 1;
	for (s_ref=1; s_ref<file_len(&s_dat_f); s_ref++)
	{
		get_rec(&s_dat_f,s_ref,rec);
		if (!*(long *)rec) add_rec(dat_f,&ref,rec);
	}
	close_file(&s_dat_f);
	return 0;
}

void rescue_file(DATA_FILE *dat_f)
{
	long ref,status=0;
	for (ref=1; ref<file_len(dat_f); ref++)
	{
		fa_seekrec(dat_f,ref);
		if (fwrite((void *)&status,sizeof status,1,dat_f->f)!=1)
		access_error(dat_f->fa_name);
	}
}

