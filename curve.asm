
		section	text,code

		xdef	_curve_area
		xdef	_curve_line
		xref	_LVOAreaDraw
		xref	_LVODraw

SHIFTS		equ	9
ONE		equ	(1<<SHIFTS)

mult		macro	* Dm,Dn
		move.l	\2,-(sp)
		move.l	\1,-(sp)
		swap	\2
		mulu.w	\1,\2
		swap	\1
		mulu.w	2(sp),\1
		add.l	\1,\2
		swap	\2
		clr.w	\2
		move.w	2(sp),\1
		mulu.w	6(sp),\1
		add.l	\1,\2
		move.l	(sp)+,\1
		addq.l	#4,sp
		endm

mult2		macro	* <a>,Dn ; n!=2
		move.l	\2,-(sp)
		move.l	d2,-(sp)
		move.l	\1,d2
		swap	\2
		mulu.w	d2,\2
		swap	d2
		mulu.w	2+\1,d2
		add.l	d2,\2
		swap	\2
		clr.w	\2
		move.w	2+\1,d2
		mulu.w	6(sp),d2
		add.l	d2,\2
		move.l	(sp)+,d2
		addq.l	#4,sp
		endm

draw_curve	macro	* rastport,v,s,GfxBase
		movem.l	d1-d7/a1/a2/a6,-(sp)
		move.l	48(sp),a2	; v
		move.l	52(sp),d2	; s
		move.l	d2,d3		; t=s
\1_10:		move.l	d3,d4		; tt=t
		mult	d3,d4		; tt=t*t
		move.l	d4,d5		; ttt=tt
		mult	d3,d5		; ttt=tt*t
		move.l	d5,d6		; ttt3=ttt
		move.l	d5,d0		; ttt3s=ttt
		asl.l	#1,d0		; ttt3s<<=1
		add.l	d0,d6		; ttt3+=ttt3s
		move.w	#2*SHIFTS,d0
		asr.l	d0,d6		; ttt3>>=2*SHIFTS
		move.l	d4,d7		; tt3=tt
		move.l	d4,d0		; tt3s=tt
		asl.l	#1,d0		; tt3s<<=1
		add.l	d0,d7		; tt3+=tt3s
		move.w	#SHIFTS,d0
		asr.l	d0,d7		; tt3>>=SHIFTS
		move.l	d3,d4		; t3=t
		move.l	d3,d0		; t3s=t
		asl.l	#1,d0		; t3s<<=1
		add.l	d0,d4		; t3+=t3s
		move.w	#2*SHIFTS,d0
		asr.l	d0,d5		; ttt>>=2*SHIFTS
		move.l	d5,d0		; mr[0]=ttt
		neg.l	d0		; mr[0]=-ttt
		add.l	d7,d0		; mr[0]+=tt3
		sub.l	d4,d0		; mr[0]-=t3
		addi.l	#ONE,d0		; mr[0]+=ONE
		move.l	d6,d1		; mr[2]=ttt3
		neg.l	d1		; mr[2]=-ttt3
		add.l	d7,d1		; mr[2]+=tt3
		asl.l	#1,d7		; tt3s=tt3<<1
		neg.l	d7		; mr[1]=-tt3s
		add.l	d6,d7		; mr[1]+=ttt3
		add.l	d4,d7		; mr[1]+=t3
		move.l	d0,d4		; mr[0]
		move.l	d1,d6		; mr[2]
		mult2	0(a2),d0	; x=xa=mr[0]*v[0][0]
		move.l	d7,d1		; mr[1]
		mult2	8(a2),d1	; xb=mr[1]*v[1][0]
		add.l	d1,d0		; x+=xb
		move.l	d6,d1		; mr[2]
		mult2	16(a2),d1	; xc=mr[2]*v[2][0]
		add.l	d1,d0		; x+=xc
		move.l	d5,d1		; ttt
		mult2	24(a2),d1	; xd=ttt*v[3][0]
		add.l	d1,d0		; x+=xd
		move.l	#SHIFTS,d1
		asr.l	d1,d0		; x>>=SHIFTS
		mult2	28(a2),d5	; yd=ttt*v[3][1]
		move.l	d5,d1		; y=yd
		mult2	4(a2),d4	; ya=mr[0]*v[0][1]
		add.l	d4,d1		; y+=ya
		mult2	12(a2),d7	; yb=mr[1]*v[1][1]
		add.l	d7,d1		; y+=yb
		mult2	20(a2),d6	; yc=mr[2]*v[2][1]
		add.l	d6,d1		; y+=yc
		move.l	#SHIFTS,d7
		asr.l	d7,d1		; y>>=SHIFTS
		move.l	44(sp),a1	; rastport
		move.l	56(sp),a6	; GfxBase
		jsr	_LVO\1(a6)
		ifc	'\1','AreaDraw'
		tst.l	d0
		bne	\1_20
		endc
		add.w	d2,d3
		cmp.w	#ONE,d3
		ble	\1_10
		moveq.l	#0,d0
\1_20:		movem.l	(sp)+,d1-d7/a1/a2/a6
		rts
		endm

_curve_area:	draw_curve AreaDraw
_curve_line:	draw_curve Draw

		end
