
/* File Access routines */
/* Delete Key module */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "file_acs.h"

extern FA_PAGE_STACK fa_page_stk;
extern FA_PAGE_MAP fa_pg_map;

void fa_underflow(INDEX_FILE *idx_f,long pr_pg_ref,long pr_pg_ref2,
int r,int *page_too_small)
{
	int i,k,l_item;
	long l_page_ref;
	FA_PAGE_PTR pag_ptr,page_ptr2,l;
	fa_get_page(idx_f,pr_pg_ref,&pag_ptr);
	fa_get_page(idx_f,pr_pg_ref2,&page_ptr2);
	if (r<pag_ptr->items_on_page-1)
	{
		r++;
		l_page_ref=pag_ptr->item_array[r].page_ref;
		fa_get_page(idx_f,l_page_ref,&l);
		k=(l->items_on_page-ORDER+1)/2-1;
		page_ptr2->item_array[ORDER-1]=pag_ptr->item_array[r];
		page_ptr2->item_array[ORDER-1].page_ref=l->bckw_page_ref;
		if (k>-1)
		{
			for (i=0; i<k; i++)
			page_ptr2->item_array[i+ORDER]=l->item_array[i];
			pag_ptr->item_array[r]=l->item_array[k];
			pag_ptr->item_array[r].page_ref=l_page_ref;
			l->bckw_page_ref=l->item_array[k].page_ref;
			l->items_on_page=(short)(l->items_on_page-k-1);
			for (i=0; i<l->items_on_page; i++)
			l->item_array[i]=l->item_array[i+k+1];
			page_ptr2->items_on_page=(short)(ORDER+k);
			*page_too_small=0;
			fa_update_page(l);
		}
		else
		{
			for (i=0; i<ORDER; i++)
			page_ptr2->item_array[i+ORDER]=l->item_array[i];
			for (i=r; i<pag_ptr->items_on_page-1; i++)
			pag_ptr->item_array[i]=pag_ptr->item_array[i+1];
			page_ptr2->items_on_page=PAGE_SIZE;
			pag_ptr->items_on_page--;
			fa_return_page(&l);
			*page_too_small=(pag_ptr->items_on_page<ORDER);
		}
		fa_update_page(page_ptr2);
	}
	else
	{
		if (r==0) l_page_ref=pag_ptr->bckw_page_ref;
		else l_page_ref=pag_ptr->item_array[r-1].page_ref;
		fa_get_page(idx_f,l_page_ref,&l);
		l_item=l->items_on_page;
		k=(l_item-ORDER+1)/2;
		if (k>=0)
		{
			for (i=ORDER-2; i>=0; i--) page_ptr2->
			item_array[i+k+1]=page_ptr2->item_array[i];
			page_ptr2->item_array[k]=pag_ptr->item_array[r];
			page_ptr2->item_array[k].page_ref=
			page_ptr2->bckw_page_ref;
			l_item=l_item-k-1;
			for (i=k-1; i>=0; i--)
			page_ptr2->item_array[i]=l->item_array[i+l_item+1];
			page_ptr2->bckw_page_ref=
			l->item_array[l_item].page_ref;
			pag_ptr->item_array[r]=l->item_array[l_item];
			pag_ptr->item_array[r].page_ref=pr_pg_ref2;
			l->items_on_page=(short)l_item;
			page_ptr2->items_on_page=(short)(ORDER+k);
			*page_too_small=0;
			fa_update_page(page_ptr2);
		}
		else
		{
			l->item_array[l_item]=pag_ptr->item_array[r];
			l->item_array[l_item].page_ref=
			page_ptr2->bckw_page_ref;
			for (i=0; i<ORDER-1; i++)
			l->item_array[i+l_item+1]=page_ptr2->item_array[i];
			l->items_on_page=PAGE_SIZE;
			pag_ptr->items_on_page--;
			fa_return_page(&page_ptr2);
			*page_too_small=(pag_ptr->items_on_page<ORDER);
		}
		fa_update_page(l);
	}
	fa_update_page(pag_ptr);
}

void fa_del_a(INDEX_FILE *idx_f,long pr_pg_ref2,long pr_pg_ref,
FA_PAGE_PTR *pag_ptr,int *k,int *page_too_small)
{
	int c;
	long x_page_ref;
	FA_PAGE_PTR page_ptr2;
	fa_get_page(idx_f,pr_pg_ref2,&page_ptr2);
	x_page_ref=page_ptr2->item_array[page_ptr2->items_on_page-1].page_ref;
	if (x_page_ref)
	{
		c=page_ptr2->items_on_page-1;
		fa_del_a(idx_f,x_page_ref,pr_pg_ref,pag_ptr,k,
		page_too_small);
		if (*page_too_small) fa_underflow(idx_f,
		pr_pg_ref2,x_page_ref,c,page_too_small);
	}
	else
	{
		fa_get_page(idx_f,pr_pg_ref,pag_ptr);
		page_ptr2->item_array[page_ptr2->items_on_page-1].page_ref=
		(*pag_ptr)->item_array[*k].page_ref;
		(*pag_ptr)->item_array[*k]=
		page_ptr2->item_array[page_ptr2->items_on_page-1];
		page_ptr2->items_on_page--;
		*page_too_small=(page_ptr2->items_on_page<ORDER);
		fa_update_page(*pag_ptr);
		fa_update_page(page_ptr2);
	}
}

int fa_del_b(INDEX_FILE *idx_f,long pr_pg_ref,long *proc_dat_ref,
int *page_too_small,char *proc_key)
{
	long c;
	int i,k,l,r;
	long x_page_ref;
	FA_PAGE_PTR pag_ptr;
	if (pr_pg_ref==0)
	{
		*page_too_small=0;
		return 1;
	}
	fa_get_page(idx_f,pr_pg_ref,&pag_ptr);
	l=0;
	r=pag_ptr->items_on_page-1;
	do
	{
		k=(l+r)/2;
		c=fa_comp_keys(proc_key,pag_ptr->item_array[k].key,
		*proc_dat_ref,pag_ptr->item_array[k].data_ref,
		idx_f->allow_dupl_keys,idx_f->key_l);
		if (c<=0) r=k-1;
		if (c>=0) l=k+1;
	}
	while (l<=r);
	if (r== -1) x_page_ref=pag_ptr->bckw_page_ref;
	else x_page_ref=pag_ptr->item_array[r].page_ref;
	if (l-r>1)
	{
		*proc_dat_ref=pag_ptr->item_array[k].data_ref;
		if (x_page_ref==0)
		{
			pag_ptr->items_on_page--;
			*page_too_small=
			(pag_ptr->items_on_page<ORDER);
			for (i=k; i<pag_ptr->items_on_page; i++)
			pag_ptr->item_array[i]=
			pag_ptr->item_array[i+1];
			fa_update_page(pag_ptr);
		}
		else
		{
			fa_del_a(idx_f,x_page_ref,pr_pg_ref,
			&pag_ptr,&k,page_too_small);
			if (*page_too_small) fa_underflow(idx_f,
			pr_pg_ref,x_page_ref,r,page_too_small);
		}
	}
	else
	{
		fa_del_b(idx_f,x_page_ref,proc_dat_ref,
		page_too_small,proc_key);
		if (*page_too_small) fa_underflow(idx_f,
		pr_pg_ref,x_page_ref,r,page_too_small);
	}
	return 0;
}

int delete_key(INDEX_FILE *idx_f,long *proc_dat_ref,char *proc_key)
{
	int page_too_small;
	FA_PAGE_PTR pag_ptr;
	if (fa_del_b(idx_f,idx_f->rr,proc_dat_ref,&page_too_small,proc_key))
	{
		idx_f->pp= -1;
		return 1;
	}
	if (page_too_small)
	{
		fa_get_page(idx_f,idx_f->rr,&pag_ptr);
		if (pag_ptr->items_on_page==0)
		{
			idx_f->rr=pag_ptr->bckw_page_ref;
			fa_return_page(&pag_ptr);
		}
	}
	idx_f->pp= -1;
	return 0;
}

