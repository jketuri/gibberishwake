
/* File Access routines */
/* Get Key module */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "file_acs.h"

extern FA_PAGE_STACK fa_page_stk;
extern FA_PAGE_MAP fa_pg_map;

int next_key(INDEX_FILE *idx_f,long *proc_dat_ref,char *proc_key)
{
	long r;
	FA_PAGE_PTR pag_ptr;
	if (idx_f->pp== -1) r=idx_f->rr;
	else
	{
		fa_get_page(idx_f,idx_f->path[idx_f->pp].page_ref,&pag_ptr);
		r=pag_ptr->item_array[idx_f->path[idx_f->pp].
		item_arr_index].page_ref;
	}
	while (r!=0)
	{
		idx_f->pp++;
		idx_f->path[idx_f->pp].page_ref=r;
		idx_f->path[idx_f->pp].item_arr_index= -1;
		fa_get_page(idx_f,r,&pag_ptr);
		r=pag_ptr->bckw_page_ref;
	}
	if (idx_f->pp!= -1)
	{
		while (idx_f->pp>0&&idx_f->path[idx_f->pp].
		item_arr_index==pag_ptr->items_on_page-1)
		{
			idx_f->pp--;
			fa_get_page(idx_f,idx_f->path[idx_f->pp].page_ref,
			&pag_ptr);
		}
		if (idx_f->path[idx_f->pp].
		item_arr_index<pag_ptr->items_on_page-1)
		{
			idx_f->path[idx_f->pp].item_arr_index++;
			memcpy(proc_key,
			pag_ptr->item_array[idx_f->path[idx_f->pp].
			item_arr_index].key,idx_f->key_l);
			*proc_dat_ref=pag_ptr->item_array[idx_f->
			path[idx_f->pp].item_arr_index].data_ref;
		}
		else idx_f->pp= -1;
	}
	return (idx_f->pp== -1);
}

int prev_key(INDEX_FILE *idx_f,long *proc_dat_ref,char *proc_key)
{
	long r;
	FA_PAGE_PTR pag_ptr;
	if (idx_f->pp== -1) r=idx_f->rr;
	else
	{
		fa_get_page(idx_f,idx_f->path[idx_f->pp].page_ref,&pag_ptr);
		idx_f->path[idx_f->pp].item_arr_index--;
		if (idx_f->path[idx_f->pp].item_arr_index== -1)
		r=pag_ptr->bckw_page_ref;
		else r=pag_ptr->item_array[idx_f->path[idx_f->pp].
		item_arr_index].page_ref;
	}
	while (r!=0)
	{
		fa_get_page(idx_f,r,&pag_ptr);
		idx_f->pp++;
		idx_f->path[idx_f->pp].page_ref=r;
		idx_f->path[idx_f->pp].item_arr_index=
		(short)(pag_ptr->items_on_page-1);
		r=pag_ptr->item_array[pag_ptr->items_on_page-1].page_ref;
	}
	if (idx_f->pp!= -1)
	{
		while ((idx_f->pp>0)&&
		(idx_f->path[idx_f->pp].item_arr_index== -1))
		{
			idx_f->pp--;
			fa_get_page(idx_f,idx_f->path[idx_f->pp].page_ref,
			&pag_ptr);
		}
		if (idx_f->path[idx_f->pp].item_arr_index>-1)
		{
			memcpy(proc_key,
			pag_ptr->item_array[idx_f->path[idx_f->pp].
			item_arr_index].key,idx_f->key_l);
			*proc_dat_ref=pag_ptr->item_array[idx_f->
			path[idx_f->pp].item_arr_index].data_ref;
		}
		else idx_f->pp= -1;
	}
	return (idx_f->pp== -1);
}

int fa_find_key(INDEX_FILE *idx_f,long *proc_dat_ref,char *proc_key)
{
	long c;
	int k,l,r;
	long pr_pg_ref;
	FA_PAGE_PTR pag_ptr;
	idx_f->pp= -1;
	pr_pg_ref=idx_f->rr;
	while (pr_pg_ref!=0)
	{
		idx_f->pp++;
		idx_f->path[idx_f->pp].page_ref=pr_pg_ref;
		fa_get_page(idx_f,pr_pg_ref,&pag_ptr);
		l=0;
		r=pag_ptr->items_on_page-1;
		do
		{
			k=(l+r)/2;
			c=fa_comp_keys(proc_key,pag_ptr->item_array[k].key,0,
			pag_ptr->item_array[k].data_ref,
			idx_f->allow_dupl_keys,idx_f->key_l);
			if (c<=0) r=k-1;
			if (c>=0) l=k+1;
		}
		while (r>=l);
		if (l-r>1)
		{
			*proc_dat_ref=pag_ptr->item_array[k].data_ref;
			idx_f->path[idx_f->pp].item_arr_index=(short)k;
			return 0;
		}
		if (r== -1) pr_pg_ref=pag_ptr->bckw_page_ref;
		else pr_pg_ref=pag_ptr->item_array[r].page_ref;
		idx_f->path[idx_f->pp].item_arr_index=(short)r;
	}
	if (idx_f->pp>-1)
	{
		while ((idx_f->pp>0)&&
		(idx_f->path[idx_f->pp].item_arr_index== -1)) idx_f->pp--;
		if (idx_f->path[idx_f->pp].item_arr_index== -1) idx_f->pp= -1;
	}
	return 1;
}

int find_key(INDEX_FILE *idx_f,long *proc_dat_ref,char *proc_key)
{
	KEY_STR temp_key;
	if (fa_find_key(idx_f,proc_dat_ref,proc_key)&&idx_f->allow_dupl_keys)
	{
		memcpy(temp_key,proc_key,idx_f->key_l);
		return (!next_key(idx_f,proc_dat_ref,proc_key)&&
		!memcmp(proc_key,temp_key,idx_f->key_l));
	}
	return 0;
}

int search_key(INDEX_FILE *idx_f,long *proc_dat_ref,char *proc_key)
{
	if (fa_find_key(idx_f,proc_dat_ref,proc_key))
	return next_key(idx_f,proc_dat_ref,proc_key);
	return 0;
}

int search_the_key(INDEX_FILE *idx_f,long *proc_dat_ref,char *proc_key,
int key_len)
{
	KEY_STR temp_key;
	memcpy(temp_key,proc_key,key_len);
	return search_key(idx_f,proc_dat_ref,proc_key)||
	memcmp(proc_key,temp_key,key_len);
}

int get_point(INDEX_FILE *idx_f,long *proc_dat_ref,char *proc_key)
{
	return prev_key(idx_f,proc_dat_ref,proc_key)||
	next_key(idx_f,proc_dat_ref,proc_key);
}

int first_key(INDEX_FILE *idx_f,long *proc_dat_ref,char *proc_key)
{
	clear_key(idx_f);
	return next_key(idx_f,proc_dat_ref,proc_key);
}

int last_key(INDEX_FILE *idx_f,long *proc_dat_ref,char *proc_key)
{
	clear_key(idx_f);
	return prev_key(idx_f,proc_dat_ref,proc_key);
}

int set_point_to_end(INDEX_FILE *idx_f)
{
	long r;
	KEY_STR key;
	return last_key(idx_f,&r,key)||next_key(idx_f,&r,key);
}

int set_point_to_beginning(INDEX_FILE *idx_f)
{
	long r;
	KEY_STR key;
	return first_key(idx_f,&r,key)||prev_key(idx_f,&r,key);
}

