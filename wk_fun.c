
/* wk_fun.c */

#include "wake.h"
#define MAIN
#undef VARIABLE
#include "wk_fun.h"

REGQUAL void jump_exec(register unsigned t)
{
    while (est>t) {
        est--;
        if (exec_stack[est].element.flag&COMPOSITE)
            exec_stack[est].element.unit.head[-1].prot--;
    }
    eel= &exec_stack[est-1].exec_elem;
}

void access_error(char *string)
{
    err_elem.flag=INDEXED;
    err_elem.type=STRING_TYPE;
    err_elem.unit.sstring=string;
    err_elem.unit.line.length=strlen(string);
    error(WAKE_CANNOT_ACCESS);
}

void copy_entries(ENTRY *entry, NOMENP nomen)
{
    ENTRY *en;
    if (entry->l) copy_entries(entry->l, nomen);
    if (entry->r) copy_entries(entry->r, nomen);
    if (!(en=ssearch(entry->name, nomen->entry))) {
        if (!(en=enter(entry->name, nomen)))
            error(WAKE_CANNOT_ALLOCATE);
        en->flag|=BORROWED;
    }
    else if (en->flag&BOUND&&en->element.flag&COMPOSITE)
        en->element.unit.head[-1].prot--;
    en->element=entry->element;
    if (en->element.flag&COMPOSITE) en->element.unit.head[-1].prot++;
    en->flag=(UBYTE)(en->flag|(BOUND&(0xff^DETERMINED)));
}

void free_composite(HEAD *head)
{
    switch (head->type) {
#if !defined(VAXC) || defined(XLIB)
    case IMAGE_TYPE:
        free_image((void *)(head+1));
        break;
    case STATE_TYPE:
        free_state((STATE *)(head+1));
        break;
#endif
#ifndef VAXC
    case VOICE_TYPE:
        free_voice((void *)(head+1));
        break;
#endif
#ifdef AMIGA
    case WAVE_TYPE:
        FreeVec(head);
        return;
#endif
    }
    memfree(head);
}

void cvs_element(ELEMENT *elem, char *s)
{
    switch (elem->type) {
    case NULL_TYPE:
        strcpy(s, "null");
        break;
    case TRUE_TYPE:
        strcpy(s, "true");
        break;
    case QUICK_TYPE:
        sprintf(s, "%s", elem->unit.pair.name->name);
        break;
    case FUNCTION_TYPE:
        sprintf(s, "%s", elem->unit.pair.name->name);
        break;
    case FLOATING_TYPE:
        sprintf(s, "%g", elem->unit.floating);
        break;
    case FRACTION_TYPE:
        sprintf(s, "%ld/%ld", elem->unit.fract.num, elem->unit.fract.den);
        break;
    case INTEGER_TYPE:
        sprintf(s, "%ld", elem->unit.integer);
        break;
    case UINTEGER_TYPE:
        sprintf(s, "%lu", elem->unit.uinteger);
        break;
    case COMPLEX_TYPE:
        sprintf(s, "%g:%g", elem->unit.compl.real, elem->unit.compl.imag);
        break;
    case STRING_TYPE:
    case WAVE_TYPE:
        sprintf(s, "%.*s", (int)elem->unit.line.length, elem->unit.sstring);
        break;
    case ENTRY_TYPE:
        strcpy(s, elem->unit.entry->name);
        break;
    case NAME_TYPE:
        sprintf(s, "/%s", elem->unit.name->name);
        break;
    case NOMEN_TYPE:
        sprintf(s, "+%p+", (void *)elem->unit.nomen);
        break;
    case FILE_TYPE:
        sprintf(s, "*%p*", (void *)elem->unit.file);
        break;
    case IMAGE_TYPE:
        sprintf(s, "#%p#", elem->unit.data);
        break;
    case VOICE_TYPE:
        sprintf(s, "&%p&", elem->unit.data);
        break;
    case SAVE_TYPE:
        sprintf(s, "$%p$", (void *)elem->unit.save);
        break;
    default:
        error(WAKE_INVALID_TYPE);
    }
}

void set_entry(int way)
{
    ENTRY *en;
    check(2);
    if (wsp[-2].type!=NAME_TYPE) error(WAKE_INVALID_TYPE);
    if (way&1||nsp[-1]==wsp[-2].unit.site.nomen) en=wsp[-2].unit.entry;
    else if (!(en=ssearch(wsp[-2].unit.entry->name, nsp[-1]->entry))) {
        if (!(en=enter(wsp[-2].unit.entry->name, nsp[-1])))
            error(WAKE_CANNOT_ALLOCATE);
        en->flag|=BORROWED;
    }
    else if (en->flag&BOUND&&en->element.flag&COMPOSITE)
        en->element.unit.head[-1].prot--;
    en->element=wsp[-1];
    if (en->element.flag&COMPOSITE) en->element.unit.head[-1].prot++;
    if (way&2) {
        if (!wsp[-1].type) error(WAKE_ILLEGAL_USE);
        en->flag|=BOUND|DETERMINED;
    }
    else en->flag=(UBYTE)(en->flag|(BOUND&(0xff^DETERMINED)));
    wsp-=2;
}

int set_key(char *key)
{
#if defined(_MSDOS) || defined(_WINDOWS)
    zwab((char *)&wsp[-2].unit.integer, key, sizeof(long));
#else
    memcpy(key, &wsp[-2].unit.integer, sizeof(long));
#endif
    switch (wsp[-1].type) {
    case FLOATING_TYPE:
        memcpy(key+sizeof(long), &wsp[-1].unit.floating, sizeof(double));
        memset(key+sizeof(long)+sizeof(double), 0, BASE_KEY_SIZE-sizeof(long)-sizeof(double));
        return sizeof(long)+sizeof(double);
    case FRACTION_TYPE:
#if defined(_MSDOS) || defined(_WINDOWS)
        zwab((char *)&wsp[-1].unit.fract.num, key+sizeof(long), sizeof(long));
        zwab((char *)&wsp[-1].unit.fract.den, key+2*sizeof(long), sizeof(long));
#else
        memcpy(key+sizeof(long), &wsp[-1].unit.fract.num, sizeof(long));
        memcpy(key+2*sizeof(long), &wsp[-1].unit.fract.den, sizeof(long));
#endif
        memset(key+3*sizeof(long), 0, BASE_KEY_SIZE-3*sizeof(long));
        return 3*sizeof(long);
    case INTEGER_TYPE:
    case UINTEGER_TYPE:
#if defined(_MSDOS) || defined(_WINDOWS)
        zwab((char *)&wsp[-1].unit.integer, key+sizeof(long), sizeof(long));
#else
        memcpy(key+sizeof(long), &wsp[-1].unit.integer, sizeof(long));
#endif
        memset(key+2*sizeof(long), 0, BASE_KEY_SIZE-2*sizeof(long));
        return 2*sizeof(long);
    case STRING_TYPE:
    case WAVE_TYPE: {
        int l=sizeof(long)+strlen(wsp[-1].unit.sstring);
        memset(key+sizeof(long), 0, BASE_KEY_SIZE-sizeof(long));
        strncpy(key+sizeof(long), wsp[-1].unit.sstring, BASE_KEY_SIZE-sizeof(long));
        return (int)min(l, BASE_KEY_SIZE);
    }
    default:
        error(WAKE_INVALID_TYPE);
    }
    return -1;
}

#ifdef VAXC
void set_fields(void)
{
    fab=cc$rms_fab;
    fab.fab$b_fac=FAB$M_GET|FAB$M_PUT;
    fab.fab$l_fna=wsp[-1].unit.sstring;
    fab.fab$b_fns=strlen(wsp[-1].unit.sstring);
    fab.fab$l_fop=FAB$V_SUP;
    fab.fab$w_mrs=BASE_KEY_SIZE+sizeof(long);
    fab.fab$b_org=FAB$C_IDX;
    fab.fab$b_rfm=FAB$C_FIX;
    fab.fab$l_xab= &xabkey;
    rab=cc$rms_rab;
    rab.rab$l_fab= &fab;
    xabkey=cc$rms_xabkey;
    xabkey.xab$b_flg=XAB$M_DUP;
    xabkey.xab$w_pos0=0;
    xabkey.xab$b_ref=0;
    xabkey.xab$b_siz0=BASE_KEY_SIZE;
}
#endif

#ifdef _MSDOS
UBYTE MQ *bigmove(UBYTE MQ *target, UBYTE MQ *source, long length)
{
    long i, m;
    m=length%0xffff;
    length>>=16;
    for (i=0; i<length; i++, source+=0xffff, target+=0xffff)
        memmove(UN target, UN source, 0xffff);
    if (m) memmove(UN target, UN source, (size_t)m);
    return target;
}

long bigread(FILE *file, UBYTE MQ *target, long length)
{
    long i, m, n=0;
    m=length%0xffff;
    length>>=16;
    for (i=0; i<length; i++, target+=0xffff) {
        if ((n=fread(UN target, 1, 0xffff, file))!=0xffff) {
            if (ferror(file)) return 0;
            break;
        }
    }
    if (i==length) {
        if (m&& (length=fread(UN target, 1, (size_t)m, file))!=m) {
            if (ferror(file)) return 0;
            m=length;
        }
    }
    else m=0;
    return (i?(i-1)<<16:0L)+n+m;
}

long bigwrite(FILE *file, UBYTE MQ *source, long length)
{
    long i, m;
    m=length%0xffff;
    length>>=16;
    for (i=0; i<length; i++, source+=0xffff)
        if (fwrite(UN source, 1, 0xffff, file)!=0xffff) return 0;
    if (m&&fwrite(UN source, 1, (size_t)m, file)!=m) return 0;
    return length;
}
#endif

/* any - = - */
void wk_out1(void)
{
    check(1);
    out_element(wsp-1, FALSE, stdout);
    fprntf(stdout, "\n");
    wsp--;
}

/* any - == - */
void wk_out2(void)
{
    check(1);
    out_element(wsp-1, TRUE, stdout);
    fprntf(stdout, "\n");
    wsp--;
}

/* filename - access - boolean */
void wk_access(void)
{
    check(1);
    if (wsp[-1].type!=STRING_TYPE) error(WAKE_INVALID_TYPE);
    wsp[-1].flag=0;
    wsp[-1].type=(UBYTE)(access(wsp[-1].unit.sstring, 0)==0);
}

/* array - aload - a0..an-1 array */
void wk_aload(void)
{
    ULONG i, l;
    check(1);
    if (!(wsp[-1].flag&INDEXED)) error(WAKE_INVALID_TYPE);
    l=wsp[-1].unit.line.length;
    stack((int)l);
    wsp[l-1]=wsp[-1];
    switch (wsp[-1].type) {
    case ARRAY_TYPE:
    case BLOCK_TYPE: {
        ELEMENT *a=wsp[-1].unit.area;
        for (i=0; i<l; i++) wsp[i-1]=a[i];
        break;
    }
    case FLOATARRAY_TYPE: {
        double *p=wsp[-1].unit.floatarray;
        for (i=0; i<l; i++) {
            wsp[i-1].flag=NUMBER;
            wsp[i-1].type=FLOATING_TYPE;
            wsp[i-1].unit.floating=p[i];
        }
        break;
    }
    case INTARRAY_TYPE: {
        long *p=wsp[-1].unit.intarray;
        for (i=0; i<l; i++) {
            wsp[i-1].flag=NUMBER|INTEGER;
            wsp[i-1].type=INTEGER_TYPE;
            wsp[i-1].unit.integer=p[i];
        }
        break;
    }
    case UINTARRAY_TYPE: {
        ULONG *p=wsp[-1].unit.uintarray;
        for (i=0; i<l; i++) {
            wsp[i-1].flag=NUMBER|INTEGER;
            wsp[i-1].type=UINTEGER_TYPE;
            wsp[i-1].unit.uinteger=p[i];
        }
        break;
    }
    case SINGLEARRAY_TYPE: {
        float *p=wsp[-1].unit.singlearray;
        for (i=0; i<l; i++) {
            wsp[i-1].flag=NUMBER;
            wsp[i-1].type=FLOATING_TYPE;
            wsp[i-1].unit.floating=(double)p[i];
        }
        break;
    }
    case SHORTARRAY_TYPE: {
        short *p=wsp[-1].unit.shortarray;
        for (i=0; i<l; i++) {
            wsp[i-1].flag=NUMBER|INTEGER;
            wsp[i-1].type=INTEGER_TYPE;
            wsp[i-1].unit.integer=(long)p[i];
        }
        break;
    }
    case USHORTARRAY_TYPE: {
        UWORD *p=wsp[-1].unit.ushortarray;
        for (i=0; i<l; i++) {
            wsp[i-1].flag=NUMBER|INTEGER;
            wsp[i-1].type=UINTEGER_TYPE;
            wsp[i-1].unit.uinteger=(ULONG)p[i];
        }
        break;
    }
    case STRING_TYPE:
    case WAVE_TYPE: {
        UBYTE *p=wsp[-1].unit.string;
        for (i=0; i<l; i++) {
            wsp[i-1].flag=NUMBER|INTEGER;
            wsp[i-1].type=UINTEGER_TYPE;
            wsp[i-1].unit.integer=(ULONG)p[i];
        }
        break;
    }
    default:
        error(WAKE_INVALID_TYPE);
    }
    wsp+=l;
}

/* size - array - array */
void wk_array(void)
{
    ULONG l;
    check(1);
    if (!(wsp[-1].flag&INTEGER)) error(WAKE_INVALID_TYPE);
    l=wsp[-1].unit.uinteger;
    wsp[-1].unit.array=(ELEMENT *)get_mem(l+1, ARRAY_TYPE, TRUE);
    wsp[-1].unit.line.length=l;
    wsp[-1].type=ARRAY_TYPE;
    wsp[-1].flag=COMPOSITE|INDEXED;
}

/* a0..an-1 array - astore - array */
void wk_astore(void)
{
    int i, l;
    check(1);
    if (!(wsp[-1].flag&INDEXED)) error(WAKE_INVALID_TYPE);
    l=wsp[-1].unit.line.length;
    check(l+1);
    switch (wsp[-1].type) {
    case ARRAY_TYPE:
    case BLOCK_TYPE: {
        ELEMENT *a=wsp[-1].unit.area;
        for (i=0; i<l; i++) a[i]=wsp[i-l-1];
        break;
    }
    case FLOATARRAY_TYPE: {
        double *p=wsp[-1].unit.floatarray;
        for (i=0; i<l; i++)
            p[i]=check_double(wsp+i-l-1);
        break;
    }
    case INTARRAY_TYPE: {
        long *p=wsp[-1].unit.intarray;
        for (i=0; i<l; i++)
            p[i]=check_integer(wsp+i-l-1);
        break;
    }
    case SINGLEARRAY_TYPE: {
        float *p=wsp[-1].unit.singlearray;
        for (i=0; i<l; i++)
            p[i]=(float)check_double(wsp+i-l-1);
        break;
    }
    case SHORTARRAY_TYPE: {
        short *p=wsp[-1].unit.shortarray;
        for (i=0; i<l; i++)
            p[i]=(short)check_integer(wsp+i-l-1);
        break;
    }
    case STRING_TYPE:
    case WAVE_TYPE: {
        UBYTE *p=wsp[-1].unit.string;
        for (i=0; i<l; i++)
            p[i]=(UBYTE)check_integer(wsp+i-l-1);
        break;
    }
    default:
        error(WAKE_INVALID_TYPE);
    }
    wsp[-l-1]=wsp[-1];
    wsp-=l;
}

/* - availmem - uinteger */
void wk_availmem(void)
{
    stack(1);
    wsp->flag=NUMBER|INTEGER;
    wsp->type=UINTEGER_TYPE;
#ifdef AMIGA
    wsp->unit.uinteger=AvailMem(MEMF_PUBLIC);
#elif defined(__linux__)
#elif defined(VAXC)
    wsp->unit.uinteger= (ULONG)-1;
#elif defined(_WIN32)
    {
        MEMORYSTATUS ms;
        GlobalMemoryStatus(&ms);
        ms.dwLength=sizeof(MEMORYSTATUS);
        wsp->unit.uinteger=ms.dwAvailVirtual;
    }
#elif defined(_WINDOWS)
    wsp->unit.uinteger=GetFreeSpace(0);
#elif defined(__APPLE__)
#else
    wsp->unit.uinteger=coreleft();
#endif
    wsp++;
}

/* nomen - begin - */
void wk_begin(void)
{
    unsigned top;
    check(1);
    if (wsp[-1].type!=NOMEN_TYPE) error(WAKE_INVALID_TYPE);
    top=nsp-nomen_stack;
    if (top>=nomen_stack_size) {
        NOMENP *new=(NOMENP *)realloc((void *)nomen_stack, (nomen_stack_size+=NOMEN_STACK_INCR)*sizeof(NOMENP));
        if (!new) {
            free((void *)nomen_stack);
            nsp=nomen_stack=(NOMENP *)
                malloc(NOMEN_STACK_SIZE*sizeof(NOMENP));
            if (!nomen_stack) memory_overflow();
            fpts("memory overflow, nomen stack restored\n", stdout);
            *nsp++=system_nomen;
            wsp--;
            wk_quit();
        }
        nomen_stack=new;
    }
    nsp=nomen_stack+top;
    *nsp++=wsp[-1].unit.nomen;
    wsp--;
}

/* size - block - block */
void wk_block(void)
{
    ULONG l;
    check(1);
    if (!(wsp[-1].flag&INTEGER)) error(WAKE_INVALID_TYPE);
    l=wsp[-1].unit.uinteger;
    wsp[-1].unit.block=(ELEMENT *)get_mem(l+1, BLOCK_TYPE, TRUE);
    wsp[-1].unit.line.length=l;
    wsp[-1].type=BLOCK_TYPE;
    wsp[-1].flag=COMPOSITE|INDEXED;
}

/* name - bound - boolean */
void wk_bound(void)
{
    check(1);
    if (wsp[-1].type!=NAME_TYPE) error(WAKE_INVALID_TYPE);
    if (wsp[-1].unit.entry->flag&BOUND) wsp[-1].type=TRUE_TYPE;
    else if (nsp[-1]!=wsp[-1].unit.site.nomen) {
        ENTRY *en=ssearch(wsp[-1].unit.entry->name, nsp[-1]->entry);
        wsp[-1].type=(UBYTE)(en&&en->flag&BOUND);
    }
    else wsp[-1].type=NULL_TYPE;
}

/* reference identifier integral - build - */
void wk_build(void)
{
#ifdef VAXC
    char key[BASE_KEY_SIZE+sizeof(long)];
    if (!base_open)
#else
        char key[BASE_KEY_SIZE];
    if (!base_index.data_f.f)
#endif
        {
            err_elem.flag=0;
            err_elem.type=NULL_TYPE;
            error(WAKE_CANNOT_ACCESS);
        }
    check(3);
    if (!(wsp[-3].flag&INTEGER)||!(wsp[-2].flag&INTEGER))
        error(WAKE_INVALID_TYPE);
    set_key(key);
#ifdef VAXC
    {
        int csc;
        memcpy(key+BASE_KEY_SIZE, &wsp[-3].unit.integer, sizeof(long));
        rab.rab$b_rac=RAB$C_KEY;
        rab.rab$l_rbf=key;
        rab.rab$w_rsz=BASE_KEY_SIZE+sizeof(long);
        if ((csc=sys$put(&rab))!=RMS$_NORMAL&&csc!=RMS$_OK_DUP)
            access_error(fab.fab$l_fna);
    }
#else
    add_key(&base_index, &wsp[-3].unit.integer, key);
#endif
    wsp-=3;
}

/* file - bytesavailable - integer */
void wk_bytesavailable(void)
{
    FILE *f;
#ifdef VAXC
    int t;
#else
    long t;
#endif
    check(1);
    if (wsp[-1].type!=FILE_TYPE) error(WAKE_INVALID_TYPE);
    f=wsp[-1].unit.file;
    t=ftell(f);
    if (fseek(f, 0, SEEK_END)) {
        err_elem.flag=0;
        err_elem.type=NULL_TYPE;
        error(WAKE_CANNOT_ACCESS);
    }
    wsp[-1].flag=NUMBER|INTEGER;
    wsp[-1].type=INTEGER_TYPE;
    wsp[-1].unit.integer=ftell(f)-t;
    if (fseek(f, t, SEEK_SET)) {
        err_elem.flag=0;
        err_elem.type=NULL_TYPE;
        error(WAKE_CANNOT_ACCESS);
    }
}

/* string - changedirectory - */
void wk_changedirectory(void)
{
    check(1);
    if (wsp[-1].type!=STRING_TYPE) error(WAKE_INVALID_TYPE);
    if (chdir(wsp[-1].unit.sstring))
        access_error(wsp[-1].unit.sstring);
    wsp--;
}

#ifdef _WINDOWS
void chkcbrk(void)
{
    MSG msg;
    while (PeekMessage(&msg, (HWND)NULL, 0, 0, PM_REMOVE)) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
}
#endif

/* - checkbreak - */
void wk_checkbreak(void)
{
#ifdef _WINDOWS
    chkcbrk();
#elif defined(__TURBOC__)
    kbhit();
#endif
}

/* - clean - */
void wk_clean(void)
{
    HEAD *p=last_save, *q, *r=NULL;
    while (p)
        if (p->prot||(p->head&&p->head->prot)) {
            r=p;
            p=p->last;
        }
        else if (p->head) {
            q=p;
            p=p->last;
            if (r) r->last=p;
            else last_save=p;
            free_composite(q);
        }
        else {
            p->flag|=MARKED;
            r=p;
            p=p->last;
        }
    p=last_save;
    r=NULL;
    while (p)
        if (p->flag&MARKED) {
            q=p;
            p=p->last;
            if (r) r->last=p;
            else last_save=p;
            free_composite(q);
        }
        else {
            r=p;
            p=p->last;
        }
    wsp=wake_stack;
}

/* - clear - */
void wk_clear(void)
{
    wsp=wake_stack;
}

/* mark any ... - cleartomark - */
void wk_cleartomark(void)
{
    ELEMENT *w=wsp;
    do if (w>wake_stack) w--;
        else error(WAKE_UNMATCHED_MARK);
    while (w->type!=MARK_TYPE);
    wsp=w;
}

/* - close - */
void wk_close(void)
{
#ifdef VAXC
    if (base_open&&sys$close(&fab)!=RMS$_NORMAL)
        access_error(fab.fab$l_fna);
    base_open=0;
#else
    if (!base_index.data_f.f) return;
    close_index(&base_index);
    base_index.data_f.f=NULL;
#endif
}

/* file - closefile - */
void wk_closefile(void)
{
    check(1);
    if (wsp[-1].type!=FILE_TYPE) error(WAKE_INVALID_TYPE);
    if (fclose(wsp[-1].unit.file)) {
        err_elem.flag=0;
        err_elem.type=NULL_TYPE;
        error(WAKE_CANNOT_ACCESS);
    }
    wsp--;
}

/* string - command - integer */
void wk_command(void)
{
    check(1);
    if (wsp[-1].type!=STRING_TYPE) error(WAKE_INVALID_TYPE);
#ifdef _WINDOWS
    wsp[-1].unit.integer=WinExec((LPCSTR)wsp[-1].unit.string, SW_SHOW);
#else
    wsp[-1].unit.integer=system(wsp[-1].unit.sstring);
#endif
    wsp[-1].type=INTEGER_TYPE;
    wsp[-1].flag=NUMBER;
}

/* nomen1 nomen2 - copy - nomen2 */
/* indexed1 indexed2 - copy - indexed2 */
/* any1..anyn integer - copy - any1..anyn anyn-integer+1..anyn */
void wk_copy(void)
{
    if (wsp[-1].flag&INDEXED) {
        check(2);
        if (wsp[-2].type!=wsp[-1].type) error(WAKE_INVALID_TYPE);
#ifdef _MSDOS
        bigmove((UBYTE MQ *)wsp[-1].unit.area, (UBYTE MQ *)wsp[-2].unit.area,
                min(wsp[-1].unit.line.length,wsp[-2].unit.line.length)*type_size[wsp[-2].type]);
#else
        memcpy(wsp[-1].unit.area, wsp[-2].unit.area, min(wsp[-1].unit.line.length, wsp[-2].unit.line.length)*type_size[wsp[-2].type]);
#endif
        wsp[-2]=wsp[-1];
        wsp--;
    }
    else if (wsp[-1].type==NOMEN_TYPE) {
        if (wsp[-2].type!=NOMEN_TYPE) error(WAKE_INVALID_TYPE);
        copy_entries(wsp[-2].unit.nomen->entry, wsp[-1].unit.nomen);
        wsp[-2]=wsp[-1];
        wsp--;
    }
    else {
        int n;
        check(1);
        if (!(wsp[-1].flag&INTEGER)) error(WAKE_INVALID_TYPE);
        n=wsp[-1].unit.integer;
        if (!n) return;
        if (wsp-wake_stack<n+1)
            error(WAKE_INDEX_OUT_OF_RANGE);
        stack(n);
        memcpy(wsp-1, wsp-n-1, n*sizeof(ELEMENT));
        wsp+=n-1;
    }
}

/* - count - integer */
void wk_count(void)
{
    stack(1);
    wsp->flag=NUMBER|INTEGER;
    wsp->type=INTEGER_TYPE;
    wsp->unit.integer=wsp-wake_stack;
    wsp++;
}

/* - countexecstack - integer */
void wk_countexecstack(void)
{
    stack(1);
    wsp->flag=NUMBER|INTEGER;
    wsp->type=INTEGER_TYPE;
    wsp->unit.integer=est;
    wsp++;
}

/* - countnomenstack - integer */
void wk_countnomenstack(void)
{
    stack(1);
    wsp->flag=NUMBER|INTEGER;
    wsp->type=INTEGER_TYPE;
    wsp->unit.integer=nsp-nomen_stack;
    wsp++;
}

/* mark any1 .. anyn - counttomark - n */
void wk_counttomark(void)
{
    ELEMENT *w=wsp;
    do if (w>wake_stack) w--;
        else error(WAKE_UNMATCHED_MARK);
    while (w->type!=MARK_TYPE);
    stack(1);
    wsp->flag=NUMBER|INTEGER;
    wsp->type=INTEGER_TYPE;
    wsp->unit.integer=wsp-w-1;
    wsp++;
}

/* - currentdirectory - currentdirectory */
void wk_currentdirectory(void)
{
    int n;
    char *cwd=getcwd(NULL, 128);
    if (!cwd) error(WAKE_CANNOT_ALLOCATE);
    stack(1);
    n=strlen(cwd);
    wsp->unit.string=(UBYTE *)get_mem(n+1,STRING_TYPE,TRUE);
    wsp->unit.line.length=n;
    wsp->type=STRING_TYPE;
    wsp->flag=COMPOSITE|INDEXED;
    strcpy(wsp->unit.sstring, cwd);
    free(cwd);
    wsp++;
}

/* - currentfile - file */
void wk_currentfile(void)
{
    int t=est;
    while (--t>=0&&exec_stack[t].element.type!=FILE_TYPE);
    if (t<0) error(WAKE_ILLEGAL_USE);
    stack(1);
    *wsp++=exec_stack[t].element;
}

/* - currentnomen - nomen */
void wk_currentnomen(void)
{
    stack(1);
    wsp->flag=0;
    wsp->type=NOMEN_TYPE;
    wsp->unit.nomen=nsp[-1];
    wsp++;
}

/* - currentstring - string */
void wk_currentstring(void)
{
    int t=est;
    while (--t>=0&&exec_stack[t].element.type!=STRING_TYPE);
    if (t<0) error(WAKE_ILLEGAL_USE);
    stack(1);
    *wsp=exec_stack[t].element;
    wsp->unit.line.length=strlen(wsp->unit.sstring);
    wsp++;
}

/* - currenttime - seconds microseconds */
void wk_currenttime(void)
{
#if defined(_MSC_VER)
    struct timeb timeb;
    ftime(&timeb);
#else
    struct timeval tv;
    gettimeofday(&tv, NULL);
#endif
    stack(2);
    wsp->flag=NUMBER|INTEGER;
    wsp->type=UINTEGER_TYPE;
#if defined(_MSC_VER)
    wsp->unit.uinteger=(ULONG)timeb.time;
#else
    wsp->unit.uinteger=tv.tv_sec;
#endif
    wsp++;
    wsp->flag=NUMBER|INTEGER;
    wsp->type=UINTEGER_TYPE;
#if defined(_MSC_VER)
    wsp->unit.uinteger = (ULONG)timeb.millitm * 1000L;
#else
    wsp->unit.uinteger=tv.tv_usec;
#endif
    wsp++;
}

/* number | string - cvi - integer */
void wk_cvi(void)
{
    check(1);
    switch (wsp[-1].type) {
    case FLOATING_TYPE:
        wsp[-1].flag|=INTEGER;
        wsp[-1].type=INTEGER_TYPE;
        wsp[-1].unit.integer=
            (long)(float)wsp[-1].unit.floating;
        return;
    case FRACTION_TYPE:
        wsp[-1].flag|=INTEGER;
        wsp[-1].type=INTEGER_TYPE;
        wsp[-1].unit.integer=wsp[-1].unit.fract.num/
            wsp[-1].unit.fract.den;
        return;
    case INTEGER_TYPE:
    case UINTEGER_TYPE:
        return;
    case STRING_TYPE:
        wsp[-1].flag=NUMBER|INTEGER;
        wsp[-1].type=INTEGER_TYPE;
        wsp[-1].unit.integer=atol(wsp[-1].unit.sstring);
        return;
    default:
        error(WAKE_INVALID_TYPE);
    }
}

/* any - cvlit - any */
void wk_cvlit(void)
{
    check(1);
    switch (wsp[-1].type) {
    case BLOCK_TYPE:
    case QUICK_TYPE:
        wsp[-1].type=ARRAY_TYPE;
        break;
    case ENTRY_TYPE:
        wsp[-1].type=NAME_TYPE;
        break;
    }
}

/* string - cvn - name */
void wk_cvn(void)
{
    char *p;
    ENTRY *e;
    NOMENP np;
    check(1);
    if (wsp[-1].type!=STRING_TYPE) error(WAKE_INVALID_TYPE);
    if (!(e=search(wsp[-1].unit.sstring, &np))) {
        if (!(p=(char *)memalloc(strlen(wsp[-1].unit.sstring)+1)))
            error(WAKE_CANNOT_ALLOCATE);
        strcpy(p, wsp[-1].unit.sstring);
        if (!(e=enter(p, nsp[-1]))) error(WAKE_CANNOT_ALLOCATE);
    }
    wsp[-1].flag=0;
    wsp[-1].type=NAME_TYPE;
    wsp[-1].unit.name=e;
    wsp[-1].unit.site.nomen=np;
}

/* number | string - cvr - real */
void wk_cvr(void)
{
    check(1);
    switch (wsp[-1].type) {
    case FLOATING_TYPE:
        return;
    case FRACTION_TYPE:
        wsp[-1].type=FLOATING_TYPE;
        wsp[-1].unit.floating=(double)wsp[-1].unit.fract.num/
            (double)wsp[-1].unit.fract.den;
        return;
    case INTEGER_TYPE:
        wsp[-1].flag=NUMBER;
        wsp[-1].type=FLOATING_TYPE;
        wsp[-1].unit.floating=(double)wsp[-1].unit.integer;
        return;
    case UINTEGER_TYPE:
        wsp[-1].flag=NUMBER;
        wsp[-1].type=FLOATING_TYPE;
        wsp[-1].unit.floating=(double)wsp[-1].unit.uinteger;
        return;
    case STRING_TYPE:
        wsp[-1].flag=NUMBER;
        wsp[-1].type=FLOATING_TYPE;
        wsp[-1].unit.floating=atof(wsp[-1].unit.sstring);
        return;
    default:
        error(WAKE_INVALID_TYPE);
    }
}

/* number base string - cvr - substring */
void wk_cvrs(void)
{
    long n;
    int b, v;
    char *p;
    check(3);
    if (!(wsp[-2].flag&INTEGER)||wsp[-1].type!=STRING_TYPE)
        error(WAKE_INVALID_TYPE);
    p=wsp[-1].unit.sstring+wsp[-1].unit.line.length;
    b=wsp[-2].unit.integer;
    n=check_integer(wsp-3);
    do {
        v=n%b;
        *--p=(char)HEX_CHR(v);
        n/=b;
    }
    while (n);
    wsp[-1].unit.sstring=p;
    wsp[-1].unit.line.length=strlen(p);
    wsp[-3]=wsp[-1];
    wsp-=2;
}

/* simple string - cvs - string */
void wk_cvs(void)
{
    check(2);
    if (wsp[-1].type!=STRING_TYPE) error(WAKE_INVALID_TYPE);
    cvs_element(wsp-2, wsp[-1].unit.sstring);
    wsp[-2]=wsp[-1];
    wsp--;
}

/* any - cvx - any */
void wk_cvx(void)
{
    check(1);
    switch (wsp[-1].type) {
    case ARRAY_TYPE:
        if (wsp[-1].flag&INTERVAL) error(WAKE_ILLEGAL_USE);
        wsp[-1].type=BLOCK_TYPE;
        break;
    case NAME_TYPE: {
        ENTRY *en=wsp[-1].unit.entry;
        if (en->flag&DETERMINED) {
            ENTRY *en=wsp[-1].unit.entry;
            if (en->element.type==BLOCK_TYPE) {
                wsp[-1].type=QUICK_TYPE;
                wsp[-1].unit.block=
                    en->element.unit.block;
                wsp[-1].unit.pair.name=en;
            }
            else wsp[-1]=en->element;
            wsp[-1].flag&=0xff^COMPOSITE;
        }
        else wsp[-1].type=ENTRY_TYPE;
        break;
    }
    }
}

/* ushortarray time - date - ushortarray */
void wk_date(void)
{
    int i;
    UWORD *a;
    struct tm *tp;
    check(2);
    if (wsp[-2].type!=USHORTARRAY_TYPE||wsp[-1].type!=INTEGER_TYPE)
        error(WAKE_INVALID_TYPE);
    if (wsp[-2].unit.line.length<9) error(WAKE_INDEX_OUT_OF_RANGE);
    a=wsp[-2].unit.ushortarray;
    if (!(tp=localtime((const time_t *)&wsp[-1].unit.integer))) error(WAKE_ILLEGAL_USE);
    for (i=0; i<9; i++) a[i]=(UWORD)((int *)tp)[i];
    wsp--;
}

/* name any - ddef - */
void wk_ddef(void)
{
    set_entry(0);
}

/* name any - ddet - */
void wk_ddet(void)
{
    set_entry(2);
}

/* name any - def - */
void wk_def(void)
{
    set_entry(1);
}

/* integer - delay - */
void wk_delay(void)
{
    check(1);
    if (!(wsp[-1].flag&INTEGER)) error(WAKE_INVALID_TYPE);
#ifdef AMIGA
    Delay(wsp[-1].unit.integer*TICKS_PER_SECOND/1000);
#elif defined(__linux__) || defined(VAXC) || defined(__APPLE__)
    usleep(wsp[-1].unit.integer * 1000);
#elif defined(_WINDOWS)
    {
        MSG msg;
        int id = SetTimer((HWND)NULL, 0, (UINT)wsp[-1].unit.integer, (TIMERPROC)NULL);
        if (!id) error(WAKE_CANNOT_ALLOCATE);
        do {
            if (GetMessage(&msg, (HWND)NULL, 0, 0) == -1) break;
            DispatchMessage(&msg);
        }
        while (msg.message != WM_TIMER || msg.wParam != (UWORD)id);
        KillTimer((HWND)NULL, id);
    }
#else
    delay(wsp[-1].unit.integer);
#endif
    wsp--;
}

/* filename - deletefile - */
void wk_deletefile(void)
{
    check(1);
    if (wsp[-1].type!=STRING_TYPE) error(WAKE_INVALID_TYPE);
    remove(wsp[-1].unit.sstring);
}

/* name any - det - */
void wk_det(void)
{
    set_entry(1|2);
}

/* name - determined - boolean */
void wk_determined(void)
{
    check(1);
    if (wsp[-1].type!=NAME_TYPE) error(WAKE_INVALID_TYPE);
    if (wsp[-1].unit.entry->flag&DETERMINED) wsp[-1].type=TRUE_TYPE;
    else if (nsp[-1]!=wsp[-1].unit.site.nomen) {
        ENTRY *en=ssearch(wsp[-1].unit.entry->name, nsp[-1]->entry);
        wsp[-1].type=(UBYTE)(en&&en->flag&DETERMINED);
    }
    else wsp[-1].type=NULL_TYPE;
}

/* any - dup - any any */
void wk_dup(void)
{
    check(1);
    stack(1);
    *wsp=wsp[-1];
    wsp++;
}

/* - end - */
void wk_end(void)
{
    if (nsp<=nomen_stack+1) error(WAKE_ILLEGAL_USE);
    nsp--;
}

/* - endnomen - nomen */
void wk_endnomen(void)
{
    if (nsp<=nomen_stack+1) error(WAKE_ILLEGAL_USE);
    stack(1);
    nsp--;
    wsp->flag=0;
    wsp->type=NOMEN_TYPE;
    wsp->unit.nomen= *nsp;
    wsp++;
}

/* any1 any2 - exch - any2 any1 */
void wk_exch(void)
{
    ELEMENT e;
    check(2);
    e=wsp[-2];
    wsp[-2]=wsp[-1];
    wsp[-1]=e;
}

/* any - exec - */
void wk_exec(void)
{
    check(1);
    switch (wsp[-1].type) {
    case FUNCTION_TYPE:
        wsp--;
        (*wsp->unit.function)();
        return;
    case BLOCK_TYPE:
    case QUICK_TYPE:
        push(--wsp);
        exec_stack[est-1].element.unit.head[-1].prot++;
        execute();
        exec_stack[est-1].element.unit.head[-1].prot--;
        eel= &exec_stack[--est-1].exec_elem;
        return;
    case ENTRY_TYPE:
        wsp--;
        exec_entry=wsp->unit.entry;
        exec_nomen=wsp->unit.site.nomen;
        entry_exec();
        return;
    }
}

/* array - execstack - subarray */
void wk_execstack(void)
{
    unsigned i, l;
    ELEMENT *a, *e;
    check(1);
    if (wsp[-1].type!=ARRAY_TYPE) error(WAKE_INVALID_TYPE);
    if (wsp[-1].unit.line.length<est) error(WAKE_INDEX_OUT_OF_RANGE);
    a=wsp[-1].unit.array;
    for (i=0; i<est; i++)
        switch (exec_stack[i].element.type) {
        case BLOCK_TYPE:
            a[i].flag=INDEXED;
            a[i].type=BLOCK_TYPE;
            a[i].unit.block=e=exec_stack[i].exec_elem;
            l=0;
            while (e->type) {
                l++;
                e++;
            }
            a[i].unit.line.length=(ULONG)l;
            break;
        case STRING_TYPE:
            a[i].flag=INDEXED;
            a[i].type=STRING_TYPE;
            a[i].unit.string=exec_stack[i].element.unit.string;
            a[i].unit.line.length=strlen(a[i].unit.sstring);
            break;
        default:
            a[i]=exec_stack[i].element;
        }
    wsp[-1].unit.line.length=est;
}

/* - exit - */
void wk_exit(void)
{
    jump_exec(exit_est);
    longjmp(exit_jump, WAKE_EXIT);
}

/* filename modenumber - file - file */
void wk_file(void)
{
    int m;
    FILE *f;
    check(2);
    if (wsp[-2].type!=STRING_TYPE||!(wsp[-1].flag&INTEGER)) error(WAKE_INVALID_TYPE);
    m=wsp[-1].unit.integer;
    f=fopen(wsp[-2].unit.sstring+strspn(wsp[-2].unit.sstring, whites),
            m == 0 ? "rb" : m == 1 ? "wb" : m == 2 ? "r+b" : "w+b");
    if (!f) access_error(wsp[-2].unit.sstring);
    wsp[-2].unit.file=f;
    wsp[-2].type=FILE_TYPE;
    wsp[-2].flag=0;
    wsp--;
}

/* file - fileposition - position */
void wk_fileposition(void)
{
    check(1);
    if (wsp[-1].type!=FILE_TYPE) error(WAKE_INVALID_TYPE);
    wsp[-1].unit.integer=ftell(wsp[-1].unit.file);
    wsp[-1].type=INTEGER_TYPE;
    wsp[-1].flag=NUMBER|INTEGER;
}

void find(char part)
{
    int l;
    ULONG i;
#ifdef VAXC
    int s;
    char key[BASE_KEY_SIZE+sizeof(long)];
#else
    long r;
    char key[BASE_KEY_SIZE];
#endif
    char skey[BASE_KEY_SIZE];
    check(3);
#ifdef VAXC
    if (!base_open)
#else
        if (!base_index.data_f.f)
#endif
            {
                err_elem.flag=0;
                err_elem.type=NULL_TYPE;
                error(WAKE_CANNOT_ACCESS);
            }
    if (wsp[-3].type!=INTARRAY_TYPE||!(wsp[-2].flag&INTEGER))
        error(WAKE_INVALID_TYPE);
    l=set_key(skey);
    if (part&2) {
        memcpy(key, skey, sizeof(long));
        memset(key+sizeof(long), 0, BASE_KEY_SIZE-sizeof(long));
    }
    else memcpy(key, skey, BASE_KEY_SIZE);
    i=0;
#ifdef VAXC
    rab.rab$l_kbf=key;
    rab.rab$b_krf=0;
    rab.rab$b_ksz=l;
    rab.rab$b_rac=RAB$C_KEY;
    if ((s=sys$find(&rab))!=RMS$_RNF) {
        if (s!=RMS$_NORMAL) access_error(fab.fab$l_fna);
        rab.rab$b_rac=RAB$C_SEQ;
        rab.rab$l_ubf=key;
        rab.rab$w_usz=BASE_KEY_SIZE+sizeof(long);
        if (part&2)
            while (i<wsp[-3].unit.line.length) {
                if ((s=sys$get(&rab))==RMS$_EOF) break;
                if (s!=RMS$_NORMAL) access_error(fab.fab$l_fna);
                if (memcmp(key, skey, l)>=0) break;
                memcpy(wsp[-3].unit.intarray+i++, key+BASE_KEY_SIZE, sizeof(long));
            }
        if (part&1)
            while (i<wsp[-3].unit.line.length) {
                if ((s=sys$get(&rab))==RMS$_EOF) break;
                if (s!=RMS$_NORMAL) access_error(fab.fab$l_fna);
                if (memcmp(key, skey, l)) break;
                memcpy(wsp[-3].unit.intarray+i++, key+BASE_KEY_SIZE, sizeof(long));
            }
        if (part&4) {
            while ((s=sys$get(&rab))!=RMS$_EOF) {
                if (s!=RMS$_NORMAL)
                    access_error(fab.fab$l_fna);
                if (memcmp(key, skey, l)) break;
            }
            while (i<wsp[-3].unit.line.length) {
                if ((s=sys$get(&rab))==RMS$_EOF) break;
                if (s!=RMS$_NORMAL)
                    access_error(fab.fab$l_fna);
                if (memcmp(key, skey, sizeof(long))) break;
                memcpy(wsp[-3].unit.intarray+i++, key+BASE_KEY_SIZE, sizeof(long));
            }
        }
    }
#else
    if (!search_key(&base_index, &r, key)) {
        if (part&2)
            while (i<wsp[-3].unit.line.length&&memcmp(key, skey, l)<0) {
                wsp[-3].unit.intarray[i++]=r;
                if (next_key(&base_index, &r, key)) break;
            }
        if (part&1)
            while (i<wsp[-3].unit.line.length&&!memcmp(key, skey, l)) {
                wsp[-3].unit.intarray[i++]=r;
                if (next_key(&base_index, &r, key)) break;
            }
        if (part&4) {
            while (!memcmp(key, skey, l))
                if (next_key(&base_index, &r, key)) break;
            while (i<wsp[-3].unit.line.length&&
                   !memcmp(key, skey, sizeof(long))) {
                wsp[-3].unit.intarray[i++]=r;
                if (next_key(&base_index, &r, key)) break;
            }
        }
    }
#endif
    if (i<wsp[-3].unit.line.length)
        memset(wsp[-3].unit.intarray+i, 0, (int)(wsp[-3].unit.line.length-i)*sizeof(long));
    wsp-=2;
}

/* intarray identifier integral - findeq - intarray */
void wk_findeq(void)
{
    find(1);
}

/* intarray identifier integral - findge - intarray */
void wk_findge(void)
{
    find(1|4);
}

/* intarray identifier integral - findgt - intarray */
void wk_findgt(void)
{
    find(4);
}

/* intarray identifier integral - findle - intarray */
void wk_findle(void)
{
    find(1|2);
}

/* intarray identifier integral - findlt - intarray */
void wk_findlt(void)
{
    find(2);
}

/* intarray identifier integral - findne - intarray */
void wk_findne(void)
{
    find(2|4);
}

void set_fixed(ELEMENT *elem)
{
    switch (elem->type) {
    case ARRAY_TYPE:
    case BLOCK_TYPE: {
        ULONG n=elem->unit.line.length;
        elem=elem->unit.area;
        while (n--) set_fixed(elem++);
    }
        break;
    case ENTRY_TYPE:
        if (elem->unit.entry->flag&DETERMINED) {
            ENTRY *en=elem->unit.entry;
            if (en->element.type==BLOCK_TYPE) {
                elem->flag=0;
                elem->type=QUICK_TYPE;
                elem->unit.block=en->element.unit.block;
                elem->unit.pair.name=en;
            }
            else *elem=en->element;
            elem->flag&=0xff^COMPOSITE;
        }
        break;
    }
}

/* name - fix - */
void wk_fix(void)
{
    check(1);
    if (wsp[-1].type!=NAME_TYPE) error(WAKE_INVALID_TYPE);
    set_fixed(&wsp[-1].unit.entry->element);
    wsp--;
}

/* element - fixed - boolean */
void wk_fixed(void)
{
    check(1);
    if (wsp[-1].flag&FIXED) wsp[-1].type=TRUE_TYPE;
    else wsp[-1].type=NULL_TYPE;
}

/* size - floatarray - floatarray */
void wk_floatarray(void)
{
    ULONG l;
    check(1);
    if (!(wsp[-1].flag&INTEGER)) error(WAKE_INVALID_TYPE);
    l=wsp[-1].unit.uinteger;
    wsp[-1].unit.floatarray=(double *)get_mem(l, FLOATARRAY_TYPE, TRUE);
    wsp[-1].unit.line.length=l;
    wsp[-1].type=FLOATARRAY_TYPE;
    wsp[-1].flag=COMPOSITE|INDEXED;
}

/* - flush - */
void wk_flush(void)
{
#ifndef _WINDOWS
    fflush(stdout);
#endif
}

/* file - flushfile - */
void wk_flushfile(void)
{
    check(1);
    if (wsp[-1].type!=FILE_TYPE) error(WAKE_INVALID_TYPE);
    fflush(wsp[-1].unit.file);
    wsp--;
}

/* init incr limit block - for - */
void wk_for(void)
{
    int xst;
    jmp_buf xjb;
    check(4);
    if (wsp[-1].type!=BLOCK_TYPE) error(WAKE_INVALID_TYPE);
    memcpy(xjb, exit_jump, sizeof(jmp_buf));
    xst=exit_est;
    exit_est=est;
    push(wsp-1);
    exec_stack[est-1].element.unit.head[-1].prot++;
    if (!setjmp(exit_jump)) {
        if (wsp[-4].type!=INTEGER_TYPE||wsp[-3].type!=INTEGER_TYPE||
            wsp[-2].type!=INTEGER_TYPE)
            if (wsp[-4].type!=UINTEGER_TYPE||wsp[-3].type!=UINTEGER_TYPE||
                wsp[-2].type!=UINTEGER_TYPE)
                if (wsp[-4].type!=FLOATING_TYPE||wsp[-3].type!=FLOATING_TYPE||
                    wsp[-2].type!=FLOATING_TYPE) {
                    ELEMENT ini=wsp[-4], inc=wsp[-3], lim=wsp[-2], zero;
                    zero.flag=NUMBER|INTEGER;
                    zero.type=INTEGER_TYPE;
                    zero.unit.integer=0;
                    wsp-=4;
                    if (gt(&inc, &zero))
                        while (le(&ini, &lim)) {
                            stack(1);
                            *wsp=ini;
                            wsp++;
                            execute();
                            addf(&ini, &ini, &inc);
                        }
                    else
                        while (ge(&ini, &lim)) {
                            stack(1);
                            *wsp=ini;
                            wsp++;
                            execute();
                            addf(&ini, &ini, &inc);
                        }
                }
                else {
                    double fini=wsp[-4].unit.floating, finc=wsp[-3].unit.floating, flim=wsp[-2].unit.floating;
                    wsp-=4;
                    if (finc>0)
                        while (fini<=flim) {
                            stack(1);
                            wsp->flag=NUMBER;
                            wsp->type=FLOATING_TYPE;
                            wsp->unit.floating=fini;
                            wsp++;
                            execute();
                            fini+=finc;
                        }
                    else
                        while (fini>=flim) {
                            stack(1);
                            wsp->flag=NUMBER;
                            wsp->type=FLOATING_TYPE;
                            wsp->unit.floating=fini;
                            wsp++;
                            execute();
                            fini+=finc;
                        }
                }
            else {
                ULONG ini=wsp[-4].unit.uinteger, inc=wsp[-3].unit.uinteger, lim=wsp[-2].unit.uinteger;
                wsp-=4;
                while (ini<=lim) {
                    stack(1);
                    wsp->flag=NUMBER|INTEGER;
                    wsp->type=UINTEGER_TYPE;
                    wsp->unit.uinteger=ini;
                    wsp++;
                    execute();
                    ini+=inc;
                }
            }
        else {
            long ini=wsp[-4].unit.integer, inc=wsp[-3].unit.integer, lim=wsp[-2].unit.integer;
            wsp-=4;
            if (inc>0)
                while (ini<=lim) {
                    stack(1);
                    wsp->flag=NUMBER|INTEGER;
                    wsp->type=INTEGER_TYPE;
                    wsp->unit.integer=ini;
                    wsp++;
                    execute();
                    ini+=inc;
                }
            else
                while (ini>=lim) {
                    stack(1);
                    wsp->flag=NUMBER|INTEGER;
                    wsp->type=INTEGER_TYPE;
                    wsp->unit.integer=ini;
                    wsp++;
                    execute();
                    ini+=inc;
                }
        }
    }
    memcpy(exit_jump, xjb, sizeof(jmp_buf));
    est=exit_est;
    exit_est=xst;
    eel= &exec_stack[est-1].exec_elem;
    exec_stack[est].element.unit.head[-1].prot--;
}

void enum_entries(ENTRY *entry)
{
    if (entry->l) enum_entries(entry->l);
    if (entry->r) enum_entries(entry->r);
    if (entry->flag&BOUND) {
        stack(2);
        wsp->flag=0;
        wsp->type=NAME_TYPE;
        wsp->unit.name=entry;
        wsp++;
        *wsp=entry->element;
        wsp++;
        execute();
    }
}

/* some block - forall - */
void wk_forall(void)
{
    int xst;
    jmp_buf xjb;
    ELEMENT element;
    check(2);
    if ((!(wsp[-2].flag&INDEXED)&&wsp[-2].type!=NOMEN_TYPE)||wsp[-1].type!=BLOCK_TYPE) error(WAKE_INVALID_TYPE);
    element=wsp[-2];
    memcpy(xjb, exit_jump, sizeof(jmp_buf));
    xst=exit_est;
    exit_est=est;
    push(wsp-1);
    element.unit.head[-1].prot++;
    exec_stack[est-1].element.unit.head[-1].prot++;
    if (!setjmp(exit_jump))
        switch (element.type) {
        case ARRAY_TYPE: {
            ULONG n=element.unit.line.length;
            ELEMENT *elem=element.unit.array;
            wsp-=2;
            while (n--) {
                stack(1);
                *wsp++= *elem++;
                execute();
            }
            break;
        }
        case BLOCK_TYPE: {
            ELEMENT *elem=element.unit.block;
            wsp-=2;
            while (elem->type) {
                stack(1);
                *wsp++= *elem++;
                execute();
            }
            break;
        }
        case FLOATARRAY_TYPE: {
            ULONG n=element.unit.line.length;
            double *p=element.unit.floatarray;
            wsp-=2;
            while (n--) {
                stack(1);
                wsp->flag=NUMBER;
                wsp->type=FLOATING_TYPE;
                wsp->unit.floating= *p++;
                wsp++;
                execute();
            }
            break;
        }
        case INTARRAY_TYPE: {
            ULONG n=element.unit.line.length;
            long *p=element.unit.intarray;
            wsp-=2;
            while (n--) {
                stack(1);
                wsp->flag=NUMBER|INTEGER;
                wsp->type=INTEGER_TYPE;
                wsp->unit.integer= *p++;
                wsp++;
                execute();
            }
            break;
        }
        case UINTARRAY_TYPE: {
            ULONG n=element.unit.line.length;
            ULONG *p=element.unit.uintarray;
            wsp-=2;
            while (n--) {
                stack(1);
                wsp->flag=NUMBER|INTEGER;
                wsp->type=UINTEGER_TYPE;
                wsp->unit.uinteger= *p++;
                wsp++;
                execute();
            }
            break;
        }
        case SINGLEARRAY_TYPE: {
            ULONG n=element.unit.line.length;
            float *p=element.unit.singlearray;
            wsp-=2;
            while (n--) {
                stack(1);
                wsp->flag=NUMBER;
                wsp->type=FLOATING_TYPE;
                wsp->unit.floating= (double)*p++;
                wsp++;
                execute();
            }
            break;
        }
        case SHORTARRAY_TYPE: {
            ULONG n=element.unit.line.length;
            short *p=element.unit.shortarray;
            wsp-=2;
            while (n--) {
                stack(1);
                wsp->flag=NUMBER|INTEGER;
                wsp->type=INTEGER_TYPE;
                wsp->unit.integer= (long)*p++;
                wsp++;
                execute();
            }
            break;
        }
        case USHORTARRAY_TYPE: {
            ULONG n=element.unit.line.length;
            UWORD *p=element.unit.ushortarray;
            wsp-=2;
            while (n--) {
                stack(1);
                wsp->flag=NUMBER|INTEGER;
                wsp->type=UINTEGER_TYPE;
                wsp->unit.uinteger= (ULONG)*p++;
                wsp++;
                execute();
            }
            break;
        }
        case STRING_TYPE:
        case WAVE_TYPE: {
            ULONG n=element.unit.line.length;
            UBYTE *p=element.unit.string;
            wsp-=2;
            while (n--) {
                stack(1);
                wsp->flag=NUMBER|INTEGER;
                wsp->type=INTEGER_TYPE;
                wsp->unit.integer= *p++;
                wsp++;
                execute();
            }
            break;
        }
        case NOMEN_TYPE:
            wsp-=2;
            if (element.unit.nomen->entry)
                enum_entries(element.unit.nomen->entry);
            break;
        default:
            error(WAKE_INVALID_TYPE);
        }
    memcpy(exit_jump, xjb, sizeof(jmp_buf));
    est=exit_est;
    exit_est=xst;
    eel= &exec_stack[est-1].exec_elem;
    exec_stack[est].element.unit.head[-1].prot--;
    element.unit.head[-1].prot--;
}

WAKE_FUNC wake_funcs[]={
    {"=",                 wk_out1},
    {"==",                wk_out2},
    {"access",            wk_access},
    {"aload",             wk_aload},
    {"array",             wk_array},
    {"astore",            wk_astore},
    {"availmem",          wk_availmem},
    {"begin",             wk_begin},
    {"block",             wk_block},
    {"bound",             wk_bound},
    {"build",             wk_build},
    {"bytesavailable",    wk_bytesavailable},
    {"changedirectory",   wk_changedirectory},
    {"checkbreak",        wk_checkbreak},
    {"clean",             wk_clean},
    {"clear",             wk_clear},
    {"cleartomark",       wk_cleartomark},
    {"close",             wk_close},
    {"closefile",         wk_closefile},
    {"command",           wk_command},
    {"copy",              wk_copy},
    {"count",             wk_count},
    {"countexecstack",    wk_countexecstack},
    {"countnomenstack",   wk_countnomenstack},
    {"counttomark",       wk_counttomark},
    {"currentdirectory",  wk_currentdirectory},
    {"currentfile",       wk_currentfile},
    {"currentnomen",      wk_currentnomen},
    {"currentstring",     wk_currentstring},
    {"currenttime",       wk_currenttime},
    {"cvi",               wk_cvi},
    {"cvlit",             wk_cvlit},
    {"cvn",               wk_cvn},
    {"cvr",               wk_cvr},
    {"cvrs",              wk_cvrs},
    {"cvs",               wk_cvs},
    {"cvx",               wk_cvx},
    {"date",              wk_date},
    {"ddef",              wk_ddef},
    {"ddet",              wk_ddet},
    {"def",               wk_def},
    {"delay",             wk_delay},
    {"deletefile",        wk_deletefile},
    {"det",               wk_det},
    {"determined",        wk_determined},
    {"dup",               wk_dup},
    {"end",               wk_end},
    {"endnomen",          wk_endnomen},
    {"exch",              wk_exch},
    {"exec",              wk_exec},
    {"execstack",         wk_execstack},
    {"exit",              wk_exit},
    {"file",              wk_file},
    {"fileposition",      wk_fileposition},
    {"findeq",            wk_findeq},
    {"findge",            wk_findge},
    {"findgt",            wk_findgt},
    {"findle",            wk_findle},
    {"findlt",            wk_findlt},
    {"findne",            wk_findne},
    {"fix",               wk_fix},
    {"fixed",             wk_fixed},
    {"floatarray",        wk_floatarray},
    {"flush",             wk_flush},
    {"flushfile",         wk_flushfile},
    {"for",               wk_for},
    {"forall",            wk_forall}};

int init_wake(void)
{
    int rv;
    ENTRY *entry;
#ifdef AMIGA
    if (!(GfxBase=(struct GfxBase *)
          OpenLibrary("graphics.library",0))) return 1;
    if (!(DiskfontBase=(struct DiskfontBase *)
          OpenLibrary("diskfont.library",0))) return 2;
    if (!(IntuitionBase=(struct IntuitionBase *)
          OpenLibrary("intuition.library",0))) return 3;
    if (!(LayersBase=(struct LayersBase *)
          OpenLibrary("layers.library",0))) return 4;
    if (!(MathBase=OpenLibrary("mathffp.library",0))) return 5;
    if (!(MathTransBase=OpenLibrary("mathtrans.library",0))) return 6;
    if (!(MathIeeeDoubBasBase=
          OpenLibrary("mathieeedoubbas.library",0))) return 7;
    if (!(MathIeeeDoubTransBase=
          OpenLibrary("mathieeedoubtrans.library",0))) return 8;
    if (!(wakePort=(struct MsgPort *)
          CreatePort("wakePort",0))) return 9;
#endif
    wsp=wake_stack=(ELEMENT *)malloc(wake_stack_size*sizeof(ELEMENT));
    if (!wake_stack) return 10;
    exec_stack=(EXECUTE *)malloc(exec_stack_size*sizeof(EXECUTE));
    if (!exec_stack) return 11;
    nsp=nomen_stack=(NOMENP *)malloc(
                                     (nomen_stack_size=NOMEN_STACK_SIZE)*sizeof(NOMENP));
    if (!nomen_stack) return 12;
    *nsp++=system_nomen=(NOMEN *)memcalloc(sizeof(NOMEN));
    if (enter_funcs(wake_funcs,ELEMS(wake_funcs))) return 13;
    if (!(entry=enter("systemnomen",system_nomen))) return 14;
    entry->flag=BOUND|DETERMINED;
    entry->element.type=NOMEN_TYPE;
    entry->element.unit.nomen=system_nomen;
    *nsp=(NOMEN *)memcalloc(sizeof(NOMEN));
    if (!(entry=enter("usernomen",system_nomen))) return 15;
    entry->flag=BOUND|DETERMINED;
    entry->element.type=NOMEN_TYPE;
    entry->element.unit.nomen= *nsp++;
    if (!(entry=enter("null",system_nomen))) return 16;
    entry->flag=BOUND;
    if (!(entry=enter("true",system_nomen))) return 17;
    entry->flag=BOUND|DETERMINED;
    entry->element.type=TRUE_TYPE;
    if (!(entry=enter("mark",system_nomen))) return 18;
    entry->flag=BOUND|DETERMINED;
    entry->element.type=MARK_TYPE;
    if (!(entry=enter("stdin",system_nomen))) return 19;
    entry->flag=BOUND|DETERMINED;
    entry->element.type=FILE_TYPE;
    entry->element.unit.file=stdin;
    if (!(entry=enter("stdout",system_nomen))) return 20;
    entry->flag=BOUND|DETERMINED;
    entry->element.type=FILE_TYPE;
    entry->element.unit.file=stdout;
    if (!(errnument=enter("errnum",system_nomen))) return 21;
    errnument->flag=BOUND;
    if (!(errelement=enter("errelem",system_nomen))) return 22;
    errelement->flag=BOUND;
    if (!(errfposent=enter("errfpos",system_nomen))) return 23;
    errfposent->flag=BOUND;
    if (!(errfnameent=enter("errfname",system_nomen))) return 24;
    errfnameent->flag=BOUND;
    if (!(entry=enter("setting",system_nomen))) return 25;
    entry->flag=BOUND|DETERMINED;
    entry->element.flag=NUMBER|INTEGER;
    entry->element.type=INTEGER_TYPE;
#ifdef AMIGA
    entry->element.unit.integer=0;
#elif defined(_WINDOWS)
    entry->element.unit.integer=2;
#elif defined(XLIB)
    entry->element.unit.integer=3;
#elif defined(__TURBOC__)
    entry->element.unit.integer=4;
#else
    entry->element.unit.integer= -1L;
#endif
    if (oflag&TEXTONLY) entry->element.unit.integer= -1L;
    if ((rv=init_fun2())!=0) return rv;
#if !defined(VAXC) || defined(XLIB)
    if (!(oflag&TEXTONLY)&&((rv=init_gra())!=0||(rv=init_pic())!=0))
        return rv;
#endif
    if ((rv=init_log())!=0||(rv=init_mat())!=0) return rv;
#ifndef VAXC
    if (!(oflag&SUPPRESS)&&(rv=init_seq())!=0) return rv;
    init_index();
#endif
#if defined(_MSC_VER)
    ftime(&rtimeb);
#elif defined(LATTICE)
    time(&rtime);
#else
    gettimeofday(&rtimeval, NULL);
#endif
    return 0;
}

void term_wake(void)
{
    wk_close();
#ifndef VAXC
    shut_seq();
#endif
#if !defined(VAXC) || defined(XLIB)
    if (!(oflag&TEXTONLY)) shut_gra();
#endif
#ifdef AMIGA
    {
        HEAD *p=last_save,*q;
        while (p) {
            q=p;
            p=p->last;
            free_composite(q);
        }
        if (wakePort) DeletePort(wakePort);
        if (MathBase) CloseLibrary(MathBase);
        if (MathTransBase) CloseLibrary(MathTransBase);
        if (MathIeeeDoubBasBase) CloseLibrary(MathIeeeDoubBasBase);
        if (MathIeeeDoubTransBase) CloseLibrary(MathIeeeDoubTransBase);
        if (LayersBase) CloseLibrary((struct Library *)LayersBase);
        if (IntuitionBase) CloseLibrary((struct Library *)IntuitionBase);
        if (DiskfontBase) CloseLibrary((struct Library *)DiskfontBase);
        if (GfxBase) CloseLibrary((struct Library *)GfxBase);
    }
#endif
}
