
/* wk_log.c */

#include "wake.h"

REGQUAL int ge(register ELEMENT *elem,register ELEMENT *elem1)
{
    int d;
    switch (elem->type) {
    case FLOATING_TYPE:
        switch (elem1->type) {
        case FLOATING_TYPE:
            return elem->unit.floating>=
                elem1->unit.floating;
        case FRACTION_TYPE:
            return elem->unit.floating>=
                (double)elem1->unit.fract.num/
                (double)elem1->unit.fract.den;
        case INTEGER_TYPE:
            return elem->unit.floating>=
                (double)elem1->unit.integer;
        case UINTEGER_TYPE:
            return elem->unit.floating>=
                (double)elem1->unit.uinteger;
        }
        break;
    case FRACTION_TYPE:
        switch (elem1->type) {
        case FLOATING_TYPE:
            return (double)elem->unit.fract.num/
                (double)elem->unit.fract.den>=
                elem1->unit.floating;
        case FRACTION_TYPE:
            if (elem->unit.fract.den==
                elem1->unit.fract.den)
                return elem->unit.fract.num>=
                    elem1->unit.fract.num;
            if (elem->unit.fract.num==
                elem1->unit.fract.num)
                return elem1->unit.fract.den>=
                    elem->unit.fract.den;
            return elem->unit.fract.num*
                elem1->unit.fract.den>=
                elem1->unit.fract.num*
                elem->unit.fract.den;
        case INTEGER_TYPE:
            return elem->unit.fract.num>=
                elem1->unit.integer*
                elem->unit.fract.den;
        case UINTEGER_TYPE:
            return elem->unit.fract.num>=
                (int)elem1->unit.uinteger*
                elem->unit.fract.den;
        }
        break;
    case INTEGER_TYPE:
        switch (elem1->type) {
        case FLOATING_TYPE:
            return (double)elem->unit.integer>=
                elem1->unit.floating;
        case FRACTION_TYPE:
            return elem->unit.integer*
                elem1->unit.fract.den>=
                elem1->unit.fract.num;
        case INTEGER_TYPE:
            return elem->unit.integer>=
                elem1->unit.integer;
        case UINTEGER_TYPE:
            return elem->unit.integer>=
                (int)elem1->unit.uinteger;
        }
        break;
    case UINTEGER_TYPE:
        switch (elem1->type) {
        case FLOATING_TYPE:
            return (double)elem->unit.uinteger>=
                elem1->unit.floating;
        case FRACTION_TYPE:
            return (int)elem->unit.uinteger*
                elem1->unit.fract.den>=
                elem1->unit.fract.num;
        case INTEGER_TYPE:
            return (int)elem->unit.uinteger>=
                elem1->unit.integer;
        case UINTEGER_TYPE:
            return elem->unit.uinteger>=
                elem1->unit.uinteger;
        }
        break;
    case STRING_TYPE:
        switch (elem1->type) {
        case STRING_TYPE:
            d = strncmp(elem->unit.sstring,
                        elem1->unit.sstring,
                        min(elem->unit.line.length, elem1->unit.line.length));
            if (d > 0) return 1;
            if (d == 0) return elem->unit.line.length == elem1->unit.line.length;
            return 0;
        case NAME_TYPE:
            d = strncmp(elem->unit.sstring,
                        elem1->unit.name->name,
                        min(elem->unit.line.length, strlen(elem1->unit.name->name)));
            if (d > 0) return 1;
            if (d == 0) return elem->unit.line.length == strlen(elem1->unit.name->name);
            return 0;
        }
        break;
    case NAME_TYPE:
        switch (elem1->type) {
        case STRING_TYPE:
            d = strncmp(elem->unit.name->name,
                        elem1->unit.sstring,
                        min(strlen(elem->unit.name->name), elem1->unit.line.length));
            if (d > 0) return 1;
            if (d == 0) return strlen(elem->unit.name->name) == elem1->unit.line.length;
            return 0;
        case NAME_TYPE:
            return strcmp(elem->unit.name->name,
                          elem1->unit.name->name)>=0;
        }
        break;
    }
    error(WAKE_INVALID_TYPE);
    return -1;
}

REGQUAL int gt(register ELEMENT *elem,register ELEMENT *elem1)
{
    int d;
    switch (elem->type) {
    case FLOATING_TYPE:
        switch (elem1->type) {
        case FLOATING_TYPE:
            return elem->unit.floating>
                elem1->unit.floating;
        case FRACTION_TYPE:
            return elem->unit.floating>
                (double)elem1->unit.fract.num/
                (double)elem1->unit.fract.den;
        case INTEGER_TYPE:
            return elem->unit.floating>
                (double)elem1->unit.integer;
        case UINTEGER_TYPE:
            return elem->unit.floating>
                (double)elem1->unit.uinteger;
        }
        break;
    case FRACTION_TYPE:
        switch (elem1->type) {
        case FLOATING_TYPE:
            return (double)elem->unit.fract.num/
                (double)elem->unit.fract.den>
                elem1->unit.floating;
        case FRACTION_TYPE:
            if (elem->unit.fract.den==
                elem1->unit.fract.den)
                return elem->unit.fract.num>
                    elem1->unit.fract.num;
            if (elem->unit.fract.num==
                elem1->unit.fract.num)
                return elem1->unit.fract.den>
                    elem->unit.fract.den;
            return elem->unit.fract.num*
                elem1->unit.fract.den>
                elem1->unit.fract.num*
                elem->unit.fract.den;
        case INTEGER_TYPE:
            return elem->unit.fract.num>
                elem1->unit.integer*
                elem->unit.fract.den;
        case UINTEGER_TYPE:
            return elem->unit.fract.num>
                (int)elem1->unit.uinteger*
                elem->unit.fract.den;
        }
        break;
    case INTEGER_TYPE:
        switch (elem1->type) {
        case FLOATING_TYPE:
            return (double)elem->unit.integer>
                elem1->unit.floating;
        case FRACTION_TYPE:
            return elem->unit.integer*
                elem1->unit.fract.den>
                elem1->unit.fract.num;
        case INTEGER_TYPE:
            return elem->unit.integer>
                elem1->unit.integer;
        case UINTEGER_TYPE:
            return elem->unit.integer>
                (int)elem1->unit.uinteger;
        }
        break;
    case UINTEGER_TYPE:
        switch (elem1->type) {
        case FLOATING_TYPE:
            return (double)elem->unit.uinteger>
                elem1->unit.floating;
        case FRACTION_TYPE:
            return (int)elem->unit.uinteger*
                elem1->unit.fract.den>
                elem1->unit.fract.num;
        case INTEGER_TYPE:
            return (int)elem->unit.uinteger>
                elem1->unit.integer;
        case UINTEGER_TYPE:
            return elem->unit.uinteger>
                elem1->unit.uinteger;
        }
        break;
    case STRING_TYPE:
        switch (elem1->type) {
        case STRING_TYPE:
            d = strncmp(elem->unit.sstring,
                        elem1->unit.sstring,
                        min(elem->unit.line.length, elem1->unit.line.length));
            if (d > 0) return 1;
            if (d == 0) return elem->unit.line.length > elem1->unit.line.length;
            return 0;
        case NAME_TYPE:
            d = strncmp(elem->unit.sstring,
                        elem1->unit.name->name,
                        min(elem->unit.line.length, strlen(elem1->unit.name->name)));
            if (d > 0) return 1;
            if (d == 0) return elem->unit.line.length > strlen(elem1->unit.name->name);
            return 0;
        }
        break;
    case NAME_TYPE:
        switch (elem1->type) {
        case STRING_TYPE:
            d = strncmp(elem->unit.name->name,
                        elem1->unit.sstring,
                        min(strlen(elem->unit.name->name), elem1->unit.line.length));
            if (d > 0) return 1;
            if (d == 0) return strlen(elem->unit.name->name) > elem1->unit.line.length;
            return 0;
        case NAME_TYPE:
            return strcmp(elem->unit.name->name,
                          elem1->unit.name->name)>0;
        }
        break;
    }
    error(WAKE_INVALID_TYPE);
    return -1;
}

REGQUAL int le(register ELEMENT *elem,register ELEMENT *elem1)
{
    int d;
    switch (elem->type) {
    case FLOATING_TYPE:
        switch (elem1->type) {
        case FLOATING_TYPE:
            return elem->unit.floating<=
                elem1->unit.floating;
        case FRACTION_TYPE:
            return elem->unit.floating<=
                (double)elem1->unit.fract.num/
                (double)elem1->unit.fract.den;
        case INTEGER_TYPE:
            return elem->unit.floating<=
                (double)elem1->unit.integer;
        case UINTEGER_TYPE:
            return elem->unit.floating<=
                (double)elem1->unit.uinteger;
        }
        break;
    case FRACTION_TYPE:
        switch (elem1->type) {
        case FLOATING_TYPE:
            return (double)elem->unit.fract.num/
                (double)elem->unit.fract.den<=
                elem1->unit.floating;
        case FRACTION_TYPE:
            if (elem->unit.fract.den==
                elem1->unit.fract.den)
                return elem->unit.fract.num<=
                    elem1->unit.fract.num;
            if (elem->unit.fract.num==
                elem1->unit.fract.num)
                return elem1->unit.fract.den<=
                    elem->unit.fract.den;
            return elem->unit.fract.num*
                elem1->unit.fract.den<=
                elem1->unit.fract.num*
                elem->unit.fract.den;
        case INTEGER_TYPE:
            return elem->unit.fract.num<=
                elem1->unit.integer*
                elem->unit.fract.den;
        case UINTEGER_TYPE:
            return elem->unit.fract.num<=
                (int)elem1->unit.uinteger*
                elem->unit.fract.den;
        }
        break;
    case INTEGER_TYPE:
        switch (elem1->type) {
        case FLOATING_TYPE:
            return (double)elem->unit.integer<=
                elem1->unit.floating;
        case FRACTION_TYPE:
            return elem->unit.integer*
                elem1->unit.fract.den<=
                elem1->unit.fract.num;
        case INTEGER_TYPE:
            return elem->unit.integer<=
                elem1->unit.integer;
        case UINTEGER_TYPE:
            return elem->unit.integer<=
                (int)elem1->unit.uinteger;
        }
        break;
    case UINTEGER_TYPE:
        switch (elem1->type) {
        case FLOATING_TYPE:
            return (double)elem->unit.uinteger<=
                elem1->unit.floating;
        case FRACTION_TYPE:
            return (int)elem->unit.uinteger*
                elem1->unit.fract.den<=
                elem1->unit.fract.num;
        case INTEGER_TYPE:
            return (int)elem->unit.uinteger<=
                elem1->unit.integer;
        case UINTEGER_TYPE:
            return elem->unit.uinteger<=
                elem1->unit.uinteger;
        }
        break;
    case STRING_TYPE:
        switch (elem1->type) {
        case STRING_TYPE:
            d = strncmp(elem->unit.sstring,
                        elem1->unit.sstring,
                        min(elem->unit.line.length, elem1->unit.line.length));
            if (d < 0) return 1;
            if (d == 0) return elem->unit.line.length == elem1->unit.line.length;
            return 0;
        case NAME_TYPE:
            d = strncmp(elem->unit.sstring,
                        elem1->unit.name->name,
                        min(elem->unit.line.length, strlen(elem1->unit.name->name)));
            if (d < 0) return 1;
            if (d == 0) return elem->unit.line.length == strlen(elem1->unit.name->name);
            return 0;
        }
        break;
    case NAME_TYPE:
        switch (elem1->type) {
        case STRING_TYPE:
            d = strncmp(elem->unit.name->name,
                        elem1->unit.sstring,
                        min(strlen(elem->unit.name->name), elem1->unit.line.length));
            if (d < 0) return 1;
            if (d == 0) return strlen(elem->unit.name->name) == elem1->unit.line.length;
            return 0;
        case NAME_TYPE:
            return strcmp(elem->unit.name->name,
                          elem1->unit.name->name)<=0;
        }
        break;
    }
    error(WAKE_INVALID_TYPE);
    return -1;
}

REGQUAL int lt(register ELEMENT *elem,register ELEMENT *elem1)
{
    int d;
    switch (elem->type) {
    case FLOATING_TYPE:
        switch (elem1->type) {
        case FLOATING_TYPE:
            return elem->unit.floating<
                elem1->unit.floating;
        case FRACTION_TYPE:
            return elem->unit.floating<
                (double)elem1->unit.fract.num/
                (double)elem1->unit.fract.den;
        case INTEGER_TYPE:
            return elem->unit.floating<
                (double)elem1->unit.integer;
        case UINTEGER_TYPE:
            return elem->unit.floating<
                (double)elem1->unit.uinteger;
        }
        break;
    case FRACTION_TYPE:
        switch (elem1->type) {
        case FLOATING_TYPE:
            return (double)elem->unit.fract.num/
                (double)elem->unit.fract.den<
                elem1->unit.floating;
        case FRACTION_TYPE:
            if (elem->unit.fract.den==
                elem1->unit.fract.den)
                return elem->unit.fract.num<
                    elem1->unit.fract.num;
            if (elem->unit.fract.num==
                elem1->unit.fract.num)
                return elem1->unit.fract.den<
                    elem->unit.fract.den;
            return elem->unit.fract.num*
                elem1->unit.fract.den<
                elem1->unit.fract.num*
                elem->unit.fract.den;
        case INTEGER_TYPE:
            return elem->unit.fract.num<
                elem1->unit.integer*
                elem->unit.fract.den;
        case UINTEGER_TYPE:
            return elem->unit.fract.num<
                (int)elem1->unit.uinteger*
                elem->unit.fract.den;
        }
        break;
    case INTEGER_TYPE:
        switch (elem1->type) {
        case FLOATING_TYPE:
            return (double)elem->unit.integer<
                elem1->unit.floating;
        case FRACTION_TYPE:
            return elem->unit.integer*
                elem1->unit.fract.den<
                elem1->unit.fract.num;
        case INTEGER_TYPE:
            return elem->unit.integer<
                elem1->unit.integer;
        case UINTEGER_TYPE:
            return elem->unit.integer<
                (int)elem1->unit.uinteger;
        }
        break;
    case UINTEGER_TYPE:
        switch (elem1->type) {
        case FLOATING_TYPE:
            return (double)elem->unit.uinteger<
                elem1->unit.floating;
        case FRACTION_TYPE:
            return (int)elem->unit.uinteger*
                elem1->unit.fract.den<
                elem1->unit.fract.num;
        case INTEGER_TYPE:
            return (int)elem->unit.uinteger<
                elem1->unit.integer;
        case UINTEGER_TYPE:
            return elem->unit.uinteger<
                elem1->unit.uinteger;
        }
        break;
    case STRING_TYPE:
        switch (elem1->type) {
        case STRING_TYPE:
            d = strncmp(elem->unit.sstring,
                        elem1->unit.sstring,
                        min(elem->unit.line.length, elem1->unit.line.length));
            if (d < 0) return 1;
            if (d == 0) return elem->unit.line.length < elem1->unit.line.length;
            return 0;
        case NAME_TYPE:
            d = strncmp(elem->unit.sstring,
                        elem1->unit.name->name,
                        min(elem->unit.line.length, strlen(elem1->unit.name->name)));
            if (d < 0) return 1;
            if (d == 0) return elem->unit.line.length < strlen(elem1->unit.name->name);
            return 0;
        }
        break;
    case NAME_TYPE:
        switch (elem1->type) {
        case STRING_TYPE:
            d = strncmp(elem->unit.name->name,
                        elem1->unit.sstring,
                        min(strlen(elem->unit.name->name), elem1->unit.line.length));
            if (d < 0) return 1;
            if (d == 0) return strlen(elem->unit.name->name) < elem1->unit.line.length;
            return 0;
        case NAME_TYPE:
            return strcmp(elem->unit.name->name,
                          elem1->unit.name->name)<0;
        }
        break;
    }
    error(WAKE_INVALID_TYPE);
    return -1;
}

REGQUAL int eq(register ELEMENT *elem,register ELEMENT *elem1)
{
    switch (elem->type) {
    case NULL_TYPE:
    case TRUE_TYPE:
        return elem->type==elem1->type;
    case FLOATING_TYPE:
        switch (elem1->type) {
        case FLOATING_TYPE:
            return elem->unit.floating==
                elem1->unit.floating;
        case FRACTION_TYPE:
            return elem->unit.floating==
                (double)elem1->unit.fract.num/
                (double)elem1->unit.fract.den;
        case INTEGER_TYPE:
            return elem->unit.floating==
                (double)elem1->unit.integer;
        case UINTEGER_TYPE:
            return elem->unit.floating==
                (double)elem1->unit.uinteger;
        default:
            return 0;
        }
    case FRACTION_TYPE:
        switch (elem1->type) {
        case FLOATING_TYPE:
            return (double)elem->unit.fract.num/
                (double)elem->unit.fract.den==
                elem1->unit.floating;
        case FRACTION_TYPE:
            return elem->unit.fract.num==
                elem1->unit.fract.num&&
                elem->unit.fract.den==
                elem1->unit.fract.den;
        case INTEGER_TYPE:
            return elem->unit.fract.num==
                elem1->unit.integer*
                elem->unit.fract.den;
        case UINTEGER_TYPE:
            return elem->unit.fract.num==
                (int)elem1->unit.uinteger*
                elem->unit.fract.den;
        default:
            return 0;
        }
    case INTEGER_TYPE:
        switch (elem1->type) {
        case FLOATING_TYPE:
            return (double)elem->unit.integer==
                elem1->unit.floating;
        case FRACTION_TYPE:
            return elem->unit.integer*
                elem1->unit.fract.den==
                elem1->unit.fract.num;
        case INTEGER_TYPE:
            return elem->unit.integer==
                elem1->unit.integer;
        case UINTEGER_TYPE:
            return elem->unit.integer==
                (int)elem1->unit.uinteger;
        default:
            return 0;
        }
    case UINTEGER_TYPE:
        switch (elem1->type) {
        case FLOATING_TYPE:
            return (double)elem->unit.uinteger==
                elem1->unit.floating;
        case FRACTION_TYPE:
            return (int)elem->unit.uinteger*
                elem1->unit.fract.den==
                elem1->unit.fract.num;
        case INTEGER_TYPE:
            return (int)elem->unit.uinteger==
                elem1->unit.integer;
        case UINTEGER_TYPE:
            return elem->unit.uinteger==
                elem1->unit.uinteger;
        default:
            return 0;
        }
    case STRING_TYPE:
        switch (elem1->type) {
        case STRING_TYPE:
            return elem->unit.line.length == elem1->unit.line.length
                && !strncmp(elem->unit.sstring,
                            elem1->unit.sstring, elem->unit.line.length);
        case NAME_TYPE:
            return elem->unit.line.length == strlen(elem1->unit.name->name)
                && !strncmp(elem->unit.sstring,
                            elem1->unit.name->name, elem->unit.line.length);
        default:
            return 0;
        }
    case NAME_TYPE:
        switch (elem1->type) {
        case STRING_TYPE:
            return strlen(elem->unit.name->name) == elem1->unit.line.length
                && !strncmp(elem->unit.name->name,
                            elem1->unit.sstring, elem1->unit.line.length);
        case NAME_TYPE:
            return !strcmp(elem->unit.name->name,
                           elem1->unit.name->name);
        default:
            return 0;
        }
    }
    return elem->type==elem1->type&&elem->unit.head==elem1->unit.head;
}

/* any any - and - value */
void wk_and(void)
{
    check(2);
    wsp[-2].type=(UBYTE)(wsp[-2].type&&wsp[-1].type);
    wsp[-2].flag=0;
    wsp--;
}

/* any any - bitand - value */
void wk_bitand(void)
{
    check(2);
    switch (wsp[-2].type) {
    case INTEGER_TYPE:
    case UINTEGER_TYPE:
        if (!(wsp[-1].flag&INTEGER)) error(WAKE_INVALID_TYPE);
        wsp[-2].unit.integer&=wsp[-1].unit.integer;
        break;
    case INTARRAY_TYPE:
        switch (wsp[-1].type) {
        case INTEGER_TYPE: {
            ULONG i;
            for (i=0; i<wsp[-2].unit.line.length;
                 i++) wsp[-2].unit.intarray[i]=
                          wsp[-2].unit.intarray[i]&
                          wsp[-1].unit.integer;
            wsp--;
            return;
        }
        case INTARRAY_TYPE: {
            ULONG i;
            for (i=0; i<wsp[-2].unit.line.length&&
                     i<wsp[-1].unit.line.length; i++)
                wsp[-2].unit.intarray[i]=
                    wsp[-2].unit.intarray[i]&
                    wsp[-1].unit.intarray[i];
            wsp--;
            return;
        }
        }
        break;
    case STRING_TYPE:
        switch (wsp[-1].type) {
        case INTEGER_TYPE:
        case UINTEGER_TYPE: {
            ULONG i;
            for (i=0; i<wsp[-2].unit.line.length;
                 i++) wsp[-2].unit.string[i]=(UBYTE)
                          (wsp[-2].unit.string[i]&
                           wsp[-1].unit.integer);
            wsp--;
            return;
        }
        case STRING_TYPE: {
            ULONG i;
            for (i=0; i<wsp[-2].unit.line.length&&
                     i<wsp[-1].unit.line.length; i++)
                wsp[-2].unit.string[i]=(UBYTE)
                    (wsp[-2].unit.string[i]&
                     wsp[-1].unit.string[i]);
            wsp--;
            return;
        }
        }
        break;
    default:
        error(WAKE_INVALID_TYPE);
    }
    wsp--;
}

/* any - bitnot - value */
void wk_bitnot(void)
{
    check(1);
    switch (wsp[-1].type) {
    case INTEGER_TYPE:
    case UINTEGER_TYPE:
        wsp[-1].unit.integer=~wsp[-1].unit.integer;
        return;
    case INTARRAY_TYPE: {
        ULONG i;
        for (i=0; i<wsp[-1].unit.line.length; i++)
            wsp[-1].unit.intarray[i]=~wsp[-1].unit.intarray[i];
        return;
    }
    case STRING_TYPE: {
        ULONG i;
        for (i=0; i<wsp[-1].unit.line.length; i++)
            wsp[-1].unit.string[i]=(UBYTE)~wsp[-1].unit.string[i];
        return;
    }
    default:
        error(WAKE_INVALID_TYPE);
    }
}

/* any any - bitor - value */
void wk_bitor(void)
{
    check(2);
    switch (wsp[-2].type) {
    case INTEGER_TYPE:
    case UINTEGER_TYPE:
        if (!(wsp[-1].flag&INTEGER)) error(WAKE_INVALID_TYPE);
        wsp[-2].unit.integer|=wsp[-1].unit.integer;
        break;
    case INTARRAY_TYPE:
        switch (wsp[-1].type) {
        case INTEGER_TYPE: {
            ULONG i;
            for (i=0; i<wsp[-2].unit.line.length;
                 i++) wsp[-2].unit.intarray[i]=
                          wsp[-2].unit.intarray[i]|
                          wsp[-1].unit.integer;
            wsp--;
            return;
        }
        case INTARRAY_TYPE: {
            ULONG i;
            for (i=0; i<wsp[-2].unit.line.length&&
                     i<wsp[-1].unit.line.length; i++)
                wsp[-2].unit.intarray[i]=
                    wsp[-2].unit.intarray[i]|
                    wsp[-1].unit.intarray[i];
            wsp--;
            return;
        }
        }
        break;
    case STRING_TYPE:
        switch (wsp[-1].type) {
        case INTEGER_TYPE:
        case UINTEGER_TYPE: {
            ULONG i;
            for (i=0; i<wsp[-2].unit.line.length;
                 i++) wsp[-2].unit.string[i]=(UBYTE)
                          (wsp[-2].unit.string[i]|
                           wsp[-1].unit.integer);
            wsp--;
            return;
        }
        case STRING_TYPE: {
            ULONG i;
            for (i=0; i<wsp[-2].unit.line.length;
                 i++) wsp[-2].unit.string[i]=(UBYTE)
                          (wsp[-2].unit.string[i]|
                           wsp[-1].unit.string[i]);
            wsp--;
            return;
        }
        }
        break;
    default:
        error(WAKE_INVALID_TYPE);
    }
    wsp--;
}

/* any any - bitxor - value */
void wk_bitxor(void)
{
    check(2);
    switch (wsp[-2].type) {
    case INTEGER_TYPE:
    case UINTEGER_TYPE:
        if (!(wsp[-1].flag&INTEGER)) error(WAKE_INVALID_TYPE);
        wsp[-2].unit.integer^=wsp[-1].unit.integer;
        break;
    case INTARRAY_TYPE:
        switch (wsp[-1].type) {
        case INTEGER_TYPE:
        case UINTEGER_TYPE: {
            ULONG i;
            for (i=0; i<wsp[-2].unit.line.length;
                 i++) wsp[-2].unit.intarray[i]=
                          wsp[-2].unit.intarray[i]^
                          wsp[-1].unit.integer;
            wsp--;
            return;
        }
        case INTARRAY_TYPE: {
            ULONG i;
            for (i=0; i<wsp[-2].unit.line.length;
                 i++) wsp[-2].unit.intarray[i]=
                          wsp[-2].unit.intarray[i]^
                          wsp[-1].unit.intarray[i];
            wsp--;
            return;
        }
        }
        break;
    case STRING_TYPE:
        switch (wsp[-1].type) {
        case INTEGER_TYPE:
        case UINTEGER_TYPE: {
            ULONG i;
            for (i=0; i<wsp[-2].unit.line.length;
                 i++) wsp[-2].unit.string[i]=(UBYTE)
                          (wsp[-2].unit.string[i]^
                           wsp[-1].unit.integer);
            wsp--;
            return;
        }
        case STRING_TYPE: {
            ULONG i;
            for (i=0; i<wsp[-2].unit.line.length;
                 i++) wsp[-2].unit.string[i]=(UBYTE)
                          (wsp[-2].unit.string[i]^
                           wsp[-1].unit.string[i]);
            wsp--;
            return;
        }
        }
        break;
    default:
        error(WAKE_INVALID_TYPE);
    }
    wsp--;
}

/* integer shift - bitshift - integer */
void wk_bitshift(void)
{
    check(2);
    if (!(wsp[-1].flag&INTEGER)) error(WAKE_INVALID_TYPE);
    switch (wsp[-2].type) {
    case INTEGER_TYPE:
    case UINTEGER_TYPE:
        if (wsp[-1].unit.integer>0)
            wsp[-2].unit.integer<<=wsp[-1].unit.integer;
        else wsp[-2].unit.integer>>= -wsp[-1].unit.integer;
        break;
    case STRING_TYPE: {
        ULONG i;
        long n=wsp[-1].unit.integer;
        if (n>0) for (i=0; i<wsp[-2].unit.line.length; i++)
                     wsp[-2].unit.string[i]=(UBYTE)(wsp[-2].unit.string[i]<<n);
        else for (i=0,n= -n; i<wsp[-2].unit.line.length; i++)
                 wsp[-2].unit.string[i]=(UBYTE)(wsp[-2].unit.string[i]>>n);
        break;
    }
    default:
        error(WAKE_INVALID_TYPE);
    }
    wsp--;
}

/* any any - eq - boolean */
void wk_eq(void)
{
    check(2);
    wsp[-2].type=(UBYTE)eq(wsp-2,wsp-1);
    wsp[-2].flag=0;
    wsp--;
}

/* some some - exclusiveunion - some */
void wk_exclusiveunion(void)
{
    check(2);
    switch (wsp[-2].type) {
    case FLOATARRAY_TYPE:
        if (wsp[-1].type==FLOATARRAY_TYPE) {
            char p;
            ULONG i,j;
            for (i=0; i<wsp[-1].unit.line.length&&
                     wsp[-1].unit.floatarray[i]; i++)
                {
                    p=0;
                    for (j=0; j<wsp[-2].unit.line.length&&
                             wsp[-2].unit.floatarray[j]; j++)
                        if (wsp[-1].unit.floatarray[i]==
                            wsp[-2].unit.floatarray[j])
                            {
                                p= -1;
                                break;
                            }
                    if (p) {
                        if (j<wsp[-2].unit.line.length-1)
                            memmove(wsp[-2].unit.floatarray+j,
                                    wsp[-2].unit.floatarray+j+1,
                                    ((int)wsp[-2].unit.line.length-j-1)*
                                    sizeof(double));
                        wsp[-2].unit.floatarray[
                                                wsp[-2].unit.line.length-1]=0.0; }
                    else if (j<wsp[-2].unit.line.length)
                        wsp[-2].unit.floatarray[j]=
                            wsp[-1].unit.floatarray[i];
                }
            wsp--;
            return;
        }
        break;
    case INTARRAY_TYPE:
        if (wsp[-1].type==INTARRAY_TYPE) {
            char p;
            ULONG i,j;
            for (i=0; i<wsp[-1].unit.line.length&&
                     wsp[-1].unit.intarray[i]; i++)
                {
                    p=0;
                    for (j=0; j<wsp[-2].unit.line.length&&
                             wsp[-2].unit.intarray[j]; j++)
                        if (wsp[-1].unit.intarray[i]==
                            wsp[-2].unit.intarray[j])
                            {
                                p= -1;
                                break;
                            }
                    if (p) {
                        if (j<wsp[-2].unit.line.length-1)
                            memmove(wsp[-2].unit.intarray+j,
                                    wsp[-2].unit.intarray+j+1,
                                    ((int)wsp[-2].unit.line.length-j-1)*
                                    sizeof(long));
                        wsp[-2].unit.intarray[
                                              wsp[-2].unit.line.length-1]=0; }
                    else if (j<wsp[-2].unit.line.length)
                        wsp[-2].unit.intarray[j]=
                            wsp[-1].unit.intarray[i];
                }
            wsp--;
            return;
        }
        break;
    case STRING_TYPE:
        if (wsp[-1].type==STRING_TYPE) {
            char p;
            ULONG i,j;
            for (i=0; i<wsp[-1].unit.line.length&&
                     wsp[-1].unit.string[i]; i++)
                {
                    p=0;
                    for (j=0; j<wsp[-2].unit.line.length&&
                             wsp[-2].unit.string[j]; j++)
                        if (wsp[-1].unit.string[i]==
                            wsp[-2].unit.string[j])
                            {
                                p= -1;
                                break;
                            }
                    if (p) {
                        if (j<wsp[-2].unit.line.length-1)
                            memmove(wsp[-2].unit.string+j,
                                    wsp[-2].unit.string+j+1,
                                    ((int)wsp[-2].unit.line.length-j-1)*
                                    sizeof(UBYTE));
                        wsp[-2].unit.string[
                                            wsp[-2].unit.line.length-1]=0; }
                    else if (j<wsp[-2].unit.line.length)
                        wsp[-2].unit.string[j]=
                            wsp[-1].unit.string[i];
                }
            wsp--;
            return;
        }
        break;
    case ARRAY_TYPE:
        if (wsp[-1].type==ARRAY_TYPE) {
            char p;
            ULONG i,j;
            for (i=0; i<wsp[-1].unit.line.length&&
                     wsp[-1].unit.array[i].type; i++)
                {
                    p=0;
                    for (j=0; j<wsp[-2].unit.line.length&&
                             wsp[-2].unit.array[j].type; j++)
                        if (eq(wsp[-1].unit.array+i,
                               wsp[-2].unit.array+j))
                            {
                                p= -1;
                                break;
                            }
                    if (p) {
                        if (j<wsp[-2].unit.line.length-1)
                            memmove(wsp[-2].unit.array+j,
                                    wsp[-2].unit.array+j+1,
                                    ((int)wsp[-2].unit.line.length-j-1)*
                                    sizeof(ELEMENT));
                        wsp[-2].unit.array[
                                           wsp[-2].unit.line.length-1].type=
                            NULL_TYPE; }
                    else if (j<wsp[-2].unit.line.length)
                        wsp[-2].unit.array[j]=
                            wsp[-1].unit.array[i];
                }
            wsp--;
            return;
        }
        break;
    }
    error(WAKE_INVALID_TYPE);
}

/* some some - ge - boolean */
void wk_ge(void)
{
    check(2);
    wsp[-2].type=(UBYTE)ge(wsp-2,wsp-1);
    wsp[-2].flag=0;
    wsp--;
}

/* some some - gt - boolean */
void wk_gt(void)
{
    check(2);
    wsp[-2].type=(UBYTE)gt(wsp-2,wsp-1);
    wsp[-2].flag=0;
    wsp--;
}

/* some some - intersection - some */
void wk_intersection(void)
{
    check(2);
    switch (wsp[-2].type) {
    case FLOATARRAY_TYPE:
        if (wsp[-1].type==FLOATARRAY_TYPE) {
            char p;
            ULONG i,j;
            for (i=0; i<wsp[-2].unit.line.length&&
                     wsp[-2].unit.floatarray[i]; i++)
                {
                    p=0;
                    for (j=0; j<wsp[-1].unit.line.length&&
                             wsp[-1].unit.floatarray[j]; j++)
                        if (wsp[-2].unit.floatarray[i]==
                            wsp[-1].unit.floatarray[j])
                            {
                                p= -1;
                                break;
                            }
                    if (!p) {
                        if (i<wsp[-2].unit.line.length-1) {
                            memmove(wsp[-2].unit.floatarray+i,
                                    wsp[-2].unit.floatarray+i+1,
                                    ((int)wsp[-2].unit.line.length-i-1)*
                                    sizeof(double));
                            i--; }
                        wsp[-2].unit.floatarray[
                                                wsp[-2].unit.line.length-1]=0.0; }
                }
            wsp--;
            return;
        }
        break;
    case INTARRAY_TYPE:
        if (wsp[-1].type==INTARRAY_TYPE) {
            char p;
            ULONG i,j;
            for (i=0; i<wsp[-2].unit.line.length&&
                     wsp[-2].unit.intarray[i]; i++)
                {
                    p=0;
                    for (j=0; j<wsp[-1].unit.line.length&&
                             wsp[-1].unit.intarray[j]; j++)
                        if (wsp[-2].unit.intarray[i]==
                            wsp[-1].unit.intarray[j])
                            {
                                p= -1;
                                break;
                            }
                    if (!p) {
                        if (i<wsp[-2].unit.line.length-1) {
                            memmove(wsp[-2].unit.intarray+i,
                                    wsp[-2].unit.intarray+i+1,
                                    ((int)wsp[-2].unit.line.length-i-1)*
                                    sizeof(long));
                            i--; }
                        wsp[-2].unit.intarray[
                                              wsp[-2].unit.line.length-1]=0; }
                }
            wsp--;
            return;
        }
        break;
    case STRING_TYPE:
        if (wsp[-1].type==STRING_TYPE) {
            char p;
            ULONG i,j;
            for (i=0; i<wsp[-2].unit.line.length&&
                     wsp[-2].unit.string[i]; i++)
                {
                    p=0;
                    for (j=0; j<wsp[-1].unit.line.length&&
                             wsp[-1].unit.string[j]; j++)
                        if (wsp[-2].unit.string[i]==
                            wsp[-1].unit.string[j])
                            {
                                p= -1;
                                break;
                            }
                    if (!p) {
                        if (i<wsp[-2].unit.line.length-1) {
                            memmove(wsp[-2].unit.string+i,
                                    wsp[-2].unit.string+i+1,
                                    ((int)wsp[-2].unit.line.length-i-1)*
                                    sizeof(UBYTE));
                            i--; }
                        wsp[-2].unit.string[
                                            wsp[-2].unit.line.length-1]=0; }
                }
            wsp--;
            return;
        }
        break;
    case ARRAY_TYPE:
        if (wsp[-1].type==ARRAY_TYPE) {
            char p;
            ULONG i,j;
            for (i=0; i<wsp[-2].unit.line.length&&
                     wsp[-2].unit.array[i].type; i++)
                {
                    p=0;
                    for (j=0; j<wsp[-1].unit.line.length&&
                             wsp[-1].unit.array[j].type; j++)
                        if (eq(wsp[-2].unit.array+i,
                               wsp[-1].unit.array+j))
                            {
                                p= -1;
                                break;
                            }
                    if (!p) {
                        if (i<wsp[-2].unit.line.length-1) {
                            memmove(wsp[-2].unit.array+i,
                                    wsp[-2].unit.array+i+1,
                                    ((int)wsp[-2].unit.line.length-i-1)*
                                    sizeof(ELEMENT));
                            i--; }
                        wsp[-2].unit.array[
                                           wsp[-2].unit.line.length-1].type=
                            NULL_TYPE; }
                }
            wsp--;
            return;
        }
        break;
    }
    error(WAKE_INVALID_TYPE);
}

/* some some - le - boolean */
void wk_le(void)
{
    check(2);
    wsp[-2].type=(UBYTE)le(wsp-2,wsp-1);
    wsp[-2].flag=0;
    wsp--;
}

/* some some - lt - boolean */
void wk_lt(void)
{
    check(2);
    wsp[-2].type=(UBYTE)lt(wsp-2,wsp-1);
    wsp[-2].flag=0;
    wsp--;
}

/* any any - ne - boolean */
void wk_ne(void)
{
    check(2);
    wsp[-2].type=(UBYTE)!eq(wsp-2,wsp-1);
    wsp[-2].flag=0;
    wsp--;
}

/* any - not - value */
void wk_not(void)
{
    check(1);
    wsp[-1].flag=0;
    wsp[-1].type=(UBYTE)!wsp[-1].type;
}

/* any any - or - value */
void wk_or(void)
{
    check(2);
    wsp[-2].type=(UBYTE)(wsp[-2].type||wsp[-1].type);
    wsp[-2].flag=0;
    wsp--;
}

/* some some - union - some */
void wk_union(void)
{
    check(2);
    switch (wsp[-2].type) {
    case FLOATARRAY_TYPE:
        if (wsp[-1].type==FLOATARRAY_TYPE) {
            char p;
            ULONG i,j;
            for (i=0; i<wsp[-1].unit.line.length&&
                     wsp[-1].unit.floatarray[i]; i++)
                {
                    p=0;
                    for (j=0; j<wsp[-2].unit.line.length&&
                             wsp[-2].unit.floatarray[j]; j++)
                        if (wsp[-1].unit.floatarray[i]==
                            wsp[-2].unit.floatarray[j])
                            {
                                p= -1;
                                break;
                            }
                    if (!p)
                        {
                            if (j<wsp[-2].unit.line.length)
                                wsp[-2].unit.floatarray[j]=
                                    wsp[-1].unit.floatarray[i];
                            else break;
                        }
                }
            wsp--;
            return;
        }
        break;
    case INTARRAY_TYPE:
        if (wsp[-1].type==INTARRAY_TYPE) {
            char p;
            ULONG i,j;
            for (i=0; i<wsp[-1].unit.line.length&&
                     wsp[-1].unit.intarray[i]; i++)
                {
                    p=0;
                    for (j=0; j<wsp[-2].unit.line.length&&
                             wsp[-2].unit.intarray[j]; j++)
                        if (wsp[-1].unit.intarray[i]==
                            wsp[-2].unit.intarray[j])
                            {
                                p= -1;
                                break;
                            }
                    if (!p)
                        {
                            if (j<wsp[-2].unit.line.length)
                                wsp[-2].unit.intarray[j]=
                                    wsp[-1].unit.intarray[i];
                            else break;
                        }
                }
            wsp--;
            return;
        }
        break;
    case STRING_TYPE:
        if (wsp[-1].type==STRING_TYPE) {
            char p;
            ULONG i,j;
            for (i=0; i<wsp[-1].unit.line.length&&
                     wsp[-1].unit.string[i]; i++)
                {
                    p=0;
                    for (j=0; j<wsp[-2].unit.line.length&&
                             wsp[-2].unit.string[j]; j++)
                        if (wsp[-1].unit.string[i]==
                            wsp[-2].unit.string[j])
                            {
                                p= -1;
                                break;
                            }
                    if (!p)
                        {
                            if (j<wsp[-2].unit.line.length)
                                wsp[-2].unit.string[j]=
                                    wsp[-1].unit.string[i];
                            else break;
                        }
                }
            wsp--;
            return;
        }
        break;
    case ARRAY_TYPE:
        if (wsp[-1].type==ARRAY_TYPE) {
            char p;
            ULONG i,j;
            for (i=0; i<wsp[-1].unit.line.length&&
                     wsp[-1].unit.array[i].type; i++)
                {
                    p=0;
                    for (j=0; j<wsp[-2].unit.line.length&&
                             wsp[-2].unit.array[j].type; j++)
                        if (eq(wsp[-1].unit.array+i,
                               wsp[-2].unit.array+j))
                            {
                                p= -1;
                                break;
                            }
                    if (!p)
                        {
                            if (j<wsp[-2].unit.line.length)
                                wsp[-2].unit.array[j]=
                                    wsp[-1].unit.array[i];
                            else break;
                        }
                }
            wsp--;
            return;
        }
        break;
    }
    error(WAKE_INVALID_TYPE);
}

/* any any - xor - value */
void wk_xor(void)
{
    check(2);
    wsp[-2].type=(UBYTE)((wsp[-2].type!=NULL_TYPE)^(wsp[-1].type!=NULL_TYPE));
    wsp[-2].flag=0;
    wsp--;
}

WAKE_FUNC log_funcs[]={
    {"and",         wk_and},
    {"bitand",      wk_bitand},
    {"bitnot",      wk_bitnot},
    {"bitor",       wk_bitor},
    {"bitshift",        wk_bitshift},
    {"bitxor",      wk_bitxor},
    {"eq",          wk_eq},
    {"exclusiveunion",  wk_exclusiveunion},
    {"ge",          wk_ge},
    {"gt",          wk_gt},
    {"intersection",    wk_intersection},
    {"le",          wk_le},
    {"lt",          wk_lt},
    {"ne",          wk_ne},
    {"not",         wk_not},
    {"or",          wk_or},
    {"union",       wk_union},
    {"xor",         wk_xor}};

int init_log(void)
{
    return enter_funcs(log_funcs,ELEMS(log_funcs));
}
