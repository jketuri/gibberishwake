
/* wk_fun2.c */

#include "wake.h"
#include "wk_fun.h"

void free_entries(ENTRY *entry)
{
    if (entry->l) free_entries(entry->l);
    if (entry->r) free_entries(entry->r);
    if (entry->flag&BOUND&&entry->element.flag&COMPOSITE)
        entry->element.unit.head[-1].prot--;
    if (!(entry->flag&BORROWED)) memfree(entry->name);
    memfree(entry);
}

/* composite - free - */
void wk_free(void)
{
    HEAD *p,*r=NULL,*t;
    check(1);
    if (!(wsp[-1].flag&COMPOSITE)) error(WAKE_ILLEGAL_USE);
    p=wsp[-1].unit.head-1;
    t=last_save;
    while (t&&t!=p) {
        r=t;
        t=t->last;
    }
    if (!t||p->prot||(p->head&&p->head->prot)) error(WAKE_ILLEGAL_USE);
    if (r) r->last=p->last;
    else last_save=p->last;
    free_composite(p);
    wsp--;
}

/* nomen - freenomen - */
void wk_freenomen(void)
{
    check(1);
    if (wsp[-1].type!=NOMEN_TYPE) error(WAKE_INVALID_TYPE);
    if (wsp[-1].unit.nomen->entry)
        free_entries(wsp[-1].unit.nomen->entry);
    wsp--;
}

/* some index - get - value */
void wk_get(void)
{
    check(2);
    if (wsp[-2].flag&INDEXED) {
        if (!(wsp[-1].flag&INTEGER)) error(WAKE_INVALID_TYPE);
        if (wsp[-1].unit.uinteger>=wsp[-2].unit.line.length)
            error(WAKE_INDEX_OUT_OF_RANGE);
    }
    else if (wsp[-1].type!=NAME_TYPE||wsp[-2].type!=NOMEN_TYPE)
        error(WAKE_INVALID_TYPE);
    switch (wsp[-2].type) {
    case ARRAY_TYPE:
    case BLOCK_TYPE:
        wsp[-2]=wsp[-2].unit.area[wsp[-1].unit.uinteger];
        break;
    case FLOATARRAY_TYPE:
        wsp[-2].flag=NUMBER;
        wsp[-2].type=FLOATING_TYPE;
        wsp[-2].unit.floating=
            wsp[-2].unit.floatarray[wsp[-1].unit.uinteger];
        break;
    case INTARRAY_TYPE:
    case UINTARRAY_TYPE:
        wsp[-2].flag=NUMBER|INTEGER;
        wsp[-2].type=INTEGER_TYPE;
        wsp[-2].unit.integer=
            wsp[-2].unit.intarray[wsp[-1].unit.uinteger];
        break;
    case SINGLEARRAY_TYPE:
        wsp[-2].flag=NUMBER;
        wsp[-2].type=FLOATING_TYPE;
        wsp[-2].unit.floating=(double)
            wsp[-2].unit.singlearray[wsp[-1].unit.uinteger];
        break;
    case SHORTARRAY_TYPE:
    case USHORTARRAY_TYPE:
        wsp[-2].flag=NUMBER|INTEGER;
        wsp[-2].type=INTEGER_TYPE;
        wsp[-2].unit.integer=(long)
            wsp[-2].unit.shortarray[wsp[-1].unit.uinteger];
        break;
    case STRING_TYPE:
    case WAVE_TYPE:
        wsp[-2].flag=NUMBER|INTEGER;
        wsp[-2].type=INTEGER_TYPE;
        wsp[-2].unit.integer=
            wsp[-2].unit.string[wsp[-1].unit.uinteger];
        break;
    case NOMEN_TYPE: {
        ENTRY *en=ssearch(wsp[-1].unit.name->name,
                          wsp[-2].unit.nomen->entry);
        if (!en) {
            err_elem=wsp[-1];
            error(WAKE_UNKNOWN_NAME);
        }
        if (!(en->flag&BOUND)) {
            err_elem.flag=0;
            err_elem.type=ENTRY_TYPE;
            err_elem.unit.entry=en;
            err_elem.unit.site.nomen=wsp[-2].unit.nomen;
            error(WAKE_ENTRY_NOT_BOUND);
        }
        wsp[-2]=en->element;
        break;
    }
    default:
        error(WAKE_INVALID_TYPE);
    }
    wsp--;
}

/* string - getenv - string */
void wk_getenv(void)
{
    char *env;
    int n;
    check(1);
    if (wsp[-1].type!=STRING_TYPE)
        error(WAKE_INVALID_TYPE);
    env=getenv((char *)wsp[-1].unit.string);
    if (!env) {
        wsp[-1].type=NULL_TYPE;
        wsp[-1].flag=0;
        return;
    }
    n=strlen(env);
    wsp[-1].unit.string=(UBYTE *)get_mem(n+1,STRING_TYPE,TRUE);
    memcpy(wsp[-1].unit.string,env,n+1);
    wsp[-1].unit.line.length=n;
    wsp[-1].type=STRING_TYPE;
    wsp[-1].flag=COMPOSITE|INDEXED;
}

/* some index count - getinterval - subsome */
void wk_getinterval(void)
{
    check(3);
    if (!(wsp[-1].flag&INTEGER)||!(wsp[-2].flag&INTEGER)||
        !(wsp[-3].flag&INDEXED)||wsp[-3].type==BLOCK_TYPE)
        error(WAKE_INVALID_TYPE);
    if ((ULONG)(wsp[-2].unit.integer+wsp[-1].unit.integer)>
        wsp[-3].unit.line.length) error(WAKE_INDEX_OUT_OF_RANGE);
    wsp[-3].unit.data=(UBYTE *)wsp[-3].unit.data+
        wsp[-2].unit.integer*type_size[wsp[-3].type];
    wsp[-3].unit.line.length=wsp[-1].unit.uinteger;
    if (wsp[-2].unit.uinteger) wsp[-3].flag&=0xff^COMPOSITE;
    wsp[-3].flag|=INTERVAL;
    wsp-=2;
}

/* string index expoftwo - getsized - integer */
void wk_getsized(void)
{
    check(3);
    if (wsp[-3].type!=STRING_TYPE||
        !(wsp[-2].flag&INTEGER)||!(wsp[-1].flag&INTEGER))
        error(WAKE_INVALID_TYPE);
    if (wsp[-2].unit.uinteger>=wsp[-3].unit.line.length)
        error(WAKE_INDEX_OUT_OF_RANGE);
    switch (wsp[-1].unit.integer) {
    case 0:       wsp[-3].flag=NUMBER|INTEGER;
        wsp[-3].type=INTEGER_TYPE;
        wsp[-3].unit.integer=
            wsp[-3].unit.string[wsp[-2].unit.uinteger];
        break;
    case 1:       wsp[-3].flag=NUMBER|INTEGER;
        wsp[-3].type=INTEGER_TYPE;
        {
            UWORD s;
            memcpy(&s,wsp[-3].unit.string+
                   wsp[-2].unit.uinteger,sizeof s);
            wsp[-3].unit.integer=s;
        }
        break;
    case 2:       wsp[-3].flag=NUMBER|INTEGER;
        wsp[-3].type=INTEGER_TYPE;
        {
            ULONG l;
            memcpy(&l,wsp[-3].unit.string+
                   wsp[-2].unit.uinteger,sizeof l);
            wsp[-3].unit.integer=l;
        }
        break;
    default:
        error(WAKE_INVALID_TYPE);
    }
    wsp-=2;
}

/* any block - if - */
void wk_if(void)
{
    check(2);
    if (wsp[-1].type!=BLOCK_TYPE) error(WAKE_INVALID_TYPE);
    wsp-=2;
    if (wsp->type) {
        push(wsp+1);
        exec_stack[est-1].element.unit.head[-1].prot++;
        execute();
        exec_stack[est-1].element.unit.head[-1].prot--;
        eel= &exec_stack[--est-1].exec_elem;
    }
}

/* any block block - ifelse - */
void wk_ifelse(void)
{
    check(3);
    if (wsp[-2].type!=BLOCK_TYPE||wsp[-1].type!=BLOCK_TYPE)
        error(WAKE_INVALID_TYPE);
    wsp-=3;
    if (wsp->type) push(wsp+1);
    else push(wsp+2);
    exec_stack[est-1].element.unit.head[-1].prot++;
    execute();
    exec_stack[est-1].element.unit.head[-1].prot--;
    eel= &exec_stack[--est-1].exec_elem;
}

/* any1..anyn integer - index - any1..anyn anyn-integer */
void wk_index(void)
{
    int n;
    check(1);
    if (!(wsp[-1].flag&INTEGER)) error(WAKE_INVALID_TYPE);
    n=wsp[-1].unit.integer;
    if (n+1>=wsp-wake_stack) error(WAKE_INDEX_OUT_OF_RANGE);
    wsp[-1]=wsp[-2-n];
}

/* size - intarray - intarray */
void wk_intarray(void)
{
    ULONG l;
    check(1);
    if (!(wsp[-1].flag&INTEGER)) error(WAKE_INVALID_TYPE);
    l=wsp[-1].unit.uinteger;
    wsp[-1].unit.intarray=(long *)get_mem(l,INTARRAY_TYPE,TRUE);
    wsp[-1].unit.line.length=l;
    wsp[-1].type=INTARRAY_TYPE;
    wsp[-1].flag=COMPOSITE|INDEXED;
}

/* some - invert - some */
void wk_invert(void)
{
    check(1);
    switch (wsp[-1].type) {
    case ARRAY_TYPE:
    case BLOCK_TYPE: {
        ULONG i,m,n;
        ELEMENT e,*a;
        a=wsp[-1].unit.area;
        m=wsp[-1].unit.line.length;
        n=m/2;
        m--;
        for (i=0; i<n; i++) {
            e=a[i];
            a[i]=a[m-i];
            a[m-i]=e;
        }
        return;
    }
    case FLOATARRAY_TYPE: {
        ULONG i,m,n;
        double d,*a;
        a=wsp[-1].unit.floatarray;
        m=wsp[-1].unit.line.length;
        n=m/2;
        m--;
        for (i=0; i<n; i++) {
            d=a[i];
            a[i]=a[m-i];
            a[m-i]=d;
        }
        return;
    }
    case INTARRAY_TYPE:
    case UINTARRAY_TYPE: {
        ULONG i,m,n,l,*a;
        a=wsp[-1].unit.uintarray;
        m=wsp[-1].unit.line.length;
        n=m/2;
        m--;
        for (i=0; i<n; i++) {
            l=a[i];
            a[i]=a[m-i];
            a[m-i]=l;
        }
        return;
    }
    case SINGLEARRAY_TYPE: {
        ULONG i,m,n;
        float f,*a;
        a=wsp[-1].unit.singlearray;
        m=wsp[-1].unit.line.length;
        n=m/2;
        m--;
        for (i=0; i<n; i++) {
            f=a[i];
            a[i]=a[m-i];
            a[m-i]=f;
        }
        return;
    }
    case SHORTARRAY_TYPE:
    case USHORTARRAY_TYPE: {
        ULONG i,m,n;
        short s,*a;
        a=wsp[-1].unit.shortarray;
        m=wsp[-1].unit.line.length;
        n=m/2;
        m--;
        for (i=0; i<n; i++) {
            s=a[i];
            a[i]=a[m-i];
            a[m-i]=s;
        }
        return;
    }
    case STRING_TYPE:
    case WAVE_TYPE: {
        ULONG i,m,n;
        UBYTE c,*s;
        s=wsp[-1].unit.string;
        m=wsp[-1].unit.line.length;
        n=m/2;
        m--;
        for (i=0; i<n; i++) {
            c=s[i];
            s[i]=s[m-i];
            s[m-i]=c;
        }
        return;
    }
    case VOICE_TYPE:
#ifdef AMIGA
        {
            UBYTE c;
            VOICE *voice;
            ULONG i,j,l,m,n,o;
            voice=(VOICE *)wsp[-1].unit.data;
            o=voice->vhdr.ctOctave;
            for (j=0; j<o; j++) {
                l=m=voice->vhdr.oneShotHiSamples<<o;
                n=m/2;
                m--;
                for (i=0; i<n; i++) {
                    c=voice->wave_pos[j][i];
                    voice->wave_pos[j][i]=
                        voice->wave_pos[j][m-i];
                    voice->wave_pos[j][m-i]=c;

                }
                m=voice->vhdr.repeatHiSamples<<o;
                n=m/2;
                m--;
                for (i=0; i<n; i++) {
                    c=voice->wave_pos[j][l+i];
                    voice->wave_pos[j][l+i]=
                        voice->wave_pos[j][l+m-i];
                    voice->wave_pos[j][l+m-i]=c;
                }
            }
            return;
        }
#endif
    default:
        error(WAKE_INVALID_TYPE);
    }
}

/* some - length - number */
void wk_length(void)
{
    check(1);
    if (wsp[-1].flag&INDEXED) {
        wsp[-1].flag=NUMBER|INTEGER;
        wsp[-1].type=UINTEGER_TYPE;
        wsp[-1].unit.uinteger=wsp[-1].unit.line.length;
    }
    else if (wsp[-1].type==NAME_TYPE) {
        wsp[-1].flag=NUMBER|INTEGER;
        wsp[-1].type=UINTEGER_TYPE;
        wsp[-1].unit.uinteger=strlen(wsp[-1].unit.name->name);
    }
    else if (wsp[-1].type==NOMEN_TYPE) {
        wsp[-1].flag=NUMBER|INTEGER;
        wsp[-1].type=UINTEGER_TYPE;
        wsp[-1].unit.uinteger=wsp[-1].unit.nomen->number;
    }
    else if (wsp[-1].type==STATE_TYPE) {
        wsp[-1].flag=NUMBER|INTEGER;
        wsp[-1].type=UINTEGER_TYPE;
        wsp[-1].unit.uinteger=wsp[-1].unit.state->number;
    } else error(WAKE_INVALID_TYPE);
}

/* array - listfiles - subarray */
void wk_listfiles(void)
{
    check(1);
    if (wsp[-1].type!=ARRAY_TYPE) error(WAKE_INVALID_TYPE);
#ifdef AMIGA
    {
        int errn;
        UBYTE *p;
        ELEMENT *a;
        LONG i=0,l,m,n;
        struct FileInfoBlock *fib,ffib;
        LONG flock,lock=Lock("",ACCESS_READ);
        a=wsp[-1].unit.array;
        l=wsp[-1].unit.line.length;
        if (!lock) error(WAKE_CANNOT_ALLOCATE);
        if (!(fib=(struct FileInfoBlock *)AllocMem(
                                                   sizeof(struct FileInfoBlock),MEMF_PUBLIC|MEMF_CLEAR))) goto cantall;
        if (!Examine(lock,fib)) goto cantacc;
        if (flock=Lock("/",ACCESS_READ)) n=strlen(fib->fib_FileName)+2;
        else n=1;
        ffib=*fib;
        while (ExNext(lock,fib)) {
            if (i==l) goto indexout;
            a[i].unit.line.length=m=strlen(fib->fib_FileName);
            if (fib->fib_DirEntryType>0) a[i].unit.line.length+=n;
            if (!(a[i].unit.string=p=(UBYTE *)get_mem(
                                                      a[i].unit.line.length+1,STRING_TYPE,FALSE))) goto cantall;
            if (fib->fib_DirEntryType>0) {
                if (flock) {
                    *p++='/';
                    memcpy(p,ffib.fib_FileName,n-2);
                    p+=n-2;
                    *p++='/'; }
                else *p++=':';
            }
            memcpy(p,fib->fib_FileName,m);
            a[i].flag=COMPOSITE|INDEXED;
            a[i].type=STRING_TYPE;
            i++;
        }
        wsp[-1].unit.line.length=i;
        if (IoErr()!=ERROR_NO_MORE_ENTRIES) goto cantacc;
        errn=0;
        goto end;
    indexout:
        errn=WAKE_INDEX_OUT_OF_RANGE;
        goto end;
    cantacc:
        errn=WAKE_CANNOT_ACCESS;
        goto end;
    cantall:
        errn=WAKE_CANNOT_ALLOCATE;
    end:  UnLock(lock);
        if (flock) UnLock(flock);
        if (fib) FreeMem(fib,sizeof(struct FileInfoBlock));
        if (errn) error(errn);
    }
#elif defined(_WINDOWS)
    {
        UBYTE *p;
        ELEMENT *a;
        LONG i=0,l,m;
#ifdef _WIN32
        int h;
        struct _finddata_t fd;
#else
        struct _find_t fd;
#endif
        a=wsp[-1].unit.array;
        l=wsp[-1].unit.line.length;
#ifdef _WIN32
        if ((h=_findfirst("*.*",&fd)) == -1) error(WAKE_CANNOT_ACCESS);
#else
        if (_dos_findfirst("*.*",_A_SUBDIR,&fd)) error(WAKE_CANNOT_ACCESS);
#endif
        do {
            if (*fd.name=='.') continue;
            if (i==l) error(WAKE_INDEX_OUT_OF_RANGE);
            a[i].unit.line.length=m=strlen(fd.name);
            if (fd.attrib&_A_SUBDIR) a[i].unit.line.length+=2;
            a[i].unit.string=p=(UBYTE *)get_mem(
                                                a[i].unit.line.length+1,STRING_TYPE,TRUE);
            if (fd.attrib&_A_SUBDIR) {
                *p++='.';
                *p++='\\';
            }
            memcpy(p,fd.name,(int)m);
            a[i].flag=COMPOSITE|INDEXED;
            a[i].type=STRING_TYPE;
            i++;
        }
#ifdef _WIN32
        while (_findnext(h,&fd)==0);
#else
        while (_dos_findnext(&fd)==0);
#endif
        wsp[-1].unit.line.length=i;
    }
#elif defined(__APPLE__) || defined(__linux__)
    {
        DIR *d;
        struct dirent *de;
        ELEMENT *a=wsp[-1].unit.array;
        long i=0,l=wsp[-1].unit.line.length;
        if (!(d=opendir("./"))) error(WAKE_CANNOT_ACCESS);
        while ((de=readdir(d))) {
            if (*de->d_name=='.') continue;
            if (i==l) error(WAKE_INDEX_OUT_OF_RANGE);
            a[i].unit.line.length=strlen(de->d_name);
            a[i].unit.string=(UBYTE *)get_mem(
                                              a[i].unit.line.length+1,STRING_TYPE,TRUE);
            memcpy(a[i].unit.string,de->d_name,
                   a[i].unit.line.length);
            a[i].flag=COMPOSITE|INDEXED;
            a[i].type=STRING_TYPE;
            i++;
        }
        wsp[-1].unit.line.length=i;
    }
#endif
}

/* key - load - value */
void wk_load(void)
{
    check(1);
    if (wsp[-1].type!=NAME_TYPE) error(WAKE_INVALID_TYPE);
    if (!(wsp[-1].unit.entry->flag&BOUND)) {
        err_elem=wsp[-1];
        err_elem.type=ENTRY_TYPE;
        error(WAKE_ENTRY_NOT_BOUND);
    }
    wsp[-1]=wsp[-1].unit.entry->element;
}

/* block - loop - */
void wk_loop(void)
{
    int xst;
    jmp_buf xjb;
    check(1);
    if (wsp[-1].type!=BLOCK_TYPE) error(WAKE_INVALID_TYPE);
    wsp--;
    memcpy(xjb,exit_jump,sizeof(jmp_buf));
    xst=exit_est;
    exit_est=est;
    push(wsp);
    exec_stack[est-1].element.unit.head[-1].prot++;
    if (!setjmp(exit_jump))
        for (;;) execute();
    memcpy(exit_jump,xjb,sizeof(jmp_buf));
    est=exit_est;
    exit_est=xst;
    eel= &exec_stack[est-1].exec_elem;
    exec_stack[est].element.unit.head[-1].prot--;
}

/* filename - makebase - */
void wk_makebase(void)
{
    check(1);
    if (wsp[-1].type!=STRING_TYPE) error(WAKE_INVALID_TYPE);
    wk_close();
#ifdef VAXC
    {
        int csc;
        set_fields();
        if ((csc=sys$create(&fab))!=RMS$_NORMAL&&
            csc!=RMS$_CREATED&&csc!=RMS$_SUPERSEDE&&csc!=RMS$_FILEPURGED||
            sys$connect(&rab)!=RMS$_NORMAL)
            access_error(fab.fab$l_fna);
        base_open=1;
    }
#else
    make_index(&base_index,wsp[-1].unit.sstring,BASE_KEY_SIZE,1);
#endif
    wsp--;
}

/* string pattern - match - suffix match | string null */
void wk_match(void)
{
    check(2);
    if (wsp[-2].type!=STRING_TYPE||wsp[-1].type!=STRING_TYPE)
        error(WAKE_INVALID_TYPE);
    if (wsp[-1].unit.line.length>wsp[-2].unit.line.length||
        memcmp(wsp[-2].unit.string,wsp[-1].unit.string,
               (size_t)wsp[-1].unit.line.length)) {
        wsp[-1].flag=0;
        wsp[-1].type=NULL_TYPE;
    }
    else {
        wsp[-2].flag=(UBYTE)((wsp[-2].flag&(0xff^COMPOSITE))|INTERVAL);
        wsp[-2].unit.string+=wsp[-1].unit.line.length;
        wsp[-2].unit.line.length-=wsp[-1].unit.line.length;
    }
}

/* - nomen - nomen */
void wk_nomen(void)
{
    stack(1);
    if (!(wsp->unit.nomen=(NOMENP)memcalloc(sizeof(NOMEN))))
        error(WAKE_CANNOT_ALLOCATE);
    wsp->type=NOMEN_TYPE;
    wsp->flag=0;
    wsp++;
}

/* array - nomenstack - subarray */
void wk_nomenstack(void)
{
    unsigned i,n;
    ELEMENT *a;
    check(1);
    if (wsp[-1].type!=ARRAY_TYPE) error(WAKE_INVALID_TYPE);
    n=nsp-nomen_stack;
    if (wsp[-1].unit.line.length<n) error(WAKE_INDEX_OUT_OF_RANGE);
    a=wsp[-1].unit.array;
    for (i=0; i<n; i++) {
        a[i].flag=0;
        a[i].type=NOMEN_TYPE;
        a[i].unit.nomen=nomen_stack[i];
    }
    wsp[-1].unit.line.length=n;
}

/* filename - openbase - */
void wk_openbase(void)
{
    check(1);
    if (wsp[-1].type!=STRING_TYPE) error(WAKE_INVALID_TYPE);
    wk_close();
#ifdef VAXC
    set_fields();
    if (sys$open(&fab)!=RMS$_NORMAL||sys$connect(&rab)!=RMS$_NORMAL)
        access_error(fab.fab$l_fna);
    base_open=1;
#else
    if (open_index(&base_index,wsp[-1].unit.sstring,BASE_KEY_SIZE,1))
        access_error(wsp[-1].unit.sstring);
#endif
    wsp--;
}

/* file - perform - */
void wk_perform(void)
{
    int n,xst;
    jmp_buf qjb,sjb,xjb;
    check(1);
    if (wsp[-1].type!=FILE_TYPE&&wsp[-1].type!=STRING_TYPE)
        error(WAKE_INVALID_TYPE);
    wsp--;
    memcpy(qjb,quit_jump,sizeof(jmp_buf));
    memcpy(sjb,stop_jump,sizeof(jmp_buf));
    memcpy(xjb,exit_jump,sizeof(jmp_buf));
    xst=exit_est;
    exit_est=est;
    if (!(n=setjmp(quit_jump))) {
        memcpy(stop_jump,quit_jump,sizeof(jmp_buf));
        memcpy(exit_jump,quit_jump,sizeof(jmp_buf));
        push(wsp);
        wake();
    }
    memcpy(exit_jump,xjb,sizeof(jmp_buf));
    memcpy(quit_jump,qjb,sizeof(jmp_buf));
    memcpy(stop_jump,sjb,sizeof(jmp_buf));
    switch (n) {
    case WAKE_QUIT:
        longjmp(quit_jump,WAKE_QUIT);
    case WAKE_STOP:
        longjmp(stop_jump,WAKE_STOP);
    }
    est=exit_est;
    exit_est=xst;
    eel= &exec_stack[est-1].exec_elem;
}

/* index value - place - */
void wk_place(void)
{
    int n;
    check(2);
    if (!(wsp[-2].flag&INTEGER)) error(WAKE_INVALID_TYPE);
    n=wsp[-2].unit.integer;
    if (n+2>=wsp-wake_stack) error(WAKE_INDEX_OUT_OF_RANGE);
    wsp[-3-n]=wsp[-1];
    wsp-=2;
}

/* any - pop - */
void wk_pop(void)
{
    check(1);
    wsp--;
}

/* file any - print - */
void wk_print(void)
{
    check(2);
    if (wsp[-2].type!=FILE_TYPE) error(WAKE_INVALID_TYPE);
    out_element(wsp-1,FALSE,wsp[-2].unit.file);
    wsp-=2;
}

/* file format somesimple - printf - */
void wk_printf(void)
{
    check(3);
    if (wsp[-3].type!=FILE_TYPE||wsp[-2].type!=STRING_TYPE)
        error(WAKE_INVALID_TYPE);
    switch (wsp[-1].type) {
    case FLOATING_TYPE:
        fprntf(wsp[-3].unit.file,wsp[-2].unit.sstring,
               wsp[-1].unit.floating);
        break;
    case FRACTION_TYPE:
        fprntf(wsp[-3].unit.file,wsp[-2].unit.sstring,
               wsp[-1].unit.fract.num,wsp[-1].unit.fract.den);
        break;
    case INTEGER_TYPE:
        fprntf(wsp[-3].unit.file,wsp[-2].unit.sstring,
               wsp[-1].unit.integer);
        break;
    case UINTEGER_TYPE:
        fprntf(wsp[-3].unit.file,wsp[-2].unit.sstring,
               wsp[-1].unit.uinteger);
        break;
    case COMPLEX_TYPE:
        fprntf(wsp[-3].unit.file,wsp[-2].unit.sstring,
               wsp[-1].unit.compl.real,wsp[-1].unit.compl.imag);
        break;
    case STRING_TYPE:
    case WAVE_TYPE:
        fprntf(wsp[-3].unit.file,wsp[-2].unit.sstring,
               UN wsp[-1].unit.string);
        break;
    case ENTRY_TYPE:
    case NAME_TYPE:
        fprntf(wsp[-3].unit.file,wsp[-2].unit.sstring,
               wsp[-1].unit.name->name);
        break;
    default:
        error(WAKE_INVALID_TYPE);
    }
    wsp-=3;
}

/* any1..anyn integer - pull - any1..anyn-integer */
void wk_pull(void)
{
    check(1);
    if (!(wsp[-1].flag&INTEGER)) error(WAKE_INVALID_TYPE);
    check(wsp[-1].unit.integer+1);
    wsp-=wsp[-1].unit.integer+1;
}

/* some index value - put - */
void wk_put(void)
{
    check(3);
    if (wsp[-3].flag&INDEXED) {
        if (!(wsp[-2].flag&INTEGER)) error(WAKE_INVALID_TYPE);
        if (wsp[-2].unit.uinteger>=wsp[-3].unit.line.length)
            error(WAKE_INDEX_OUT_OF_RANGE);
    }
    else if (wsp[-2].type!=NAME_TYPE||wsp[-3].type!=NOMEN_TYPE)
        error(WAKE_INVALID_TYPE);
    switch (wsp[-3].type) {
    case ARRAY_TYPE:
    case BLOCK_TYPE:
        if (wsp[-3].flag&COMPOSITE&&wsp[-1].flag&COMPOSITE)
            wsp[-1].unit.head[-1].head=wsp[-3].unit.head-1;
        wsp[-3].unit.area[wsp[-2].unit.uinteger]=wsp[-1];
        break;
    case FLOATARRAY_TYPE:
        wsp[-3].unit.floatarray[wsp[-2].unit.uinteger]=
            check_double(wsp-1);
        break;
    case INTARRAY_TYPE:
    case UINTARRAY_TYPE:
        wsp[-3].unit.intarray[wsp[-2].unit.uinteger]=
            check_integer(wsp-1);
        break;
    case SINGLEARRAY_TYPE:
        wsp[-3].unit.singlearray[wsp[-2].unit.uinteger]=
            (float)check_double(wsp-1);
        break;
    case SHORTARRAY_TYPE:
    case USHORTARRAY_TYPE:
        wsp[-3].unit.shortarray[wsp[-2].unit.uinteger]=
            (short)check_integer(wsp-1);
        break;
    case STRING_TYPE:
    case WAVE_TYPE:
        wsp[-3].unit.string[wsp[-2].unit.uinteger]=
            (UBYTE)check_integer(wsp-1);
        break;
    case NOMEN_TYPE: {
        ENTRY *en=ssearch(wsp[-2].unit.name->name,
                          wsp[-3].unit.nomen->entry);
        if (en) {
            if (en->flag&BOUND&&
                en->element.flag&COMPOSITE)
                en->element.unit.head[-1].prot--;
        }
        else if (!(en=enter(wsp[-2].unit.name->name,
                            wsp[-3].unit.nomen))) error(WAKE_CANNOT_ALLOCATE);
        else en->flag|=BORROWED;
        en->element=wsp[-1];
        if (wsp[-1].flag&COMPOSITE)
            wsp[-1].unit.head[-1].prot++;
        en->flag=(UBYTE)((en->flag&(0xff^DETERMINED))|BOUND);
        break;
    }
    default:
        error(WAKE_INVALID_TYPE);
    }
    wsp-=3;
}

/* some index some2 - putinterval - */
void wk_putinterval(void)
{
    check(3);
    if (!(wsp[-2].flag&INTEGER)||!(wsp[-3].flag&INDEXED)||
        wsp[-1].type!=wsp[-3].type) error(WAKE_INVALID_TYPE);
    if (wsp[-2].unit.integer+wsp[-1].unit.line.length>
        wsp[-3].unit.line.length) error(WAKE_INDEX_OUT_OF_RANGE);
#ifdef _MSDOS
    bigmove((UBYTE MQ *)wsp[-3].unit.area+wsp[-2].unit.integer*
            type_size[wsp[-3].type],(UBYTE MQ *)wsp[-1].unit.area,
            wsp[-1].unit.line.length*type_size[wsp[-3].type]);
#else
    memmove((UBYTE *)wsp[-3].unit.area+wsp[-2].unit.integer*
            type_size[wsp[-3].type],wsp[-1].unit.area,
            (int)wsp[-1].unit.line.length*type_size[wsp[-3].type]);
#endif
    wsp-=3;
}

/* string index value expoftwo - putsized - */
void wk_putsized(void)
{
    check(4);
    if (wsp[-4].type!=STRING_TYPE||
        !(wsp[-3].flag&INTEGER)||!(wsp[-2].flag&INTEGER)||
        !(wsp[-1].flag&INTEGER)) error(WAKE_INVALID_TYPE);
    if (wsp[-3].unit.uinteger>=wsp[-4].unit.line.length)
        error(WAKE_INDEX_OUT_OF_RANGE);
    switch (wsp[-1].unit.integer) {
    case 0:       wsp[-4].unit.string[wsp[-3].unit.uinteger]=
            (UBYTE)wsp[-2].unit.integer;
        break;
    case 1: {
        UWORD s=(UWORD)wsp[-2].unit.integer;
        memcpy(wsp[-4].unit.string+
               wsp[-3].unit.uinteger,&s,sizeof s);
    }
        break;
    case 2:       {
        ULONG l=wsp[-2].unit.uinteger;
        memcpy(wsp[-4].unit.string+
               wsp[-3].unit.uinteger,&l,sizeof l);
    }
        break;
    default:
        error(WAKE_INVALID_TYPE);
    }
    wsp-=4;
}

/* - quit - */
void wk_quit(void)
{
    stop_level=0;
    jump_exec(1);
    longjmp(quit_jump,WAKE_QUIT);
}

/* file - read - byte | null */
void wk_read(void)
{
    check(1);
    if (wsp[-1].type!=FILE_TYPE) error(WAKE_INVALID_TYPE);
    if ((wsp[-1].unit.integer=fgtc(wsp[-1].unit.file))!=EOF) {
        wsp[-1].flag=NUMBER|INTEGER;
        wsp[-1].type=INTEGER_TYPE;
    }
    else wsp[-1].type=NULL_TYPE;
}

/* file string - readhexstring - substring bool */
void wk_readhexstring(void)
{
    ULONG l;
    int c,d;
    FILE *f;
    UBYTE *p;
    check(2);
    if (wsp[-2].type!=FILE_TYPE||wsp[-1].type!=STRING_TYPE)
        error(WAKE_INVALID_TYPE);
    f=wsp[-2].unit.file;
    p=wsp[-1].unit.string;
    l=wsp[-1].unit.line.length;
    while (l) {
        if ((c=fgtc(f))==EOF) break;
        if ((d=fgtc(f))==EOF) {
            *p++=(UBYTE)(HEX_VAL(c)<<4);
            break;
        }
        *p++=(UBYTE)(HEX_VAL(c)<<4|HEX_VAL(d));
        l--;
    }
    *p++='\0';
    wsp[-2]=wsp[-1];
    wsp[-2].unit.line.length-=l;
    wsp[-1].flag=0;
    wsp[-1].type=(UBYTE)(l==0);
}

/* file string - readline - substring bool */
void wk_readline(void)
{
    int l;
    char *p;
    check(2);
    if (wsp[-2].type!=FILE_TYPE||wsp[-1].type!=STRING_TYPE)
        error(WAKE_INVALID_TYPE);
    p=fgts(wsp[-1].unit.sstring,(int)wsp[-1].unit.line.length+1,
           wsp[-2].unit.file);
    if (!p&&ferror(wsp[-2].unit.file)) {
        err_elem.flag=0;
        err_elem.type=NULL_TYPE;
        error(WAKE_CANNOT_ACCESS);
    }
    wsp[-2]=wsp[-1];
    l=strlen(wsp[-2].unit.sstring);
    if (wsp[-2].unit.string[l-1]=='\n') {
        wsp[-2].unit.string[--l]='\0';
        wsp[-1].type=(UBYTE)(p!=NULL);
    }
    else wsp[-1].type=0;
    wsp[-1].flag=0;
    wsp[-2].unit.line.length=l;
}

/* file some - readtyped - subsome bool */
void wk_readtyped(void)
{
    long l,n;
    UBYTE MQ *p;
    check(2);
    if (wsp[-2].type!=FILE_TYPE||!(wsp[-1].flag&INDEXED))
        error(WAKE_INVALID_TYPE);
    p=(UBYTE MQ *)wsp[-1].unit.data;
    l=wsp[-1].unit.line.length*(long)type_size[wsp[-1].type];
#ifdef _MSDOS
    n=bigread(wsp[-2].unit.file,p,l);
#else
    n=fread(p,1,(size_t)l,wsp[-2].unit.file);
#endif
    if (ferror(wsp[-2].unit.file)) {
        err_elem.flag=0;
        err_elem.type=NULL_TYPE;
        error(WAKE_CANNOT_ACCESS);
    }
    wsp[-2]=wsp[-1];
    wsp[-2].unit.line.length=n/(long)type_size[wsp[-1].type];
    wsp[-1].flag=0;
    wsp[-1].type=(UBYTE)(l==n);
}

/* oldfilename newfilename - renamefile - */
void wk_renamefile(void)
{
    check(2);
    if (wsp[-2].type!=STRING_TYPE||wsp[-1].type!=STRING_TYPE)
        error(WAKE_INVALID_TYPE);
    rename(wsp[-2].unit.sstring,wsp[-1].unit.sstring);
    wsp-=2;
}

/* integer block - repeat - */
void wk_repeat(void)
{
    long n;
    int xst;
    jmp_buf xjb;
    check(2);
    if (!(wsp[-2].flag&INTEGER)||wsp[-1].type!=BLOCK_TYPE)
        error(WAKE_INVALID_TYPE);
    memcpy(xjb,exit_jump,sizeof(jmp_buf));
    xst=exit_est;
    exit_est=est;
    push(wsp-1);
    exec_stack[est-1].element.unit.head[-1].prot++;
    wsp-=2;
    if (!setjmp(exit_jump)) {
        n=wsp->unit.integer;
        while (n--) execute();
    }
    memcpy(exit_jump,xjb,sizeof(jmp_buf));
    est=exit_est;
    exit_est=xst;
    eel= &exec_stack[est-1].exec_elem;
    exec_stack[est].element.unit.head[-1].prot--;
}

/* save - restore - */
void wk_restore(void)
{
    HEAD *p=last_save,*q,*r=NULL,*s;
    check(1);
    if (wsp[-1].type!=SAVE_TYPE) error(WAKE_INVALID_TYPE);
    s=wsp[-1].unit.save;
    while (p&&p!=s)
        if (p->prot||(p->head&&p->head->prot)) {
            r=p;
            p=p->last;
        }
        else if (p->head) {
            q=p;
            p=p->last;
            if (r) r->last=p;
            else last_save=p;
            free_composite(q);
        }
        else {
            p->flag|=MARKED;
            r=p;
            p=p->last;
        }
    p=last_save;
    r=NULL;
    while (p&&p!=s)
        if (p->flag&MARKED) {
            q=p;
            p=p->last;
            if (r) r->last=p;
            else last_save=p;
            free_composite(q);
        }
        else {
            r=p;
            p=p->last;
        }
    if (p!=s) error(WAKE_ILLEGAL_USE);
    wsp--;
}

/* any1..anyn n i - roll - anyn-i+1..anyn any1..anyi */
void wk_roll(void)
{
    int a,n;
    ELEMENT e;
    check(2);
    if (!(wsp[-2].flag&INTEGER)||!(wsp[-1].flag&INTEGER))
        error(WAKE_INVALID_TYPE);
    n=(int)wsp[-2].unit.integer;
    a=(int)wsp[-1].unit.integer;
    check(n+2);
    if (n>1) {
        if (a>0)
            while (a--) {
                e=wsp[-3];
                memmove((void *)(wsp-n-1),(void *)(wsp-n-2),
                        (n-1)*sizeof(ELEMENT));
                wsp[-n-2]=e;
            }
        else
            while (a++) {
                e=wsp[-n-2];
                memmove((void *)(wsp-n-2),(void *)(wsp-n-1),
                        (n-1)*sizeof(ELEMENT));
                wsp[-3]=e;
            }
    }
    wsp-=2;
}

/* filename - run - */
void wk_run(void)
{
    FILE *f;
    int n,xst;
    jmp_buf qjb,sjb,xjb;
    char *nm=file_name;
    check(1);
    if (wsp[-1].type!=STRING_TYPE) error(WAKE_INVALID_TYPE);
    f=fopen(wsp[-1].unit.sstring+strspn(wsp[-1].unit.sstring,whites),"rb");
    if (!f) access_error(wsp[-1].unit.sstring);
    wsp--;
    file_name=wsp->unit.sstring;
    memcpy(qjb,quit_jump,sizeof(jmp_buf));
    memcpy(sjb,stop_jump,sizeof(jmp_buf));
    memcpy(xjb,exit_jump,sizeof(jmp_buf));
    xst=exit_est;
    exit_est=est;
    if (!(n=setjmp(quit_jump))) {
        memcpy(stop_jump,quit_jump,sizeof(jmp_buf));
        memcpy(exit_jump,quit_jump,sizeof(jmp_buf));
        wsp->flag=0;
        wsp->type=FILE_TYPE;
        wsp->unit.file=f;
        push(wsp);
        wake();
    }
    fclose(f);
    file_name=nm;
    memcpy(exit_jump,xjb,sizeof(jmp_buf));
    memcpy(quit_jump,qjb,sizeof(jmp_buf));
    memcpy(stop_jump,sjb,sizeof(jmp_buf));
    switch (n) {
    case WAKE_QUIT:
        longjmp(quit_jump,WAKE_QUIT);
    case WAKE_STOP:
        longjmp(stop_jump,WAKE_STOP);
    }
    est=exit_est;
    exit_est=xst;
    eel= &exec_stack[est-1].exec_elem;
}

/* - save - save */
void wk_save(void)
{
    stack(1);
    wsp->flag=0;
    wsp->type=SAVE_TYPE;
    wsp->unit.save=last_save;
    wsp++;
}

/* string pattern - search - suffix match prefix | string null */
void wk_search(void)
{
    ULONG i,l,n;
    UBYTE *p,*s;
    check(2);
    if (wsp[-2].type!=STRING_TYPE||wsp[-1].type!=STRING_TYPE)
        error(WAKE_INVALID_TYPE);
    if (wsp[-1].unit.line.length>wsp[-2].unit.line.length) {
        wsp[-1].flag=0;
        wsp[-1].type=NULL_TYPE;
        return;
    }
    s=wsp[-2].unit.string;
    p=wsp[-1].unit.string;
    l=wsp[-1].unit.line.length;
    n=wsp[-2].unit.line.length-l+1;
    for (i=0; i<n; i++)
        if (!memcmp(s+i,p,(size_t)l)) {
            stack(1);
            wsp->flag=wsp[-2].flag;
            wsp->type=STRING_TYPE;
            wsp->unit.string=s;
            wsp->unit.line.length=i;
            wsp[-1].flag=(UBYTE)((wsp[-2].flag&(0xff^COMPOSITE))|INTERVAL);
            wsp[-1].type=STRING_TYPE;
            wsp[-1].unit.string=s+i;
            wsp[-1].unit.line.length=l;
            wsp[-2].flag=wsp[-1].flag;
            wsp[-2].unit.string=s+i+l;
            wsp[-2].unit.line.length-=i+l;
            wsp++;
            return;
        }
    wsp[-1].flag=0;
    wsp[-1].type=NULL_TYPE;
}

/* file position - setfileposition - */
void wk_setfileposition(void)
{
    check(2);
    if (wsp[-2].type!=FILE_TYPE||!(wsp[-1].flag&INTEGER))
        error(WAKE_INVALID_TYPE);
    if (fseek(wsp[-2].unit.file,wsp[-1].unit.integer,SEEK_SET)) {
        err_elem.flag=0;
        err_elem.type=NULL_TYPE;
        error(WAKE_CANNOT_ACCESS);
    }
    wsp-=2;
}

/* size - shortarray - shortarray */
void wk_shortarray(void)
{
    ULONG l;
    check(1);
    if (!(wsp[-1].flag&INTEGER)) error(WAKE_INVALID_TYPE);
    l=wsp[-1].unit.uinteger;
    wsp[-1].unit.shortarray=(short *)get_mem(l,SHORTARRAY_TYPE,TRUE);
    wsp[-1].unit.line.length=l;
    wsp[-1].type=SHORTARRAY_TYPE;
    wsp[-1].flag=COMPOSITE|INDEXED;
}

/* size - singlearray - singlearray */
void wk_singlearray(void)
{
    ULONG l;
    check(1);
    if (!(wsp[-1].flag&INTEGER)) error(WAKE_INVALID_TYPE);
    l=wsp[-1].unit.uinteger;
    wsp[-1].unit.singlearray=(float *)get_mem(l,SINGLEARRAY_TYPE,TRUE);
    wsp[-1].unit.line.length=l;
    wsp[-1].type=SINGLEARRAY_TYPE;
    wsp[-1].flag=COMPOSITE|INDEXED;
}

/* file string - skip - */
void wk_skip(void)
{
    int c;
    long i,l;
    char *s;
    FILE *f;
    check(2);
    if (wsp[-2].type!=FILE_TYPE||wsp[-1].type!=STRING_TYPE)
        error(WAKE_INVALID_TYPE);
    f=wsp[-2].unit.file;
    s=wsp[-1].unit.sstring;
    l=strlen(s);
    i=0;
    while ((c=fgtc(f))!=EOF)
        if (c!=s[i]) i=0;
        else if (++i==l) break;
    wsp-=2;
}

/* file string - skiptochars - */
void wk_skiptochars(void)
{
    int c;
    char *s;
    FILE *f;
    check(2);
    if (wsp[-2].type!=FILE_TYPE||wsp[-1].type!=STRING_TYPE)
        error(WAKE_INVALID_TYPE);
    f=wsp[-2].unit.file;
    s=wsp[-1].unit.sstring;
    while ((c=fgtc(f))!=EOF&&!strchr(s,c));
    wsp-=2;
}

/* composite comparisionblock - sort - composite */
void wk_sort(void)
{
    long i,j,k,l;
    check(2);
    if (!(wsp[-2].flag&INDEXED)||wsp[-1].type!=BLOCK_TYPE)
        error(WAKE_INVALID_TYPE);
    l=wsp[-2].unit.line.length;
    push(wsp-1);
    exec_stack[est-1].element.unit.head[-1].prot++;
    switch (wsp[-2].type) {
    case ARRAY_TYPE:
    case BLOCK_TYPE: {
        ELEMENT *a=wsp[-2].unit.area,t;
        wsp--;
        for (k=l>>1; k; k>>=1)
            for (i=k; i<l; i++)
                for (j=i-k; j>=0; j-=k) {
                    stack(2);
                    *wsp++=a[j];
                    *wsp++=a[j+k];
                    execute();
                    check(1);
                    wsp--;
                    if (wsp->type) {
                        t=a[j];
                        a[j]=a[j+k];
                        a[j+k]=t;
                    }
                    else break;
                }
        break;
    }
    case FLOATARRAY_TYPE: {
        double *a=wsp[-2].unit.floatarray,t;
        wsp--;
        for (k=l>>1; k; k>>=1)
            for (i=k; i<l; i++)
                for (j=i-k; j>=0; j-=k) {
                    stack(2);
                    wsp->flag=wsp[1].flag=NUMBER;
                    wsp->type=wsp[1].type=FLOATING_TYPE;
                    wsp->unit.floating=a[j];
                    wsp++;
                    wsp->unit.floating=a[j+k];
                    wsp++;
                    execute();
                    check(1);
                    wsp--;
                    if (wsp->type) {
                        t=a[j];
                        a[j]=a[j+k];
                        a[j+k]=t;
                    }
                    else break;
                }
        break;
    }
    case INTARRAY_TYPE: {
        long *a=wsp[-2].unit.intarray,t;
        wsp--;
        for (k=l>>1; k; k>>=1)
            for (i=k; i<l; i++)
                for (j=i-k; j>=0; j-=k) {
                    stack(2);
                    wsp->flag=wsp[1].flag=NUMBER|INTEGER;
                    wsp->type=wsp[1].type=INTEGER_TYPE;
                    wsp->unit.integer=a[j];
                    wsp++;
                    wsp->unit.integer=a[j+k];
                    wsp++;
                    execute();
                    check(1);
                    wsp--;
                    if (wsp->type) {
                        t=a[j];
                        a[j]=a[j+k];
                        a[j+k]=t;
                    }
                    else break;
                }
        break;
    }
    case UINTARRAY_TYPE:{
        ULONG *a=wsp[-2].unit.uintarray,t;
        wsp--;
        for (k=l>>1; k; k>>=1)
            for (i=k; i<l; i++)
                for (j=i-k; j>=0; j-=k) {
                    stack(2);
                    wsp->flag=wsp[1].flag=NUMBER|INTEGER;
                    wsp->type=wsp[1].type=UINTEGER_TYPE;
                    wsp->unit.uinteger=a[j];
                    wsp++;
                    wsp->unit.uinteger=a[j+k];
                    wsp++;
                    execute();
                    check(1);
                    wsp--;
                    if (wsp->type) {
                        t=a[j];
                        a[j]=a[j+k];
                        a[j+k]=t;
                    }
                    else break;
                }
        break;
    }
    case SINGLEARRAY_TYPE: {
        float *a=wsp[-2].unit.singlearray,t;
        wsp--;
        for (k=l>>1; k; k>>=1)
            for (i=k; i<l; i++)
                for (j=i-k; j>=0; j-=k) {
                    stack(2);
                    wsp->flag=wsp[1].flag=NUMBER;
                    wsp->type=wsp[1].type=FLOATING_TYPE;
                    wsp->unit.floating=(double)a[j];
                    wsp++;
                    wsp->unit.floating=(double)a[j+k];
                    wsp++;
                    execute();
                    check(1);
                    wsp--;
                    if (wsp->type) {
                        t=a[j];
                        a[j]=a[j+k];
                        a[j+k]=t;
                    }
                    else break;
                }
        break;
    }
    case SHORTARRAY_TYPE: {
        short *a=wsp[-2].unit.shortarray,t;
        wsp--;
        for (k=l>>1; k; k>>=1)
            for (i=k; i<l; i++)
                for (j=i-k; j>=0; j-=k) {
                    stack(2);
                    wsp->type=wsp[1].type=NUMBER|INTEGER;
                    wsp->type=wsp[1].type=INTEGER_TYPE;
                    wsp->unit.integer=(long)a[j];
                    wsp++;
                    wsp->unit.integer=(long)a[j+k];
                    wsp++;
                    execute();
                    check(1);
                    wsp--;
                    if (wsp->type) {
                        t=a[j];
                        a[j]=a[j+k];
                        a[j+k]=t;
                    }
                    else break;
                }
        break;
    }
    case USHORTARRAY_TYPE: {
        UWORD *a=wsp[-2].unit.ushortarray,t;
        wsp--;
        for (k=l>>1; k; k>>=1)
            for (i=k; i<l; i++)
                for (j=i-k; j>=0; j-=k) {
                    stack(2);
                    wsp->type=wsp[1].type=NUMBER|INTEGER;
                    wsp->type=wsp[1].type=UINTEGER_TYPE;
                    wsp->unit.uinteger=(ULONG)a[j];
                    wsp++;
                    wsp->unit.uinteger=(ULONG)a[j+k];
                    wsp++;
                    execute();
                    check(1);
                    wsp--;
                    if (wsp->type) {
                        t=a[j];
                        a[j]=a[j+k];
                        a[j+k]=t;
                    }
                    else break;
                }
        break;
    }
    case STRING_TYPE:
    case WAVE_TYPE: {
        UBYTE *a=wsp[-2].unit.string,t;
        wsp--;
        for (k=l>>1; k; k>>=1)
            for (i=k; i<l; i++)
                for (j=i-k; j>=0; j-=k) {
                    stack(2);
                    wsp->type=wsp[1].type=NUMBER|INTEGER;
                    wsp->type=wsp[1].type=INTEGER_TYPE;
                    wsp->unit.integer=(long)a[j];
                    wsp++;
                    wsp->unit.integer=(long)a[j+k];
                    wsp++;
                    execute();
                    check(1);
                    wsp--;
                    if (wsp->type) {
                        t=a[j];
                        a[j]=a[j+k];
                        a[j+k]=t;
                    }
                    else break;
                }
        break;
    }
    default:
        error(WAKE_INVALID_TYPE);
    }
    eel= &exec_stack[--est-1].exec_elem;
    exec_stack[est].element.unit.head[-1].prot--;
}

/* filename - stat - size time */
void wk_stat(void)
{
    struct stat s;
    check(1);
    if (wsp[-1].type!=STRING_TYPE) error(WAKE_INVALID_TYPE);
    if (stat(wsp[-1].unit.sstring+strspn(wsp[-1].unit.sstring,
                                         whites),&s)) access_error(wsp[-1].unit.sstring);
    stack(1);
    wsp[-1].flag=NUMBER|INTEGER;
    wsp[-1].type=INTEGER_TYPE;
    wsp[-1].unit.integer=s.st_size;
    wsp->flag=NUMBER|INTEGER;
    wsp->type=INTEGER_TYPE;
    wsp->unit.integer=s.st_mtime;
    wsp++;
}

/* file - status - bool */
void wk_status(void)
{
#ifdef _WIN32
    struct _stat stat;
#endif
    check(1);
    if (wsp[-1].type!=FILE_TYPE) error(WAKE_INVALID_TYPE);
    wsp[-1].flag=0;
#ifdef _DCC
    wsp[-1].type=(wsp[-1].unit.file->sd_Flags&__SIF_OPEN)!=0;
#elif defined(__TURBOC__)
    wsp[-1].type=(wsp[-1].unit.file->flags&(_F_RDWR|_F_READ|_F_WRIT))!=0;
#elif defined(VAXC)
    wsp[-1].type=((*wsp[-1].unit.file)->_flag&(_IOREAD|_IOWRT|_IORW))!=0;
#elif defined(__APPLE__)
    wsp[-1].type=(UBYTE)((wsp[-1].unit.file->_flags&(__SRD|__SWR))!=0);
#elif defined(_WIN32)
    _fstat(fileno(wsp[-1].unit.file), &stat);
    wsp[-1].type=stat.st_mode&(_S_IREAD|_S_IWRITE)!=0;
#else
    wsp[-1].type=(UBYTE)((wsp[-1].unit.file->_flags^(_IO_NO_READS|_IO_NO_WRITES))!=0);
#endif
}

/* - stop - */
void wk_stop(void)
{
    if (!stop_level) error(WAKE_ILLEGAL_USE);
    error(WAKE_STOPPED);
}

/* block - stopped - true | null */
void wk_stopped(void)
{
    int sst,xst;
    jmp_buf sjb,xjb;
    check(1);
    wsp--;
    memcpy(xjb,exit_jump,sizeof(jmp_buf));
    memcpy(sjb,stop_jump,sizeof(jmp_buf));
    sst=stop_est;
    stop_est=est;
    xst=exit_est;
    exit_est=est;
    stop_level++;
    push(wsp);
    exec_stack[est-1].element.unit.head[-1].prot++;
    if (!setjmp(stop_jump)) {
        memcpy(exit_jump,stop_jump,sizeof(jmp_buf));
        execute();
        stack(1);
        wsp->flag=0;
        wsp->type=NULL_TYPE;
        wsp++;
    }
    memcpy(exit_jump,xjb,sizeof(jmp_buf));
    memcpy(stop_jump,sjb,sizeof(jmp_buf));
    est=stop_est;
    stop_est=sst;
    exit_est=xst;
    eel= &exec_stack[est-1].exec_elem;
    exec_stack[est].element.unit.head[-1].prot--;
    stop_level--;
}

/* size - string - string */
void wk_string(void)
{
    ULONG n;
    check(1);
    if (!(wsp[-1].flag&INTEGER)) error(WAKE_INVALID_TYPE);
    n=wsp[-1].unit.uinteger;
    wsp[-1].unit.string=(UBYTE *)get_mem(n+1,STRING_TYPE,TRUE);
    wsp[-1].unit.line.length=n;
    wsp[-1].type=STRING_TYPE;
    wsp[-1].flag=COMPOSITE|INDEXED;
}

/* - time - seconds */
void wk_time(void)
{
    time_t t;
    stack(1);
    wsp->flag=NUMBER|INTEGER;
    wsp->type=UINTEGER_TYPE;
    wsp->unit.uinteger=time(&t);
    wsp++;
}

/* file - token - token */
void wk_token(void)
{
    int t;
    ELEMENT elem;
    check(1);
    if (wsp[-1].type!=FILE_TYPE) error(WAKE_INVALID_TYPE);
    t=est;
    push(wsp-1);
    if (token(&elem)==EOF) elem.type=NULL_TYPE;
    if (elem.type==ENTRY_TYPE&&!elem.unit.entry) {
        char *p;
        if (!(p=(char *)memalloc(strlen(strbuf)+1)))
            error(WAKE_CANNOT_ALLOCATE);
        strcpy(p,strbuf);
        if (!(elem.unit.entry=enter(p,nsp[-1]))) {
            memfree(p);
            error(WAKE_CANNOT_ALLOCATE); }
        elem.unit.site.nomen=nsp[-1];
    }
    wsp[-1]=elem;
    est=t;
    eel= &exec_stack[est-1].exec_elem;
}

/* ushortarray - totime - ushortarray time */
void wk_totime(void)
{
    int i;
    UWORD *a;
    time_t t;
    struct tm tm;
    check(1);
    if (wsp[-1].type!=USHORTARRAY_TYPE) error(WAKE_INVALID_TYPE);
    if (wsp[-1].unit.line.length<9) error(WAKE_INDEX_OUT_OF_RANGE);
    stack(1);
    a=wsp[-1].unit.ushortarray;
    for (i=0; i<9; i++) ((int *)&tm)[i]=(int)a[i];
    if ((t=mktime(&tm))==(time_t)-1) error(WAKE_ILLEGAL_USE);
    for (i=0; i<9; i++) a[i]=(UWORD)((int *)&tm)[i];
    wsp->flag=NUMBER|INTEGER;
    wsp->type=UINTEGER_TYPE;
    wsp->unit.uinteger=t;
    wsp++;
}

/* integer - type - integer */
void wk_toupper(void)
{
    check(1);
    if (wsp[-1].type!=INTEGER_TYPE) error(WAKE_INVALID_TYPE);
    wsp[-1].unit.integer=toupper((long)wsp[-1].type);
}

/* any - type - integer */
void wk_type(void)
{
    check(1);
    wsp[-1].unit.integer=(long)wsp[-1].type;
    wsp[-1].type=INTEGER_TYPE;
    wsp[-1].flag=NUMBER|INTEGER;
}

/* size - uintarray - uintarray */
void wk_uintarray(void)
{
    ULONG l;
    check(1);
    if (!(wsp[-1].flag&INTEGER)) error(WAKE_INVALID_TYPE);
    l=wsp[-1].unit.uinteger;
    wsp[-1].unit.uintarray=(ULONG *)get_mem(l,UINTARRAY_TYPE,TRUE);
    wsp[-1].unit.line.length=l;
    wsp[-1].type=UINTARRAY_TYPE;
    wsp[-1].flag=COMPOSITE|INDEXED;
}

/* name - unbind - */
void wk_unbind(void)
{
    ENTRY *en;
    check(1);
    if (wsp[-1].type!=NAME_TYPE) error(WAKE_INVALID_TYPE);
    en=wsp[-1].unit.entry;
    if (en->flag&BOUND) {
        if (en->element.flag&COMPOSITE)
            en->element.unit.head[-1].prot--;
        en->flag&=0xff^BOUND;
    }
    wsp--;
}

/* - usertime - usertime */
void wk_usertime(void)
{
#if defined(_MSC_VER)
    struct timeb timeb;
#else
    struct timeval tv;
#endif
    stack(1);
    wsp->flag=NUMBER|INTEGER;
    wsp->type=UINTEGER_TYPE;
#if defined(_MSC_VER)
    ftime(&timeb);
    wsp->unit.uinteger=(ULONG)(timeb.time - rtimeb.time) * 1000L +
        (timeb.millitm - rtimeb.millitm);
#elif defined(LATTICE)
    wsp->unit.uinteger=(time(&wsp->unit.integer)-rtime)*1000;
#else
    gettimeofday(&tv, NULL);
    wsp->unit.uinteger=(ULONG)((tv.tv_sec - rtimeval.tv_sec) * 1000L +
                               (tv.tv_usec - rtimeval.tv_usec) / 1000L);
#endif
    wsp++;
}

/* size - ushortarray - ushortarray */
void wk_ushortarray(void)
{
    ULONG l;
    check(1);
    if (!(wsp[-1].flag&INTEGER)) error(WAKE_INVALID_TYPE);
    l=wsp[-1].unit.uinteger;
    wsp[-1].unit.ushortarray=(UWORD *)get_mem(l,USHORTARRAY_TYPE,TRUE);
    wsp[-1].unit.line.length=l;
    wsp[-1].type=USHORTARRAY_TYPE;
    wsp[-1].flag=COMPOSITE|INDEXED;
}

/* file byte - write - */
void wk_write(void)
{
    check(2);
    if (wsp[-2].type!=FILE_TYPE||!(wsp[-1].flag&INTEGER))
        error(WAKE_INVALID_TYPE);
    fptc((int)wsp[-1].unit.integer,wsp[-2].unit.file);
    wsp-=2;
}

/* file string - writehexstring - */
void wk_writehexstring(void)
{
    FILE *f;
    ULONG i,l;
    UBYTE b,*s;
    check(2);
    if (wsp[-2].type!=FILE_TYPE||wsp[-1].type!=STRING_TYPE)
        error(WAKE_INVALID_TYPE);
    s=wsp[-1].unit.string;
    l=wsp[-1].unit.line.length;
    f=wsp[-2].unit.file;
    for (i=0; i<l; i++) {
        b=(UBYTE)(s[i]>>4);
        fptc(HEX_CHR(b),f);
        b=(UBYTE)(s[i]&0xf);
        fptc(HEX_CHR(b),f);
    }
    wsp-=2;
}

/* file string - writeline - */
void wk_writeline(void)
{
    check(2);
    if (wsp[-2].type!=FILE_TYPE||wsp[-1].type!=STRING_TYPE)
        error(WAKE_INVALID_TYPE);
    fpts(wsp[-1].unit.sstring,wsp[-2].unit.file);
    wsp-=2;
}

/* file some - writetyped- */
void wk_writetyped(void)
{
    ULONG l;
    UBYTE MQ *p;
    check(2);
    if (wsp[-2].type!=FILE_TYPE||!(wsp[-1].flag&INDEXED))
        error(WAKE_INVALID_TYPE);
    p=(UBYTE MQ *)wsp[-1].unit.data;
    l=wsp[-1].unit.line.length*(long)type_size[wsp[-1].type];
#ifdef _MSDOS
    bigwrite(wsp[-2].unit.file,p,l);
#else
    fwrite(p,1,l,wsp[-2].unit.file);
#endif
    if (ferror(wsp[-2].unit.file)) {
        err_elem.flag=0;
        err_elem.type=NULL_TYPE;
        error(WAKE_CANNOT_ACCESS);
    }
    wsp-=2;
}

WAKE_FUNC fun2_funcs[]={
    {"free",              wk_free},
    {"freenomen",         wk_freenomen},
    {"get",               wk_get},
    {"getinterval",       wk_getinterval},
    {"getsized",          wk_getsized},
    {"if",                wk_if},
    {"ifelse",            wk_ifelse},
    {"index",             wk_index},
    {"intarray",          wk_intarray},
    {"invert",            wk_invert},
    {"length",            wk_length},
    {"listfiles",         wk_listfiles},
    {"load",              wk_load},
    {"loop",              wk_loop},
    {"makebase",          wk_makebase},
    {"match",             wk_match},
    {"nomen",             wk_nomen},
    {"nomenstack",        wk_nomenstack},
    {"openbase",          wk_openbase},
    {"perform",           wk_perform},
    {"place",             wk_place},
    {"pop",               wk_pop},
    {"print",             wk_print},
    {"printf",            wk_printf},
    {"pull",              wk_pull},
    {"put",               wk_put},
    {"putinterval",       wk_putinterval},
    {"putsized",          wk_putsized},
    {"quit",              wk_quit},
    {"read",              wk_read},
    {"readhexstring",     wk_readhexstring},
    {"readline",          wk_readline},
    {"readtyped",         wk_readtyped},
    {"renamefile",        wk_renamefile},
    {"repeat",            wk_repeat},
    {"restore",           wk_restore},
    {"roll",              wk_roll},
    {"run",               wk_run},
    {"save",              wk_save},
    {"search",            wk_search},
    {"setfileposition",   wk_setfileposition},
    {"shortarray",        wk_shortarray},
    {"singlearray",       wk_singlearray},
    {"skip",              wk_skip},
    {"skiptochars",       wk_skiptochars},
    {"sort",              wk_sort},
    {"stat",              wk_stat},
    {"status",            wk_status},
    {"stop",              wk_stop},
    {"stopped",           wk_stopped},
    {"string",            wk_string},
    {"time",              wk_time},
    {"token",             wk_token},
    {"totime",            wk_totime},
    {"toupper",           wk_toupper},
    {"type",              wk_type},
    {"uintarray",         wk_uintarray},
    {"unbind",            wk_unbind},
    {"usertime",          wk_usertime},
    {"ushortarray",       wk_ushortarray},
    {"write",             wk_write},
    {"writehexstring",    wk_writehexstring},
    {"writeline",         wk_writeline},
    {"writetyped",        wk_writetyped}};

int init_fun2(void)
{
    return enter_funcs(fun2_funcs,ELEMS(fun2_funcs));
}
