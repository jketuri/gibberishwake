
% Postscript driver for Wake
% Run this file before any graphics generating programs
% and redirect output to postscript device (to a file or serial port).
% There must not be any extra standard output during graphics drawing.
% E.g. wk >code.ps post.wk yourprog.wk
% This generates postscript code for similar image as in screen
% as a landscape page. term function must be called explicitly.
% flood function and complement mode cannot be used because the nature of ps.

systemnomen begin

% /ps_xsize 793 det % American
% /ps_ysize 613 det
/ps_xsize 876 det % European
/ps_ysize 618 det

/PI180 180.0 PI div det

/ps_arc (arc) det
/ps_arcn (arcn) det
/ps_closepath (closepath) det
/ps_currentpoint (currentpoint) det
/ps_curveto (curveto) det
/ps_fill (fill) det
/ps_grestore (grestore) det
/ps_gsave (gsave) det
/ps_initgraphics (initgraphics) det
/ps_initmatrix (initmatrix) det
/ps_lineto (lineto) det
/ps_moveto (moveto) det
/ps_newpath (newpath) det
/ps_rcurveto (rcurveto) det
/ps_rlineto (rlineto) det
/ps_rmoveto (rmoveto) det
/ps_rotate (rotate) det
/ps_scale (scale) det
/ps_setgray (setgray) det
/ps_setlinewidth (setlinewidth) det
/ps_show (show) det
/ps_stroke (stroke) det
/ps_translate (translate) det

% any - ps_out -
/ps_out {stdout exch print stdout ( ) print} det

% any - ps_new -
/ps_new {stdout exch print stdout (\n) print} det

% radians - ps_angle - degrees
/ps_angle {PI180 mul neg 90.0 add} det

% - ps_check -
/ps_check
{
	ps_area not {ps_currentpoint ps_out ps_stroke ps_out ps_moveto ps_new} if
} det

% - ps_check1 -
/ps_check1
{
	ps_area not {ps_stroke ps_new} if
} det

% - ps_fcoord -
/ps_fcoord
{
	ps_fact not
	{
		ps_initmatrix ps_new
		ps_rotangle PI180 mul neg ps_out ps_rotate ps_new
		ps_sxf ps_syf ps_out ps_out ps_scale ps_new
		ps_lwf ps_out ps_setlinewidth ps_new
		/ps_fact true def
	} if
} det

% - ps_icoord -
/ps_icoord
{
	ps_fact
	{
		ps_initmatrix ps_new
		ps_rotangle PI180 mul neg ps_out ps_rotate ps_new
		ps_sxi ps_syi ps_out ps_out ps_scale ps_new
		ps_lwi ps_out ps_setlinewidth ps_new
		/ps_fact null def
	} if
} det

% number - ps_coord -
/ps_coord {type FLOATING_TYPE eq {ps_fcoord} {ps_icoord} ifelse} det

% xrange yrange - ps_set_vals -
/ps_set_vals
{
	/ps_syf exch ps_ysize mul def
	/ps_sxf exch ps_xsize mul def
	/ps_lwf 1 ps_sxf div def
	/ps_lwi 1 ps_sxi div def
} det

% radius angle endangle - arc -
/arc
{
	3 copy
	2 index ps_coord
	1 index sin 3 index mul neg ps_out
	1 index cos 3 index mul neg ps_out ps_rmoveto ps_out
	ps_currentpoint ps_new ps_check1 ps_newpath ps_new
	exch 3 -1 roll ps_out ps_angle ps_out ps_angle ps_out
	ps_arcn ps_new
	arc
} det

% - area -
/area
{
	ps_check
	/ps_area true def
	area
} det

% dx1 dy1 dx2 dy2 ax2 ay2 - curve -
/curve
{
	6 copy
	dup ps_coord
	6 -2 roll ps_out ps_out
	4 -2 roll ps_out ps_out
	ps_out ps_out
	ps_curveto ps_new
	curve
} det

% rdx1 rdy1 rdx2 rdy2 rax2 ray2 - curverel
/curverel
{
	6 copy
	dup ps_coord
	6 -2 roll ps_out ps_out
	4 -2 roll ps_out ps_out
	ps_out ps_out
	ps_rcurveto ps_new
	curverel
} det

% angle - direction -
/direction
{
	/rotangle 1 index def
	direction
} det

% x y - draw -
/draw
{
	2 copy
	dup ps_coord
	ps_out ps_out ps_lineto ps_new
	draw
} det

% a b - drawrel -
/drawrel
{
	2 copy
	dup ps_coord
	ps_out ps_out ps_rlineto ps_new
	drawrel
} det

% - fill -
/fill
{
	ps_area
	{
		ps_fill ps_new
	}
	{
		ps_closepath ps_out
		ps_currentpoint ps_out
		ps_stroke ps_out
		ps_moveto ps_new
	}
	ifelse
	fill
} det

% - line -
/line
{
	/ps_area null def
	line
} det

% x y - move -
/move
{
	2 copy
	ps_check1
	dup ps_coord
	ps_out ps_out ps_moveto ps_new
	move
} det

% a b - moverel -
/moverel
{
	2 copy
	ps_check
	dup ps_coord
	ps_out ps_out ps_rmoveto ps_new
	moverel
} det

% x y - origo -
/origin
{
	2 copy
	ps_fact null def ps_fcoord
	ps_out ps_out ps_translate ps_new
	origin
} det

% x y - pixel -
/pixel
{
	2 copy
	dup ps_coord
	ps_gsave ps_new
	ps_out ps_out ps_moveto ps_new
	ps_currentpoint ps_out ps_fact { ps_lwf } { ps_lwi} if ps_out
	0 ps_out 360 ps_out ps_arc ps_out ps_fill ps_new
	ps_grestore ps_new
	pixel
} det

% a b - range -
/range
{
	2 copy ps_set_vals
	range
} det

% mode - setmode -
/setmode
{
	dup 0 eq {0.0} {0.5} ifelse
	ps_out ps_setgray ps_new
	setmode
} det

% - term -
/term
{
	ps_check1
	(showpage) ps_new
	ps_grestore ps_new
	term
} det

% string - text -
/text
{
	stdout (\(%s\) ) 2 index printf
	ps_show ps_new
	text
} det

save
init
dimensions
/ps_syi exch ps_ysize div def
/ps_sxi exch ps_xsize div def
/ps_rotangle 0.0 def
(%!) ps_new
ps_gsave ps_new
1.0 1.0 ps_set_vals
ps_initgraphics ps_new
(/Helvetica findfont 8 scalefont setfont) ps_new
0 ps_out 0 ps_out ps_moveto ps_new
/ps_area null def
/ps_fact true def
ps_icoord
restore

end
