
# makefile
# MSC, R=1 ready

BD = \bin\
LD = \obj\
!IFDEF D
OD = \dosobj\
!ELSE
OD = \obj\
!ENDIF

$(BD)wk.exe: $(OD)wk.obj $(OD)wake.obj $(OD)wk_fun.obj $(OD)wk_fun2.obj\
 $(OD)wk_gra.obj $(OD)wk_gra2.obj $(OD)wk_log.obj $(OD)wk_mat.obj\
 $(OD)wk_mat2.obj $(OD)wk_pic.obj $(OD)wk_seq.obj $(OD)wk_seq2.obj\
 $(LD)file_acs.lib wk.def $(OD)wk.res
!IFDEF R
	link @wkr.rse
!ELSE
	link @wk.rse
!ENDIF
	rc /k $(OD)wk.res $(BD)wk.exe
	dir $(BD)wk.exe

$(LD)file_acs.lib: $(OD)f_access.obj $(OD)f_addkey.obj $(OD)f_getkey.obj\
 $(OD)f_delkey.obj $(OD)f_setkey.obj
 	del $(LD)file_acs.lib
 	lib $(LD)file_acs.lib @file_acs.rse

{}.c{$(OD)}.obj:
!IFDEF D
	cl /AL /Fo$(OD) /G2rsy /Ox /WX /c $<
!ELSEIFDEF R
	cl /AL /Fo$(OD) /G2Arsy /Ox /WX /c $<
!ELSE
	cl /AL /Fo$(OD) /G2Ay /Od /WX /Zi /c $<
!ENDIF

{}.rc{$(OD)}.res:
	rc -fo$(OD)$(@B).res -r $<
