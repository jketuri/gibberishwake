$! do wk executable
$ compile: subroutine
$ if f$search("<.OBJ>''p1'.OBJ") .eqs. "" then goto comp1
$ if f$cvtime(f$file_attributes("<.OBJ>''p1'.OBJ","RDT")) .ges.-
 f$cvtime(f$file_attributes("''p1'.C","RDT")) then exit
$ comp1:
$ write sys$output "Compiling ''p1'.C"
$ cc /define=XLIB /object=<.OBJ>'p1'.OBJ 'p1'.C
$ compflag == 1
$ endsubroutine
$ compflag == 0
$ call compile wk
$ call compile wake
$ call compile wake_fun
$ call compile wake_gra
$ call compile wake_log
$ call compile wake_mat
$ call compile wake_pic
$ if f$search("WK.EXE") .eqs. "" .or. compflag
$ then
$  write sys$output "Linking to WK.EXE"
$  set default <.obj>
$  link /executable=<->wk.exe -
wk.obj,wake.obj,wake_fun.obj,wake_gra.obj,wake_log.obj,wake_mat.obj,-
wake_pic.obj,sys$input/options
sys$share:decw$dwtlibshr/share
sys$share:decw$xlibshr/share
$  set default <->
$ endif
