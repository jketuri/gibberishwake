Wake is a complete coding environment, though it lacks threading and
network support. You should use your own head for threading and
network.  Wake syntax bases to PostScript-language and borrows
mechanisms from FORTH-language.  Wake has a dynamic binding mechanism,
and it can be optimized to translate variable references inline with
pointers, look at words 'det' and 'fix'. This feature is similar to
PostScript bind- and //-operators. Wake has explicit dynamic scoping,
which is exercised with nomen-objects, which correspond to
dict-objects in PostScript. Although Wake is a completely interpreted
language, it is fast at execution. It can render in some way
PostScript- and Adobe Illustrator-drawings, though not perfectly,
because it uses native graphics libraries found in various
environments. It is supported in Windows, Linux, VAX/VMS, MS-DOS and
Mac OSX-environments. It has extensive support for processing
MIDI-sequences. This is very convenient for serial paradigm of
composition, where you calculate your score and convert it with
tables. Wake has number types for integer numbers, rational numbers,
complex numbers and floating point numbers.

For newer versions of Mac OSX there is needed:
https://www.xquartz.org/
for X Window System

for compilation there is needed:
http://www.iodbc.org/dataspace/doc/iodbc/wiki/iodbcWiki/WelcomeVisitors
for some header files.
