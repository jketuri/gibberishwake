
/* wk_seq2.c */

#include "wake.h"
#include "wk_gra.h"
#include "wk_seq.h"

void shut_seq(void)
{
    if (!seqshare) return;
    if (seqshare->gflag&PROCEEDING) halt_seq();
    {
#ifdef AMIGA
        UBYTE i, j;
        if (narrator_rb) {
            if (narrator_rb->message.io_Device) {
                AbortIO((struct IORequest *)narrator_rb);
                CloseDevice((struct IORequest *)narrator_rb);
            }
            DeleteExtIO((struct IORequest *)narrator_rb);
        }
        if (narMsgPort) DeletePort(narMsgPort);
        if (TranslatorBase) CloseLibrary(TranslatorBase);
        for (i=0; i<4; i++)
            for (j=0; j<3; j++)
                if (IOAudio[i][j]) {
                    if (IOAudio[i][j]->ioa_Request.io_Device)
                        AbortIO((struct IORequest *)IOAudio[i][j]);
                    if (i||j) DeleteExtIO((struct IORequest *)IOAudio[i][j]);
                }
        if (IOAudio[0][0]) {
            if (IOAudio[0][0]->ioa_Request.io_Device)
                CloseDevice((struct IORequest *)IOAudio[0][0]);
            DeleteExtIO((struct IORequest *)IOAudio[0][0]);
        }
        if (audMsgPort) DeletePort(audMsgPort);
        if (IOExtSer[0] && IOExtSer[0]->IOSer.io_Device)
            CloseDevice((struct IORequest *)IOExtSer[0]);
        for (i=0; i<3; i++)
            if (IOExtSer[i]) DeleteExtIO((struct IORequest *)IOExtSer[i]);
        if (serMsgPort) DeletePort(serMsgPort);
        if (timerequest[0] && timerequest[0]->tr_node.io_Device)
            CloseDevice((struct IORequest *)timerequest[0]);
        for (i=0; i<3; i++)
            if (timerequest[i]) DeleteExtIO((struct IORequest *)timerequest[i]);
        if (tmrMsgPort) DeletePort(tmrMsgPort);
#elif defined(__APPLE__)
        if (midiOutEndpointRef)
            MIDIEndpointDispose(midiOutEndpointRef);
        if (midiInEndpointRef) {
            MIDIPortDisconnectSource(midiInPortRef, midiInEndpointRef);
            MIDIEndpointDispose(midiInEndpointRef);
        }
        if (midiInPortRef)
            MIDIPortDispose(midiInPortRef);
        if (midiOutPortRef)
            MIDIPortDispose(midiOutPortRef);
        if (midiClientRef)
            MIDIClientDispose(midiClientRef);
        if (auGraph) {
            AUGraphStop(auGraph);
            DisposeAUGraph(auGraph);
        }
#elif defined(__linux__)
        if (seq_handle) snd_seq_close(seq_handle);
        if (rawmidi_in_handle) {
            snd_rawmidi_drain(rawmidi_in_handle);
            snd_rawmidi_close(rawmidi_in_handle);
        }
        if (rawmidi_out_handle) {
            snd_rawmidi_drain(rawmidi_out_handle);
            snd_rawmidi_close(rawmidi_out_handle);
        }
        if (seqshare!=(SEQSHARE *)-1) shmdt((char *)seqshare);
        if (shmid!=-1) shmctl(shmid, IPC_RMID, 0);
#elif defined(_WINDOWS)
        UBYTE i, j;
        for (i=0; i<2; i++)
            if (seqshare->midiout[i]) {
                midiOutReset(seqshare->midiout[i]);
                wait_midi(i);
                for (j=0; j<2; j++)
                    if (seqshare->midihdr[i][j]) memfree(seqshare->midihdr[i][j]);
                midiOutClose(seqshare->midiout[i]);
                if (seqshare->events[i]) CloseHandle(seqshare->events[i]);
            }
        if (seqshare->wavehdr) {
            if (seqshare->wavehdr->lpData) {
                waveOutReset(seqshare->waveout);
                waveOutUnprepareHeader(seqshare->waveout, seqshare->wavehdr, 
                                       sizeof(WAVEHDR));
            }
            memfree(seqshare->wavehdr);
        }
        if (midiin) {
            midiInClose(midiin);
            if (midiinhdr) memfree(midiinhdr);
        }
        if (seqshare->waveout) waveOutClose(seqshare->waveout);
        if (seqshare->red_pen) DeleteObject(seqshare->red_pen);
        if (seqshare->red_brush) DeleteObject(seqshare->red_brush);
#endif
    }
}

/* - cease - */
void wk_cease(void)
{
#ifdef AMIGA
    AbortIO((struct IORequest *)narrator_rb);
    narrator_rb->message.io_Data=NULL;
#endif
}

/* sequence sequence1 factor - changerate - sequence */
void wk_changerate(void)
{
    int i;
    ULONG stamp;
    UBYTE MQ *seq_pos[2], MQ *seq_end[2], length;
    float factor;
    check(3);
    factor=1.0/(float)check_double(wsp-1);
    for (i=0; i<2; i++) {
        if (wsp[i-3].type!=STRING_TYPE) error(WAKE_INVALID_TYPE);
        seq_pos[i]=(UBYTE MQ *)wsp[i-3].unit.string;
        seq_end[i]=seq_pos[i]+wsp[i-3].unit.line.length;
    }
    while (seq_pos[1]<seq_end[1]) {
        seq_pos[1]=getvarnum(seq_pos[1], &stamp);
        length=*seq_pos[1]++;
        if (seq_pos[0]+sizeof(ULONG)+length>seq_end[0])
            error(WAKE_INDEX_OUT_OF_RANGE);
        stamp=(ULONG)((float)stamp*factor+.5);
        seq_pos[0]=putvarnum(seq_pos[0], stamp);
        *seq_pos[0]++=length;
        bigmove((UBYTE MQ *)seq_pos[0], (UBYTE MQ *)seq_pos[1], length);
        if (*seq_pos[1] == SPECIAL) break;
        seq_pos[0]+=length;
        seq_pos[1]+=length;
    }
    wsp--;
}

/* sequence sequence1 - convert - sequence */
void wk_convert(void)
{
    int i;
    char pass;
    ULONG stamp, delta=0L;
    UBYTE MQ *seq_pos[2], MQ *seq_end[2], MQ *pos, MQ *end, MQ *len_pos, 
        channel, command;
    check(2);
    for (i=0; i<2; i++) {
        if (wsp[i-2].type!=STRING_TYPE) error(WAKE_INVALID_TYPE);
        seq_pos[i]=(UBYTE MQ *)wsp[i-2].unit.string;
        seq_end[i]=seq_pos[i]+wsp[i-2].unit.line.length;
    }
    if (wsp[-2].unit.line.length<wsp[-1].unit.line.length)
        error(WAKE_INDEX_OUT_OF_RANGE);
    while (seq_pos[1]<seq_end[1]) {
        seq_pos[1]=getvarnum(seq_pos[1], &stamp);
        stamp+=delta;
        pos=seq_pos[0];
        seq_pos[0]=putvarnum(seq_pos[0], stamp);
        len_pos=seq_pos[0]++;
        seq_pos[1]++;
        end=seq_pos[1]+seq_pos[1][-1];
        if (seqshare->gflag&SYSEX)
            while (seq_pos[1]<end && 
                   (*seq_pos[1]&0x80) == 0)
                *seq_pos[0]++= *seq_pos[1]++;
        else while (seq_pos[1]<end && 
                    (*seq_pos[1]&0x80) == 0)
                 seq_pos[1]++;
        while (seq_pos[1]<end) {
            command=(UBYTE)(*seq_pos[1]&0xf0);
            channel=(UBYTE)(*seq_pos[1]&0xf);
            if (*seq_pos[1] == 0xf0) {
                if (seqshare->gflag&SYSEX) {
                    *seq_pos[0]++= *seq_pos[1]++;
                    while (seq_pos[1]<end && 
                           (*seq_pos[1]&0x80) == 0)
                        *seq_pos[0]++= *seq_pos[1]++; }
                else {
                    seq_pos[1]++;
                    while (seq_pos[1]<end && 
                           (*seq_pos[1]&0x80) == 0)
                        seq_pos[1]++; }
                continue; }
            pass=(char)(!(seqshare->conv_flag&CHANNELS)||!(seqshare->channels[channel]&0x80));
            if (pass) {
                if (seqshare->conv_flag&CHANNELS)
                    *seq_pos[0]++=(UBYTE)(command|seqshare->channels[channel]);
                else *seq_pos[0]++= *seq_pos[1];
            }
            seq_pos[1]++;
            switch (command) {
            case NOTE_ON:
            case NOTE_OFF:
                while (seq_pos[1]<end && (*seq_pos[1]&0x80) == 0) {
                    if (pass)
                        if (seqshare->note_flag&(1<<channel))
                            if (seqshare->cnv_str[channel][*seq_pos[1]]&0x80) {
                                seq_pos[1]+=2;
                                continue;
                            }
                            else *seq_pos[0]++=seqshare->cnv_str[channel][*seq_pos[1]];
                        else *seq_pos[0]++= *seq_pos[1];
                    else {
                        seq_pos[1]+=2;
                        continue;
                    }
                    *seq_pos[0]++=(UBYTE)((seqshare->vel_flag&(1<<channel))?
                                          seqshare->vel_str[channel][seq_pos[1][1]]:seq_pos[1][1]);
                    seq_pos[1]+=2; }
                continue;
            case PROGRAM_CHANGE:
                while (seq_pos[1]<end && (*seq_pos[1]&0x80) == 0) {
                    if (pass) {
                        if (seqshare->conv_flag&PROGRAMS) *seq_pos[0]++=seqshare->programs[*seq_pos[1]];
                        else *seq_pos[0]++= *seq_pos[1];
                    }
                    seq_pos[1]++; }
                continue;
            case CHANNEL_PRESSURE:
                while (seq_pos[1]<end && (*seq_pos[1]&0x80) == 0) {
                    if (pass)
                        *seq_pos[0]++= *seq_pos[1];
                    seq_pos[1]++; }
                continue;
            default:
                while (seq_pos[1]<end && (*seq_pos[1]&0x80) == 0) {
                    if (pass) {
                        *seq_pos[0]++= *seq_pos[1]++;
                        *seq_pos[0]++= *seq_pos[1]++; }
                    else seq_pos[1]+=2; }
            } }
        *len_pos=(UBYTE)(seq_pos[0]-len_pos-1);
        if (!*len_pos) {
            delta=stamp;
            seq_pos[0]=pos;
        }
        else delta=0L;
    }
    wsp--;
}

/* boolean - display - */
void wk_display(void)
{
    check(1);
    if (seqshare->gflag&PROCEEDING) error(WAKE_ILLEGAL_USE);
    if (wsp[-1].type) seqshare->gflag|=DISPLAYING;
    else seqshare->gflag&=0xffff^DISPLAYING;
    wsp--;
}

/* chanarray notearray velarray sequence - distribution -
   chanarray notearray velarray */
void wk_distribution(void)
{
    int i;
    char pass;
    long *intarray[3];
    UBYTE MQ *seq_pos, MQ *seq_end, MQ *chn_end, channel, command, length;
    check(4);
    if (wsp[-1].type!=STRING_TYPE) error(WAKE_INVALID_TYPE);
    for (i=0; i<3; i++)
        if (wsp[i-4].type) {
            if (wsp[i-4].type!=INTARRAY_TYPE)
                error(WAKE_INVALID_TYPE);
            if (i) {
                if (wsp[i-4].unit.line.length<128)
                    error(WAKE_INDEX_OUT_OF_RANGE);
            }
            else if (wsp[i-4].unit.line.length<16)
                error(WAKE_INDEX_OUT_OF_RANGE);
            intarray[i]=wsp[i-4].unit.intarray;
            memset(intarray[i], 0, 
                   (size_t)wsp[i-4].unit.line.length*sizeof(long));
        }
        else intarray[i]=NULL;
    seq_pos=(UBYTE MQ *)wsp[-1].unit.string;
    seq_end=seq_pos+wsp[-1].unit.line.length;
    while (seq_pos<seq_end) {
        while (*seq_pos&0x80) seq_pos++;
        seq_pos++;
        length= *seq_pos++;
        if (*seq_pos == SPECIAL) break;
        chn_end=seq_pos+length;
        while (seq_pos<chn_end && (*seq_pos&0x80) == 0)
            seq_pos++;
        while (seq_pos<chn_end) {
            command=(UBYTE)(*seq_pos&0xf0);
            channel=(UBYTE)(*seq_pos&0xf);
            pass=(char)(!(seqshare->conv_flag&CHANNELS)||!(seqshare->channels[channel]&0x80));
            seq_pos++;
            switch (command) {
            case NOTE_ON:
                while (seq_pos<chn_end && (*seq_pos&0x80) == 0) {
                    if (pass && seq_pos[1]) {
                        if (intarray[0]) intarray[0][channel]++;
                        if (intarray[1]) intarray[1][*seq_pos]++;
                        if (intarray[2]) intarray[2][seq_pos[1]]++; }
                    seq_pos+=2; }
                continue;
            case PROGRAM_CHANGE:
            case CHANNEL_PRESSURE:
                while (seq_pos<chn_end && (*seq_pos&0x80) == 0)
                    seq_pos++;
                continue;
            case SYSTEM_EXCLUSIVE:
                while (seq_pos<chn_end && (*seq_pos&0x80) == 0)
                    seq_pos++;
                continue;
            default:
                while (seq_pos<chn_end && (*seq_pos&0x80) == 0)
                    seq_pos+=2;
            } }
    }
    wsp--;
}

/* boolean - echo - */
void wk_echo(void)
{
    check(1);
    if (wsp[-1].type) seqshare->gflag|=ECHOING;
    else seqshare->gflag&=0xffff^ECHOING;
    wsp--;
}

/* boolean - emulation - */
void wk_emulation(void)
{
    check(1);
    if (!(wsp[-1].flag&INTEGER)) error(WAKE_INVALID_TYPE);
    seqshare->emulation=(char)wsp[-1].unit.integer;
    wsp--;
}

/* string string1 - english - string */
void wk_english(void)
{
    check(2);
    if (wsp[-2].type!=STRING_TYPE||wsp[-1].type!=STRING_TYPE)
        error(WAKE_INVALID_TYPE);
#ifdef AMIGA
    if (Translate(wsp[-1].unit.sstring, strlen(wsp[-1].unit.sstring), 
                  wsp[-2].unit.sstring, wsp[-2].unit.line.length))
        error(WAKE_INDEX_OUT_OF_RANGE);
#endif
    wsp--;
}

/* seqshare->sequence - extent - integer */
void wk_extent(void)
{
    UBYTE MQ *seq_pos, MQ *seq_end;
    check(1);
    if (wsp[-1].type!=STRING_TYPE) error(WAKE_INVALID_TYPE);
    seq_pos=wsp[-1].unit.string;
    seq_end=seq_pos+wsp[-1].unit.line.length;
    while (seq_pos<seq_end) {
        while (*seq_pos&0x80) seq_pos++;
        seq_pos++;
        if (seq_pos[1] == SPECIAL) {
            seq_pos+=2;
            break;
        }
        seq_pos+= *seq_pos+1;
    }
    wsp[-1].flag=NUMBER|INTEGER;
    wsp[-1].type=INTEGER_TYPE;
    wsp[-1].unit.integer=seq_pos-(UBYTE MQ *)wsp[-1].unit.string;
}

/* string string1 - finnish - string */
void wk_finnish(void)
{
    check(2);
    if (wsp[-2].type!=STRING_TYPE||wsp[-1].type!=STRING_TYPE)
        error(WAKE_INVALID_TYPE);
#ifdef AMIGA
    if (trans_finnish(wsp[-1].unit.string, 
                      wsp[-2].unit.string, wsp[-2].unit.line.length))
        error(WAKE_INDEX_OUT_OF_RANGE);
#endif
    wsp--;
}

/* - halt - */
void wk_halt(void)
{
    halt_seq();
}

/* sequence sequence1 - join - sequence */
void wk_join(void)
{
    int i;
    long length;
    ULONG stamp, delta;
    UBYTE MQ *seq_pos[2], MQ *seq_end[2], MQ *pos;
    check(2);
    for (i=0; i<2; i++) {
        if (wsp[i-2].type!=STRING_TYPE) error(WAKE_INVALID_TYPE);
        seq_pos[i]=(UBYTE MQ *)wsp[i-2].unit.string;
        seq_end[i]=seq_pos[i]+wsp[i-2].unit.line.length;
    }
    pos=seq_pos[0];
    while (pos<seq_end[0]) {
        seq_pos[0]=pos;
        pos=getvarnum(pos, &stamp);
        if (pos[1] == SPECIAL) break;
        pos+= *pos+1;
    }
    seq_pos[1]=getvarnum(seq_pos[1], &delta);
    stamp+=delta;
    pos=seq_pos[1];
    while (pos<seq_end[1]) {
        if (pos[1] == SPECIAL) {
            pos+=2;
            break;
        }
        pos+= *pos+1;
        while (*pos&0x80) pos++;
        pos++;
    }
    length=pos-seq_pos[1];
    if (seq_pos[0]+sizeof(ULONG)+length>seq_end[0])
        error(WAKE_INDEX_OUT_OF_RANGE);
    seq_pos[0]=putvarnum(seq_pos[0], stamp);
    bigmove((UBYTE MQ *)seq_pos[0], (UBYTE MQ *)seq_pos[1], length);
    wsp--;
}

/* boolean - keyboard - */
void wk_keyboard(void)
{
    check(1);
    if (seqshare->gflag&PROCEEDING) error(WAKE_ILLEGAL_USE);
    if (wsp[-1].type) seqshare->gflag|=KEYBOARD;
    else seqshare->gflag&=0xffff^KEYBOARD;
    wsp--;
}

/* voicename - loadvoice - voice */
void wk_loadvoice(void)
{
#ifdef AMIGA
    LONG file;
    int errn, i, j;
    VOICE *voice=NULL;
    ULONG chunk_size, l;
    UBYTE *chunk_data=NULL;
    float frequence, savarts;
    BYTE codetodelta[]={-34, -21, -13, -8, -5, -3, -2, -1, 0, 1, 2, 3, 5, 8, 13, 21};
    check(1);
    if (wsp[-1].type!=STRING_TYPE) error(WAKE_INVALID_TYPE);
    if (!(file=Open(wsp[-1].unit.sstring, MODE_OLDFILE)))
        access_error(wsp[-1].unit.sstring);
    if (!(voice=(VOICE *)get_mem(1, VOICE_TYPE, FALSE))) goto cantall;
    if (test(file, "FORM")||
        Read(file, &chunk_size, sizeof chunk_size)!=sizeof chunk_size||
        test(file, "8SVXVHDR")||
        Read(file, &chunk_size, sizeof chunk_size)!=sizeof chunk_size||
        Read(file, &voice->vhdr, sizeof(VHDR))!=sizeof(VHDR)||
        voice->vhdr.ctOctave>12) goto fault;
    while (test(file, "BODY")) if (skip(file) == -1) goto fault;
    if (Read(file, &chunk_size, sizeof chunk_size)!=
        sizeof chunk_size) goto fault;
    l=voice->length=voice->vhdr.oneShotHiSamples+
        voice->vhdr.repeatHiSamples;
    for (i=1; i<voice->vhdr.ctOctave; i++) voice->length+=(l<<=1);
    if (!(voice->data=(UBYTE *)AllocMem(voice->length, 
                                        MEMF_CHIP|MEMF_CLEAR|MEMF_PUBLIC))) goto fault;
    if (voice->vhdr.sCompression) {
        ULONG d, x;
        if (!(chunk_data=(UBYTE *)AllocMem(chunk_size, MEMF_PUBLIC))) goto cantall;
        if (Read(file, chunk_data, chunk_size)!=chunk_size) goto fault;
        l=(chunk_size-2)<<1;
        l=min(l, voice->length);
        x=chunk_data[1];
        for (i=0; i<l; i++) {
            d=chunk_data[2+i>>1];
            if (i&1) d&=0xf;
            else d>>=4;
            x+=codetodelta[d];
            voice->data[i]=x;
        }
        FreeMem(chunk_data, chunk_size);
    }
    else {
        chunk_size=min(chunk_size, voice->length);
        if (Read(file, voice->data, chunk_size)!=chunk_size) goto fault;
    }
    Close(file);
    frequence=(float)voice->vhdr.samplesperSec/
        (float)voice->vhdr.samplesperHiCycle;
#ifdef LATTICE
    savarts=(float)(log10((float)(frequence/440.0))*1000.0);
#else
    savarts=log10(frequence/440.0)*1000.0;
#endif
    i=(LONG)(12.0*savarts/301.0)+69;
    for (j=i; j<128; j++) voice->note_byte[j]=0;
    for (j=i-1; j>=0; j--) {
        voice->note_byte[j]=(i-j)/12+1;
        if (voice->note_byte[j]>=voice->vhdr.ctOctave)
            voice->note_byte[j]=voice->vhdr.ctOctave-1;
    }
    *voice->wave_pos=voice->data;
    l=voice->vhdr.oneShotHiSamples+voice->vhdr.repeatHiSamples;
    for (j=1; j<voice->vhdr.ctOctave; j++) {
        voice->wave_pos[j]=voice->wave_pos[j-1]+l;
        l<<=1;
    }
    wsp[-1].unit.line.length=voice->length;
    wsp[-1].type=VOICE_TYPE;
    wsp[-1].flag=COMPOSITE;
    wsp[-1].unit.data=(void *)voice;
    return;
 cantall:
    errn=WAKE_CANNOT_ALLOCATE;
    goto end;
 fault: errn=WAKE_FAULTY_FORM;
 end:   if (voice) {
        if (chunk_data) FreeMem(chunk_data, chunk_size);
        if (voice->data) FreeMem(voice->data, voice->length);
        last_save=*(HEAD **)((HEAD *)voice-1);
        memfree((HEAD *)voice-1);
    }
    Close(file);
    error(errn);
#elif defined(__linux__)
#elif defined(_WINDOWS)
    int errn;
    ULONG i, l;
    HMMIO mmio;
    MMCKINFO par, sub;
    WAVEFORMAT *wf=NULL;
    void *data=NULL, *b=NULL;
    check(1);
    if (wsp[-1].type!=STRING_TYPE) error(WAKE_INVALID_TYPE);
    if (!(mmio=mmioOpen(wsp[-1].unit.sstring, NULL, 
                        MMIO_READ|MMIO_ALLOCBUF))) access_error(wsp[-1].unit.sstring);
    par.fccType=mmioFOURCC('W', 'A', 'V', 'E');
    sub.ckid=mmioFOURCC('f', 'm', 't', ' ');
    if (mmioDescend(mmio, &par, NULL, MMIO_FINDRIFF)||
        mmioDescend(mmio, &sub, &par, MMIO_FINDCHUNK)) goto fault;
    wf=(WAVEFORMAT *)memalloc(sub.cksize);
    if (mmioRead(mmio, (HPSTR)wf, sub.cksize)!=(int)sub.cksize||
        mmioAscend(mmio, &sub, 0)) goto fault;
    sub.ckid=mmioFOURCC('d', 'a', 't', 'a');
    if (mmioDescend(mmio, &sub, &par, MMIO_FINDCHUNK)||!sub.cksize) goto fault;
    l=sub.cksize/wf->nBlockAlign*waveformat.nChannels;
    if (!(data=get_mem(l+(sample_type == STRING_TYPE), sample_type, FALSE))) goto cantall;
    switch (wf->nChannels) {
    case 1:
    case 2:
        break;
    default:
        goto fault;
    }
    switch (wf->nBlockAlign) {
    case 1:
    case 2:
    case 4:
        break;
    default:
        goto fault;
    }
    if (wf->nChannels!=waveformat.nChannels||wf->nBlockAlign!=waveformat.nBlockAlign) {
        if (!(b=memalloc(sub.cksize))) goto fault;
        if (mmioRead(mmio, b, sub.cksize)!=(int)sub.cksize) goto fault;
        switch (wf->nChannels) {
        case 1:
            switch (waveformat.nChannels) {
            case 1:
                switch (wf->nBlockAlign) {
                case 1:
                    if (waveformat.nBlockAlign == 2)
                        for (i=0; i<l; i++) ((USHORT *)data)[i]=
                                                (USHORT)((float)((UBYTE *)b)[i]/(float)0xff*(float)0xffff+.5);
                    break;
                case 2:
                    if (waveformat.nBlockAlign == 1)
                        for (i=0; i<l; i++) ((UBYTE *)data)[i]=
                                                (UBYTE)((float)((USHORT *)b)[i]/(float)0xffff*(float)0xff+.5);
                    break;
                }
                break;
            case 2:
                switch (wf->nBlockAlign) {
                case 1:
                    switch (waveformat.nBlockAlign) {
                    case 2:
                        for (i=0; i<l; i+=2)
                            ((UBYTE *)data)[i]=((UBYTE *)data)[i+1]=
                                ((UBYTE *)b)[i>>1];
                        break;
                    case 4:
                        for (i=0; i<l; i+=2)
                            ((USHORT *)data)[i]=((USHORT *)data)[i+1]=
                                (USHORT)((float)((UBYTE *)b)[i>>1]/(float)0xff*
                                         (float)0xffff+.5);
                        break;
                    }
                    break;
                case 2:
                    switch (waveformat.nBlockAlign) {
                    case 2:
                        for (i=0; i<l; i+=2)
                            ((UBYTE *)data)[i]=((UBYTE *)data)[i+1]=
                                (UBYTE)((float)((USHORT *)b)[i>>1]/(float)0xffff*
                                        (float)0xff+.5);
                        break;
                    case 4:
                        for (i=0; i<l; i+=2)
                            ((USHORT *)data)[i]=((USHORT *)data)[i+1]=
                                ((USHORT *)b)[i>>1];
                        break;
                    }
                    break;
                }
                break;
            }
            break;
        case 2:
            switch (waveformat.nChannels) {
            case 1:
                switch (wf->nBlockAlign) {
                case 2:
                    switch (waveformat.nBlockAlign) {
                    case 1:
                        for (i=0; i<l; i++) ((UBYTE *)data)[i]=(UBYTE)
                                                ((((UBYTE *)b)[i<<1]+((UBYTE *)b)[(i<<1)+1])>>1);
                        break;
                    case 2:
                        for (i=0; i<l; i++) ((UWORD *)data)[i]=(UWORD)
                                                ((UWORD)((float)((UBYTE *)b)[i]/(float)0xff*
                                                         (float)0xffff+.5));
                        break;
                    }
                case 4:
                    switch (waveformat.nBlockAlign) {
                    case 1:
                        for (i=0; i<l; i++) ((UBYTE *)data)[i]=
                                                (UBYTE)((float)((((UWORD *)b)[i<<1]+
                                                                 ((UWORD *)b)[(i<<1)+1])>>1)/(float)0xffff*
                                                        (float)0xff+.5);
                        break;
                    case 2:
                        for (i=0; i<l; i++) ((UWORD *)data)[i]=(UWORD)
                                                ((((UWORD *)b)[i<<1]+((UWORD *)b)[(i<<1)+1])>>1);
                        break;
                    }
                    break;
                }
                break;
            case 2:
                switch (wf->nBlockAlign) {
                case 2:
                    if (waveformat.nBlockAlign == 4)
                        for (i=0; i<l; i+=2) {
                            ((USHORT *)data)[i]=(USHORT)((float)((UBYTE *)b)[i]/
                                                         (float)0xff*(float)0xffff+.5);
                            ((USHORT *)data)[i+1]=(USHORT)((float)((UBYTE *)b)[i+1]/
                                                           (float)0xff*(float)0xffff+.5);
                        }
                    break;
                case 4:
                    if (waveformat.nBlockAlign == 2)
                        for (i=0; i<l; i+=2) {
                            ((UBYTE *)data)[i]=
                                (UBYTE)((float)((USHORT *)b)[i]/(float)0xffff*(float)0xff+.5);
                            ((UBYTE *)data)[i+1]=
                                (UBYTE)((float)((USHORT *)b)[i+1]/(float)0xffff*(float)0xff+.5);
                        }
                    break;
                }
                break;
            }
            break;
        }
    }
    else if (mmioRead(mmio, data, sub.cksize)!=(int)sub.cksize) goto fault;
    mmioClose(mmio, 0);
    wsp[-1].unit.line.length=l;
    wsp[-1].type=sample_type;
    wsp[-1].flag=COMPOSITE|INDEXED;
    wsp[-1].unit.data=data;
    return;
 cantall:
    errn=WAKE_CANNOT_ALLOCATE;
    goto end;
 fault: errn=WAKE_FAULTY_FORM;
 end:   if (wf) memfree(wf);
    if (b) memfree(b);
    if (data) {
        last_save=*(HEAD **)((HEAD *)data-1);
        memfree((HEAD *)data-1);
    }
    mmioClose(mmio, 0);
    error(errn);
#endif
}

/* string channelmask - narrate - */
void wk_narrate(void)
{
    check(2);
    if (seqshare->emulation!=NOEMULATION) error(WAKE_ILLEGAL_USE);
    if (wsp[-2].type!=STRING_TYPE||!(wsp[-1].flag&INTEGER))
        error(WAKE_INVALID_TYPE);
#ifdef AMIGA
    if (narrator_rb->message.io_Data)
        WaitIO((struct IORequest *)narrator_rb);
    ch_mask=(UBYTE)wsp[-1].unit.integer;
    narrator_rb->ch_masks= &ch_mask;
    narrator_rb->nm_masks=1;
    narrator_rb->message.io_Data=wsp[-2].unit.string;
    narrator_rb->message.io_Length=strlen(wsp[-2].unit.sstring);
    SendIO((struct IORequest *)narrator_rb);
#endif
    wsp-=2;
}

/* sequence - play - */
void wk_play(void)
{
    check(1);
    if (wsp[-1].type!=STRING_TYPE && wsp[-1].type!=NULL_TYPE) error(WAKE_INVALID_TYPE);
    if (seqshare->gflag&PROCEEDING) error(WAKE_ILLEGAL_USE);
    if (seqshare->sequence) ((HEAD MQ *)seqshare->sequence)[-1].prot--;
    if (wsp[-1].type) {
        wsp[-1].unit.head[-1].prot++;
        seqshare->sequence=(UBYTE MQ *)wsp[-1].unit.string;
        seqshare->seq_end=seqshare->sequence+wsp[-1].unit.line.length;
        seqshare->gflag|=PLAYING;
    }
    else {
        seqshare->sequence=NULL;
        seqshare->gflag&=0xffff^PLAYING;
    }
    wsp--;
}

/* integer - playmode - */
void wk_playmode(void)
{
    check(1);
    if (!(wsp[-1].flag&INTEGER)) error(WAKE_INVALID_TYPE);
    seqshare->pmode=(UBYTE)wsp[-1].unit.integer;
    wsp--;
}

/* - proceed - */
void wk_proceed(void)
{
#ifdef __linux__
    /*
      pthread_attr_t attr;
      struct sched_param param;
    */
#endif
    if (seqshare->gflag&PROCEEDING) error(WAKE_ILLEGAL_USE);
    seqshare->gflag&=0xffff^ALLOCERROR^INVERTING^READY^RAISED;
#ifdef AMIGA
    if (IOExtSer[0]->IOSer.io_Device) {
        if (!CheckIO((struct IORequest *)IOExtSer[ser_ion]))
            WaitIO((struct IORequest *)IOExtSer[ser_ion]);
        while (GetMsg(IOExtSer[ser_ion]->IOSer.io_Message.mn_ReplyPort)); }
    free_audio();
    procTask=(struct Task *)CreateTask("procTask", 
                                       thatTask->tc_Node.ln_Pri, (APTR)taskproc, 4000);
    if (!procTask) {
        allocate();
        error(WAKE_CANNOT_ALLOCATE);
    }
    Wait(1L);
    if (seqshare->gflag&ALLOCERROR) {
        seqshare->gflag&=0xffff^ALLOCERROR;
        Forbid();
        DeleteTask(procTask);
        Permit();
        procTask=NULL;
        allocate();
        error(WAKE_CANNOT_ALLOCATE);
    }
#elif defined(__linux__) || defined(__APPLE__)
    if (taskproc_init()) {
        taskproc_clean();
        error(WAKE_CANNOT_ALLOCATE);
    }
    /*
      pthread_attr_init(&attr);
      memset(&param, 0, sizeof param);
      param.sched_priority = sched_get_priority_max(SCHED_RR);
      if (pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED) || pthread_attr_setschedpolicy(&attr, SCHED_RR) || pthread_attr_setschedparam(&attr, &param) || pthread_create(&taskproc_id, &attr, taskproc, (void *)NULL)) error(WAKE_CANNOT_ALLOCATE);
      pthread_attr_destroy(&attr);
    */
    if (pthread_create(&taskproc_id, NULL, taskproc, (void *)NULL)) error(WAKE_CANNOT_ALLOCATE);
    /*
      switch (taskproc_pid=fork()) {
      case 0:
      taskproc();
      return;
      case -1:
      error(WAKE_CANNOT_ALLOCATE);
      }
    */
#elif defined(_WINDOWS)
    {
        TIMECAPS tc;
        if (timeGetDevCaps(&tc, sizeof tc)||tc.wPeriodMin>1||
            taskproc_init()) error(WAKE_CANNOT_ALLOCATE);
        if (timeBeginPeriod(1)||
            !(timerid=timeSetEvent(1, 0, taskproc, (DWORD)seqshare, TIME_PERIODIC))) {
            UBYTE n;
            for (n=0; n<16; n++)
                memfree(seqshare->cnv_old[n]);
            error(WAKE_CANNOT_ALLOCATE);
        }
    }
#else
    taskproc();
#endif
    seqshare->gflag|=PROCEEDING;
}

/* channel frequency volume wave - raisesound - */
void wk_raisesound(void)
{
    check(4);
    if (!(wsp[-4].flag&INTEGER)||!(wsp[-3].flag&NUMBER)||
        !(wsp[-2].flag&INTEGER)) error(WAKE_INVALID_TYPE);
#ifdef AMIGA
    {
        int h=wsp[-4].unit.integer;
        if (wsp[-1].type!=WAVE_TYPE) error(WAKE_INVALID_TYPE);
        if (h<0||h>3) error(WAKE_INDEX_OUT_OF_RANGE);
        AbortIO((struct IORequest *)IOAudio[h][0]);
        IOAudio[h][0]->ioa_Request.io_Unit=(struct Unit *)(1<<h);
        IOAudio[h][0]->ioa_Request.io_Flags=IOF_QUICK|ADIOF_PERVOL;
        IOAudio[h][0]->ioa_Request.io_Command=CMD_WRITE;
        IOAudio[h][0]->ioa_Data=wsp[-1].unit.string;
        IOAudio[h][0]->ioa_Length=wsp[-1].unit.line.length;
        IOAudio[h][0]->ioa_Volume=wsp[-2].unit.integer>>10;
        IOAudio[h][0]->ioa_Period=(UWORD)((float)clock_value/
                                          ((float)check_double(wsp-3)*(float)IOAudio[h][0]->ioa_Length)+.5);
        BeginIO((struct IORequest *)IOAudio[h][0]);
    }
#elif defined(__linux__)
#elif defined(_WINDOWS)
    {
        double f, w;
        if (seqshare->waveout) {
            if (wsp[-1].type!=sample_type) error(WAKE_INVALID_TYPE);
            if (seqshare->wavehdr->lpData) {
                waveOutReset(seqshare->waveout);
                waveOutUnprepareHeader(seqshare->waveout, seqshare->wavehdr, 
                                       sizeof(WAVEHDR));
            }
            f=modf(wsp[-3].unit.floating/((double)wsp[-1].unit.line.length/
                                          (double)waveformat.nSamplesPerSec), &w);
            seqshare->wavehdr->dwFlags=0;
            seqshare->wavehdr->dwLoops=0;
            seqshare->wavehdr->lpData=wsp[-1].unit.data;
            seqshare->wavehdr->dwBufferLength=wsp[-1].unit.line.length*
                waveformat.nBlockAlign;
            waveOutSetPitch(seqshare->waveout, (ULONG)w<<16|(ULONG)(f*65536.0));
            waveOutSetVolume(seqshare->waveout, wsp[-2].unit.integer);
            waveOutPrepareHeader(seqshare->waveout, seqshare->wavehdr, sizeof(WAVEHDR));
            waveOutWrite(seqshare->waveout, seqshare->wavehdr, sizeof(WAVEHDR)); }
    }
#endif
    wsp-=4;
}

int readsign(FILE *file, char *sign)
{
    while (*sign)
        if (fgetc(file)!=*sign) return EOF;
        else sign++;
    return 0;
}

long bytes2long(UBYTE b1, UBYTE b2, UBYTE b3, UBYTE b4)
{
    return ((long)b1<<24)|((long)b2<<16)|((long)b3<<8)|b4;
}

short bytes2word(UBYTE b1, UBYTE b2)
{
    return (short)(((short)b1<<8)|b2);
}

/* string filename - readmidi - sequence */
void wk_readmidi(void)
{
    int errn, n;
    float notediv, smptediv;
    short format, ntrks=0, division;
    ULONG length, value, basis=0L, recent=0L, tempo=500000;
    UBYTE needed, status=0, type, MQ *seq_pos, MQ *seq_end, MQ *len_pos=NULL, 
        bytes[4], chantype[]={0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 1, 1, 2, 0};
    struct TRACK {
        ULONG current;
        UBYTE *start;
        UBYTE *place;
        UBYTE *end;
        UBYTE status;
        char sysexcont;
    } *track, *tracks=NULL;
    FILE *f;
    check(2);
    if (wsp[-2].type!=STRING_TYPE||wsp[-1].type!=STRING_TYPE)
        error(WAKE_INVALID_TYPE);
    if (!(f=fopen(wsp[-1].unit.sstring+
                  strspn(wsp[-1].unit.sstring, whites), "rb")))
        access_error(wsp[-1].unit.sstring);
    if (readsign(f, "MThd") == EOF) goto fault;
    if (fread(bytes, 1, 4, f)!=4) goto fault;
    length=bytes2long(bytes[0], bytes[1], bytes[2], bytes[3]);
    if (fread(bytes, 1, 2, f)!=2) goto fault;
    length-=2;
    format=bytes2word(bytes[0], bytes[1]);
    if (fread(bytes, 1, 2, f)!=2) goto fault;
    length-=2;
    ntrks=bytes2word(bytes[0], bytes[1]);
    if (fread(bytes, 1, 2, f)!=2) goto fault;
    length-=2;
    division=bytes2word(bytes[0], bytes[1]);
    while (length>0)
        if (fgetc(f) == EOF) goto fault;
        else length--;
    if (!(tracks=(struct TRACK *)memcalloc(ntrks*sizeof(struct TRACK))))
        goto cantall;
    n=0;
    while (readsign(f, "MTrk")!=EOF && n<ntrks) {
        if (fread(bytes, 1, 4, f)!=4) goto fault;
        length=bytes2long(bytes[0], bytes[1], bytes[2], bytes[3]);
        if (!(tracks[n].start=(UBYTE *)memalloc(length))) goto cantall;
        length=bigread(f, tracks[n].start, length);
        tracks[n].end=tracks[n].start+length;
        tracks[n].place=getvarnum(tracks[n].start, &tracks[n].current);
        n++;
    }
    if (n<ntrks) goto fault;
    notediv=(float)division*(float)1000.0;
    smptediv=(float)(division>>8)*(float)(division&0xff)*(float)1000.0;
    seq_pos=(UBYTE MQ *)wsp[-2].unit.string;
    seq_end=seq_pos+wsp[-2].unit.line.length;
    if (ntrks)
        for (;;) {
            value=(ULONG)-1L;
            track=NULL;
            for (n=0; n<ntrks; n++)
                if (tracks[n].place<tracks[n].end) {
                    if (tracks[n].sysexcont) {
                        track=tracks+n;
                        break;
                    }
                    else if (!track||tracks[n].current<value) {
                        track=tracks+n;
                        value=track->current;
                    }
                }
            if (!track) break;
            value=basis+(division>0?
                         (ULONG)((float)track->current*(float)tempo/notediv+.5):
                         (ULONG)((float)track->current/smptediv+.5));
            if (track->sysexcont && *track->place!=0xf7) goto fault;
            if (*track->place&0x80) track->status=*track->place++;
            else if (!track->status) goto fault;
            needed=chantype[track->status>>4];
            if (needed) {
                if (!len_pos||value!=recent||
                    (UWORD)*len_pos+(UWORD)needed+(track->status!=status)>255) {
                    if (seq_pos+needed+2*sizeof(ULONG)+4>seq_end)
                        goto indexout;
                    status=track->status;
                    seq_pos=putvarnum(seq_pos, value-recent);
                    len_pos=seq_pos++;
                    *seq_pos++=status;
                    *len_pos=1;
                    recent=value;
                }
                else if (track->status!=status) {
                    if (seq_pos+needed+sizeof(ULONG)+3>seq_end)
                        goto indexout;
                    status=track->status;
                    *seq_pos++=status;
                    (*len_pos)++;
                }
                else if (seq_pos+needed+sizeof(ULONG)+2>seq_end)
                    goto indexout;
                *seq_pos++=*track->place++;
                if (needed>1) *seq_pos++=*track->place++;
                *len_pos=(UBYTE)(*len_pos+needed);
            }
            else switch (track->status) {
                case 0xff:
                    type=*track->place++;
                    track->place=getvarnum(track->place, &length);
                    switch (type) {
                    case 0x2f:
                        track->place=track->end;
                        break;
                    case 0x51:
                        basis=value;
                        tempo=bytes2long(0, track->place[0], 
                                         track->place[1], track->place[2]);
                        value=track->current;
                        for (n=0; n<ntrks; n++)
                            tracks[n].current-=value;
                        break; }
                    track->place+=length;
                    break;
                case 0xf0:
                case 0xf7:
                    track->place=getvarnum(track->place, &length);
                    if (!length) break;
                    if ((track->status == 0xf7 && !track->sysexcont)||
                        !(seqshare->gflag&SYSEX)) {
                        track->place+=length;
                        break;
                    }
                    if (!len_pos||*len_pos == 255||value!=recent) {
                        if (seq_pos+length+2*sizeof(ULONG)+4+length/255*2>
                            seq_end) goto indexout;
                        seq_pos=putvarnum(seq_pos, value-recent);
                        len_pos=seq_pos++;
                        *len_pos=0;
                        recent=value;
                    }
                    else if (seq_pos+length+sizeof(ULONG)+5+length/255*2>seq_end)
                        goto indexout;
                    if (track->status == 0xf0) {
                        *seq_pos++=track->status;
                        (*len_pos)++;
                    }
                    do {
                        if (*len_pos == 255) {
                            seq_pos=putvarnum(seq_pos, 0L);
                            len_pos=seq_pos++;
                            *len_pos=0;
                        }
                        n=255-*len_pos;
                        n=min(n, (int)length);
                        memcpy(seq_pos, track->place, n);
                        seq_pos+=n;
                        track->place+=n;
                        *len_pos=(UBYTE)(*len_pos+n);
                        length-=n;
                    }
                    while (length);
                    track->sysexcont=(char)(seq_pos[-1]!=0xf7);
                    track->status=0;
                    status=0xff;
                    break;
                default:
                    goto fault; }
            if (track->place<track->end) {
                track->place=getvarnum(track->place, &value);
                track->current+=value;
            } }
    if (seq_pos+sizeof(ULONG)+2>seq_end) goto indexout;
    seq_pos=putvarnum(seq_pos, 0L);
    *seq_pos++=1;
    *seq_pos=SPECIAL;
    wsp--;
    errn=0;
    goto end;
 cantall:
    errn=WAKE_CANNOT_ALLOCATE;
    goto end;
 fault:
    errn=WAKE_FAULTY_FORM;
    goto end;
 indexout:
    errn=WAKE_INDEX_OUT_OF_RANGE;
 end:
    fclose(f);
    if (tracks) {
        for (n=0; n<ntrks; n++)
            if (tracks[n].start) memfree(tracks[n].start);
        memfree(tracks);
    }
    if (errn) error(errn);
}

/* sequence - record - */
void wk_record(void)
{
    check(1);
    if (wsp[-1].type!=STRING_TYPE && wsp[-1].type!=NULL_TYPE) error(WAKE_INVALID_TYPE);
    if (seqshare->gflag&PROCEEDING) error(WAKE_ILLEGAL_USE);
    if (seqshare->rec_sequence) ((HEAD MQ *)seqshare->rec_sequence)[-1].prot--;
    if (wsp[-1].type) {
        wsp[-1].unit.head[-1].prot++;
        seqshare->rec_sequence=(UBYTE MQ *)wsp[-1].unit.string;
        seqshare->rec_seq_end=seqshare->rec_sequence+wsp[-1].unit.line.length;
        seqshare->gflag|=RECORDING;
    }
    else {
        seqshare->rec_sequence=NULL;
        seqshare->gflag&=0xffff^RECORDING;
    }
    wsp--;
}

/* - reset - */
void wk_reset(void)
{
    int n;
#ifdef _DCC
    float f=(float)pow((float)10.0, (float)0.025);
#else
    float f=(float)pow(10.0, 0.025);
#endif
    seqshare->note_freq[33]=(float)55.0;
    for (n=45; n<118; n+=12)
        seqshare->note_freq[n]=(float)(seqshare->note_freq[n-12]*2.0);
    seqshare->note_freq[21]=(float)(seqshare->note_freq[33]/2.0);
    seqshare->note_freq[9]=(float)(seqshare->note_freq[21]/2.0);
    for (n=8; n>=0; n--)
        seqshare->note_freq[n]=(float)(seqshare->note_freq[n+1]/f);
    for (n=10; n<128; n++)
        if ((n-9)%12) seqshare->note_freq[n]=(float)(seqshare->note_freq[n-1]*f);
    seqshare->gflag&=0xffff^RATING;
    seqshare->rate=256;
}

/* sequence sequence1 - reverse - sequence */
void wk_reverse(void)
{
    int i;
    UBYTE MQ *seq_pos[2], MQ *seq_end[2], MQ *pos, MQ *end, channel, command, 
        vel[16][128];
    check(2);
    for (i=0; i<2; i++) {
        if (wsp[i-2].type!=STRING_TYPE) error(WAKE_INVALID_TYPE);
        seq_pos[i]=(UBYTE MQ *)wsp[i-2].unit.string;
        seq_end[i]=seq_pos[i]+wsp[i-2].unit.line.length;
    }
    if (wsp[-2].unit.line.length<wsp[-1].unit.line.length)
        error(WAKE_INDEX_OUT_OF_RANGE);
    pos=seq_pos[1];
    end=seq_end[1];
    while (pos<end) {
        while (*pos&0x80) pos++;
        pos++;
        if (pos[1] == SPECIAL) {
            pos+=2;
            break;
        }
        pos+= *pos+1;
    }
    seq_pos[0]+=(pos-seq_pos[1]);
    *--seq_pos[0]=SPECIAL;
    *--seq_pos[0]=1;
    memset(vel, 0, sizeof vel);
    while (seq_pos[1]<seq_end[1]) {
        pos=seq_pos[1];
        while (*seq_pos[1]&0x80) seq_pos[1]++;
        seq_pos[1]++;
        i=seq_pos[1]-pos;
        memcpy(UN(seq_pos[0]-=i), UN pos, i);
        if (seq_pos[1][1] == SPECIAL) break;
        i=*seq_pos[1]++;
        end=seq_pos[0];
        memcpy(UN(seq_pos[0]-=i), UN seq_pos[1], i);
        pos=seq_pos[0];
        while (pos<end && (*pos&0x80) == 0) pos++;
        while (pos<end) {
            command=(UBYTE)(*pos&0xf0);
            channel=(UBYTE)(*pos&0xf);
            if (command == NOTE_OFF) *pos=(UBYTE)(NOTE_ON|channel);
            pos++;
            switch (command) {
            case NOTE_ON:
                while (pos<end && (*pos&0x80) == 0) {
                    if (pos[1]) {
                        vel[channel][*pos]=pos[1];
                        pos[1]=0; }
                    else if (vel[channel][*pos])
                        pos[1]=vel[channel][*pos];
                    else error(WAKE_FAULTY_FORM);
                    pos+=2; }
                continue;
            case NOTE_OFF:
                while (pos<end && (*pos&0x80) == 0) {
                    if (vel[channel][*pos]) pos[1]=vel[channel][*pos];
                    else error(WAKE_FAULTY_FORM);
                    pos+=2; }
                continue;
            case PROGRAM_CHANGE:
            case CHANNEL_PRESSURE:
                while (pos<end && (*pos&0x80) == 0) pos++;
                continue;
            case SYSTEM_EXCLUSIVE:
                while (pos<end && (*pos&0x80) == 0) pos++;
                continue;
            default:
                while (pos<end && (*pos&0x80) == 0) pos+=2;
            } }
        *--seq_pos[0]=(UBYTE)i;
        seq_pos[1]+=i;
    }
    wsp--;
}

/* string integer - send - */
void wk_send(void)
{
    check(2);
    if (seqshare->gflag&PROCEEDING) error(WAKE_ILLEGAL_USE);
    if (wsp[-2].type!=STRING_TYPE||!(wsp[-1].flag&INTEGER))
        error(WAKE_INVALID_TYPE);
#ifdef AMIGA
    if (seqshare->emulation!=ONLYEMULATION && IOExtSer[0]->IOSer.io_Device)
        midi_out(wsp[-2].unit.string, wsp[-1].unit.integer);
    if (seqshare->emulation!=NOEMULATION)
        emulate(wsp[-2].unit.string, (UBYTE)wsp[-1].unit.integer);
#elif defined(__linux__)
    if (seq_handle) {
        if (seqshare->emulation!=ONLYEMULATION)
            midi_out(wsp[-2].unit.string, wsp[-1].unit.integer);
        if (seqshare->emulation!=NOEMULATION)
            emulate(wsp[-2].unit.string, wsp[-1].unit.integer);
    }
#elif defined(_WINDOWS)
    if (seqshare->gflag&PROCEEDING) {
        MSG msg;
        seqshare->key_msg = wsp[-2].unit.string;
        seqshare->key_len = wsp[-1].unit.integer;
        seqshare->gflag |= RAISED;
        while (seqshare->gflag & RAISED) PeekMessage(&msg,  (HWND)NULL,  0,  0,  PM_NOREMOVE);
    }
    else {
        if (seqshare->emulation != ONLYEMULATION  &&  seqshare->midiout[0])
            midi_out(seqshare, wsp[-2].unit.string,  wsp[-1].unit.integer,  0);
        if (seqshare->emulation != NOEMULATION  &&  seqshare->midiout[1])
            midi_out(seqshare,  wsp[-2].unit.string,  wsp[-1].unit.integer,  1);
    }
#endif
    wsp-=2;
}

/* string - setchannels - */
void wk_setchannels(void)
{
    check(1);
    switch (wsp[-1].type) {
    case STRING_TYPE:
        if (wsp[-1].unit.line.length<16) error(WAKE_INDEX_OUT_OF_RANGE);
        memcpy(seqshare->channels, wsp[-1].unit.string, 16);
        seqshare->conv_flag|=CHANNELS;
        seqshare->gflag|=CONVERTING;
        break;
    case NULL_TYPE:
        seqshare->conv_flag&=0xff^CHANNELS;
        if (!seqshare->note_flag && !seqshare->conv_flag && !seqshare->vel_flag) seqshare->gflag&=0xffff^CONVERTING;
        break;
    default:
        error(WAKE_INVALID_TYPE);
    }
    wsp--;
}

/* channel string - setconversion - */
void wk_setconversion(void)
{
    check(2);
    if (!(wsp[-2].flag&INTEGER)) error(WAKE_INVALID_TYPE);
    if (wsp[-2].unit.integer<0||wsp[-2].unit.integer>15)
        error(WAKE_INDEX_OUT_OF_RANGE);
    switch (wsp[-1].type) {
    case STRING_TYPE:
        if (wsp[-1].unit.line.length<128) error(WAKE_INDEX_OUT_OF_RANGE);
        memcpy(seqshare->cnv_str[wsp[-2].unit.integer], wsp[-1].unit.string, 128);
        seqshare->note_flag|=1<<wsp[-2].unit.integer;
        seqshare->gflag|=CONVERTING;
        break;
    case NULL_TYPE:
        seqshare->note_flag&=0xffff^(1<<wsp[-2].unit.integer);
        if (!seqshare->note_flag && !seqshare->conv_flag && !seqshare->vel_flag) seqshare->gflag&=0xffff^CONVERTING;
        break;
    default:
        error(WAKE_INVALID_TYPE);
    }
    wsp-=2;
}

/* integer - setkeychannel - */
void wk_setkeychannel(void)
{
    check(1);
    if (!(wsp[-1].flag&INTEGER)) error(WAKE_INVALID_TYPE);
    if (wsp[-1].unit.integer<0||wsp[-1].unit.integer>15)
        error(WAKE_INDEX_OUT_OF_RANGE);
    seqshare->key_chan=(UBYTE)wsp[-1].unit.integer;
}

/* mode pitch rate sex volume - setnarrator - */
void wk_setnarrator(void)
{
    int i;
    check(5);
    for (i=0; i<5; i++)
        if (!(wsp[i-5].flag&INTEGER)) error(WAKE_INVALID_TYPE);
#ifdef AMIGA
    narrator_rb->mode=wsp[-5].unit.integer;
    narrator_rb->pitch=wsp[-4].unit.integer;
    narrator_rb->rate=wsp[-3].unit.integer;
    narrator_rb->sex=wsp[-2].unit.integer;
    narrator_rb->volume=wsp[-1].unit.integer;
#endif
    wsp-=5;
}

/* string - setprograms - */
void wk_setprograms(void)
{
    check(1);
    switch (wsp[-1].type) {
    case STRING_TYPE:
        if (wsp[-1].unit.line.length<128) error(WAKE_INDEX_OUT_OF_RANGE);
        memcpy(seqshare->programs, wsp[-1].unit.string, 128);
        seqshare->conv_flag|=PROGRAMS;
        seqshare->gflag|=CONVERTING;
        break;
    case NULL_TYPE:
        seqshare->conv_flag&=0xff^PROGRAMS;
        if (!seqshare->note_flag && !seqshare->conv_flag && !seqshare->vel_flag) seqshare->gflag&=0xffff^CONVERTING;
        break;
    default:
        error(WAKE_INVALID_TYPE);
    }
    wsp--;
}

/* rate - setrate - */
void wk_setrate(void)
{
    check(1);
    seqshare->rate=(ULONG)(float)(check_double(wsp-1)*256.0+.5);
    if (seqshare->rate == 256) seqshare->gflag&=0xffff^RATING;
    else seqshare->gflag|=RATING;
    wsp--;
}

/* channel string - setvelocities - */
void wk_setvelocities(void)
{
    check(2);
    if (!(wsp[-2].flag&INTEGER)) error(WAKE_INVALID_TYPE);
    if (wsp[-2].unit.integer<0||wsp[-2].unit.integer>15)
        error(WAKE_INDEX_OUT_OF_RANGE);
    switch (wsp[-1].type) {
    case STRING_TYPE:
        if (wsp[-1].unit.line.length<128) error(WAKE_INDEX_OUT_OF_RANGE);
        memcpy(seqshare->vel_str[wsp[-2].unit.integer], wsp[-1].unit.string, 128);
        seqshare->vel_flag|=1<<wsp[-2].unit.integer;
        seqshare->gflag|=CONVERTING;
        break;
    case NULL_TYPE:
        seqshare->vel_flag&=0xffff^(1<<wsp[-2].unit.integer);
        if (!seqshare->note_flag && !seqshare->conv_flag && !seqshare->vel_flag) seqshare->gflag&=0xffff^CONVERTING;
        break;
    default:
        error(WAKE_INVALID_TYPE);
    }
    wsp-=2;
}

/* channel voice - setvoice - */
void wk_setvoice(void)
{
    check(2);
    if (!(wsp[-2].flag&INTEGER)) error(WAKE_INVALID_TYPE);
    if (wsp[-2].unit.integer<0||wsp[-2].unit.integer>15)
        error(WAKE_INDEX_OUT_OF_RANGE);
#ifdef AMIGA
    switch (wsp[-1].type) {
    case VOICE_TYPE:
        seqshare->voice_map[wsp[-2].unit.integer]=
            wsp[-1].unit.data;
        break;
    case NULL_TYPE:
        seqshare->voice_map[wsp[-2].unit.integer]=NULL;
        break;
    default:
        error(WAKE_INVALID_TYPE);
    }
#endif
    wsp-=2;
}

/* channel - silence - */
void wk_silence(void)
{
    check(1);
    if (!(wsp[-1].flag&INTEGER)) error(WAKE_INVALID_TYPE);
#ifdef AMIGA
    {
        int h=wsp[-1].unit.integer;
        if (h<0||h>3) error(WAKE_INDEX_OUT_OF_RANGE);
        IOAudio[h][0]->ioa_Request.io_Unit=(struct Unit *)(1<<h);
        AbortIO((struct IORequest *)IOAudio[h][0]);
    }
#elif defined(__linux__)
#elif defined(_WINDOWS)
    if (seqshare->waveout  &&  seqshare->wavehdr->lpData) {
        waveOutReset(seqshare->waveout);
        waveOutUnprepareHeader(seqshare->waveout, seqshare->wavehdr, 
                               sizeof(WAVEHDR));
        seqshare->wavehdr->lpData=NULL;
    }
#endif
    wsp--;
}

/* boolean - sysex - */
void wk_sysex(void)
{
    check(1);
    if (wsp[-1].type) seqshare->gflag|=SYSEX;
    else seqshare->gflag&=0xffff^SYSEX;
    wsp--;
}

/* - timerfactor - real */
void wk_timerfactor(void)
{
    stack(1);
    wsp->type=FLOATING_TYPE;
    wsp->flag=NUMBER;
#ifdef AMIGA
    {
        struct timeval tv;
        struct timerequest *tr;
        if (!(tr=(struct timerequest *)CreateExtIO(tmrMsgPort, 
                                                   sizeof(struct timerequest)))) error(WAKE_CANNOT_ALLOCATE);
        tr->tr_node.io_Device=timerequest[0]->tr_node.io_Device;
        tr->tr_node.io_Unit=timerequest[0]->tr_node.io_Unit;
        tr->tr_node.io_Command=TR_GETSYSTIME;
        tr->tr_node.io_Flags=IOF_QUICK;
        DoIO((struct IORequest *)tr);
        tv=tr->tr_time;
        tr->tr_node.io_Command=TR_ADDREQUEST;
        tr->tr_node.io_Flags=IOF_QUICK;
        tr->tr_time.tv_secs=1;
        tr->tr_time.tv_micro=0;
        DoIO((struct IORequest *)tr);
        tr->tr_node.io_Command=TR_GETSYSTIME;
        tr->tr_node.io_Flags=IOF_QUICK;
        DoIO((struct IORequest *)tr);
        SubTime(&tr->tr_time, &tv);
        DeleteExtIO((struct IORequest *)tr);
        wsp->unit.floating=1000000.0/
            ((double)tr->tr_time.tv_secs*1000000.0+
             (double)tr->tr_time.tv_micro);
    }
#else
    wsp->unit.floating=1.0;
#endif
    wsp++;
}

/* sequence sequence1 sequence2 - unite - sequence */
void wk_unite(void)
{
    int i, j;
    char ended[3];
    ULONG pstamp[3], stamp[3], sstamp;
    UBYTE MQ *seq_pos[3], MQ *seq_end[3], MQ *len_pos, n;
    check(3);
    for (i=0; i<3; i++) {
        if (wsp[i-3].type!=STRING_TYPE) error(WAKE_INVALID_TYPE);
        seq_pos[i]=(UBYTE MQ *)wsp[i-3].unit.string;
        seq_end[i]=seq_pos[i]+wsp[i-3].unit.line.length;
        pstamp[i]=0L;
        ended[i]=0;
    }
    for (i=1; i<3; i++) seq_pos[i]=getvarnum(seq_pos[i], stamp+i);
    len_pos=NULL;
    sstamp=max(stamp[1], stamp[2]);
    for (i=1, j=2; !ended[1]||!ended[2]; i=i == 1?2:1, j=j == 1?2:1)
        if (!ended[i] && (ended[j]||stamp[i]<=stamp[j])) {
            n= *seq_pos[i]++;
            if (*seq_pos[i] == SPECIAL) {
                ended[i]= -1;
                continue;
            }
            if (!len_pos||stamp[i]>*stamp)
                if (seq_pos[0]+2*sizeof(ULONG)+3+n<=seq_end[0]) {
                    seq_pos[0]=putvarnum(seq_pos[0], stamp[i]-*pstamp);
                    *pstamp= *stamp=pstamp[i]=stamp[i];
                    len_pos=seq_pos[0]++;
                    *len_pos=n;
                }
                else error(WAKE_INDEX_OUT_OF_RANGE);
            else if ((UWORD)*len_pos+(UWORD)n>255)
                if (seq_pos[0]+2*sizeof(ULONG)+3+n<=seq_end[0]) {
                    seq_pos[0]=putvarnum(seq_pos[0], 0L);
                    len_pos=seq_pos[0]++;
                    *len_pos=n;
                }
                else error(WAKE_INDEX_OUT_OF_RANGE);
            else if (seq_pos[0]+sizeof(ULONG)+2+n<=seq_end[0])
                *len_pos=(UBYTE)(*len_pos+n);
            else error(WAKE_INDEX_OUT_OF_RANGE);
            memcpy(UN seq_pos[0], UN seq_pos[i], n);
            seq_pos[i]+=n;
            seq_pos[0]+=n;
            if (seq_pos[i]<seq_end[i]) {
                seq_pos[i]=getvarnum(seq_pos[i], stamp+i);
                sstamp=stamp[i]+=pstamp[i];
                pstamp[i]=stamp[i];
            }
            else ended[i]= -1;
        }
    if (seq_pos[0]+sizeof(ULONG)+2<=seq_end[0]) {
        seq_pos[0]=putvarnum(seq_pos[0], sstamp-*pstamp);
        *seq_pos[0]++=1;
        *seq_pos[0]=SPECIAL;
    }
    else error(WAKE_INDEX_OUT_OF_RANGE);
    wsp-=2;
}

/* length - wave - wave */
void wk_wave(void)
{
    check(1);
    if (!(wsp[-1].flag&INTEGER)) error(WAKE_INVALID_TYPE);
#ifdef AMIGA
    {
        HEAD *head=(HEAD *)AllocVec(sizeof(HEAD)+wsp[-1].unit.integer+1, 
                                    MEMF_PUBLIC|MEMF_CHIP|MEMF_CLEAR);
        if (!head) error(WAKE_CANNOT_ALLOCATE);
        head->type=WAVE_TYPE;
        head->last=last_save;
        last_save=head;
        wsp[-1].unit.data=(void *)(head+1);
        wsp[-1].unit.line.length=wsp[-1].unit.integer;
        wsp[-1].type=WAVE_TYPE;
        wsp[-1].flag=COMPOSITE|INDEXED;
    }
#elif defined(__linux__)
#elif defined(_WINDOWS)
    wsp[-1].unit.data=get_mem(wsp[-1].unit.integer*waveformat.nBlockAlign+1, 
                              STRING_TYPE, TRUE);
    wsp[-1].unit.line.length=wsp[-1].unit.integer;
    wsp[-1].type=sample_type;
    wsp[-1].flag=COMPOSITE|INDEXED;
#endif
}

int writelong(FILE *file, long value)
{
    return fputc((int)(value>>24&0xff), file) == EOF||
        fputc((int)(value>>16&0xff), file) == EOF||
        fputc((int)(value>>8&0xff), file) == EOF||
        fputc((int)(value&0xff), file) == EOF;
}

int writeshort(FILE *file, short value)
{
    return fputc((int)(value>>8&0xff), file) == EOF||
        fputc((int)(value&0xff), file) == EOF;
}

int writevarnum(FILE *file, ULONG value)
{
    int n=0;
    ULONG number=value&0x7f;
    while (value>>=7) {
        number<<=8;
        number|=(value&0x7f)|0x80;
    }
    for (;;) {
        if (fputc((int)(number&0xff), file) == EOF) return -1;
        n++;
        if (number&0x80) number>>=8;
        else return n;
    }
    return n;
}

/* sequence filename - writemidi - */
void wk_writemidi(void)
{
    int l, n;
    FILE *f;
    UBYTE MQ *seq_pos, MQ *seq_end, MQ *chn_end, MQ *pos;
    long length=0L,  offset;
    ULONG value;
    check(2);
    if (wsp[-2].type!=STRING_TYPE||wsp[-1].type!=STRING_TYPE)
        error(WAKE_INVALID_TYPE);
    if (!(f=fopen(wsp[-1].unit.sstring+
                  strspn(wsp[-1].unit.sstring, whites), "wb")))
        access_error(wsp[-1].unit.sstring);
    seq_pos=(UBYTE MQ *)wsp[-2].unit.string;
    seq_end=seq_pos+wsp[-2].unit.line.length;
    if (fwrite("MThd", 1, 4, f)!=4||writelong(f, 6L)||writeshort(f, 0)||
        writeshort(f, 1)||writeshort(f, 1000)||
        fwrite("MTrk", 1, 4, f)!=4||(offset=ftell(f))<0||
        fwrite((char *)&length, 1, 4, f)!=4||
        fwrite("\x00\xff\x51\x03\x0f\x42\x40", 1, 7, f)!=7) goto cantacc;
    while (seq_pos<seq_end) {
        seq_pos=getvarnum(seq_pos, &value);
        if (seq_pos[1] == SPECIAL) break;
        if ((n=writevarnum(f, value)) == -1) goto cantacc;
        chn_end=seq_pos;
        chn_end+=*seq_pos++;
        while (seq_pos<chn_end) {
            if (*seq_pos == SYSTEM_EXCLUSIVE||(*seq_pos&0x80) == 0) {
                if (*seq_pos == SYSTEM_EXCLUSIVE) {
                    if (fputc((int)*seq_pos, f) == EOF) goto cantacc;
                    seq_pos++;
                }
                else if (fputc(0xf7, f) == EOF) goto cantacc;
                pos=seq_pos;
                while (seq_pos<chn_end && (*seq_pos&0x80) == 0) seq_pos++;
                l=seq_pos-pos;
                if ((n=writevarnum(f, (long)l)) == -1||
                    fwrite(pos, 1, l, f)!=(unsigned)l) goto cantacc;
                continue;
            }
            switch (*seq_pos&0xf0) {
            case PROGRAM_CHANGE:
            case CHANNEL_PRESSURE:
                n=1;
                break;
            default:
                n=2; }
            if (fputc((int)*seq_pos, f) == EOF) goto cantacc;
            seq_pos++;
            while ((*seq_pos&0x80) == 0) {
                if (fputc((int)*seq_pos, f) == EOF) goto cantacc;
                seq_pos++;
                if (n>1) {
                    if (fputc((int)*seq_pos, f) == EOF) goto cantacc;
                    seq_pos++;
                }
                if (seq_pos>=chn_end) break;
                if (fputc(0, f) == EOF) goto cantacc;
            } }
    }
    if (fwrite("\x00\xff\x2f\x00", 1, 4, f)!=4) goto cantacc;
    length = ftell(f) - offset - 4;
    if (fseek(f, offset, 0)<0||writelong(f, length)) goto cantacc;
    fclose(f);
    wsp-=2;
    return;
 cantacc:
    fclose(f);
    error(WAKE_CANNOT_ACCESS);
}

WAKE_FUNC seq_funcs[]={
    {"cease",              wk_cease}, 
    {"changerate",         wk_changerate}, 
    {"convert",            wk_convert}, 
    {"display",            wk_display}, 
    {"distribution",       wk_distribution}, 
    {"echo",               wk_echo}, 
    {"emulation",          wk_emulation}, 
    {"english",            wk_english}, 
    {"extent",             wk_extent}, 
    {"finnish",            wk_finnish}, 
    {"halt",               wk_halt}, 
    {"join",               wk_join}, 
    {"keyboard",           wk_keyboard}, 
    {"loadvoice",          wk_loadvoice}, 
    {"narrate",            wk_narrate}, 
    {"play",               wk_play}, 
    {"playmode",           wk_playmode}, 
    {"proceed",            wk_proceed}, 
    {"raisesound",         wk_raisesound}, 
    {"readmidi",           wk_readmidi}, 
    {"record",             wk_record}, 
    {"reset",              wk_reset}, 
    {"reverse",            wk_reverse}, 
    {"send",               wk_send}, 
    {"setchannels",        wk_setchannels}, 
    {"setconversion",      wk_setconversion}, 
    {"setkeychannel",      wk_setkeychannel}, 
    {"setnarrator",        wk_setnarrator}, 
    {"setprograms",        wk_setprograms}, 
    {"setrate",            wk_setrate}, 
    {"setvelocities",      wk_setvelocities}, 
    {"setvoice",           wk_setvoice}, 
    {"silence",            wk_silence}, 
    {"sysex",              wk_sysex}, 
    {"timerfactor",        wk_timerfactor}, 
    {"unite",              wk_unite}, 
    {"wave",               wk_wave}, 
    {"writemidi",          wk_writemidi}};

int init_seq2(void)
{
    return enter_funcs(seq_funcs, ELEMS(seq_funcs));
}
