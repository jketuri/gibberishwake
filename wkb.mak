
# wk.mak
# BCC, D=1 dos, R=1 ready

BD=..\bin\
LD=..\lib\
OD=..\obj\

.path.bin=$(BD)
.path.lib=$(LD)
.path.obj=$(OD)
.path.res=$(OD)

wk.exe: wk.obj wake.obj wk_fun.obj wk_fun2.obj wk_gra.obj wk_gra2.obj\
 wk_log.obj wk_mat.obj wk_mat2.obj wk_pic.obj wk_seq.obj wk_seq2.obj wk.def\
 file_acs.lib wk.def wk.res
!if $d(D)
	tlink @wkd.rsp
!elif $d(R)
	tlink @wkr.rsp
!else
	tlink @wk.rsp
!endif
!if !$d(D)
	rc /k $(OD)wk.res $(BD)wk.exe
!endif
	dir $(BD)wk.exe

file_acs.lib: f_access.obj f_addkey.obj f_getkey.obj f_delkey.obj f_setkey.obj
	del $(LD)file_acs.lib
	tlib $(LD)$& @file_acs.rsp

.c.obj:
!if $d(D)
	bcc -2 -c -ml -n$(OD) -r -G -O -Z $<
!elif $d(R)
	bcc -2 -c -ml -n$(OD) -r -G -O -Z -W $<
!else
	bcc -2 -c -ml -n$(OD) -v -W $<
!endif

.rc.res:
	rc -fo$(OD)$&.res -r $<
