
/* wk_mat.h */

#ifdef MAIN
#define VARIABLE
#else
#define VARIABLE extern
#endif

#include <math.h>
#ifdef __TURBOC__
#define PI M_PI
#endif
#ifndef PI
#define PI ((double)3.141592653589793)
#endif

typedef ELEMENT *(*MAT_FUNC)(register ELEMENT *elem,
register ELEMENT *elem1,register ELEMENT *elem2);

extern NOMEN *system_nomen;

VARIABLE unsigned long rseed;
VARIABLE short rseed1,rseed2,rseed3;

extern REGQUAL long gcd(register long n,register long m);
extern REGQUAL int canon(register ELEMENT *elem);
extern REGQUAL ELEMENT *absf(register ELEMENT *elem);
extern REGQUAL ELEMENT *negf(register ELEMENT *elem);
extern REGQUAL ELEMENT *addf(register ELEMENT *elem,register ELEMENT *elem1,
register ELEMENT *elem2);
extern REGQUAL ELEMENT *subf(register ELEMENT *elem,register ELEMENT *elem1,
register ELEMENT *elem2);
extern REGQUAL ELEMENT *mulf(register ELEMENT *elem,register ELEMENT *elem1,
register ELEMENT *elem2);
extern REGQUAL ELEMENT *divf(register ELEMENT *elem,register ELEMENT *elem1,
register ELEMENT *elem2);
