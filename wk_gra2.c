
/* wk_gra2.c */

#include "wake.h"
#include "wk_mat.h"
#include "wk_gra.h"

void wk_stroke(void);

/* ux uy [matrix] - dtransform - dx dy */
void wk_dtransform(void)
{
    float x,y,*r;
    check(2);
    if (wsp[-1].type==SINGLEARRAY_TYPE) {
        check(3);
        if (wsp[-1].unit.line.length<6)
            error(WAKE_INDEX_OUT_OF_RANGE);
        r=wsp[-1].unit.singlearray;
        x=(float)check_double(wsp-3);
        y=(float)check_double(wsp-2);
        wsp--;
    }
    else {
        r=trans;
        x=(float)check_double(wsp-2);
        y=(float)check_double(wsp-1);
    }
    wsp[-2].flag=NUMBER;
    wsp[-2].type=FLOATING_TYPE;
    wsp[-2].unit.floating=x*r[A]+y*r[C];
    wsp[-1].flag=NUMBER;
    wsp[-1].type=FLOATING_TYPE;
    wsp[-1].unit.floating=x*r[B]+y*r[D];
}

/* - expose - */
void wk_expose(void)
{
#if defined(VGALIB)
    if (!exposed) {
        vga_flip();
        if (n_colors<=256) vga_setcolor(color_n==0?1:color_n);
        else vga_setrgbcolor(rgbcolor.red,rgbcolor.green,
                             rgbcolor.blue);
        exposed=1;
    }
#elif defined(_WINDOWS)
    BringWindowToTop(window);
    SetActiveWindow(window);
    SetFocus(window);
#elif defined(XLIB)
    XRaiseWindow(display, window);
    XSetInputFocus(display, window, RevertToNone, CurrentTime);
#endif
}

void fill(void)
{
#ifdef _WINDOWS
    if (n_counts) {
        if (n_points>point_o+1) {
            close_poly();
            store_poly();
        }
        PolyPolygon(dc,points,counts,n_counts);
    }
    else Polygon(dc,points,n_points);
#elif defined(XLIB)
    if (n_counts) {
        int i,n;
        if (n_points>point_o+1) {
            close_poly();
            store_poly();
        }
        XSetFunction(display,gcb,GXclear);
        XFillRectangle(display,bitmap,gcb,0,0,width,height);
        XSetFunction(display,gcb,GXxor);
        for (i=0,n=0; i<n_counts; n+=counts[i++])
            XFillPolygon(display,bitmap,gcb,points+n,counts[i],
                         Complex,CoordModeOrigin);
        XSetClipMask(display,gc,bitmap);
        XFillRectangle(display,drawable,gc,0,0,width,height);
        XSetClipMask(display,gc,None);
    }
    else XFillPolygon(display,drawable,gc,points,n_points,Complex,
                      CoordModeOrigin);
#else
    if (n_points>point_o+1) {
        close_poly();
        store_poly();
    }
    fill_area();
#endif
}

/* - fill - */
void wk_fill(void)
{
    fill();
    n_counts=n_points=0;
}

/* - fillout - */
void wk_fillout(void)
{
#ifdef AMIGA
    int fg;
    fill();
    fg=rastport->FgPen;
    SetAPen(rastport,
            setrgb(rgboutcolor.red,rgboutcolor.green,rgboutcolor.blue));
    wk_stroke();
    SetAPen(rastport,fg);
#elif defined(VGALIB)
    fill();
    setrgb(rgboutcolor.red,rgboutcolor.green,rgboutcolor.blue);
    wk_stroke();
    setrgb(rgbcolor.red,rgbcolor.green,rgbcolor.blue);
#elif defined(_WINDOWS)
    SelectObject(dc,out_pen);
    fill();
    SelectObject(dc,pen);
    n_counts=n_points=0;
#elif defined(XLIB)
    fill();
    if (dither) set_dithered_fg(RGB2GRAY(rgboutcolor.red,
                                         rgboutcolor.green,rgboutcolor.blue));
    else XSetForeground(display,gc,alloc_color(rgboutcolor.red,
                                               rgboutcolor.green,rgboutcolor.blue));
    wk_stroke();
    if (dither) set_dithered_fg(RGB2GRAY(rgbcolor.red,rgbcolor.green,
                                         rgbcolor.blue));
    else XSetForeground(display,gc,alloc_color(rgbcolor.red,
                                               rgbcolor.green,rgbcolor.blue));
    n_counts=n_points=0;
#else
    setcolor(out_color);
    fill();
    setcolor(fillsettings.color);
    n_counts=n_points=0;
#endif
}

/* - flood - */
void wk_flood(void)
{
#ifdef AMIGA
    Flood(rastport,1,rastport->cp_x,rastport->cp_y);
#elif defined(VGALIB)
#elif defined(_WINDOWS)
    GetCurrentPositionEx(dc,&pt);
    FloodFill(dc,pt.x,pt.y,logpen.lopnColor);
#elif defined(XLIB)
#else
    floodfill(getx(),gety(),1);
#endif
}

/* - general - */
void wk_general(void)
{
    gflag|=GENERAL;
}

/* x y width height - getimage - image */
void wk_getimage(void)
{
    void *image;
    int x,y,xs,ys;
    check(4);
    x=check_integer(wsp-4);
    y=check_integer(wsp-3);
    xs=check_integer(wsp-2);
    ys=check_integer(wsp-1);
    if (x+xs>width) xs=width-x;
    if (y+ys>height) ys=height-y;
    if (!(image=make_image((int)wsp[-2].unit.integer,
                           (int)wsp[-1].unit.integer)))
        error(WAKE_CANNOT_ALLOCATE);
#ifdef AMIGA
    BltBitMap(bbitmap,x,y,*((struct BitMap **)image+1),0,0,xs,ys,0xcc,
              0xff,NULL);
#elif defined(VGALIB)
#elif defined(_WINDOWS)
    SelectObject(dcmem,*(HBITMAP *)image);
    BitBlt(dcmem,0,0,xs,ys,dc,x,y,SRCCOPY);
#elif defined(XLIB)
    XSetFunction(display,gc,GXcopy);
    XCopyArea(display,drawable,*(Pixmap *)image,gc,x,y,xs,ys,0,0);
#else
    getimage(x,y,x+xs-1,y+ys-1,*(char **)image);
#endif
    wsp[-4].type=IMAGE_TYPE;
    wsp[-4].unit.data=image;
    wsp[-4].flag=0;
    wsp-=3;
}

/* matrix - identmatrix - matrix */
void wk_identmatrix(void)
{
    float *r;
    check(1);
    if (wsp[-1].type!=SINGLEARRAY_TYPE) error(WAKE_INVALID_TYPE);
    if (wsp[-1].unit.line.length<6) error(WAKE_INDEX_OUT_OF_RANGE);
    r=wsp[-1].unit.singlearray;
    r[A]=r[D]=(float)1.0;
    r[B]=r[C]=r[Tx]=r[Ty]=(float)0.0;
}

/* dx dy [matrix] - idtransform - ux uy */
void wk_idtransform(void)
{
    float dx,dy,*r;
    check(2);
    if (wsp[-1].type==SINGLEARRAY_TYPE) {
        check(3);
        if (wsp[-1].unit.line.length<6)
            error(WAKE_INDEX_OUT_OF_RANGE);
        r=wsp[-1].unit.singlearray;
        dx=(float)check_double(wsp-3);
        dy=(float)check_double(wsp-2);
        wsp--;
    }
    else {
        r=trans;
        dx=(float)check_double(wsp-2);
        dy=(float)check_double(wsp-1);
    }
    inv_dtrans(&dx,&dy,r);
    wsp[-2].flag=NUMBER;
    wsp[-2].type=FLOATING_TYPE;
    wsp[-2].unit.floating=(double)dx;
    wsp[-1].flag=NUMBER;
    wsp[-1].type=FLOATING_TYPE;
    wsp[-1].unit.floating=(double)dy;
}

/* width height - image - image */
void wk_image(void)
{
    void *image;
    check(2);
    if (!(wsp[-2].flag&INTEGER)||!(wsp[-1].flag&INTEGER))
        error(WAKE_INVALID_TYPE);
    if (!(image=make_image((int)wsp[-2].unit.integer,
                           (int)wsp[-1].unit.integer)))
        error(WAKE_CANNOT_ALLOCATE);
    wsp[-2].unit.data=image;
    wsp[-2].type=IMAGE_TYPE;
    wsp[-2].flag=COMPOSITE;
    wsp--;
}

/* - init - */
void wk_init(void)
{
    n_counts=n_points=0;
    first.x=first.y=0;
    trans[A]=(float)width;
    trans[D]=(float)height;
    trans[B]=trans[C]=trans[Tx]=trans[Ty]=(float)0.0;
    gflag=0;
}

/* - initmatrix - */
void wk_initmatrix(void)
{
    trans[A]=(float)width;
    trans[D]=(float)height;
    trans[B]=trans[C]=trans[Tx]=trans[Ty]=(float)0.0;
}

/* matrix1 matrix - invertmatrix - matrix */
void wk_invertmatrix(void)
{
    int i;
    check(2);
    for (i= -2; i<0; i++) {
        if (wsp[i].type!=SINGLEARRAY_TYPE) error(WAKE_INVALID_TYPE);
        if (wsp[i].unit.line.length<6) error(WAKE_INDEX_OUT_OF_RANGE);
    }
    inv_mat(wsp[-1].unit.singlearray,wsp[-2].unit.singlearray);
    wsp[-2]=wsp[-1];
    wsp--;
}

/* dx dy [matrix] - itransform - ux uy */
void wk_itransform(void)
{
    float x,y,*r;
    check(2);
    if (wsp[-1].type==SINGLEARRAY_TYPE) {
        check(3);
        if (wsp[-1].unit.line.length<6)
            error(WAKE_INDEX_OUT_OF_RANGE);
        r=wsp[-1].unit.singlearray;
        x=(float)check_double(wsp-3);
        y=(float)check_double(wsp-2);
        wsp--;
    }
    else {
        r=trans;
        x=(float)check_double(wsp-2);
        y=(float)check_double(wsp-1);
    }
    inv_trans(&x,&y,r);
    wsp[-2].flag=NUMBER;
    wsp[-2].type=FLOATING_TYPE;
    wsp[-2].unit.floating=(double)x;
    wsp[-1].flag=NUMBER;
    wsp[-1].type=FLOATING_TYPE;
    wsp[-1].unit.floating=(double)y;
}

/* - line - */
void wk_line(void)
{
    gflag&=0xff^AREA;
}

#ifdef _WINDOWS
int CALLBACK fontenumproc(ENUMLOGFONT *elf,NEWTEXTMETRIC *ntm,int fonttype,
                          LPARAM lparam)
{
    char *p;
    HEAD *head;
    if (!elf) return 1;
    if (fonttype&TRUETYPE_FONTTYPE) p=(char *)elf->elfFullName;
    else p=elf->elfLogFont.lfFaceName;
    if (!(head=(HEAD *)memcalloc(sizeof(HEAD)+strlen(p)+1))) return 0;
    strcpy((char *)(head+1),p);
    head->type=STRING_TYPE;
    head->last=last_save;
    last_save=head;
    (*(long *)lparam)++;
    return 1;
}
#endif

/* - listfonts - array */
void wk_listfonts(void)
{
    ELEMENT *a=NULL;
#ifdef AMIGA
    STRPTR p;
    ULONG l,m;
    long i,n=0;
    struct TextAttr *ta;
    stack(1);
    m=AvailFonts((STRPTR)NULL,0,AFF_DISK);
    if (!(p=(STRPTR)AllocMem(m,MEMF_PUBLIC))) error(WAKE_CANNOT_ALLOCATE);
    AvailFonts(p,m,AFF_DISK);
    n=((struct AvailFontsHeader *)p)->afh_NumEntries;
    if (!(a=(ELEMENT *)get_mem(n,ARRAY_TYPE,FALSE))) {
        if (p) FreeMem(p,m);
        error(WAKE_CANNOT_ALLOCATE);
    }
    for (i=0; i<n; i++) {
        ta=&((struct AvailFonts *)
             (p+sizeof(struct AvailFontsHeader)))[i].af_Attr;
        l=strlen(ta->ta_Name);
        if (!(a[i].unit.string=(UBYTE *)get_mem(l+1,STRING_TYPE,
                                                FALSE))) {
            if (p) FreeMem(p,m);
            error(WAKE_CANNOT_ALLOCATE);
        }
        strcpy(a[i].unit.sstring,ta->ta_Name);
        a[i].unit.line.length=l;
        a[i].flag=COMPOSITE|INDEXED;
        a[i].type=STRING_TYPE;
    }
    if (p) FreeMem(p,l);
#elif defined(_WINDOWS)
    int r;
    long i,n=0;
    FONTENUMPROC fep;
    HEAD *ls=last_save,*p;
    stack(1);
    fep=(FONTENUMPROC)MakeProcInstance((FARPROC)fontenumproc,instance);
    r=EnumFontFamilies(hdc,(LPCSTR)NULL,fep,(long)&n);
    FreeProcInstance((FARPROC)fep);
    if (r!=1) goto err;
    if (!(a=(ELEMENT *)get_mem(n,ARRAY_TYPE,FALSE))) goto err;
    p=last_save->last;
    for (i=n-1; i>=0; i--) {
        a[i].unit.string=(UBYTE *)(p+1);
        a[i].unit.line.length=strlen((char *)(p+1));
        a[i].flag=COMPOSITE|INDEXED;
        a[i].type=STRING_TYPE;
        p=p->last;
    }
    goto ok;
 err: while (last_save!=ls) {
        p=last_save->last;
        memfree(last_save);
        last_save=p;
    }
    error(WAKE_CANNOT_ALLOCATE);
 ok:
#elif defined(XLIB)
    char **p;
    int i,n=0;
    stack(1);
    if (!(p=XListFonts(display,"*",0xfffffff,&n)))
        error(WAKE_CANNOT_ALLOCATE);
    if (!(a=(ELEMENT *)get_mem(n,ARRAY_TYPE,FALSE))) {
        if (p) XFreeFontNames(p);
        error(WAKE_CANNOT_ALLOCATE);
    }
    for (i=0; i<n; i++) {
        a[i].unit.line.length=strlen(p[i]);
        if (!(a[i].unit.string=(UBYTE *)get_mem(
                                                a[i].unit.line.length+1,STRING_TYPE,FALSE))) {
            if (p) XFreeFontNames(p);
            error(WAKE_CANNOT_ALLOCATE);
        }
        memcpy(a[i].unit.string,p[i],a[i].unit.line.length);
        a[i].flag=COMPOSITE|INDEXED;
        a[i].type=STRING_TYPE;
    }
    if (p) XFreeFontNames(p);
#endif
    wsp->type=ARRAY_TYPE;
    wsp->flag=COMPOSITE|INDEXED;
    wsp->unit.array=a;
    wsp->unit.line.length=n;
    wsp++;
}

/* - matrix - matrix */
void wk_matrix(void)
{
    float *matrix;
    stack(1);
    matrix=(float *)get_mem(6,SINGLEARRAY_TYPE,TRUE);
    wsp->unit.singlearray=matrix;
    wsp->type=SINGLEARRAY_TYPE;
    wsp->unit.line.length=6;
    wsp->flag=COMPOSITE|INDEXED;
    matrix[A]=matrix[D]=(float)1.0;
    matrix[B]=matrix[C]=matrix[Tx]=matrix[Ty]=(float)0.0;
    wsp++;
}

/* x y - move - */
void wk_move(void)
{
    check(2);
    if (gflag&GENERAL) {
        float x=(float)check_double(wsp-2),
            y=(float)check_double(wsp-1);
        start_poly(XCOORD(x,y),YCOORD(x,y));
    }
    else start_poly((int)check_integer(wsp-2),(int)check_integer(wsp-1));
    wsp-=2;
}

/* a b - moverel - */
void wk_moverel(void)
{
    check(2);
    if (gflag&GENERAL) {
        float x=(float)check_double(wsp-2),
            y=(float)check_double(wsp-1);
        start_rpoly(XRELAT(x,y),YRELAT(x,y));
    }
    else start_rpoly((int)check_integer(wsp-2),(int)check_integer(wsp-1));
    wsp-=2;
}

/* - ncolors - n */
void wk_ncolors(void)
{
    stack(1);
    wsp->flag=NUMBER|INTEGER;
    wsp->type=UINTEGER_TYPE;
    wsp->unit.uinteger=n_colors;
    wsp++;
}

/* - newarea - */
void wk_newarea(void)
{
    n_counts=n_points=0;
}

/* px py [matrix] - proportions - [matrix] */
void wk_proportions(void)
{
    check(2);
    if (wsp[-1].type==SINGLEARRAY_TYPE) {
        float *r;
        check(3);
        if (wsp[-1].unit.line.length<6)
            error(WAKE_INDEX_OUT_OF_RANGE);
        r=wsp[-1].unit.singlearray;
        r[A]=(float)check_double(wsp-3);
        r[D]=(float)check_double(wsp-2);
        r[B]=r[C]=r[Tx]=r[Ty]=(float)0.0;
        wsp[-3]=wsp[-1];
    }
    else {
        float m[6],t[6];
        m[A]=(float)check_double(wsp-2);
        m[D]=(float)check_double(wsp-1);
        m[B]=m[C]=m[Tx]=m[Ty]=(float)0.0;
        mul_mat(t,m,trans);
        memcpy(trans,t,sizeof trans);
    }
    wsp-=2;
}

/* image xo yo xs ys x y oper - putimage - */
void wk_putimage(void)
{
    void *image;
    int xo,yo,xs,ys,x,y;
    check(8);
    if (wsp[-8].type!=IMAGE_TYPE||!(wsp[-1].flag&INTEGER))
        error(WAKE_INVALID_TYPE);
    image=wsp[-8].unit.data;
    xo=check_integer(wsp-7);
    yo=check_integer(wsp-6);
    xs=check_integer(wsp-5);
    ys=check_integer(wsp-4);
    x=check_integer(wsp-3);
    y=check_integer(wsp-2);
    if (x+xs>width) xs=width-x;
    if (y+ys>height) ys=height-y;
#ifdef AMIGA
    BltBitMap(*((struct BitMap **)image+1),xo,yo,bbitmap,x,y,xs,ys,
              wsp[-1].unit.integer,0xff,NULL);
#elif defined(VGALIB)
#elif defined(_WINDOWS)
    SelectObject(dcmem,*(HBITMAP *)image);
    BitBlt(dc,x,y,xs,ys,dcmem,xo,yo,wsp[-1].unit.integer);
#elif defined(XLIB)
    XSetFunction(display,gc,wsp[-1].unit.integer);
    XCopyArea(display,*(Pixmap *)image,drawable,gc,xo,yo,xs,ys,x,y);
    XSetFunction(display,gc,GXcopy);
#else
    putimage(x,y,*(char **)image,wsp[-1].unit.integer);
#endif
    wsp-=8;
}

/* - reach - integer | x y true | null */
void wk_reach(void)
{
#ifdef AMIGA
    struct IntuiMessage *imsg;
    while (imsg = (struct IntuiMessage *)GetMsg(window->UserPort))
        if (handle_imsg(imsg)) return;
#elif defined(VGALIB)
    int k;
    if ((k = vga_getkey()) != 0) {
        stack(1);
        wsp->flag = NUMBER | INTEGER;
        wsp->type = INTEGER_TYPE;
        wsp->unit.integer = k;
        wsp++;
        return;
    }
#elif defined(_WINDOWS)
    MSG msg;
    if (handle_msg()) return;
    if (PeekMessage(&msg, (HWND)NULL, 0, 0, PM_REMOVE)) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
    if (handle_msg()) return;
#elif defined(XLIB)
    XEvent event;
    while (XCheckWindowEvent(display, window, KeyPressMask | ButtonPressMask, &event))
        if (handle_event(&event)) return;
#else
    if (bioskey(1)) {
        stack(1);
        wsp->flag = NUMBER | INTEGER;
        wsp->type = INTEGER_TYPE;
        wsp->unit.integer = bioskey(0);
        wsp++;
        return;
    }
#endif
    stack(1);
    wsp->flag = 0;
    wsp->type = NULL_TYPE;
    wsp++;
}

/* filename - readimage - image */
void wk_readimage(void)
{
    void *image;
    check(1);
    if (wsp[-1].type!=STRING_TYPE) error(WAKE_INVALID_TYPE);
#ifdef AMIGA
    {
        BMHD bmhd;
        LONG file;
        UWORD bpr;
        struct BitMap *bp;
        int offset,scanline,rrl;
        if (!(file=look_image(wsp[-1].unit.sstring,&bmhd)))
            error(WAKE_ILLEGAL_USE);
        if (bmhd.nPlanes>depth) {
            Close(file);
            error(WAKE_ILLEGAL_USE);
        }
        if (!(image=make_image(bmhd.w,bmhd.h))) {
            Close(file);
            error(WAKE_CANNOT_ALLOCATE);
        }
        bp= *((struct BitMap **)image+1);
        bp->Depth=bmhd.nPlanes;
        bpr=bitmap->BytesPerRow;
        bitmap->BytesPerRow=0;
        rrl=bp->BytesPerRow;
        bitmap->Depth=1;
        if (bmhd.nPlanes<depth) {
            UBYTE n;
            for (n=bmhd.nPlanes; n<depth; n++)
                BltClear(bp->Planes[n],RASSIZE(*(UWORD *)image,
                                               *((UWORD *)image+1)),0);
        }
        if (bmhd.compression==cmpByteRun1)
            if (bmhd.masking==mskHasMask)
                for (scanline=0,offset=0; scanline<bmhd.h; scanline++,
                         offset+=bp->BytesPerRow)
                    {
                        if (read_packed_scanline(file,bp,offset,0)||
                            read_packed_scanline(file,bitmap,0,rrl)) goto fault;
                    }
            else for (scanline=0,offset=0; scanline<bmhd.h; scanline++,
                          offset+=bp->BytesPerRow) {
                    if (read_packed_scanline(file,bp,offset,0)) goto fault;
                }
        else if (bmhd.masking==mskHasMask)
            for (scanline=0,offset=0; scanline<bmhd.h; scanline++,
                     offset+=bp->BytesPerRow) {
                if (read_scanline(file,bp,offset,0)||
                    read_scanline(file,bitmap,0,rrl)) goto fault;
            }
        else for (scanline=0,offset=0; scanline<bmhd.h; scanline++,
                      offset+=bp->BytesPerRow) if (read_scanline(file,bp,offset,0))
                                                   goto fault;
        Close(file);
        bp->Depth=depth;
        bitmap->BytesPerRow=bpr;
        bitmap->Depth=depth;
        wsp[-1].type=IMAGE_TYPE;
        wsp[-1].unit.data=image;
        wsp[-1].flag=0;
        return;
    fault:  Close(file);
        free_image(image);
        last_save= *(HEAD **)((HEAD *)image-1);
        memfree((HEAD *)image-1);
        bp->Depth=depth;
        bitmap->BytesPerRow=bpr;
        bitmap->Depth=depth;
        error(WAKE_FAULTY_FORM);
    }
#elif defined(VGALIB)
#elif defined(_WINDOWS)
    {
        BITMAPINFO *bmi;
        UBYTE MQ *bits=read_bitmap(wsp[-1].unit.sstring,&bmi);
        if (!(image=make_image((int)bmi->bmiHeader.biWidth,
                               (int)bmi->bmiHeader.biHeight))) {
            memfree(bits);
            memfree(bmi);
            error(WAKE_CANNOT_ALLOCATE);
        }
        SetDIBits(dc,*(HBITMAP *)image,0,(UINT)bmi->bmiHeader.biHeight,bits,
                  bmi,paletteBased?DIB_PAL_COLORS:DIB_RGB_COLORS);
        memfree(bits);
        memfree(bmi);
        wsp[-1].type=IMAGE_TYPE;
        wsp[-1].unit.data=image;
        wsp[-1].flag=0;
    }
#elif defined(XLIB)
    {
        int x,y;
        Pixmap pm;
        unsigned int w,h;
        switch (XReadBitmapFile(display,window,wsp[-1].unit.sstring,
                                &w,&h,&pm,&x,&y)) {
        case BitmapOpenFailed:
            access_error(wsp[-1].unit.sstring);
        case BitmapFileInvalid:
            error(WAKE_FAULTY_FORM);
        case BitmapNoMemory:
            error(WAKE_CANNOT_ALLOCATE);
        }
        if (!(image=get_mem(1,IMAGE_TYPE,FALSE))) {
            XFreePixmap(display,pm);
            error(WAKE_CANNOT_ALLOCATE);
        }
        *(Pixmap *)image=pm;
        wsp[-1].type=IMAGE_TYPE;
        wsp[-1].unit.data=image;
        wsp[-1].flag=0;
    }
#else
    {
        FILE *f;
        short w,h;
        unsigned s;
        if (!(f=fopen(wsp[-1].unit.sstring,"rb")))
            access_error(wsp[-1].unit.sstring);
        if (fread(&w,1,sizeof(short),f)!=sizeof(short)||
            fread(&h,1,sizeof(short),f)!=sizeof(short)) {
            fclose(f);
            error(WAKE_FAULTY_FORM);
        }
        if (!(image=make_image(w,h))) {
            fclose(f);
            error(WAKE_CANNOT_ALLOCATE);
        }
        s=imagesize(0,0,*(short *)image,*((short *)image+1))-2*sizeof(short);
        if (fread((short *)image+2,1,s,f)!=s) {
            free_image(image);
            last_save= *(HEAD **)((HEAD *)image-1);
            memfree((HEAD *)image-1);
            fclose(f);
            error(WAKE_FAULTY_FORM);
        }
        fclose(f);
        wsp[-1].type=IMAGE_TYPE;
        wsp[-1].unit.data=image;
        wsp[-1].flag=0;
        return;
    }
#endif
}

/* - recycle - */
void wk_recycle(void)
{
#if !defined(XLIB)
    color_n=0;
#endif
}

/* state - restate - */
void wk_restate(void)
{
    STATE *t;
    check(1);
    if (wsp[-1].type!=STATE_TYPE) error(WAKE_INVALID_TYPE);
    t=wsp[-1].unit.state;
#ifdef AMIGA
    Move(rastport,t->currentpoint.x,t->currentpoint.y);
    SetAPen(rastport,setrgb(t->rgbcolor.red,t->rgbcolor.green,
                            t->rgbcolor.blue));
#elif defined(VGALIB)
    x_coord=t->currentpoint.x;
    y_coord=t->currentpoint.y;
    setrgb(t->rgbcolor.red,t->rgbcolor.green,t->rgbcolor.blue);
#elif defined(_WINDOWS)
    MoveTo(dc,t->currentpoint.x,t->currentpoint.y);
    logpen.lopnColor=setrgb(t->rgbcolor.red,t->rgbcolor.green,
                            t->rgbcolor.blue,TRUE);
    setpen();
    if (out_pen) DeleteObject(out_pen);
    out_pen=CreatePen(PS_SOLID,1,setrgb(t->rgboutcolor.red,
                                        t->rgboutcolor.green,t->rgboutcolor.blue,TRUE));
#elif defined(XLIB)
    x_coord=t->currentpoint.x;
    y_coord=t->currentpoint.y;
    if (dither) set_dithered_fg(RGB2GRAY(t->rgbcolor.red,
                                         t->rgbcolor.green,t->rgbcolor.blue));
    else XSetForeground(display,gc,alloc_color(t->rgbcolor.red,
                                               t->rgbcolor.green,t->rgbcolor.blue));
#else
    moveto(t->currentpoint.x,t->currentpoint.y);
    setcolor(fillsettings.color=setrgb(t->rgbcolor.red,t->rgbcolor.green,
                                       t->rgbcolor.blue));
    setfillstyle(fillsettings.pattern,fillsettings.color);
    out_color=setrgb(t->rgboutcolor.red,t->rgboutcolor.green,
                     t->rgboutcolor.blue);
#endif
    first=t->first;
    n_points=t->number;
    if (n_points) bigmove((UBYTE MQ *)points,(UBYTE MQ *)t->points,
                          (long)n_points*sizeof(POINT));
    n_counts=t->n_counts;
    if (n_counts) memcpy(counts,t->counts,n_counts*sizeof(int));
    point_o=t->point_o;
    gflag=t->flag;
    memcpy(trans,t->trans,sizeof trans);
    rgbcolor=t->rgbcolor;
    rgboutcolor=t->rgboutcolor;
    wsp--;
}

/* angle [matrix] - rotate - [matrix] */
void wk_rotate(void)
{
    float a,c,s;
    check(1);
    if (wsp[-1].type==SINGLEARRAY_TYPE) {
        float *r;
        check(2);
        if (wsp[-1].unit.line.length<6)
            error(WAKE_INDEX_OUT_OF_RANGE);
        r=wsp[-1].unit.singlearray;
        a=(float)check_double(wsp-2);
        c=(float)cos(a);
        s=(float)sin(a);
        r[A]=r[D]=c;
        r[B]=s;
        r[C]= -s;
        r[Tx]=r[Ty]=(float)0.0;
        wsp[-2]=wsp[-1];
    }
    else {
        float m[6],t[6];
        a=(float)check_double(wsp-1);
        c=(float)cos(a);
        s=(float)sin(a);
        m[A]=m[D]=c;
        m[B]=s;
        m[C]= -s;
        m[Tx]=m[Ty]=(float)0.0;
        mul_mat(t,m,trans);
        memcpy(trans,t,sizeof trans);
    }
    wsp--;
}

#if defined(_MSDOS)&&!defined(_WINDOWS)
void scroll_screen(int dx,int dy,int x0,int y0,int x1,int y1)
{
    unsigned s;
    char far *bitmap;
    s=imagesize(1,1,(x1-x0+1)-abs(dx),(y1-y0+1)-abs(dy));
    if (!(bitmap=(char far *)_graphgetmem(s)))
        error(WAKE_CANNOT_ALLOCATE);
    getimage(x0-(dx<0)*dx,y0-(dy<0)*dy,x1-(dx>0)*dx,y1-(dy>0)*dy,bitmap);
    putimage(x0+(dx>0)*dx,y0+(dy>0)*dy,bitmap,COPY_PUT);
    _graphfreemem(bitmap,s);
    setfillstyle(SOLID_FILL,0);
    if (dx<0) bar(x1+dx+1,y0,x1,y1);
    else if (dx>0) bar(x0,y0,x0+dx-1,y1);
    if (dy<0) bar(x0+(dx>0)*dx,y1+dy+1,x1+(dx<0)*dx,y1);
    else if (dy>0) bar(x0+(dx>0)*dx,y0,x1+(dx<0)*dx,y0+dy-1);
    setfillstyle(fillsettings.pattern,fillsettings.color);
}
#elif defined(XLIB)
void scroll_area(Display *dsp,Drawable drw,GC g,int dx,int dy,int x,int y,
                 int h,int v)
{
    XCopyArea(dsp,drw,drw,g,x-(dx<0)*dx,y-(dy<0)*dy,h-(dx>0)*dx,
              v-(dy>0)*dy,x+(dx>0)*dx,y+(dy>0)*dy);
    if (dx<0) XClearArea(dsp,drw,x+h+dx,y,-dx,v,FALSE);
    else if (dx>0) XClearArea(dsp,drw,x,y,dx-1,h,FALSE);
    if (dy<0) XClearArea(dsp,drw,x+(dx>0)*dx,y+h+dy,h+(dx<0)*dx-(dx>0)*dx,
                         -dy,FALSE);
    else if (dy>0) XClearArea(dsp,drw,x+(dx>0)*dx,y,h+(dx<0)*dx-(dx>0)*dx,
                              dy,FALSE);
}
#endif

/* dx dy x0 y0 x1 y1 - scroll - */
void wk_scroll(void)
{
    check(6);
    if (gflag&GENERAL) {
        float dx=(float)check_double(wsp-6),
            dy=(float)check_double(wsp-5),
            x0=(float)check_double(wsp-4),
            y0=(float)check_double(wsp-3),
            x1=(float)check_double(wsp-2),
            y1=(float)check_double(wsp-1);
#ifdef AMIGA
        ScrollRaster(rastport,XRELAT(dx,dy),YRELAT(dx,dy),
                     XCOORD(x0,y0),YCOORD(x0,y0),XCOORD(x1,y1),YCOORD(x1,y1));
#elif defined(VGALIB)
#elif defined(_WINDOWS)
        RECT r;
        r.left=XCOORD(x0,y0);
        r.top=YCOORD(x0,y0);
        r.right=XCOORD(x1,y1);
        r.bottom=YCOORD(x1,y1);
        ScrollDC(dc,XRELAT(dx,dy),YRELAT(dx,dy),&r,&r,
                 (HRGN)NULL,(RECT *)NULL);
#elif defined(XLIB)
        scroll_area(display,drawable,gc,XRELAT(dx,dy),YRELAT(dx,dy),
                    XCOORD(x0,y0),YCOORD(x0,y0),XCOORD(x1,y1),YCOORD(x1,y1));
#else
        scroll_screen(XRELAT(dx,dy),YRELAT(dx,dy),
                      XCOORD(x0,y0),YCOORD(x0,y0),XCOORD(x1,y1),YCOORD(x1,y1));
#endif
    }
    else
#ifdef AMIGA
        ScrollRaster(rastport,check_integer(wsp-6),check_integer(wsp-5),
                     check_integer(wsp-4),check_integer(wsp-3),check_integer(wsp-2),
                     check_integer(wsp-1));
#elif defined(VGALIB)
#elif defined(_WINDOWS)
    {
        RECT r;
        r.left=check_integer(wsp-4);
        r.top=check_integer(wsp-3);
        r.right=check_integer(wsp-2);
        r.bottom=check_integer(wsp-1);
        ScrollDC(dc,(int)check_integer(wsp-6),
                 (int)check_integer(wsp-5),&r,&r,(HRGN)NULL,(RECT *)NULL);
    }
#elif defined(XLIB)
    scroll_area(display,drawable,gc,check_integer(wsp-6),
                check_integer(wsp-5),check_integer(wsp-4),check_integer(wsp-3),
                check_integer(wsp-2),check_integer(wsp-1));
#else
    scroll_screen(check_integer(wsp-6),check_integer(wsp-5),
                  check_integer(wsp-4),check_integer(wsp-3),check_integer(wsp-2),
                  check_integer(wsp-1));
#endif
    wsp-=6;
}

/* image - setdestination - */
void wk_setdestination(void)
{
    check(1);
#ifdef AMIGA
    switch (wsp[-1].type) {
    case NULL_TYPE:
        bbitmap=rastport->BitMap=dbitmap;
        width=viewport->DWidth;
        height=viewport->DHeight;
        break;
    case IMAGE_TYPE:
        bbitmap=rastport->BitMap=
            *((struct BitMap **)wsp[-1].unit.data+1);
        width= *(UWORD *)wsp[-1].unit.data;
        height= *((UWORD *)wsp[-1].unit.data+1);
        break;
    default:
        error(WAKE_INVALID_TYPE);
    }
#elif defined(_WINDOWS)
    switch (wsp[-1].type) {
    case NULL_TYPE:
        if (dcalt) {
            if (!(ddc=GetWindowDC(window)))
                error(WAKE_CANNOT_ALLOCATE);
            DeleteDC(dcalt);
            dcalt=(HDC)NULL;
        }
        dpen=(HPEN)NULL;
        dfont=(HFONT)NULL;
        if (paletteBased) {
            UnrealizeObject(palette);
            SelectPalette(ddc,palette,FALSE);
            RealizePalette(ddc);
        }
        width=GetDeviceCaps(ddc,HORZRES);
        height=GetDeviceCaps(ddc,VERTRES);
        dc=ddc;
        if (font) DeleteObject(font);
        wk_init();
        break;
    case IMAGE_TYPE: {
        BITMAP bm;
        HBITMAP t= *(HBITMAP *)wsp[-1].unit.data;
        if (ddc) {
            if (!(dcalt=CreateCompatibleDC(ddc)))
                error(WAKE_CANNOT_ALLOCATE);
            ReleaseDC(window,ddc);
            ddc=(HDC)NULL;
        }
        dpen=(HPEN)NULL;
        dfont=(HFONT)NULL;
        SelectObject(dcalt,t);
        if (paletteBased) {
            UnrealizeObject(palette);
            SelectPalette(dcalt,palette,FALSE);
            RealizePalette(dcalt);
        }
        GetObject(t,sizeof(BITMAP),&bm);
        width=bm.bmWidth;
        height=bm.bmHeight;
        dc=dcalt;
        if (font) DeleteObject(font);
        wk_init();
        break;
    }
    default:
        error(WAKE_INVALID_TYPE);
    }
#elif defined(XLIB)
    {
        int t;
        unsigned int u;
        Window wnd;
        switch (wsp[-1].type) {
        case NULL_TYPE:
            drawable=window;
            break;
        case IMAGE_TYPE:
            drawable= *(Pixmap *)wsp[-1].unit.data;
            break;
        default:
            error(WAKE_INVALID_TYPE);
        }
        XGetGeometry(display,drawable,&wnd,&t,&t,(unsigned int *)&width,(unsigned int *)&height,&u,&u);
        wk_init();
    }
#endif
    wsp--;
}

/* integer - setfillrule - */
void wk_setfillrule(void)
{
    check(1);
    if (!(wsp[-1].flag&INTEGER)) error(WAKE_INVALID_TYPE);
#ifdef _WINDOWS
    switch (wsp[-1].unit.integer) {
    case 0:
        SetPolyFillMode(dc,ALTERNATE);
        break;
    case 1:
        SetPolyFillMode(dc,WINDING);
        break; }
#elif defined(XLIB)
    switch (wsp[-1].unit.integer) {
    case 0:
        XSetFillRule(display,gc,EvenOddRule);
        break;
    case 1:
        XSetFillRule(display,gc,WindingRule);
        break; }
#else
    rule=wsp[-1].unit.integer?-1:1;
#endif
    wsp--;
}

/* fontname size style - setfont - (Amiga,GDI) */
/* fontname - setfont - (XLIB) */
/* facenumber size - setfont - (BGI) */
/* fontsize - setfont - (VGALIB) */
void wk_setfont(void)
{
#ifdef AMIGA
    UWORD s;
    struct TextAttr ta;
    check(3);
    if (wsp[-3].type!=STRING_TYPE||
        !(wsp[-2].flag&INTEGER)||!(wsp[-1].flag&INTEGER))
        error(WAKE_INVALID_TYPE);
    if (textfont) CloseFont(textfont);
    memset(&ta,0,sizeof ta);
    ta.ta_Name=(STRPTR)wsp[-3].unit.string;
    ta.ta_YSize=s=wsp[-2].unit.integer;
    textfont=(struct TextFont *)OpenFont(&ta);
    if (!textfont||textfont->tf_YSize!=s) {
        ta.ta_YSize=s;
        textfont=(struct TextFont *)OpenDiskFont(&ta);
    }
    if (!textfont) error(WAKE_CANNOT_ALLOCATE);
    SetFont(rastport,textfont);
    SetSoftStyle(rastport,wsp[-1].unit.integer,AskSoftStyle(rastport));
    wsp-=3;
#elif defined(VGALIB)
    wsp--;
#elif defined(_WINDOWS)
    LOGFONT lf;
    check(3);
    if (wsp[-3].type!=STRING_TYPE||
        !(wsp[-2].flag&INTEGER)||!(wsp[-1].flag&INTEGER))
        error(WAKE_INVALID_TYPE);
    memset(&lf,0,sizeof lf);
    strncpy(lf.lfFaceName,wsp[-3].unit.sstring,LF_FACESIZE);
    lf.lfHeight=wsp[-2].unit.integer;
    if (wsp[-1].unit.integer&1) lf.lfUnderline=1;
    if (wsp[-1].unit.integer&(1<<1)) lf.lfWeight=FW_BOLD;
    if (wsp[-1].unit.integer&(1<<2)) lf.lfItalic=1;
    if (dfont) SelectObject(dc,dfont);
    if (font) DeleteObject(font);
    font=CreateFontIndirect(&lf);
    if (!font||!(dfont=SelectObject(dc,font)))
        error(WAKE_CANNOT_ALLOCATE);
    wsp-=3;
#elif defined(XLIB)
    check(1);
    if (wsp[-1].type!=STRING_TYPE) error(WAKE_INVALID_TYPE);
    if (font_ptr&&font_ptr->fid) XUnloadFont(display,font_ptr->fid);
    if (!(font_ptr=XLoadQueryFont(display,wsp[-1].unit.sstring))) {
        XSetFont(display,gc,def_font);
        font_ptr=XQueryFont(display,def_font);
        error(WAKE_CANNOT_ALLOCATE);
    }
    XSetFont(display,gc,font_ptr->fid);
    wsp--;
#else
    check(2);
    if (!(wsp[-2].flag&INTEGER)||!(wsp[-1].flag&INTEGER))
        error(WAKE_INVALID_TYPE);
    settextstyle(wsp[-2].unit.integer,HORIZ_DIR,wsp[-1].unit.integer);
    wsp-=2;
#endif
}

/* screenangle screenfrequency - sethalftone - */
void wk_sethalftone(void)
{
    check(2);
#ifdef XLIB
    if (dither) {
        free_tiles();
        if (setup_halftone(check_double(wsp-2),check_double(wsp-1)))
            error(WAKE_CANNOT_ALLOCATE);
    }
#endif
    wsp-=2;
}

/* matrix - setmatrix - */
void wk_setmatrix(void)
{
    int i;
    check(1);
    if (wsp[-1].type != ARRAY_TYPE &&
        (wsp[-1].type < FLOATARRAY_TYPE ||
         wsp[-1].type > STRING_TYPE))
        error(WAKE_INVALID_TYPE);
    if (wsp[-1].unit.line.length<6) error(WAKE_INDEX_OUT_OF_RANGE);
    switch (wsp[-1].type) {
    case ARRAY_TYPE:
        for (i = 0; i < 6; i++)
            trans[i] = (float)check_double(wsp[-1].unit.area + i);
        break;
    case FLOATARRAY_TYPE:
        for (i = 0; i < 6; i++)
            trans[i] = (float)wsp[-1].unit.floatarray[i];
        break;
    case INTARRAY_TYPE:
        for (i = 0; i < 6; i++)
            trans[i] = (float)wsp[-1].unit.intarray[i];
        break;
    case UINTARRAY_TYPE:
        for (i = 0; i < 6; i++)
            trans[i] = (float)wsp[-1].unit.uintarray[i];
        break;
    case SINGLEARRAY_TYPE:
        memcpy(trans, wsp[-1].unit.singlearray, sizeof trans);
        break;
    case SHORTARRAY_TYPE:
        for (i = 0; i < 6; i++)
            trans[i] = (float)wsp[-1].unit.shortarray[i];
        break;
    case USHORTARRAY_TYPE:
        for (i = 0; i < 6; i++)
            trans[i] = (float)wsp[-1].unit.ushortarray[i];
        break;
    case STRING_TYPE:
        for (i = 0; i < 6; i++)
            trans[i] = (float)wsp[-1].unit.string[i];
        break;
    }
    wsp--;
}

/* red green blue - setrgbbackcolor - */
void wk_setrgbbackcolor(void)
{
    double r,g,b;
    check(3);
    r=check_double(wsp-3);
    g=check_double(wsp-2);
    b=check_double(wsp-1);
    r=min(r,1.0);
    g=min(g,1.0);
    b=min(b,1.0);
#ifdef AMIGA
    rgbbackcolor.red=(UBYTE)(r*15.0+.5);
    rgbbackcolor.green=(UBYTE)(g*15.0+.5);
    rgbbackcolor.blue=(UBYTE)(b*15.0+.5);
    SetRGB4(viewport,0,rgbbackcolor.red,rgbbackcolor.green,
            rgbbackcolor.blue);
#elif defined(VGALIB)
    rgbbackcolor.red=(UBYTE)(r*255.0+.5);
    rgbbackcolor.green=(UBYTE)(g*255.0+.5);
    rgbbackcolor.blue=(UBYTE)(b*255.0+.5);
    vga_setpalette(0,rgbbackcolor.red>>2,rgbbackcolor.green>>2,
                   rgbbackcolor.blue>>2);
#elif defined(_WINDOWS)
    rgbbackcolor.red=(UBYTE)(r*255.0+.5);
    rgbbackcolor.green=(UBYTE)(g*255.0+.5);
    rgbbackcolor.blue=(UBYTE)(b*255.0+.5);
    setbackbrush();
#elif defined(XLIB)
    rgbbackcolor.red=(UWORD)(r*65535.0+.5);
    rgbbackcolor.green=(UWORD)(g*65535.0+.5);
    rgbbackcolor.blue=(UWORD)(b*65535.0+.5);
    if (dither) set_dithered_bg(RGB2GRAY(rgbbackcolor.red,
                                         rgbbackcolor.green,rgbbackcolor.blue));
    else {
        unsigned long n;
        XSetBackground(display,gc,n=alloc_color(rgbbackcolor.red,
                                                rgbbackcolor.green,rgbbackcolor.blue));
        XSetWindowBackground(display,window,n);
    }
#else
    rgbbackcolor.red=(UBYTE)(r*255.0+.5);
    rgbbackcolor.green=(UBYTE)(g*255.0+.5);
    rgbbackcolor.blue=(UBYTE)(b*255.0+.5);
    setrgbpalette(0,rgbbackcolor.red,rgbbackcolor.green,
                  rgbbackcolor.blue);
#endif
    wsp-=3;
}

/* red green blue - setrgbcolor - */
void wk_setrgbcolor(void)
{
    double r,g,b;
    check(3);
    r=check_double(wsp-3);
    g=check_double(wsp-2);
    b=check_double(wsp-1);
    r=min(r,1.0);
    g=min(g,1.0);
    b=min(b,1.0);
#ifdef AMIGA
    rgbcolor.red=(UBYTE)(r*15.0+.5);
    rgbcolor.green=(UBYTE)(g*15.0+.5);
    rgbcolor.blue=(UBYTE)(b*15.0+.5);
    SetAPen(rastport,
            setrgb(rgbcolor.red,rgbcolor.green,rgbcolor.blue));
#elif defined(VGALIB)
    rgbcolor.red=(UBYTE)(r*255.0+.5);
    rgbcolor.green=(UBYTE)(g*255.0+.5);
    rgbcolor.blue=(UBYTE)(b*255.0+.5);
    setrgb(rgbcolor.red,rgbcolor.green,rgbcolor.blue);
#elif defined(_WINDOWS)
    rgbcolor.red=(UBYTE)(r*255.0+.5);
    rgbcolor.green=(UBYTE)(g*255.0+.5);
    rgbcolor.blue=(UBYTE)(b*255.0+.5);
    logpen.lopnColor=setrgb(rgbcolor.red,rgbcolor.green,
                            rgbcolor.blue,TRUE);
    setpen();
#elif defined(XLIB)
    rgbcolor.red=(UWORD)(r*65535.0+.5);
    rgbcolor.green=(UWORD)(g*65535.0+.5);
    rgbcolor.blue=(UWORD)(b*65535.0+.5);
    if (dither) set_dithered_fg(RGB2GRAY(rgbcolor.red,rgbcolor.green,
                                         rgbcolor.blue));
    else XSetForeground(display,gc,alloc_color(rgbcolor.red,
                                               rgbcolor.green,rgbcolor.blue));
#else
    rgbcolor.red=(UBYTE)(r*255.0+.5);
    rgbcolor.green=(UBYTE)(g*255.0+.5);
    rgbcolor.blue=(UBYTE)(b*255.0+.5);
    setcolor(fillsettings.color=
             setrgb(rgbcolor.red,rgbcolor.green,rgbcolor.blue));
    setfillstyle(fillsettings.pattern,fillsettings.color);
#endif
    wsp-=3;
}

/* red green blue - setrgboutcolor - */
void wk_setrgboutcolor(void)
{
    double r,g,b;
    check(3);
    r=check_double(wsp-3);
    g=check_double(wsp-2);
    b=check_double(wsp-1);
    r=min(r,1.0);
    g=min(g,1.0);
    b=min(b,1.0);
#ifdef AMIGA
    rgboutcolor.red=(UBYTE)(r*15.0+.5);
    rgboutcolor.green=(UBYTE)(g*15.0+.5);
    rgboutcolor.blue=(UBYTE)(b*15.0+.5);
#elif defined(_WINDOWS)
    rgboutcolor.red=(UBYTE)(r*255.0+.5);
    rgboutcolor.green=(UBYTE)(g*255.0+.5);
    rgboutcolor.blue=(UBYTE)(b*255.0+.5);
    if (out_pen) DeleteObject(out_pen);
    out_pen=CreatePen(PS_SOLID,1,setrgb(rgboutcolor.red,
                                        rgboutcolor.green,rgboutcolor.blue,TRUE));
#elif defined(XLIB)
    rgboutcolor.red=(UWORD)(r*65535.0+.5);
    rgboutcolor.green=(UWORD)(g*65535.0+.5);
    rgboutcolor.blue=(UWORD)(b*65535.0+.5);
#else
    rgboutcolor.red=(UBYTE)(r*255.0+.5);
    rgboutcolor.green=(UBYTE)(g*255.0+.5);
    rgboutcolor.blue=(UBYTE)(b*255.0+.5);
#endif
    wsp-=3;
}

/* x y filename - showimage - */
void wk_showimage(void)
{
    int x,y;
    check(3);
    if (!(wsp[-3].flag&INTEGER)||!(wsp[-2].flag&INTEGER)||
        wsp[-1].type!=STRING_TYPE) error(WAKE_INVALID_TYPE);
    x=wsp[-3].unit.integer;
    y=wsp[-2].unit.integer;
#ifdef AMIGA
    {
        BMHD bmhd;
        LONG file;
        UWORD bpr;
        int scanline,rrl,w,h;
        if (!(file=look_image(wsp[-1].unit.sstring,&bmhd))) {
            wsp-=3;
            return;
        }
        if (bmhd.nPlanes>depth) {
            Close(file);
            error(WAKE_ILLEGAL_USE);
        }
        w=min(bmhd.w,width);
        if (x+w>width) w=width-x;
        h=bmhd.h;
        if (y+h>height) h=height-y;
        bpr=bitmap->BytesPerRow;
        bitmap->BytesPerRow=w+15>>4<<1;
        rrl=(bmhd.w+15>>4<<1)-bitmap->BytesPerRow;
        if (bmhd.nPlanes<depth) {
            UBYTE n;
            for (n=bmhd.nPlanes; n<depth; n++)
                BltClear(bitmap->Planes[n],RASSIZE(width,1),0);
        }
        if (bmhd.compression==cmpByteRun1)
            if (bmhd.masking==mskHasMask)
                for (scanline=0; scanline<h; scanline++) {
                    bitmap->Depth=bmhd.nPlanes;
                    if (read_packed_scanline(file,bitmap,0,rrl)) goto fault;
                    bitmap->Depth=depth;
                    BltBitMap(bitmap,0,0,bbitmap,x,y+scanline,w,1,0xcc,0xff,NULL);
                    bitmap->Depth=1;
                    if (read_packed_scanline(file,bitmap,0,rrl)) goto fault;
                }
            else for (scanline=0; scanline<h; scanline++) {
                    bitmap->Depth=bmhd.nPlanes;
                    if (read_packed_scanline(file,bitmap,0,rrl)) goto fault;
                    bitmap->Depth=depth;
                    BltBitMap(bitmap,0,0,bbitmap,x,y+scanline,w,1,0xcc,0xff,NULL);
                }
        else if (bmhd.masking==mskHasMask)
            for (scanline=0; scanline<h; scanline++) {
                bitmap->Depth=bmhd.nPlanes;
                if (read_scanline(file,bitmap,0,rrl)) goto fault;
                bitmap->Depth=depth;
                BltBitMap(bitmap,0,0,bbitmap,x,y+scanline,w,1,0xcc,0xff,NULL);
                bitmap->Depth=1;
                if (read_scanline(file,bitmap,0,rrl)) goto fault;
            }
        else for (scanline=0; scanline<h; scanline++) {
                bitmap->Depth=bmhd.nPlanes;
                if (read_scanline(file,bitmap,0,rrl)) goto fault;
                bitmap->Depth=depth;
                BltBitMap(bitmap,0,0,bbitmap,x,y+scanline,w,1,0xcc,0xff,NULL);
            }
        Close(file);
        bitmap->Depth=depth;
        bitmap->BytesPerRow=bpr;
        wsp-=3;
        return;
    fault:  Close(file);
        bitmap->Depth=depth;
        bitmap->BytesPerRow=bpr;
        error(WAKE_FAULTY_FORM);
    }
#elif defined(VGALIB)
#elif defined(_WINDOWS)
    {
        BITMAPINFO *bmi;
        UBYTE MQ *bits=read_bitmap(wsp[-1].unit.sstring,&bmi);
        SetDIBitsToDevice(dc,x,y,(int)bmi->bmiHeader.biWidth,
                          (int)bmi->bmiHeader.biHeight,0,0,0,(UINT)bmi->bmiHeader.biHeight,bits,
                          bmi,paletteBased?DIB_PAL_COLORS:DIB_RGB_COLORS);
        memfree(bits);
        memfree(bmi);
        wsp-=3;
    }
#elif defined(XLIB)
    wsp-=3;
#else
    {
        FILE *f;
        int j,l;
        short w,h;
        if (!(f=fopen(wsp[-1].unit.sstring,"rb")))
            access_error(wsp[-1].unit.sstring);
        if (fread(&w,1,sizeof(short),f)!=sizeof(short)||
            fread(&h,1,sizeof(short),f)!=sizeof(short)) goto fault;
        l=(wsp[-2].unit.integer+7)/8*n_planes;
        *(short *)bitmap=wsp[-2].unit.integer-1;
        for (j=0; j<h; j++) {
            if (fread(bitmap+4,1,l,f)!=l) goto fault;
            putimage(x,y+j,bitmap,COPY_PUT);
        }
        *(short *)bitmap=width-1;
        wsp-=3;
        return;
    fault:  fclose(f);
        error(WAKE_FAULTY_FORM);
    }
#endif
}

/* - specific - */
void wk_specific(void)
{
    gflag&=0xff^GENERAL;
}

/* - state - state */
void wk_state(void)
{
    STATE *t;
    stack(1);
    t=(STATE *)get_mem(1,STATE_TYPE,TRUE);
#ifdef AMIGA
    t->currentpoint.x=rastport->cp_x;
    t->currentpoint.y=rastport->cp_y;
#elif defined(VGALIB)
    t->currentpoint.x=x_coord;
    t->currentpoint.y=y_coord;
#elif defined(_WINDOWS)
    GetCurrentPositionEx(dc,&pt);
    t->currentpoint.x=pt.x;
    t->currentpoint.y=pt.y;
#elif defined(XLIB)
    t->currentpoint.x=x_coord;
    t->currentpoint.y=y_coord;
#else
    t->currentpoint.x=getx();
    t->currentpoint.y=gety();
#endif
    t->first=first;
    if (n_points) {
        if (!(t->points=(POINT *)memalloc(n_points*sizeof(POINT))))
            goto cantall;
        bigmove((UBYTE MQ *)t->points,(UBYTE MQ *)points,
                (long)n_points*sizeof(POINT));
    }
    t->number=(short)n_points;
    t->n_counts=n_counts;
    if (n_counts) {
        if (!(t->counts=(int *)memalloc(n_counts*sizeof(int))))
            {
                memfree(t->points);
                goto cantall;
            }
        memcpy(t->counts,counts,n_counts*sizeof(int));
    }
    t->point_o=point_o;
    t->flag=gflag;
    memcpy(t->trans,trans,sizeof trans);
    t->rgbcolor=rgbcolor;
    t->rgboutcolor=rgboutcolor;
    wsp->type=STATE_TYPE;
    wsp->flag=COMPOSITE;
    wsp->unit.state=t;
    wsp++;
    return;
 cantall:
    last_save= *(HEAD **)((HEAD *)t-1);
    memfree((HEAD *)t-1);
    error(WAKE_CANNOT_ALLOCATE);
}

/* string - stringwidth - wx wy */
void wk_stringwidth(void)
{
    check(1);
    if (wsp[-1].type!=STRING_TYPE) error(WAKE_INVALID_TYPE);
    stack(2);
    if (gflag&GENERAL) {
        float dx,dy;
#ifdef AMIGA
        dx=(float)TextLength(rastport,wsp[-1].unit.sstring,
                             strlen(wsp[-1].unit.sstring));
        dy=(float)rastport->TxHeight;
#elif defined(VGALIB)
        dx=8.0;
        dy=8.0;
#elif defined(_WIN32)
        SIZE s;
        GetTextExtentPoint32(dc,wsp[-1].unit.sstring,
                             strlen(wsp[-1].unit.sstring),&s);
        dx=(float)s.cx;
        dy=(float)s.cy;
#elif defined(_WINDOWS)
        dw=GetTextExtent(dc,wsp[-1].unit.sstring,
                         strlen(wsp[-1].unit.sstring));
        dx=(float)pt.x;
        dy=(float)pt.y;
#elif defined(XLIB)
        dx=(float)XTextWidth(font_ptr,wsp[-1].unit.sstring,
                             strlen(wsp[-1].unit.sstring));
        dy=font_ptr->max_bounds.ascent+font_ptr->max_bounds.descent;
#else
        dx=(float)textwidth(wsp[-1].unit.sstring);
        dy=(float)textheight(wsp[-1].unit.sstring);
#endif
        inv_dtrans(&dx,&dy,trans);
        wsp[-1].flag=NUMBER;
        wsp[-1].type=FLOATING_TYPE;
        wsp[-1].unit.floating=(double)dx;
        wsp->flag=NUMBER;
        wsp->type=FLOATING_TYPE;
        wsp->unit.floating=(double)dy;
    }
    else {
#ifdef AMIGA
        wsp[-1].flag=NUMBER|INTEGER;
        wsp[-1].type=INTEGER_TYPE;
        wsp[-1].unit.integer=TextLength(rastport,wsp[-1].unit.sstring,
                                        strlen(wsp[-1].unit.sstring));
        wsp->flag=NUMBER|INTEGER;
        wsp->type=INTEGER_TYPE;
        wsp->unit.integer=rastport->TxHeight;
#elif defined(VGALIB)
        wsp[-1].flag=NUMBER|INTEGER;
        wsp[-1].type=INTEGER_TYPE;
        wsp[-1].unit.integer=8;
        wsp->flag=NUMBER|INTEGER;
        wsp->type=INTEGER_TYPE;
        wsp->unit.integer=8;
#elif defined(_WIN32)
        SIZE s;
        GetTextExtentPoint32(dc,wsp[-1].unit.sstring,
                             strlen(wsp[-1].unit.sstring),&s);
        wsp[-1].flag=NUMBER|INTEGER;
        wsp[-1].type=INTEGER_TYPE;
        wsp[-1].unit.integer=s.cx;
        wsp->flag=NUMBER|INTEGER;
        wsp->type=INTEGER_TYPE;
        wsp->unit.integer=s.cy;
#elif defined(_WINDOWS)
        dw=GetTextExtent(dc,wsp[-1].unit.sstring,
                         strlen(wsp[-1].unit.sstring));
        wsp[-1].flag=NUMBER|INTEGER;
        wsp[-1].type=INTEGER_TYPE;
        wsp[-1].unit.integer=pt.x;
        wsp->flag=NUMBER|INTEGER;
        wsp->type=INTEGER_TYPE;
        wsp->unit.integer=pt.y;
#elif defined(XLIB)
        wsp[-1].flag=NUMBER|INTEGER;
        wsp[-1].type=INTEGER_TYPE;
        wsp[-1].unit.integer=XTextWidth(font_ptr,
                                        wsp[-1].unit.sstring,
                                        strlen(wsp[-1].unit.sstring));
        wsp->flag=NUMBER|INTEGER;
        wsp->type=INTEGER_TYPE;
        wsp->unit.integer=8;
#else
        wsp[-1].flag=NUMBER|INTEGER;
        wsp[-1].type=INTEGER_TYPE;
        wsp[-1].unit.integer=textwidth(wsp[-1].unit.sstring);
        wsp->flag=NUMBER|INTEGER;
        wsp->type=INTEGER_TYPE;
        wsp->unit.integer=textheight(wsp[-1].unit.sstring);
#endif
    }
    wsp++;
}

/* - stroke - */
void wk_stroke(void)
{
#ifdef AMIGA
    int i,m,n;
    for (i=0,m=0,n=0; i<n_counts; i++) {
        m+=counts[i];
        Move(rastport,points[0].x,points[0].y);
        while (++n<m) Draw(rastport,points[n].x,points[n].y);
    }
    n_counts=n_points=0;
#elif defined(VGALIB)
    int i,m,n;
    for (i=0,m=0,n=0; i<n_counts; i++) {
        m+=counts[i];
        while (++n<m)
            vga_drawline(points[n-1].x,points[n-1].y,
                         points[n].x,points[n].y);
    }
    n_counts=n_points=0;
#elif defined(_WINDOWS)
    int i,n;
    for (i=0,n=0; i<n_counts; n+=counts[i++])
        Polyline(dc,points+n,counts[i]);
    Polyline(dc,points+n,n_points-point_o);
    n_counts=n_points=0;
#elif defined(XLIB)
    int i,n;
    for (i=0,n=0; i<n_counts; n+=counts[i++])
        XDrawLines(display,drawable,gc,points+n,counts[i],CoordModeOrigin);
    XDrawLines(display,drawable,gc,points+n,n_points-point_o,
               CoordModeOrigin);
    n_counts=n_points=0;
#else
    int i,n;
    for (i=0,n=0; i<n_counts; n+=counts[i++])
        drawpoly(counts[i],(int *)(points+n));
    drawpoly(n_points-point_o,(int *)(points+n));
    n_counts=n_points=0;
#endif
}

/* boolean - synchronization - */
void wk_synchronization(void)
{
    check(1);
#ifdef XLIB
    XSynchronize(display,(int)(wsp[-1].type!=0));
#endif
    wsp--;
}

/* - synchronize - */
void wk_synchronize(void)
{
#ifdef XLIB
    XSync(display,FALSE);
#endif
}

/* string - text - */
void wk_text(void)
{
    check(1);
    if (wsp[-1].type!=STRING_TYPE) error(WAKE_INVALID_TYPE);
#ifdef AMIGA
    Move(rastport,rastport->cp_x,rastport->cp_y+rastport->TxBaseline);
    Text(rastport,wsp[-1].unit.sstring,strlen(wsp[-1].unit.sstring));
    Move(rastport,rastport->cp_x,rastport->cp_y-rastport->TxBaseline);
#elif defined(VGALIB)
    {
        UBYTE *p=wsp[-1].unit.string,*q;
        int n=(fontsize*fontsize+7)/8,r,s,t;
        while (*p) {
            t=7;
            q=fontbmap+(int)*p*n;
            for (r=0; r<fontsize; r++)
                for (s=0; s<fontsize; s++) {
                    if (*q&1<<t) vga_drawpixel(x_coord+s,y_coord+r);
                    if (t==0) {
                        t=7;
                        q++;
                    }
                    else t--;
                }
            x_coord+=fontsize;
            p++;
        }
    }
#elif defined(_WIN32)
    {
        SIZE s;
        int l=strlen(wsp[-1].unit.sstring);
        GetCurrentPositionEx(dc,&pt);
        TextOut(dc,pt.x,pt.y,wsp[-1].unit.sstring,l);
        GetTextExtentPoint32(dc,wsp[-1].unit.sstring,l,&s);
        MoveTo(dc,pt.x+s.cx,pt.y);
    }
#elif defined(_WINDOWS)
    {
        int l=strlen(wsp[-1].unit.sstring);
        GetCurrentPositionEx(dc,&pt);
        TextOut(dc,pt.x,pt.y,wsp[-1].unit.sstring,l);
        MoveTo(dc,pt.x+LOWORD(GetTextExtent(dc,wsp[-1].unit.sstring,l)),
               pt.y);
    }
#elif defined(XLIB)
    {
        int l=strlen(wsp[-1].unit.sstring);
        if (opaque) XDrawImageString(display,drawable,gc,x_coord,y_coord+
                                     font_ptr->max_bounds.ascent,wsp[-1].unit.sstring,l);
        else XDrawString(display,drawable,gc,x_coord,y_coord+
                         font_ptr->max_bounds.ascent,wsp[-1].unit.sstring,l);
        x_coord+=XTextWidth(font_ptr,wsp[-1].unit.sstring,l);
    }
#else
    outtext(wsp[-1].unit.sstring);
#endif
    wsp--;
}

/* ux uy [matrix] - transform - dx dy */
void wk_transform(void)
{
    float x,y,*r;
    check(2);
    if (wsp[-1].type==SINGLEARRAY_TYPE) {
        check(3);
        if (wsp[-1].unit.line.length<6)
            error(WAKE_INDEX_OUT_OF_RANGE);
        r=wsp[-1].unit.singlearray;
        x=(float)check_double(wsp-3);
        y=(float)check_double(wsp-2);
        wsp--;
    }
    else {
        r=trans;
        x=(float)check_double(wsp-2);
        y=(float)check_double(wsp-1);
    }
    wsp[-2].flag=NUMBER;
    wsp[-2].type=FLOATING_TYPE;
    wsp[-2].unit.floating=x*r[A]+y*r[C]+r[Tx];
    wsp[-1].flag=NUMBER;
    wsp[-1].type=FLOATING_TYPE;
    wsp[-1].unit.floating=x*r[B]+y*r[D]+r[Ty];
}

/* dx dy [matrix] - translate - [matrix] */
void wk_translate(void)
{
    check(2);
    if (wsp[-1].type==SINGLEARRAY_TYPE) {
        float *r;
        check(3);
        if (wsp[-1].unit.line.length<6)
            error(WAKE_INDEX_OUT_OF_RANGE);
        r=wsp[-1].unit.singlearray;
        r[A]=r[D]=(float)1.0;
        r[B]=r[C]=(float)0.0;
        r[Tx]=(float)check_double(wsp-3);
        r[Ty]=(float)check_double(wsp-2);
        wsp[-3]=wsp[-1];
    }
    else {
        float m[6],t[6];
        m[A]=m[D]=(float)1.0;
        m[B]=m[C]=(float)0.0;
        m[Tx]=(float)check_double(wsp-2);
        m[Ty]=(float)check_double(wsp-1);
        mul_mat(t,m,trans);
        memcpy(trans,t,sizeof trans);
    }
    wsp-=2;
}

/* - wipe - */
void wk_wipe(void)
{
#ifdef AMIGA
    SetRast(rastport,rastport->BgPen);
    color_n=0;
#elif defined(VGALIB)
    vga_clear();
    color_n=0;
#elif defined(_WINDOWS)
    if (cbrush) {
        HBRUSH b=SelectObject(dc,cbrush);
        PatBlt(dc,0,0,width,height,PATCOPY);
        SelectObject(dc,b);
    }
    color_n=0;
#elif defined(XLIB)
    /*
      unsigned long pixel;
      for (pixel=pixel_min; pixel<=pixel_max; pixel++)
      XFreeColors(display,XDefaultColormap(display,screen),&pixel,1,0);
    */
    pixel_min=0xffffffff;
    pixel_max=0x0;
    if (dither)
        set_dithered_fg(RGB2GRAY(rgbbackcolor.red,rgbbackcolor.green,
                                 rgbbackcolor.blue));
    else XSetForeground(display,gc,alloc_color(rgbbackcolor.red,
                                               rgbbackcolor.green,rgbbackcolor.blue));
    XFillRectangle(display,drawable,gc,0,0,width,height);
    if (dither) {
        set_dithered_fg(RGB2GRAY(rgbcolor.red,rgbcolor.green,
                                 rgbcolor.blue));
        set_dithered_bg(RGB2GRAY(rgbbackcolor.red,rgbbackcolor.green,
                                 rgbbackcolor.blue));
    }
    else {
        XSetForeground(display,gc,alloc_color(rgbcolor.red,
                                              rgbcolor.green,rgbcolor.blue));
        XSetBackground(display,gc,alloc_color(rgbbackcolor.red,
                                              rgbbackcolor.green,rgbbackcolor.blue));
    }
#else
    cleardevice();
    color_n=0;
#endif
}

/* image filename - writeimage - */
void wk_writeimage(void)
{
    check(2);
    if (wsp[-2].type!=IMAGE_TYPE||wsp[-1].type!=STRING_TYPE)
        error(WAKE_INVALID_TYPE);
#ifdef AMIGA
    {
        UWORD v;
        RGBVAL l;
        BMHD bmhd;
        ULONG chunk_size;
        struct BitMap *bp;
        LONG file,curpos,bodypos;
        int n,offset,pn,scanline;
        UBYTE pad=0;
        void *image=wsp[-2].unit.data;
        if (!(file=Open(wsp[-1].unit.sstring,MODE_NEWFILE)))
            access_error(wsp[-1].unit.sstring);
        if (Write(file,"FORM",4)!=4) goto fail;
        chunk_size=0;
        if (Write(file,&chunk_size,sizeof chunk_size)!=sizeof chunk_size||
            Write(file,"ILBMBMHD",8)!=8) goto fail;
        chunk_size=sizeof bmhd;
        if (Write(file,&chunk_size,sizeof chunk_size)!=sizeof chunk_size)
            goto fail;
        memset(&bmhd,0,sizeof bmhd);
        bmhd.w= *(UWORD *)image;
        bmhd.h= *((UWORD *)image+1);
        bmhd.nPlanes=depth;
        bmhd.masking=mskNone;
        bmhd.compression=cmpNone;
        bmhd.xAspect=aspectx;
        bmhd.yAspect=aspecty;
        bmhd.pageWidth=width;
        bmhd.pageHeight=height;
        if (Write(file,&bmhd,sizeof bmhd)!=sizeof bmhd||
            Write(file,"CMAP",4)!=4) goto fail;
        chunk_size=n_colors*3;
        if (Write(file,&chunk_size,sizeof chunk_size)!=sizeof chunk_size)
            goto fail;
        for (n=0; n<n_colors; n++) {
            v=GetRGB4(colormap,n);
            l.red=(v>>8&0xf)<<4;
            l.green=(v>>4&0xf)<<4;
            l.blue=(v&0xf)<<4;
            if (Write(file,&l,3)!=3) goto fail;
        }
        if (chunk_size&1&&Write(file,&pad,1)!=1) goto fail;
        if (Write(file,"CAMG",4)!=4) goto fail;
        chunk_size=4;
        if (Write(file,&chunk_size,sizeof chunk_size)!=
            sizeof chunk_size||Write(file,viewport->Modes,4)!=4)
            goto fail;
        if ((bodypos=Seek(file,0,OFFSET_CURRENT))==-1) goto fail;
        if (Write(file,"BODY",4)!=4) goto fail;
        chunk_size=0;
        if (Write(file,&chunk_size,sizeof chunk_size)!=sizeof chunk_size)
            goto fail;
        bp= *((struct BitMap **)image+1);
        for (scanline=0,offset=0; scanline<bmhd.h; scanline++,
                 offset+=bp->BytesPerRow) for (pn=0; pn<bp->Depth; pn++)
                                              if (Write(file,bp->Planes[pn]+offset,bp->BytesPerRow)!=
                                                  bp->BytesPerRow) goto fail;
        if ((curpos=Seek(file,0,OFFSET_CURRENT))==-1) goto fail;
        chunk_size=curpos-bodypos-8;
        if (Seek(file,bodypos+4,OFFSET_BEGINNING)==-1||
            Write(file,&chunk_size,sizeof chunk_size)!=sizeof chunk_size||
            curpos&1&&Write(file,&pad,1)!=1) goto fail;
        chunk_size=curpos;
        if (Seek(file,4,OFFSET_BEGINNING)==-1||
            Write(file,&chunk_size,sizeof chunk_size)!=sizeof chunk_size)
            goto fail;
        Close(file);
        wsp-=2;
        return;
    fail:   Close(file);
        error(WAKE_CANNOT_ACCESS);
    }
#elif defined(VGALIB)
#elif defined(_WINDOWS)
    {
        FILE *f;
        BITMAP bm;
        UBYTE MQ *bits;
        BITMAPINFO *bmi;
        BITMAPFILEHEADER bmfh;
        BITMAPINFOHEADER *bmih;
        HBITMAP t= *(HBITMAP *)wsp[-2].unit.data;
        int scanlinelength;
        if (!(f=fopen(wsp[-1].unit.sstring,"wb")))
            access_error(wsp[-1].unit.sstring);
        GetObject(t,sizeof(BITMAP),(LPSTR)&bm);
        if (paletteBased) {
            if (!(bmi=(BITMAPINFO *)memcalloc(sizeof(BITMAPINFO)+
                                              256*sizeof(RGBQUAD))))
                {
                    fclose(f);
                    error(WAKE_CANNOT_ALLOCATE);
                }
            bmi->bmiHeader.biBitCount=(UWORD)(bm.bmPlanes*bm.bmBitsPixel);
        }
        else {
            if (!(bmi=(BITMAPINFO *)memcalloc(sizeof(BITMAPINFO)))) {
                fclose(f);
                error(WAKE_CANNOT_ALLOCATE);
            }
            bmi->bmiHeader.biBitCount=24;
        }
        bmih=&bmi->bmiHeader;
        bmih->biSize=sizeof(BITMAPINFOHEADER);
        bmih->biWidth=bm.bmWidth;
        bmih->biHeight=bm.bmHeight;
        bmih->biPlanes=1;
        bmih->biCompression=BI_RGB;
        scanlinelength=(bmih->biWidth*(long)bmih->biBitCount+31)>>5<<2;
        if (!(bits=(UBYTE MQ *)memcalloc(bmih->biHeight*
                                         (long)scanlinelength))) {
            fclose(f);
            memfree(bmi);
            error(WAKE_CANNOT_ALLOCATE);
        }
        GetDIBits(dc,t,0,(UINT)bmih->biHeight,bits,bmi,DIB_RGB_COLORS);
        memset(&bmfh,0,sizeof bmfh);
        bmfh.bfType=(WORD)'B'|(WORD)'M'<<8;
        bmfh.bfSize=sizeof(BITMAPFILEHEADER)+sizeof(BITMAPINFOHEADER)+
            bmih->biHeight*(long)scanlinelength;
        bmfh.bfOffBits=sizeof(BITMAPINFOHEADER);
        if (paletteBased) {
            long cml=n_colors*sizeof(RGBQUAD);
            bmfh.bfSize+=cml;
            bmfh.bfOffBits+=cml;
        }
        if (fwrite(&bmfh,sizeof(BITMAPFILEHEADER),1,f)!=1||
            fwrite(bmih,sizeof(BITMAPINFOHEADER),1,f)!=1) goto fail;
        if (paletteBased) {
            int i,n;
            RGBQUAD *colors=bmi->bmiColors;
            n=1<<bmih->biBitCount;
            for (i=0; i<n; i++)
                if (fwrite(colors+i,sizeof(RGBQUAD),1,f)!=1)
                    goto fail;
        }
        if (fwrite(bits,scanlinelength,(int)bmih->biHeight,f)!=
            (unsigned)bmih->biHeight) goto fail;
        memfree(bits);
        memfree(bmi);
        fclose(f);
        wsp-=2;
        return;
    fail:   memfree(bits);
        memfree(bmi);
        fclose(f);
        error(WAKE_CANNOT_ACCESS);
    }
#elif defined(XLIB)
    {
        int t;
        unsigned int w,h,u;
        Window wnd;
        Pixmap pm= *(Pixmap *)wsp[-2].unit.data;
        if (!XGetGeometry(display,pm,&wnd,&t,&t,&w,&h,&u,&u))
            error(WAKE_CANNOT_ALLOCATE);
        switch (XWriteBitmapFile(display,wsp[-1].unit.sstring,pm,
                                 w,h,0,0)) {
        case BitmapOpenFailed:
            error(WAKE_CANNOT_ACCESS);
        case BitmapNoMemory:
            error(WAKE_CANNOT_ALLOCATE);
        }
        wsp-=2;
    }
#else
    {
        FILE *f;
        unsigned s;
        void *image=wsp[-2].unit.data;
        if (!(f=fopen(wsp[-1].unit.sstring,"wb")))
            access_error(wsp[-1].unit.sstring);
        s=imagesize(0,0,*(short *)image,*((short *)image+1));
        if (fwrite(*(void **)image,1,s,f)!=s) {
            fclose(f);
            access_error(wsp[-1].unit.sstring);
        }
        fclose(f);
        wsp-=2;
    }
#endif
}

WAKE_FUNC gra2_funcs[]={
    {"dtransform",      wk_dtransform},
    {"expose",      wk_expose},
    {"fill",        wk_fill},
    {"fillout",     wk_fillout},
    {"flood",       wk_flood},
    {"general",     wk_general},
    {"getimage",        wk_getimage},
    {"identmatrix",     wk_identmatrix},
    {"idtransform",     wk_idtransform},
    {"image",       wk_image},
    {"init",        wk_init},
    {"initmatrix",      wk_initmatrix},
    {"invertmatrix",    wk_invertmatrix},
    {"itransform",      wk_itransform},
    {"line",        wk_line},
    {"listfonts",       wk_listfonts},
    {"matrix",      wk_matrix},
    {"move",        wk_move},
    {"moverel",     wk_moverel},
    {"ncolors",     wk_ncolors},
    {"newarea",     wk_newarea},
    {"proportions",     wk_proportions},
    {"putimage",        wk_putimage},
    {"reach",       wk_reach},
    {"readimage",       wk_readimage},
    {"recycle",     wk_recycle},
    {"restate",     wk_restate},
    {"rotate",      wk_rotate},
    {"scroll",      wk_scroll},
    {"setdestination",  wk_setdestination},
    {"setfillrule",     wk_setfillrule},
    {"setfont",     wk_setfont},
    {"sethalftone",     wk_sethalftone},
    {"setmatrix",       wk_setmatrix},
    {"setrgbbackcolor", wk_setrgbbackcolor},
    {"setrgbcolor",     wk_setrgbcolor},
    {"setrgboutcolor",  wk_setrgboutcolor},
    {"showimage",       wk_showimage},
    {"specific",        wk_specific},
    {"state",       wk_state},
    {"stringwidth",     wk_stringwidth},
    {"stroke",      wk_stroke},
    {"synchronization", wk_synchronization},
    {"synchronize",     wk_synchronize},
    {"text",        wk_text},
    {"transform",       wk_transform},
    {"translate",       wk_translate},
    {"wipe",        wk_wipe},
    {"writeimage",      wk_writeimage}};

int init_gra2(void)
{
    return enter_funcs(gra2_funcs,ELEMS(gra2_funcs));
}
