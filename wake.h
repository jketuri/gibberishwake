
/* wake.h */

#if defined(_Windows) || defined(_WIN32)
#define _WINDOWS
#endif
#ifdef __MSDOS__
#define _MSDOS
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <setjmp.h>
#include <ctype.h>
#ifdef _DCC
extern __stkargs int setjmp(jmp_buf);
extern __stkargs void longjmp(jmp_buf,int);
#endif
#define REMOTE (1<<0)
#define SUPPRESS (1<<1)
#define TEXTONLY (1<<2)
#ifdef AMIGA
#include <exec/types.h>
#define HAMDISPLAY (1<<3)
#endif
#ifdef _WINDOWS
#include <windows.h>
#endif
#ifdef XLIB
#ifdef VAXC
#define FUNCPROTO
#include <X11/intrinsic.h>
#include <X11/xresource.h>
#endif
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#endif
#ifdef __TURBOC__
#include <alloc.h>
#include <mem.h>
#define MQ huge
#define UN (void far *)
#elif defined(_MSC_VER)  && !defined(_WIN32)
#define MQ __huge
#define UN (void __far *)
#else
#define MQ
#define UN
#endif

#ifdef _DCC
#define REGQUAL __regargs
#else
#define REGQUAL
#endif

#ifdef MAIN
#define VARIABLE
#else
#define VARIABLE extern
#endif

#ifdef _WINDOWS
#ifdef _WIN32
#define EXPORT
#else
#define EXPORT __export
#endif
#ifdef _WIN32
#define handle(p) GlobalHandle(p)
#elif defined(_MSC_VER)
#define handle(p) ((HGLOBAL)LOWORD(GlobalHandle((WORD)(__segment)p)))
#else
#define handle(p) ((HGLOBAL)LOWORD(GlobalHandle((WORD)(void _seg *)p)))
#endif
#define memalloc(n) GlobalLock(GlobalAlloc(GMEM_MOVEABLE|GMEM_SHARE,n))
#define memcalloc(n) GlobalLock(GlobalAlloc(GHND|GMEM_SHARE,n))
#define memrealloc(p,n) GlobalLock(GlobalReAlloc(handle(p),n,\
GMEM_MOVEABLE|GMEM_SHARE))
#define memfree(p) GlobalFree(handle(p))
#elif defined(__TURBOC__)
#define memalloc(n) farmalloc(n)
#define memcalloc(n) farcalloc(1,n)
#define memrealloc(p,n) farrealloc(p,n)
#define memfree(p) farfree(p)
#else
#define memalloc(n) malloc(n)
#define memcalloc(n) calloc(1,n)
#define memrealloc(p,n) realloc(p,n)
#define memfree(p) free(p)
#endif

#ifdef __APPLE__
#include <iodbcunix.h>
#endif
#ifdef _WINDOWS
#define _CDECL __cdecl
#else
#define _CDECL
#include <sqltypes.h>
#endif

extern LPSTR message(DWORD n);
extern void report(LPCSTR msg);

#ifdef VGALIB
extern long _CDECL wftell(FILE *f);
extern int _CDECL wungetc(int,FILE *);
extern int _CDECL vfgetc(FILE *);
extern char _CDECL *vfgets(char *,int,FILE *);
#define ftll wftell
#define ngtc wungetc
#define fgtc vfgetc
#define fgts vfgets
#define fptc fputc
#define fpts fputs
#define fprntf fprintf
#elif defined(_WINDOWS) || defined(XLIB)
extern long _CDECL wftell(FILE *f);
extern int _CDECL wungetc(int,FILE *);
extern int _CDECL wfgetc(FILE *);
extern char* _CDECL wfgets(char *,int,FILE *);
extern int _CDECL wfputc(int,FILE *);
extern int _CDECL wfputs(char *,FILE *);
extern int _CDECL wfprintf(FILE *,char *,...);
#define ftll wftell
#define ngtc wungetc
#define fgtc wfgetc
#define fgts wfgets
#define fptc wfputc
#define fpts wfputs
#define fprntf wfprintf
#else
#define fgtc fgetc
#define fgts fgets
#define ftll ftell
#define fptc fputc
#define fpts fputs
#define ngtc ungetc
#define fprntf fprintf
#endif

#define TRUE 1
#define FALSE 0
#define ELEMS(array) (sizeof array/sizeof *array)

#define WAKE_STRBUF_SIZE 256
#define WAKE_STACK_SIZE 128
#define WAKE_STACK_INCR 64
#define EXEC_STACK_SIZE 64
#define EXEC_STACK_INCR 32
#define NOMEN_STACK_SIZE 16
#define NOMEN_STACK_INCR 8

#define WAKE_STOP -3
#define WAKE_EXIT -2
#define WAKE_QUIT -1
#define WAKE_STOPPED 0
#define WAKE_UNKNOWN_NAME 1
#define WAKE_STACK_UNDERFLOW 2
#define WAKE_ILLEGAL_USE 3
#define WAKE_INVALID_TYPE 4
#define WAKE_INDEX_OUT_OF_RANGE 5
#define WAKE_DIVISION_BY_ZERO 6
#define WAKE_SYNTAX_ERROR 7
#define WAKE_CANNOT_ACCESS 8
#define WAKE_CANNOT_ALLOCATE 9
#define WAKE_ENTRY_NOT_BOUND 10
#define WAKE_UNMATCHED_MARK 11
#define WAKE_FAULTY_FORM 12

#define NULL_TYPE 0
#define TRUE_TYPE 1
#define ARRAY_TYPE 2
#define BLOCK_TYPE 3
#define QUICK_TYPE 4
#define FUNCTION_TYPE 5
#define FLOATING_TYPE 6
#define FRACTION_TYPE 7
#define INTEGER_TYPE 8
#define UINTEGER_TYPE 9
#define COMPLEX_TYPE 10
#define FLOATARRAY_TYPE 11
#define INTARRAY_TYPE 12
#define UINTARRAY_TYPE 13
#define SINGLEARRAY_TYPE 14
#define SHORTARRAY_TYPE 15
#define USHORTARRAY_TYPE 16
#define STRING_TYPE 17
#define ENTRY_TYPE 18
#define NAME_TYPE 19
#define NOMEN_TYPE 20
#define FILE_TYPE 21
#define IMAGE_TYPE 22
#define VOICE_TYPE 23
#define SAVE_TYPE 24
#define STATE_TYPE 25
#define MARK_TYPE 26
#define WAVE_TYPE 27
#define NUMTYPES 28

#define COMPOSITE (1<<0)
#define INTERVAL (1<<1)
#define INDEXED (1<<2)
#define NUMBER (1<<3)
#define INTEGER (1<<4)
#define FIXED (1<<5)
#define TEMPORARY (1<<6)

#define BOUND (1<<0)
#define BORROWED (1<<1)
#define DETERMINED (1<<2)

#define MARKED (1<<0)

#ifndef min
#define min(a,b) (((a)<(b))?(a):(b))
#endif
#ifndef max
#define max(a,b) (((a)>(b))?(a):(b))
#endif

#define check(number) if (wsp-wake_stack<number) error(WAKE_STACK_UNDERFLOW);

#ifdef AMIGA
typedef struct {
	ULONG oneShotHiSamples,repeatHiSamples,samplesperHiCycle;
	UWORD samplesperSec;
	UBYTE ctOctave,sCompression;
	LONG volume;
} VHDR;

typedef struct {
	VHDR vhdr;
	UBYTE note_byte[128];
	UBYTE *wave_pos[12];
	UBYTE *data;
	ULONG length;
} VOICE;
#else
typedef unsigned char UBYTE;
#ifndef __GNUC__
typedef unsigned long ULONG;
#endif
#endif

#ifdef XLIB
#define POINT XPoint
typedef struct {
	UWORD red,green,blue;
} RGBVAL;
#else
#ifndef _WINDOWS
typedef struct {
	short x,y;
} POINT;
#endif
typedef struct {
	UBYTE red,green,blue;
} RGBVAL;
#endif

typedef struct
{
	POINT currentpoint;
	POINT first;
	POINT *points;
	short number;
	int n_counts;
	int *counts;
	int point_o;
	UBYTE flag;
	float trans[6];
	RGBVAL rgbcolor;
	RGBVAL rgboutcolor;
} STATE;

typedef struct {
	char *name;
	void (*function)(void);
} WAKE_FUNC;

typedef struct {
	struct ENTRY *entry;
	ULONG number;
} NOMEN;

typedef NOMEN *NOMENP;

typedef struct {
	long num,den;
} FRACT;

typedef struct {
	float real,imag;
} COMPLEX;

typedef struct HEAD {
	UBYTE flag;
	UBYTE type;
	ULONG prot;
	struct HEAD *head;
	struct HEAD *last;
} HEAD;

typedef union {
	struct {
		void *ptr;
		struct ELEMENT *elem;
	} exec;
	struct {
		void *ptr;
		ULONG length;
	} line;
	struct {
		void *ptr;
		struct ENTRY *name;
	} pair;
	struct {
		void *ptr;
		NOMENP nomen;
	} site;
	void *data;
	HEAD *head;
	struct ELEMENT *area;
	struct ELEMENT *array;
	struct ELEMENT *block;
	void (*function)(void);
	double floating;
	FRACT fract;
	long integer;
	ULONG uinteger;
	COMPLEX compl;
	double *floatarray;
	long *intarray;
	ULONG *uintarray;
	float *singlearray;
	short *shortarray;
	UWORD *ushortarray;
	UBYTE *string;
	char *sstring;
	struct ENTRY *entry;
	struct ENTRY *name;
	NOMENP nomen;
	FILE *file;
	HEAD *save;
	STATE *state;
} UNIT;

typedef struct ELEMENT {
	UBYTE flag;
	UBYTE type;
	UNIT unit;
} ELEMENT;

typedef struct ENTRY {
	char b;
	UBYTE flag;
	char *name;
	ELEMENT element;
	struct ENTRY *l,*r;
} ENTRY;

typedef struct
{
	ELEMENT element;
	ELEMENT *exec_elem;
} EXECUTE;

VARIABLE jmp_buf exit_jump;
VARIABLE jmp_buf stop_jump;
VARIABLE jmp_buf quit_jump;
VARIABLE int stop_level;
VARIABLE int out_level;
VARIABLE char strbuf[WAKE_STRBUF_SIZE];
VARIABLE ELEMENT *wake_stack;
VARIABLE ELEMENT *wsp;
VARIABLE EXECUTE *exec_stack;
VARIABLE NOMENP *nomen_stack;
VARIABLE NOMENP *nsp;
VARIABLE ENTRY *exec_entry;
VARIABLE NOMENP exec_nomen;
VARIABLE ENTRY *errnument;
VARIABLE ENTRY *errelement;
VARIABLE ENTRY *errfposent;
VARIABLE ENTRY *errfnameent;
VARIABLE ELEMENT err_elem;
VARIABLE char *file_name;
VARIABLE HEAD *last_save;
VARIABLE ELEMENT **eel;
VARIABLE long file_pos;
VARIABLE void *temp_mem;
VARIABLE unsigned exit_est;
VARIABLE unsigned stop_est;
VARIABLE unsigned est;
#ifdef MAIN
VARIABLE unsigned wake_stack_size=WAKE_STACK_SIZE;
VARIABLE unsigned exec_stack_size=EXEC_STACK_SIZE;
#else
VARIABLE unsigned wake_stack_size;
VARIABLE unsigned exec_stack_size;
#endif

extern UBYTE oflag;
extern UBYTE type_size[NUMTYPES];
extern char *whites;

#ifdef AMIGA
int test(LONG file,char *s);
int skip(LONG file);
#endif
void memory_overflow(void);
REGQUAL ENTRY *ssearch(register char *name,register ENTRY *entry);
REGQUAL ENTRY *search(register char *name,register NOMENP *nomenp);
REGQUAL ENTRY *enter(register char *name,register NOMENP nomen);
REGQUAL void stack(register unsigned number);
REGQUAL void push(register ELEMENT *e);
REGQUAL void check_complex(register ELEMENT *e,register COMPLEX *c);
REGQUAL double check_double(register ELEMENT *e);
REGQUAL long check_integer(register ELEMENT *e);
REGQUAL ULONG check_uinteger(register ELEMENT *e);
REGQUAL void *get_mem(register ULONG num,register UBYTE type,
register char quit);
REGQUAL int token(register ELEMENT *elem);
int enter_funcs(WAKE_FUNC funcs[],int n_funcs);
void out_element(ELEMENT *elem,int typ,FILE *f);
void error(int err_num);
void entry_exec(void);
void execute(void);
void wake(void);

REGQUAL void jump_exec(register unsigned t);
void access_error(char *);
void free_composite(HEAD *head);
int init_wake(void);
void term_wake(void);
#ifdef _MSDOS
UBYTE MQ *bigmove(UBYTE MQ *target,UBYTE MQ *source,long l);
long bigread(FILE *file,UBYTE MQ *target,long length);
long bigwrite(FILE *file,UBYTE MQ *source,long length);
#else
#define bigmove memcpy
#define bigread(f,t,l) fread(t,1,l,f)
#define bigwrite(f,s,l) fwrite(s,1,l,f)
#endif

REGQUAL long gcd(register long n,register long m);
REGQUAL int canon(register ELEMENT *elem);
REGQUAL int ge(register ELEMENT *elem,register ELEMENT *elem1);
REGQUAL int gt(register ELEMENT *elem,register ELEMENT *elem1);
REGQUAL int le(register ELEMENT *elem,register ELEMENT *elem1);
REGQUAL int lt(register ELEMENT *elem,register ELEMENT *elem1);
REGQUAL int eq(register ELEMENT *elem,register ELEMENT *elem1);
REGQUAL ELEMENT *absf(register ELEMENT *elem);
REGQUAL ELEMENT *negf(register ELEMENT *elem);
REGQUAL ELEMENT *addf(register ELEMENT *elem,register ELEMENT *elem1,
register ELEMENT *elem2);
REGQUAL ELEMENT *subf(register ELEMENT *elem,register ELEMENT *elem1,
register ELEMENT *elem2);
REGQUAL ELEMENT *mulf(register ELEMENT *elem,register ELEMENT *elem1,
register ELEMENT *elem2);
REGQUAL ELEMENT *divf(register ELEMENT *elem,register ELEMENT *elem1,
register ELEMENT *elem2);
void matmul(register ELEMENT *c,ELEMENT *a,ELEMENT *b,
long p,long n,long q);

