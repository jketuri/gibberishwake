
/* wk_mat.c */

#include "wake.h"
#define MAIN
#undef VARIABLE
#include "wk_mat.h"

extern int init_mat2(void);

REGQUAL ELEMENT *addf_float_float(register ELEMENT *elem,
                                  register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=FLOATING_TYPE;
    elem->unit.floating=elem1->unit.floating+elem2->unit.floating;
    return elem;
}

REGQUAL ELEMENT *addf_float_fract(register ELEMENT *elem,
                                  register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=FLOATING_TYPE;
    elem->unit.floating=elem1->unit.floating+
        (double)elem2->unit.fract.num/(double)elem2->unit.fract.den;
    return elem;
}

REGQUAL ELEMENT *addf_float_int(register ELEMENT *elem,
                                register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=FLOATING_TYPE;
    elem->unit.floating=elem1->unit.floating+(double)elem2->unit.integer;
    return elem;
}

REGQUAL ELEMENT *addf_float_uint(register ELEMENT *elem,
                                 register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=FLOATING_TYPE;
    elem->unit.floating=elem1->unit.floating+(double)elem2->unit.uinteger;
    return elem;
}

REGQUAL ELEMENT *addf_float_complex(register ELEMENT *elem,
                                    register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=COMPLEX_TYPE;
    elem->unit.compl.real=(float)elem1->unit.floating+
        elem2->unit.compl.real;
    elem->unit.compl.imag=elem2->unit.compl.imag;
    return elem;
}

REGQUAL ELEMENT *addf_fract_float(register ELEMENT *elem,
                                  register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=FLOATING_TYPE;
    elem->unit.floating=(double)elem1->unit.fract.num/
        (double)elem1->unit.fract.den+elem2->unit.floating;
    return elem;
}

REGQUAL ELEMENT *addf_fract_fract(register ELEMENT *elem,
                                  register ELEMENT *elem1,register ELEMENT *elem2)
{
    if (elem1->unit.fract.den!=elem2->unit.fract.den) {
        elem->unit.fract.num=elem1->unit.fract.num*
            elem2->unit.fract.den+elem2->unit.fract.num*
            elem1->unit.fract.den;
        elem->unit.fract.den=elem1->unit.fract.den*
            elem2->unit.fract.den;
    }
    else {
        elem->unit.fract.num=elem1->unit.fract.num+
            elem2->unit.fract.num;
        elem->unit.fract.den=elem1->unit.fract.den;
    }
    canon(elem);
    return elem;
}

REGQUAL ELEMENT *addf_fract_int(register ELEMENT *elem,
                                register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=FRACTION_TYPE;
    elem->unit.fract.num=elem1->unit.fract.num+elem2->unit.integer*
        elem1->unit.fract.den;
    elem->unit.fract.den=elem1->unit.fract.den;
    return elem;
}

REGQUAL ELEMENT *addf_fract_uint(register ELEMENT *elem,
                                 register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=FRACTION_TYPE;
    elem->unit.fract.num=elem1->unit.fract.num+elem2->unit.uinteger*
        elem1->unit.fract.den;
    elem->unit.fract.den=elem1->unit.fract.den;
    return elem;
}

REGQUAL ELEMENT *addf_fract_complex(register ELEMENT *elem,
                                    register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=COMPLEX_TYPE;
    elem->unit.compl.real=(float)elem1->unit.fract.num/
        (float)elem1->unit.fract.den+elem2->unit.compl.real;
    elem->unit.compl.imag=elem2->unit.compl.imag;
    return elem;
}

REGQUAL ELEMENT *addf_int_float(register ELEMENT *elem,
                                register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=FLOATING_TYPE;
    elem->unit.floating=(double)elem1->unit.integer+elem2->unit.floating;
    return elem;
}

REGQUAL ELEMENT *addf_int_fract(register ELEMENT *elem,
                                register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=FRACTION_TYPE;
    elem->unit.fract.num=elem1->unit.integer*elem2->unit.fract.den+
        elem2->unit.fract.num;
    elem->unit.fract.den=elem2->unit.fract.den;
    return elem;
}

REGQUAL ELEMENT *addf_int_int(register ELEMENT *elem,
                              register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->flag|=INTEGER;
    elem->type=INTEGER_TYPE;
    elem->unit.integer=elem1->unit.integer+elem2->unit.integer;
    return elem;
}

REGQUAL ELEMENT *addf_int_uint(register ELEMENT *elem,
                               register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->flag|=INTEGER;
    elem->type=INTEGER_TYPE;
    elem->unit.integer=elem1->unit.integer+elem2->unit.uinteger;
    return elem;
}

REGQUAL ELEMENT *addf_int_complex(register ELEMENT *elem,
                                  register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=COMPLEX_TYPE;
    elem->unit.compl.real=(float)elem1->unit.integer+
        elem2->unit.compl.real;
    elem->unit.compl.imag=elem2->unit.compl.imag;
    return elem;
}

REGQUAL ELEMENT *addf_uint_float(register ELEMENT *elem,
                                 register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=FLOATING_TYPE;
    elem->unit.floating=(double)elem1->unit.uinteger+
        elem2->unit.floating;
    return elem;
}

REGQUAL ELEMENT *addf_uint_fract(register ELEMENT *elem,
                                 register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=FRACTION_TYPE;
    elem->unit.fract.num=elem1->unit.uinteger*
        elem2->unit.fract.den+
        elem2->unit.fract.num;
    elem->unit.fract.den=elem2->unit.fract.den;
    return elem;
}

REGQUAL ELEMENT *addf_uint_int(register ELEMENT *elem,
                               register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->flag|=INTEGER;
    elem->type=INTEGER_TYPE;
    elem->unit.integer=elem1->unit.uinteger+elem2->unit.integer;
    return elem;
}

REGQUAL ELEMENT *addf_uint_uint(register ELEMENT *elem,
                                register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->flag|=INTEGER;
    elem->type=UINTEGER_TYPE;
    elem->unit.uinteger=elem1->unit.uinteger+elem2->unit.uinteger;
    return elem;
}

REGQUAL ELEMENT *addf_uint_complex(register ELEMENT *elem,
                                   register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=COMPLEX_TYPE;
    elem->unit.compl.real=(float)elem1->unit.uinteger+
        elem2->unit.compl.real;
    elem->unit.compl.imag=elem2->unit.compl.imag;
    return elem;
}

REGQUAL ELEMENT *addf_complex_float(register ELEMENT *elem,
                                    register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=COMPLEX_TYPE;
    elem->unit.compl.real=elem1->unit.compl.real+
        (float)elem2->unit.floating;
    elem->unit.compl.imag=elem1->unit.compl.imag;
    return elem;
}

REGQUAL ELEMENT *addf_complex_fract(register ELEMENT *elem,
                                    register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=COMPLEX_TYPE;
    elem->unit.compl.real=elem1->unit.compl.real+
        (float)elem2->unit.fract.num/(float)elem2->unit.fract.den;
    elem->unit.compl.imag=elem1->unit.compl.imag;
    return elem;
}

REGQUAL ELEMENT *addf_complex_int(register ELEMENT *elem,
                                  register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=COMPLEX_TYPE;
    elem->unit.compl.real=elem1->unit.compl.real+
        (float)elem2->unit.integer;
    elem->unit.compl.imag=elem1->unit.compl.imag;
    return elem;
}

REGQUAL ELEMENT *addf_complex_uint(register ELEMENT *elem,
                                   register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=COMPLEX_TYPE;
    elem->unit.compl.real=elem1->unit.compl.real+
        (float)elem2->unit.uinteger;
    elem->unit.compl.imag=elem1->unit.compl.imag;
    return elem;
}

REGQUAL ELEMENT *addf_complex_complex(register ELEMENT *elem,
                                      register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=COMPLEX_TYPE;
    elem->unit.compl.real=elem1->unit.compl.real+
        elem2->unit.compl.real;
    elem->unit.compl.imag=elem1->unit.compl.imag+
        elem2->unit.compl.imag;
    return elem;
}

REGQUAL ELEMENT *addf_floatarray_float(register ELEMENT *elem,
                                       register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.floatarray[i]=
                                                  elem1->unit.floatarray[i]+elem2->unit.floating;
    return elem;
}

REGQUAL ELEMENT *addf_floatarray_fract(register ELEMENT *elem,
                                       register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    double d=(double)elem2->unit.fract.num/(double)elem2->unit.fract.den;
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.floatarray[i]=
                                                  elem1->unit.floatarray[i]+d;
    return elem;
}

REGQUAL ELEMENT *addf_floatarray_int(register ELEMENT *elem,
                                     register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.floatarray[i]=
                                                  elem1->unit.floatarray[i]+(double)elem2->unit.integer;
    return elem;
}

REGQUAL ELEMENT *addf_floatarray_uint(register ELEMENT *elem,
                                      register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.floatarray[i]=
                                                  elem1->unit.floatarray[i]+(double)elem2->unit.uinteger;
    return elem;
}

REGQUAL ELEMENT *addf_floatarray_floatarray(register ELEMENT *elem,
                                            register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.floatarray[i]=
                            elem1->unit.floatarray[i]+elem2->unit.floatarray[i];
    return elem;
}

REGQUAL ELEMENT *addf_floatarray_intarray(register ELEMENT *elem,
                                          register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.floatarray[i]=
                            elem1->unit.floatarray[i]+(double)elem2->unit.intarray[i];
    return elem;
}

REGQUAL ELEMENT *addf_floatarray_uintarray(register ELEMENT *elem,
                                           register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.floatarray[i]=
                            elem1->unit.floatarray[i]+(double)elem2->unit.uintarray[i];
    return elem;
}

REGQUAL ELEMENT *addf_floatarray_singlearray(register ELEMENT *elem,
                                             register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.floatarray[i]=
                            elem1->unit.floatarray[i]+(double)elem2->unit.singlearray[i];
    return elem;
}

REGQUAL ELEMENT *addf_floatarray_shortarray(register ELEMENT *elem,
                                            register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.floatarray[i]=
                            elem1->unit.floatarray[i]+(double)elem2->unit.shortarray[i];
    return elem;
}

REGQUAL ELEMENT *addf_floatarray_ushortarray(register ELEMENT *elem,
                                             register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.floatarray[i]=
                            elem1->unit.floatarray[i]+(double)elem2->unit.ushortarray[i];
    return elem;
}

REGQUAL ELEMENT *addf_floatarray_string(register ELEMENT *elem,
                                        register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.floatarray[i]=
                            elem1->unit.floatarray[i]+(double)elem2->unit.string[i];
    return elem;
}

REGQUAL ELEMENT *addf_intarray_int(register ELEMENT *elem,
                                   register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.intarray[i]=
                                                  elem1->unit.intarray[i]+elem2->unit.integer;
    return elem;
}

REGQUAL ELEMENT *addf_intarray_uint(register ELEMENT *elem,
                                    register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.intarray[i]=
                                                  elem1->unit.intarray[i]+elem2->unit.uinteger;
    return elem;
}

REGQUAL ELEMENT *addf_intarray_intarray(register ELEMENT *elem,
                                        register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.intarray[i]=
                            elem1->unit.intarray[i]+elem2->unit.intarray[i];
    return elem;
}

REGQUAL ELEMENT *addf_intarray_uintarray(register ELEMENT *elem,
                                         register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.intarray[i]=
                            elem1->unit.intarray[i]+elem2->unit.uintarray[i];
    return elem;
}

REGQUAL ELEMENT *addf_intarray_shortarray(register ELEMENT *elem,
                                          register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.intarray[i]=
                            elem1->unit.intarray[i]+elem2->unit.shortarray[i];
    return elem;
}

REGQUAL ELEMENT *addf_intarray_ushortarray(register ELEMENT *elem,
                                           register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.intarray[i]=
                            elem1->unit.intarray[i]+elem2->unit.ushortarray[i];
    return elem;
}

REGQUAL ELEMENT *addf_intarray_string(register ELEMENT *elem,
                                      register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.intarray[i]=
                            elem1->unit.intarray[i]+elem2->unit.string[i];
    return elem;
}

REGQUAL ELEMENT *addf_uintarray_int(register ELEMENT *elem,
                                    register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.uintarray[i]=
                                                  elem1->unit.uintarray[i]+elem2->unit.integer;
    return elem;
}

REGQUAL ELEMENT *addf_uintarray_uint(register ELEMENT *elem,
                                     register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.uintarray[i]=
                                                  elem1->unit.uintarray[i]+elem2->unit.uinteger;
    return elem;
}

REGQUAL ELEMENT *addf_uintarray_intarray(register ELEMENT *elem,
                                         register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.uintarray[i]=
                            elem1->unit.uintarray[i]+elem2->unit.intarray[i];
    return elem;
}

REGQUAL ELEMENT *addf_uintarray_uintarray(register ELEMENT *elem,
                                          register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.intarray[i]=
                            elem1->unit.uintarray[i]+elem2->unit.uintarray[i];
    return elem;
}

REGQUAL ELEMENT *addf_uintarray_shortarray(register ELEMENT *elem,
                                           register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.uintarray[i]=
                            elem1->unit.uintarray[i]+elem2->unit.shortarray[i];
    return elem;
}

REGQUAL ELEMENT *addf_uintarray_ushortarray(register ELEMENT *elem,
                                            register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.uintarray[i]=
                            elem1->unit.uintarray[i]+elem2->unit.ushortarray[i];
    return elem;
}

REGQUAL ELEMENT *addf_uintarray_string(register ELEMENT *elem,
                                       register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.uintarray[i]=
                            elem1->unit.uintarray[i]+elem2->unit.string[i];
    return elem;
}

REGQUAL ELEMENT *addf_string_int(register ELEMENT *elem,
                                 register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    *elem= *elem1;
    for (i=0; i<elem->unit.line.length; i++) elem->unit.string[i]=(UBYTE)
                                                 (elem1->unit.string[i]+elem2->unit.integer);
    return elem;
}

REGQUAL ELEMENT *addf_string_uint(register ELEMENT *elem,
                                  register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    *elem= *elem1;
    for (i=0; i<elem->unit.line.length; i++) elem->unit.string[i]=(UBYTE)
                                                 (elem1->unit.string[i]+elem2->unit.uinteger);
    return elem;
}

REGQUAL ELEMENT *addf_string_string(register ELEMENT *elem,
                                    register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.string[i]=(UBYTE)
                            (elem1->unit.string[i]+elem2->unit.string[i]);
    return elem;
}

MAT_FUNC addf_func[12][12]={
    {addf_float_float,addf_float_fract,addf_float_int,addf_float_uint,
     addf_float_complex},
    {addf_fract_float,addf_fract_fract,addf_fract_int,addf_fract_uint,
     addf_fract_complex},
    {addf_int_float,addf_int_fract,addf_int_int,addf_int_uint,
     addf_int_complex},
    {addf_uint_float,addf_uint_fract,addf_uint_int,addf_uint_uint,
     addf_uint_complex},
    {addf_complex_float,addf_complex_fract,addf_complex_int,addf_complex_uint,
     addf_complex_complex},
    {addf_floatarray_float,addf_floatarray_fract,addf_floatarray_int,
     addf_floatarray_uint,NULL,addf_floatarray_floatarray,
     addf_floatarray_intarray,addf_floatarray_uintarray,
     addf_floatarray_singlearray,addf_floatarray_shortarray,
     addf_floatarray_ushortarray,addf_floatarray_string},
    {NULL,NULL,addf_intarray_int,addf_intarray_uint,NULL,NULL,
     addf_intarray_intarray,addf_intarray_uintarray,NULL,
     addf_intarray_shortarray,addf_intarray_ushortarray,addf_intarray_string},
    {NULL,NULL,addf_uintarray_int,addf_uintarray_uint,NULL,NULL,
     addf_uintarray_intarray,addf_uintarray_uintarray,NULL,
     addf_uintarray_shortarray,addf_uintarray_ushortarray,addf_uintarray_string},
    {NULL},
    {NULL},
    {NULL},
    {NULL,NULL,addf_string_int,addf_string_uint,NULL,NULL,NULL,NULL,NULL,NULL,
     NULL,addf_string_string}};

REGQUAL ELEMENT *addf(register ELEMENT *elem,register ELEMENT *elem1,
                      register ELEMENT *elem2)
{
    if (elem1->type==ARRAY_TYPE)
        switch (elem2->type) {
        case ARRAY_TYPE: {
            ULONG i,l=min(elem1->unit.line.length,
                          elem2->unit.line.length);
            *elem= *elem1;
            for (i=0; i<l; i++)
                addf(elem->unit.array+i,
                     elem1->unit.array+i,
                     elem2->unit.array+i);
            return elem;
        }
        case FLOATING_TYPE:
        case FRACTION_TYPE:
        case INTEGER_TYPE:
        case UINTEGER_TYPE:
        case COMPLEX_TYPE: {
            ULONG i;
            *elem= *elem1;
            for (i=0; i<elem1->unit.line.length; i++)
                addf(elem->unit.array+i,
                     elem1->unit.array+i,elem2);
            return elem;
        }
        }
    else if (elem1->type>=FLOATING_TYPE&&elem1->type<=STRING_TYPE&&
             elem2->type>=FLOATING_TYPE&&elem2->type<=STRING_TYPE) {
        register MAT_FUNC func=
            addf_func[elem1->type-FLOATING_TYPE][elem2->type-FLOATING_TYPE];
        if (func) return (*func)(elem,elem1,elem2); }
    error(WAKE_INVALID_TYPE);
    return NULL;
}

REGQUAL ELEMENT *subf_float_float(register ELEMENT *elem,
                                  register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=FLOATING_TYPE;
    elem->unit.floating=elem1->unit.floating-elem2->unit.floating;
    return elem;
}

REGQUAL ELEMENT *subf_float_fract(register ELEMENT *elem,
                                  register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=FLOATING_TYPE;
    elem->unit.floating=elem1->unit.floating-
        (double)elem2->unit.fract.num/(double)elem2->unit.fract.den;
    return elem;
}

REGQUAL ELEMENT *subf_float_int(register ELEMENT *elem,
                                register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=FLOATING_TYPE;
    elem->unit.floating=elem1->unit.floating-(double)elem2->unit.integer;
    return elem;
}

REGQUAL ELEMENT *subf_float_uint(register ELEMENT *elem,
                                 register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=FLOATING_TYPE;
    elem->unit.floating=elem1->unit.floating-
        (double)elem2->unit.uinteger;
    return elem;
}

REGQUAL ELEMENT *subf_float_complex(register ELEMENT *elem,
                                    register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=COMPLEX_TYPE;
    elem->unit.compl.real=(float)elem1->unit.floating-
        elem2->unit.compl.real;
    elem->unit.compl.imag=elem2->unit.compl.imag;
    return elem;
}

REGQUAL ELEMENT *subf_fract_float(register ELEMENT *elem,
                                  register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=FLOATING_TYPE;
    elem->unit.floating=(double)elem1->unit.fract.num/
        (double)elem1->unit.fract.den-elem2->unit.floating;
    return elem;
}

REGQUAL ELEMENT *subf_fract_fract(register ELEMENT *elem,
                                  register ELEMENT *elem1,register ELEMENT *elem2)
{
    if (elem1->unit.fract.den!=elem2->unit.fract.den) {
        elem->unit.fract.num=elem1->unit.fract.num*
            elem2->unit.fract.den-elem2->unit.fract.num*
            elem1->unit.fract.den;
        elem->unit.fract.den=elem1->unit.fract.den*
            elem2->unit.fract.den;
    }
    else {
        elem->unit.fract.num=elem1->unit.fract.num-
            elem2->unit.fract.num;
        elem->unit.fract.den=elem1->unit.fract.den;
    }
    canon(elem);
    return elem;
}

REGQUAL ELEMENT *subf_fract_int(register ELEMENT *elem,
                                register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=FRACTION_TYPE;
    elem->unit.fract.num=elem1->unit.fract.num-elem2->unit.integer*
        elem1->unit.fract.den;
    elem->unit.fract.den=elem1->unit.fract.den;
    return elem;
}

REGQUAL ELEMENT *subf_fract_uint(register ELEMENT *elem,
                                 register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=FRACTION_TYPE;
    elem->unit.fract.num=elem1->unit.fract.num-elem2->unit.uinteger*
        elem1->unit.fract.den;
    elem->unit.fract.den=elem1->unit.fract.den;
    return elem;
}

REGQUAL ELEMENT *subf_fract_complex(register ELEMENT *elem,
                                    register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=COMPLEX_TYPE;
    elem->unit.compl.real=(float)elem1->unit.fract.num/
        (float)elem1->unit.fract.den-elem2->unit.compl.real;
    elem->unit.compl.imag=elem2->unit.compl.imag;
    return elem;
}

REGQUAL ELEMENT *subf_int_float(register ELEMENT *elem,
                                register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=FLOATING_TYPE;
    elem->unit.floating=(double)elem1->unit.integer-elem2->unit.floating;
    return elem;
}

REGQUAL ELEMENT *subf_int_fract(register ELEMENT *elem,
                                register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=FRACTION_TYPE;
    elem->unit.fract.num=elem1->unit.integer*elem2->unit.fract.den-
        elem2->unit.fract.num;
    elem->unit.fract.den=elem2->unit.fract.den;
    return elem;
}

REGQUAL ELEMENT *subf_int_int(register ELEMENT *elem,
                              register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->flag|=INTEGER;
    elem->type=INTEGER_TYPE;
    elem->unit.integer=elem1->unit.integer-elem2->unit.integer;
    return elem;
}

REGQUAL ELEMENT *subf_int_uint(register ELEMENT *elem,
                               register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->flag|=INTEGER;
    elem->type=INTEGER_TYPE;
    elem->unit.integer=elem1->unit.integer-elem2->unit.uinteger;
    return elem;
}

REGQUAL ELEMENT *subf_int_complex(register ELEMENT *elem,
                                  register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=COMPLEX_TYPE;
    elem->unit.compl.real=(float)elem1->unit.integer-
        elem2->unit.compl.real;
    elem->unit.compl.imag=elem2->unit.compl.imag;
    return elem;
}

REGQUAL ELEMENT *subf_uint_float(register ELEMENT *elem,
                                 register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=FLOATING_TYPE;
    elem->unit.floating=(double)elem1->unit.uinteger-elem2->unit.floating;
    return elem;
}

REGQUAL ELEMENT *subf_uint_fract(register ELEMENT *elem,
                                 register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=FRACTION_TYPE;
    elem->unit.fract.num=elem1->unit.uinteger*elem2->unit.fract.den-
        elem2->unit.fract.num;
    elem->unit.fract.den=elem2->unit.fract.den;
    return elem;
}

REGQUAL ELEMENT *subf_uint_int(register ELEMENT *elem,
                               register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->flag|=INTEGER;
    elem->type=INTEGER_TYPE;
    elem->unit.integer=elem1->unit.uinteger-elem2->unit.integer;
    return elem;
}

REGQUAL ELEMENT *subf_uint_uint(register ELEMENT *elem,
                                register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->flag|=INTEGER;
    elem->type=UINTEGER_TYPE;
    elem->unit.uinteger=elem1->unit.uinteger-elem2->unit.uinteger;
    return elem;
}

REGQUAL ELEMENT *subf_uint_complex(register ELEMENT *elem,
                                   register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=COMPLEX_TYPE;
    elem->unit.compl.real=(float)elem1->unit.uinteger-
        elem2->unit.compl.real;
    elem->unit.compl.imag=elem2->unit.compl.imag;
    return elem;
}

REGQUAL ELEMENT *subf_complex_float(register ELEMENT *elem,
                                    register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=COMPLEX_TYPE;
    elem->unit.compl.real=elem1->unit.compl.real-
        (float)elem2->unit.floating;
    elem->unit.compl.imag=elem1->unit.compl.imag;
    return elem;
}

REGQUAL ELEMENT *subf_complex_fract(register ELEMENT *elem,
                                    register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=COMPLEX_TYPE;
    elem->unit.compl.real=elem1->unit.compl.real-
        (float)elem2->unit.fract.num/(float)elem2->unit.fract.den;
    elem->unit.compl.imag=elem1->unit.compl.imag;
    return elem;
}

REGQUAL ELEMENT *subf_complex_int(register ELEMENT *elem,
                                  register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=COMPLEX_TYPE;
    elem->unit.compl.real=elem1->unit.compl.real-
        (float)elem2->unit.integer;
    elem->unit.compl.imag=elem1->unit.compl.imag;
    return elem;
}

REGQUAL ELEMENT *subf_complex_uint(register ELEMENT *elem,
                                   register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=COMPLEX_TYPE;
    elem->unit.compl.real=elem1->unit.compl.real-
        (float)elem2->unit.uinteger;
    elem->unit.compl.imag=elem1->unit.compl.imag;
    return elem;
}

REGQUAL ELEMENT *subf_complex_complex(register ELEMENT *elem,
                                      register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=COMPLEX_TYPE;
    elem->unit.compl.real=elem1->unit.compl.real-
        elem2->unit.compl.real;
    elem->unit.compl.imag=elem1->unit.compl.imag-
        elem2->unit.compl.imag;
    return elem;
}

REGQUAL ELEMENT *subf_floatarray_float(register ELEMENT *elem,
                                       register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.floatarray[i]=
                                                  elem1->unit.floatarray[i]-elem2->unit.floating;
    return elem;
}

REGQUAL ELEMENT *subf_floatarray_fract(register ELEMENT *elem,
                                       register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    double d=(double)elem2->unit.fract.num/(double)elem2->unit.fract.den;
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.floatarray[i]=
                                                  elem1->unit.floatarray[i]-d;
    return elem;
}

REGQUAL ELEMENT *subf_floatarray_int(register ELEMENT *elem,
                                     register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.floatarray[i]=
                                                  elem1->unit.floatarray[i]-(double)elem2->unit.integer;
    return elem;
}

REGQUAL ELEMENT *subf_floatarray_uint(register ELEMENT *elem,
                                      register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem1->unit.floatarray[i]=
                                                  elem1->unit.floatarray[i]-(double)elem2->unit.uinteger;
    return elem;
}

REGQUAL ELEMENT *subf_floatarray_floatarray(register ELEMENT *elem,
                                            register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.floatarray[i]=
                            elem1->unit.floatarray[i]-elem2->unit.floatarray[i];
    return elem;
}

REGQUAL ELEMENT *subf_floatarray_intarray(register ELEMENT *elem,
                                          register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.floatarray[i]=
                            elem1->unit.floatarray[i]-(double)elem2->unit.intarray[i];
    return elem;
}

REGQUAL ELEMENT *subf_floatarray_uintarray(register ELEMENT *elem,
                                           register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.floatarray[i]=
                            elem1->unit.floatarray[i]-(double)elem2->unit.uintarray[i];
    return elem;
}

REGQUAL ELEMENT *subf_floatarray_singlearray(register ELEMENT *elem,
                                             register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.floatarray[i]=
                            elem1->unit.floatarray[i]-(double)elem2->unit.singlearray[i];
    return elem;
}

REGQUAL ELEMENT *subf_floatarray_shortarray(register ELEMENT *elem,
                                            register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.floatarray[i]=
                            elem1->unit.floatarray[i]-(double)elem2->unit.shortarray[i];
    return elem;
}

REGQUAL ELEMENT *subf_floatarray_ushortarray(register ELEMENT *elem,
                                             register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.floatarray[i]=
                            elem1->unit.floatarray[i]-(double)elem2->unit.ushortarray[i];
    return elem;
}

REGQUAL ELEMENT *subf_floatarray_string(register ELEMENT *elem,
                                        register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.floatarray[i]=
                            elem1->unit.floatarray[i]-(double)elem2->unit.string[i];
    return elem;
}

REGQUAL ELEMENT *subf_intarray_int(register ELEMENT *elem,
                                   register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.intarray[i]=
                                                  elem1->unit.intarray[i]-elem2->unit.integer;
    return elem;
}

REGQUAL ELEMENT *subf_intarray_uint(register ELEMENT *elem,
                                    register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.intarray[i]=
                                                  elem1->unit.intarray[i]-elem2->unit.uinteger;
    return elem;
}

REGQUAL ELEMENT *subf_intarray_intarray(register ELEMENT *elem,
                                        register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.intarray[i]=
                            elem1->unit.intarray[i]-elem2->unit.intarray[i];
    return elem;
}

REGQUAL ELEMENT *subf_intarray_uintarray(register ELEMENT *elem,
                                         register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.intarray[i]=
                            elem1->unit.intarray[i]-elem2->unit.uintarray[i];
    return elem;
}

REGQUAL ELEMENT *subf_intarray_shortarray(register ELEMENT *elem,
                                          register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.intarray[i]=
                            elem1->unit.intarray[i]-elem2->unit.shortarray[i];
    return elem;
}

REGQUAL ELEMENT *subf_intarray_ushortarray(register ELEMENT *elem,
                                           register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.intarray[i]=
                            elem1->unit.intarray[i]-elem2->unit.ushortarray[i];
    return elem;
}

REGQUAL ELEMENT *subf_intarray_string(register ELEMENT *elem,
                                      register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.intarray[i]=
                            elem1->unit.intarray[i]-elem2->unit.string[i];
    return elem;
}

REGQUAL ELEMENT *subf_uintarray_int(register ELEMENT *elem,
                                    register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.uintarray[i]=
                                                  elem1->unit.uintarray[i]-elem2->unit.integer;
    return elem;
}

REGQUAL ELEMENT *subf_uintarray_uint(register ELEMENT *elem,
                                     register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.uintarray[i]=
                                                  elem1->unit.uintarray[i]-elem2->unit.uinteger;
    return elem;
}

REGQUAL ELEMENT *subf_uintarray_intarray(register ELEMENT *elem,
                                         register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.uintarray[i]=
                            elem1->unit.uintarray[i]-elem2->unit.intarray[i];
    return elem;
}

REGQUAL ELEMENT *subf_uintarray_uintarray(register ELEMENT *elem,
                                          register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.uintarray[i]=
                            elem1->unit.uintarray[i]-elem2->unit.uintarray[i];
    return elem;
}

REGQUAL ELEMENT *subf_uintarray_shortarray(register ELEMENT *elem,
                                           register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.uintarray[i]=
                            elem1->unit.uintarray[i]-elem2->unit.shortarray[i];
    return elem;
}

REGQUAL ELEMENT *subf_uintarray_ushortarray(register ELEMENT *elem,
                                            register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.uintarray[i]=
                            elem1->unit.uintarray[i]-elem2->unit.ushortarray[i];
    return elem;
}

REGQUAL ELEMENT *subf_uintarray_string(register ELEMENT *elem,
                                       register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.uintarray[i]=
                            elem1->unit.uintarray[i]-elem2->unit.string[i];
    return elem;
}

REGQUAL ELEMENT *subf_string_int(register ELEMENT *elem,
                                 register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    *elem= *elem1;
    for (i=0; i<elem->unit.line.length; i++) elem->unit.string[i]=(UBYTE)
                                                 (elem1->unit.string[i]-elem2->unit.integer);
    return elem;
}

REGQUAL ELEMENT *subf_string_uint(register ELEMENT *elem,
                                  register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    *elem= *elem1;
    for (i=0; i<elem->unit.line.length; i++) elem->unit.string[i]=(UBYTE)
                                                 (elem1->unit.string[i]-elem2->unit.uinteger);
    return elem;
}

REGQUAL ELEMENT *subf_string_string(register ELEMENT *elem,
                                    register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.string[i]=(UBYTE)
                            (elem1->unit.string[i]-elem2->unit.string[i]);
    return elem;
}

MAT_FUNC subf_func[12][12]={
    {subf_float_float,subf_float_fract,subf_float_int,subf_float_uint,
     subf_float_complex},
    {subf_fract_float,subf_fract_fract,subf_fract_int,subf_fract_uint,
     subf_fract_complex},
    {subf_int_float,subf_int_fract,subf_int_int,subf_int_uint,
     subf_int_complex},
    {subf_uint_float,subf_uint_fract,subf_uint_int,subf_uint_uint,
     subf_uint_complex},
    {subf_complex_float,subf_complex_fract,subf_complex_int,subf_complex_uint,
     subf_complex_complex},
    {subf_floatarray_float,subf_floatarray_fract,subf_floatarray_int,
     subf_floatarray_uint,NULL,subf_floatarray_floatarray,
     subf_floatarray_intarray,subf_floatarray_uintarray,
     subf_floatarray_singlearray,subf_floatarray_shortarray,
     subf_floatarray_ushortarray,subf_floatarray_string},
    {NULL,NULL,subf_intarray_int,subf_intarray_uint,NULL,NULL,
     subf_intarray_intarray,subf_intarray_uintarray,NULL,
     subf_intarray_shortarray,subf_intarray_ushortarray,subf_intarray_string},
    {NULL,NULL,subf_uintarray_int,subf_uintarray_uint,NULL,NULL,
     subf_uintarray_intarray,subf_uintarray_uintarray,NULL,
     subf_uintarray_shortarray,subf_uintarray_ushortarray,subf_uintarray_string},
    {NULL}, {NULL}, {NULL},
    {NULL,NULL,subf_string_int,subf_string_uint,NULL,NULL,NULL,NULL,NULL,NULL,
     NULL,subf_string_string}};

REGQUAL ELEMENT *subf(register ELEMENT *elem,register ELEMENT *elem1,
                      register ELEMENT *elem2)
{
    if (elem1->type==ARRAY_TYPE)
        switch (elem2->type) {
        case ARRAY_TYPE: {
            ULONG i,l=min(elem1->unit.line.length,
                          elem2->unit.line.length);
            *elem= *elem1;
            for (i=0; i<l; i++)
                subf(elem->unit.array+i,
                     elem1->unit.array+i,
                     elem2->unit.array+i);
            return elem;
        }
        case FLOATING_TYPE:
        case FRACTION_TYPE:
        case INTEGER_TYPE:
        case UINTEGER_TYPE:
        case COMPLEX_TYPE: {
            ULONG i;
            *elem= *elem1;
            for (i=0; i<elem1->unit.line.length; i++)
                subf(elem->unit.array+i,
                     elem1->unit.array+i,elem2);
            return elem;
        }
        }
    else if (elem1->type>=FLOATING_TYPE&&elem1->type<=STRING_TYPE&&
             elem2->type>=FLOATING_TYPE&&elem2->type<=STRING_TYPE) {
        register MAT_FUNC func=
            subf_func[elem1->type-FLOATING_TYPE][elem2->type-FLOATING_TYPE];
        if (func) return (*func)(elem,elem1,elem2); }
    error(WAKE_INVALID_TYPE);
    return NULL;
}

REGQUAL ELEMENT *mulf_float_float(register ELEMENT *elem,
                                  register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=FLOATING_TYPE;
    elem->unit.floating=elem1->unit.floating*elem2->unit.floating;
    return elem;
}

REGQUAL ELEMENT *mulf_float_fract(register ELEMENT *elem,
                                  register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=FLOATING_TYPE;
    elem->unit.floating=elem1->unit.floating*
        ((double)elem2->unit.fract.num/(double)elem2->unit.fract.den);
    return elem;
}

REGQUAL ELEMENT *mulf_float_int(register ELEMENT *elem,
                                register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=FLOATING_TYPE;
    elem->unit.floating=elem1->unit.floating*(double)elem2->unit.integer;
    return elem;
}

REGQUAL ELEMENT *mulf_float_uint(register ELEMENT *elem,
                                 register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=FLOATING_TYPE;
    elem->unit.floating=elem1->unit.floating*(double)elem2->unit.uinteger;
    return elem;
}

REGQUAL ELEMENT *mulf_float_complex(register ELEMENT *elem,
                                    register ELEMENT *elem1,register ELEMENT *elem2)
{
    float im=(float)elem1->unit.floating*elem2->unit.compl.imag;
    if (im==0.0) {
        elem->type=FLOATING_TYPE;
        elem->unit.floating=elem1->unit.floating*
            (double)elem2->unit.compl.real;
    }
    else {
        elem->type=COMPLEX_TYPE;
        elem->unit.compl.real=(float)elem1->unit.floating*
            elem2->unit.compl.real;
        elem->unit.compl.imag=im;
    }
    return elem;
}

REGQUAL ELEMENT *mulf_fract_float(register ELEMENT *elem,
                                  register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=FLOATING_TYPE;
    elem->unit.floating=(double)elem1->unit.fract.num/
        (double)elem1->unit.fract.den*elem2->unit.floating;
    return elem;
}

REGQUAL ELEMENT *mulf_fract_fract(register ELEMENT *elem,
                                  register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->unit.fract.num=elem1->unit.fract.num*elem2->unit.fract.num;
    elem->unit.fract.den=elem1->unit.fract.den*elem2->unit.fract.den;
    canon(elem);
    return elem;
}

REGQUAL ELEMENT *mulf_fract_int(register ELEMENT *elem,
                                register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->unit.fract.num=elem1->unit.fract.num*elem2->unit.integer;
    elem->unit.fract.den=elem1->unit.fract.den;
    canon(elem);
    return elem;
}

REGQUAL ELEMENT *mulf_fract_uint(register ELEMENT *elem,
                                 register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->unit.fract.num=elem1->unit.fract.num*elem2->unit.uinteger;
    elem->unit.fract.den=elem1->unit.fract.den;
    canon(elem);
    return elem;
}

REGQUAL ELEMENT *mulf_fract_complex(register ELEMENT *elem,
                                    register ELEMENT *elem1,register ELEMENT *elem2)
{
    float f;
    f=(float)elem1->unit.fract.num/(float)elem1->unit.fract.den;
    elem->type=COMPLEX_TYPE;
    elem->unit.compl.real=f*elem2->unit.compl.real;
    elem->unit.compl.imag=f*elem2->unit.compl.imag;
    return elem;
}

REGQUAL ELEMENT *mulf_int_float(register ELEMENT *elem,
                                register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=FLOATING_TYPE;
    elem->unit.floating=(double)elem1->unit.integer*elem2->unit.floating;
    return elem;
}

REGQUAL ELEMENT *mulf_int_fract(register ELEMENT *elem,
                                register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=FRACTION_TYPE;
    elem->unit.fract.num=elem1->unit.integer*elem2->unit.fract.num;
    elem->unit.fract.den=elem2->unit.fract.den;
    canon(elem);
    return elem;
}

REGQUAL ELEMENT *mulf_int_int(register ELEMENT *elem,
                              register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->flag|=INTEGER;
    elem->type=INTEGER_TYPE;
    elem->unit.integer=elem1->unit.integer*elem2->unit.integer;
    return elem;
}

REGQUAL ELEMENT *mulf_int_uint(register ELEMENT *elem,
                               register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->flag|=INTEGER;
    elem->type=INTEGER_TYPE;
    elem->unit.integer=elem1->unit.integer*elem2->unit.uinteger;
    return elem;
}

REGQUAL ELEMENT *mulf_int_complex(register ELEMENT *elem,
                                  register ELEMENT *elem1,register ELEMENT *elem2)
{
    float im=(float)elem1->unit.integer*elem2->unit.compl.imag;
    if (im==0.0) {
        elem->type=FLOATING_TYPE;
        elem->unit.floating=(double)elem1->unit.integer*
            (double)elem2->unit.compl.real;
    }
    else {
        elem->type=COMPLEX_TYPE;
        elem->unit.compl.real=(float)elem1->unit.integer*
            elem2->unit.compl.real;
        elem->unit.compl.imag=im;
    }
    return elem;
}

REGQUAL ELEMENT *mulf_uint_float(register ELEMENT *elem,
                                 register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=FLOATING_TYPE;
    elem->unit.floating=(double)elem1->unit.uinteger*elem2->unit.floating;
    return elem;
}

REGQUAL ELEMENT *mulf_uint_fract(register ELEMENT *elem,
                                 register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=FRACTION_TYPE;
    elem->unit.fract.num=elem1->unit.uinteger*elem2->unit.fract.num;
    elem->unit.fract.den=elem2->unit.fract.den;
    canon(elem);
    return elem;
}

REGQUAL ELEMENT *mulf_uint_int(register ELEMENT *elem,
                               register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->flag|=INTEGER;
    elem->type=INTEGER_TYPE;
    elem->unit.integer=elem1->unit.uinteger*elem2->unit.integer;
    return elem;
}

REGQUAL ELEMENT *mulf_uint_uint(register ELEMENT *elem,
                                register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->flag|=INTEGER;
    elem->type=UINTEGER_TYPE;
    elem->unit.uinteger=elem1->unit.uinteger*elem2->unit.uinteger;
    return elem;
}

REGQUAL ELEMENT *mulf_uint_complex(register ELEMENT *elem,
                                   register ELEMENT *elem1,register ELEMENT *elem2)
{
    float im=(float)elem1->unit.integer*elem2->unit.compl.imag;
    if (im==0.0) {
        elem->type=FLOATING_TYPE;
        elem->unit.floating=(double)elem1->unit.uinteger*
            (double)elem2->unit.compl.real;
    }
    else {
        elem->type=COMPLEX_TYPE;
        elem->unit.compl.real=(float)elem1->unit.uinteger*
            elem2->unit.compl.real;
        elem->unit.compl.imag=im;
    }
    return elem;
}

REGQUAL ELEMENT *mulf_complex_float(register ELEMENT *elem,
                                    register ELEMENT *elem1,register ELEMENT *elem2)
{
    float im=(float)(elem1->unit.compl.imag*elem2->unit.floating);
    if (im==0.0) {
        elem->type=FLOATING_TYPE;
        elem->unit.floating=(double)elem1->unit.compl.real*
            elem2->unit.floating;
    }
    else {
        elem->type=COMPLEX_TYPE;
        elem->unit.compl.real=elem1->unit.compl.real*
            (float)elem2->unit.floating;
        elem->unit.compl.imag=im;
    }
    return elem;
}

REGQUAL ELEMENT *mulf_complex_fract(register ELEMENT *elem,
                                    register ELEMENT *elem1,register ELEMENT *elem2)
{
    float f;
    f=(float)elem2->unit.fract.num/(float)elem2->unit.fract.den;
    elem->type=COMPLEX_TYPE;
    elem->unit.compl.real=elem1->unit.compl.real*f;
    elem->unit.compl.imag=elem1->unit.compl.imag*f;
    return elem;
}

REGQUAL ELEMENT *mulf_complex_int(register ELEMENT *elem,
                                  register ELEMENT *elem1,register ELEMENT *elem2)
{
    float im=(float)(elem1->unit.compl.imag*elem2->unit.floating);
    if (im==0.0) {
        elem->type=FLOATING_TYPE;
        elem->unit.floating=(double)elem1->unit.compl.real*
            (double)elem2->unit.integer;
    }
    else {
        elem->type=COMPLEX_TYPE;
        elem->unit.compl.real=elem1->unit.compl.real*
            (float)elem2->unit.integer;
        elem->unit.compl.imag=im;
    }
    return elem;
}

REGQUAL ELEMENT *mulf_complex_uint(register ELEMENT *elem,
                                   register ELEMENT *elem1,register ELEMENT *elem2)
{
    float im=(float)(elem1->unit.compl.imag*elem2->unit.floating);
    if (im==0.0) {
        elem->type=FLOATING_TYPE;
        elem->unit.floating=(double)elem1->unit.compl.real*
            (double)elem2->unit.uinteger;
    }
    else {
        elem->type=COMPLEX_TYPE;
        elem->unit.compl.real=elem1->unit.compl.real*
            (float)elem2->unit.uinteger;
        elem->unit.compl.imag=im;
    }
    return elem;
}

REGQUAL ELEMENT *mulf_complex_complex(register ELEMENT *elem,
                                      register ELEMENT *elem1,register ELEMENT *elem2)
{
    float im=elem1->unit.compl.real*elem2->unit.compl.imag+
        elem1->unit.compl.imag*elem2->unit.compl.real;
    if (im==0.0) {
        elem->type=FLOATING_TYPE;
        elem->unit.floating=(double)elem1->unit.compl.real*
            (double)elem2->unit.compl.real-
            (double)elem1->unit.compl.imag*
            (double)elem2->unit.compl.imag;
    }
    else {
        elem->type=COMPLEX_TYPE;
        elem->unit.compl.real=elem1->unit.compl.real*
            elem2->unit.compl.real-elem1->unit.compl.imag*
            elem2->unit.compl.imag;
        elem->unit.compl.imag=im;
    }
    return elem;
}

REGQUAL ELEMENT *mulf_floatarray_float(register ELEMENT *elem,
                                       register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.floatarray[i]=
                                                  elem1->unit.floatarray[i]*elem2->unit.floating;
    return elem;
}

REGQUAL ELEMENT *mulf_floatarray_fract(register ELEMENT *elem,
                                       register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    double d=(double)elem2->unit.fract.num/(double)elem2->unit.fract.den;
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.floatarray[i]=
                                                  elem1->unit.floatarray[i]*d;
    return elem;
}

REGQUAL ELEMENT *mulf_floatarray_int(register ELEMENT *elem,
                                     register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.floatarray[i]=
                                                  elem1->unit.floatarray[i]*(double)elem2->unit.integer;
    return elem;
}

REGQUAL ELEMENT *mulf_floatarray_uint(register ELEMENT *elem,
                                      register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.floatarray[i]=
                                                  elem1->unit.floatarray[i]*(double)elem2->unit.uinteger;
    return elem;
}

REGQUAL ELEMENT *mulf_floatarray_floatarray(register ELEMENT *elem,
                                            register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.floatarray[i]=
                            elem1->unit.floatarray[i]*elem2->unit.floatarray[i];
    return elem;
}

REGQUAL ELEMENT *mulf_floatarray_intarray(register ELEMENT *elem,
                                          register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.floatarray[i]=
                            elem1->unit.floatarray[i]*(double)elem2->unit.intarray[i];
    return elem;
}

REGQUAL ELEMENT *mulf_floatarray_uintarray(register ELEMENT *elem,
                                           register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.floatarray[i]=
                            elem1->unit.floatarray[i]*(double)elem2->unit.uintarray[i];
    return elem;
}

REGQUAL ELEMENT *mulf_floatarray_singlearray(register ELEMENT *elem,
                                             register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.floatarray[i]=
                            elem1->unit.floatarray[i]*(double)elem2->unit.singlearray[i];
    return elem;
}

REGQUAL ELEMENT *mulf_floatarray_shortarray(register ELEMENT *elem,
                                            register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.floatarray[i]=
                            elem1->unit.floatarray[i]*(double)elem2->unit.shortarray[i];
    return elem;
}

REGQUAL ELEMENT *mulf_floatarray_ushortarray(register ELEMENT *elem,
                                             register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.floatarray[i]=
                            elem1->unit.floatarray[i]*(double)elem2->unit.ushortarray[i];
    return elem;
}

REGQUAL ELEMENT *mulf_floatarray_string(register ELEMENT *elem,
                                        register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.floatarray[i]=
                            elem1->unit.floatarray[i]*(double)elem2->unit.string[i];
    return elem;
}

REGQUAL ELEMENT *mulf_intarray_int(register ELEMENT *elem,
                                   register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.intarray[i]=
                                                  elem1->unit.intarray[i]*elem2->unit.integer;
    return elem;
}

REGQUAL ELEMENT *mulf_intarray_uint(register ELEMENT *elem,
                                    register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.intarray[i]=
                                                  elem1->unit.intarray[i]*elem2->unit.uinteger;
    return elem;
}

REGQUAL ELEMENT *mulf_intarray_intarray(register ELEMENT *elem,
                                        register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.intarray[i]=
                            elem1->unit.intarray[i]*elem2->unit.intarray[i];
    return elem;
}

REGQUAL ELEMENT *mulf_intarray_uintarray(register ELEMENT *elem,
                                         register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.intarray[i]=
                            elem1->unit.intarray[i]*elem2->unit.uintarray[i];
    return elem;
}

REGQUAL ELEMENT *mulf_intarray_shortarray(register ELEMENT *elem,
                                          register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.intarray[i]=
                            elem1->unit.intarray[i]*elem2->unit.shortarray[i];
    return elem;
}

REGQUAL ELEMENT *mulf_intarray_ushortarray(register ELEMENT *elem,
                                           register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.intarray[i]=
                            elem1->unit.intarray[i]*elem2->unit.ushortarray[i];
    return elem;
}

REGQUAL ELEMENT *mulf_intarray_string(register ELEMENT *elem,
                                      register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.intarray[i]=
                            elem1->unit.intarray[i]*elem2->unit.string[i];
    return elem;
}

REGQUAL ELEMENT *mulf_uintarray_int(register ELEMENT *elem,
                                    register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.uintarray[i]=
                                                  elem1->unit.uintarray[i]*elem2->unit.integer;
    return elem;
}

REGQUAL ELEMENT *mulf_uintarray_uint(register ELEMENT *elem,
                                     register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.uintarray[i]=
                                                  elem1->unit.uintarray[i]*elem2->unit.uinteger;
    return elem;
}

REGQUAL ELEMENT *mulf_uintarray_intarray(register ELEMENT *elem,
                                         register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.uintarray[i]=
                            elem1->unit.uintarray[i]*elem2->unit.intarray[i];
    return elem;
}

REGQUAL ELEMENT *mulf_uintarray_uintarray(register ELEMENT *elem,
                                          register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.uintarray[i]=
                            elem1->unit.uintarray[i]*elem2->unit.uintarray[i];
    return elem;
}

REGQUAL ELEMENT *mulf_uintarray_shortarray(register ELEMENT *elem,
                                           register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.uintarray[i]=
                            elem1->unit.uintarray[i]*elem2->unit.shortarray[i];
    return elem;
}

REGQUAL ELEMENT *mulf_uintarray_ushortarray(register ELEMENT *elem,
                                            register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.uintarray[i]=
                            elem1->unit.uintarray[i]*elem2->unit.ushortarray[i];
    return elem;
}

REGQUAL ELEMENT *mulf_uintarray_string(register ELEMENT *elem,
                                       register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.uintarray[i]=
                            elem1->unit.uintarray[i]*elem2->unit.string[i];
    return elem;
}

REGQUAL ELEMENT *mulf_singlearray_float(register ELEMENT *elem,
                                       register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.singlearray[i]=
                                                  elem1->unit.singlearray[i]*elem2->unit.floating;
    return elem;
}

REGQUAL ELEMENT *mulf_singlearray_fract(register ELEMENT *elem,
                                       register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    float f=(float)elem2->unit.fract.num/(float)elem2->unit.fract.den;
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.singlearray[i]=
                                                  elem1->unit.singlearray[i]*f;
    return elem;
}

REGQUAL ELEMENT *mulf_singlearray_int(register ELEMENT *elem,
                                     register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.singlearray[i]=
                                                  elem1->unit.singlearray[i]*(float)elem2->unit.integer;
    return elem;
}

REGQUAL ELEMENT *mulf_singlearray_uint(register ELEMENT *elem,
                                      register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.singlearray[i]=
                                                  elem1->unit.singlearray[i]*(float)elem2->unit.uinteger;
    return elem;
}

REGQUAL ELEMENT *mulf_singlearray_floatarray(register ELEMENT *elem,
                                            register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.singlearray[i]=
                            elem1->unit.singlearray[i]*elem2->unit.floatarray[i];
    return elem;
}

REGQUAL ELEMENT *mulf_singlearray_intarray(register ELEMENT *elem,
                                          register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.singlearray[i]=
                            elem1->unit.singlearray[i]*(float)elem2->unit.intarray[i];
    return elem;
}

REGQUAL ELEMENT *mulf_singlearray_uintarray(register ELEMENT *elem,
                                           register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.singlearray[i]=
                            elem1->unit.singlearray[i]*(float)elem2->unit.uintarray[i];
    return elem;
}

REGQUAL ELEMENT *mulf_singlearray_singlearray(register ELEMENT *elem,
                                             register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.singlearray[i]=
                            elem1->unit.singlearray[i]*(float)elem2->unit.singlearray[i];
    return elem;
}

REGQUAL ELEMENT *mulf_singlearray_shortarray(register ELEMENT *elem,
                                            register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.singlearray[i]=
                            elem1->unit.singlearray[i]*(float)elem2->unit.shortarray[i];
    return elem;
}

REGQUAL ELEMENT *mulf_singlearray_ushortarray(register ELEMENT *elem,
                                             register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.singlearray[i]=
                            elem1->unit.singlearray[i]*(float)elem2->unit.ushortarray[i];
    return elem;
}

REGQUAL ELEMENT *mulf_singlearray_string(register ELEMENT *elem,
                                        register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.singlearray[i]=
                            elem1->unit.singlearray[i]*(float)elem2->unit.string[i];
    return elem;
}

REGQUAL ELEMENT *mulf_string_int(register ELEMENT *elem,
                                 register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    *elem= *elem1;
    for (i=0; i<elem->unit.line.length; i++) elem->unit.string[i]=(UBYTE)
                                                 (elem1->unit.string[i]*elem2->unit.integer);
    return elem;
}

REGQUAL ELEMENT *mulf_string_uint(register ELEMENT *elem,
                                  register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    *elem= *elem1;
    for (i=0; i<elem->unit.line.length; i++) elem->unit.string[i]=(UBYTE)
                                                 (elem1->unit.string[i]*elem2->unit.uinteger);
    return elem;
}

REGQUAL ELEMENT *mulf_string_string(register ELEMENT *elem,
                                    register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++) elem->unit.string[i]=(UBYTE)
                            (elem1->unit.string[i]*elem2->unit.string[i]);
    return elem;
}

MAT_FUNC mulf_func[12][12]={
    {mulf_float_float,mulf_float_fract,mulf_float_int,mulf_float_uint,
     mulf_float_complex},
    {mulf_fract_float,mulf_fract_fract,mulf_fract_int,mulf_fract_uint,
     mulf_fract_complex},
    {mulf_int_float,mulf_int_fract,mulf_int_int,mulf_int_uint,
     mulf_int_complex},
    {mulf_uint_float,mulf_uint_fract,mulf_uint_int,mulf_uint_uint,
     mulf_uint_complex},
    {mulf_complex_float,mulf_complex_fract,mulf_complex_int,mulf_complex_uint,
     mulf_complex_complex},
    {mulf_floatarray_float,mulf_floatarray_fract,mulf_floatarray_int,
     mulf_floatarray_uint,NULL,mulf_floatarray_floatarray,
     mulf_floatarray_intarray,mulf_floatarray_uintarray,
     mulf_floatarray_singlearray,mulf_floatarray_shortarray,
     mulf_floatarray_ushortarray,mulf_floatarray_string},
    {NULL,NULL,mulf_intarray_int,mulf_intarray_uint,NULL,NULL,
     mulf_intarray_intarray,mulf_intarray_uintarray,NULL,
     mulf_intarray_shortarray,mulf_intarray_ushortarray,mulf_intarray_string},
    {NULL,NULL,mulf_uintarray_int,mulf_uintarray_uint,NULL,NULL,
     mulf_uintarray_intarray,mulf_uintarray_uintarray,NULL,
     mulf_uintarray_shortarray,mulf_uintarray_ushortarray,mulf_uintarray_string},
    {mulf_singlearray_float,mulf_singlearray_fract,mulf_singlearray_int,
     mulf_singlearray_uint,NULL,mulf_singlearray_floatarray,
     mulf_singlearray_intarray,mulf_singlearray_uintarray,
     mulf_singlearray_singlearray,mulf_singlearray_shortarray,
     mulf_singlearray_ushortarray,mulf_singlearray_string},
    {NULL},
    {NULL},
    {NULL,NULL,mulf_string_int,mulf_string_uint,NULL,NULL,NULL,NULL,NULL,NULL,
     NULL,mulf_string_string}};

REGQUAL ELEMENT *mulf(register ELEMENT *elem,register ELEMENT *elem1,
                      register ELEMENT *elem2)
{
    if (elem1->type==ARRAY_TYPE)
        switch (elem2->type) {
        case ARRAY_TYPE: {
            ULONG i,l=min(elem1->unit.line.length,
                          elem2->unit.line.length);
            *elem= *elem1;
            for (i=0; i<l; i++)
                mulf(elem->unit.array+i,
                     elem1->unit.array+i,
                     elem2->unit.array+i);
            return elem;
        }
        case FLOATING_TYPE:
        case FRACTION_TYPE:
        case INTEGER_TYPE:
        case UINTEGER_TYPE:
        case COMPLEX_TYPE: {
            ULONG i;
            *elem= *elem1;
            for (i=0; i<elem1->unit.line.length; i++)
                mulf(elem->unit.array+i,
                     elem1->unit.array+i,elem2);
            return elem;
        }
        }
    else if (elem1->type>=FLOATING_TYPE&&elem1->type<=STRING_TYPE&&
             elem2->type>=FLOATING_TYPE&&elem2->type<=STRING_TYPE) {
        register MAT_FUNC func=
            mulf_func[elem1->type-FLOATING_TYPE][elem2->type-FLOATING_TYPE];
        if (func) return (*func)(elem,elem1,elem2); }
    error(WAKE_INVALID_TYPE);
    return NULL;
}

int init_mat(void)
{
    ENTRY *entry;
    if (init_mat2()||
        !(entry=enter("PI",system_nomen))) return -1;
    entry->flag=BOUND|DETERMINED;
    entry->element.flag=NUMBER;
    entry->element.type=FLOATING_TYPE;
    entry->element.unit.floating=PI;
    return 0;
}
