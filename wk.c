
/* wk.c */

#include "wake.h"
#include "wk_gra.h"
#ifndef VAXC
#include "wk_seq.h"
#endif
#if defined(__APPLE__) || defined(__linux__)
#include <termios.h>
#endif
#ifdef XLIB
#include <X11/keysym.h>
#endif
#ifdef AMIGA
#include <fcntl.h>
#elif defined(__GNUC__)
#include <unistd.h>
#elif defined(_MSC_VER)
#include <io.h>
#include <direct.h>
#define getcwd _getcwd
#define chdir _chdir
#elif defined(__TURBOC__)
#include <io.h>
#include <dir.h>
#elif defined(VAXC)
#include <ssdef.h>
#include <signal.h>
#endif
#include <stdarg.h>
#if defined(VGALIB) || defined(_WINDOWS) || defined(XLIB)
#define BUFF_SIZE 256
#define HIST_SIZE 16

char buffer[BUFF_SIZE];
char out_bf[BUFF_SIZE];
char *hist[HIST_SIZE];
int bpos, cpos, epos, len;
int column, row, columns, rows;
int xchar, ychar;
int hidx, hnum;
int ungotc;
char cungot;
#ifdef _WINDOWS
#ifndef _WIN32
#define GetCurrentThread GetCurrentTask
#endif
HANDLE instance;
RECT rect;
HWND hwnd;
HDC hdc;
UBYTE MQ *cstring;
ULONG cindex, clength;
#elif defined(XLIB)
char focus;
int wxsize, wysize;
XFontStruct *ffontp;
Window wnd;
GC gcx;

int do_event(XEvent *event);
#endif
#endif

extern void wk_perform(void);
extern void wk_run(void);
extern int seq_brk(void);

UBYTE oflag;
char *wk_name="WK";
char *cwd;

void handle_name(char *name)
{
    stack(1);
    wsp->flag=0;
    wsp->type=STRING_TYPE;
    wsp->unit.sstring=name;
    wsp->unit.line.length=strlen(name);
    wsp++;
    wk_run();
}

void handle_option(char *option)
{
    while (*option) {
        switch (toupper(*option)) {
        case 'D':
            chdir(option+1);
            return;
        case 'R':
            oflag|=REMOTE;
            break;
        case 'S':
            oflag|=SUPPRESS;
            break;
        case 'T':
            oflag|=TEXTONLY;
            break;
#ifdef _WINDOWS
        case 'I':
            midiindev=atoi(option+1);
            return;
        case 'O':
            midioutdev[0]=atoi(option+1);
            return;
        case 'E':
            midioutdev[1]=atoi(option+1);
            return;
#elif defined(_MSDOS)
        case 'G':
            graphdriver=atoi(option+1);
            return;
        case 'M':
            graphmode=atoi(option+1);
            return;
        case 'P':
            graphpath=option+1;
            return;
#elif defined(__linux__) || defined(__APPLE__)
        case 'I':
            midiindev=option+1;
            return;
        case 'O':
            midioutdev[0]=option+1;
            return;
        case 'E':
            midioutdev[1]=option+1;
            return;
#endif
        }
        option++; }
}

void handle_params(char *params, int options)
{
    int i;
    char e;
    while (*(params+=strspn(params, whites))&&*params!='$') {
        i=strcspn(params, whites);
        e=params[i];
        params[i]='\0';
        if (*params=='-') {
            if (options) handle_option(params+1);
        }
        else if (!options) handle_name(params);
        params[i]=e;
        params+=strcspn(params, whites);
    }
    if (*params=='$'&&*++params&&!options) {
        stack(1);
        wsp->flag=INDEXED;
        wsp->type=STRING_TYPE;
        wsp->unit.sstring=params;
        wsp->unit.line.length=strlen(params);
        wsp++;
        wk_perform();
    }
}

#if defined(__APPLE__) || defined(__linux__) || defined(_WINDOWS)
long _CDECL wftell(FILE *f)
{
    if (f!=stdin) return ftell(f);
    return (long)cpos;
}

int _CDECL wungetc(int c, FILE *f)
{
    if (f!=stdin) return ungetc(c, f);
    cungot=1;
    ungotc=c;
    return c;
}
#endif

#if defined(__APPLE__) || defined(__linux__)
int _CDECL vfgetc(FILE *f)
{
    if (f!=stdin) return fgetc(f);
    if (cungot) {
        cungot=0;
        return ungotc;
    }
    if (cpos>=epos) {
        int c;
        cpos=epos=0;
        while ((c=getchar())!=10) {
            switch (c) {
            case 27:
                if (getchar()==91)
                    switch (c=getchar()) {
                    case 65:
                        if (hnum) {
                            if (hidx) hidx--;
                            else hidx=hnum-1;
                            cpos=epos=strlen(hist[hidx]);
                            memcpy(buffer, hist[hidx], epos);
                            printf("\n%s", hist[hidx]);
                        }
                        break;
                    case 66:
                        if (hnum) {
                            if (hidx<hnum-1) hidx++;
                            else hidx=0;
                            cpos=epos=strlen(hist[hidx]);
                            memcpy(buffer, hist[hidx], epos);
                            printf("\n%s", hist[hidx]);
                        }
                        break;
                    case 67:
                        if (cpos<epos) {
                            cpos++;
                            putchar(27);
                            putchar(91);
                            putchar(67);
                        }
                        break;
                    case 68:
                        if (cpos>0) {
                            cpos--;
                            putchar(27);
                            putchar(91);
                            putchar(68);
                        }
                        break; }
                break;
            case 127:
                if (cpos>0) {
                    int l;
                    cpos--;
                    if ((l=epos-cpos)>1)
                        memmove(buffer+cpos, buffer+cpos+1, l-1);
                    epos--;
                    putchar(27);
                    putchar(91);
                    putchar(68);
                    putchar(27);
                    putchar(91);
                    putchar(80);
                }
                break;
            default:
                if (epos==BUFF_SIZE-1||cpos==BUFF_SIZE-1)
                    break;
                if (epos>cpos)
                    memmove(buffer+cpos+1, buffer+cpos, epos-cpos);
                buffer[cpos++]=c;
                epos++;
                putchar(27);
                putchar(91);
                putchar(64);
                putchar(c); }
        }
        if (epos) {
            if (hnum<HIST_SIZE) hidx=hnum;
            if (hist[hidx]) memfree(hist[hidx]);
            hist[hidx]=(char *)memalloc(epos+1);
            memcpy(hist[hidx], buffer, epos);
            hist[hidx][epos]='\0';
            if (++hidx>hnum) hnum=hidx;
            if (hidx==HIST_SIZE) hidx=0;
        }
        buffer[epos++]='\n';
        cpos=0;
        putchar('\n');
    }
    return (int)buffer[cpos++];
}

char _CDECL *vfgets(char *s, int n, FILE *f)
{
    int c;
    if (f!=stdin) return fgets(s, n, f);
    while (n>1) {
        c=wfgetc(stdin);
        if (c=='\n'||c==EOF) break;
        *s++=c;
        n--;
    }
    *s='\0';
    return s;
}
#endif

#if defined(_WINDOWS) || defined(XLIB)
#ifdef XLIB
void toggle_caret(void)
{
    if (!focus) return;
    XSetFunction(display, gcx, GXinvert);
    XFillRectangle(display, wnd, gcx, column*xchar, row*ychar, xchar, ychar);
    XSetFunction(display, gcx, GXcopy);
}
#endif

void print(char *string)
{
    int m, n, p=0;
#ifdef _WINDOWS
    if (columns<=0||rows<=0) return;
#else
    if (columns<=0||rows<=0) return;
#endif
    while (string[p]) {
        n=strcspn(string+p, "\t\n");
        while (n) {
            m=min(columns-column, n);
#ifdef _WINDOWS
            TextOut(hdc, column*xchar, row*ychar, string+p, m);
#else
            XDrawString(display, wnd, gcx, column*xchar,
                        row*ychar+ffontp->max_bounds.ascent, string+p, m);
#endif
            column+=m;
            if (column==columns) {
                column=0;
                if (row==rows-1)
#ifdef _WINDOWS
                    ScrollDC(hdc, 0, -ychar, &rect, &rect, (HRGN)NULL,
                             (RECT *)NULL);
#else
                scroll_area(display, wnd, gcx,
                            0, -ychar, 0, 0, wxsize, wysize);
#endif
                else row++;
            }
            p+=m;
            n-=m;
        }
        switch (string[p]) {
        case '\t':
            n=8-column%8;
            while (n) {
                m=min(columns-column, n);
                column+=m;
                if (column==columns) {
                    column=0;
                    if (row==rows-1)
#ifdef _WINDOWS
                        ScrollDC(hdc, 0, -ychar,
                                 &rect, &rect, (HRGN)NULL,
                                 (RECT *)NULL);
#else
                    scroll_area(display, wnd, gcx,
                                0, -ychar, 0, 0, wxsize, wysize);
#endif
                    else row++;
                }
                n-=m;
            }
            p++;
            break;
        case '\n':
            column=0;
            if (row==rows-1)
#ifdef _WINDOWS
                ScrollDC(hdc, 0, -ychar, &rect, &rect, (HRGN)NULL,
                         (RECT *)NULL);
#else
            scroll_area(display, wnd, gcx,
                        0, -ychar, 0, 0, wxsize, wysize);
#endif
            else row++;
            p++;
            break;
        }
    }
#ifdef _WINDOWS
    SetCaretPos(column*xchar, row*ychar);
#endif
}

void wprint(char *string)
{
#ifdef _WINDOWS
    MSG msg;
    HideCaret(hwnd);
    print(string);
    ShowCaret(hwnd);
    if (PeekMessage(&msg, (HWND)NULL, 0, 0,PM_REMOVE)) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
#else
    XEvent event;
    toggle_caret();
    print(string);
    toggle_caret();
    if (XCheckWindowEvent(display, wnd, KeyPressMask, &event))
        do_event(&event);
#endif
}

int _CDECL wfgetc(FILE *f)
{
#ifdef _WINDOWS
    MSG msg;
#else
    XEvent event;
#endif
    if (f!=stdin) return fgetc(f);
#if defined(__APPLE__) || defined(__linux__)
    if (oflag&TEXTONLY) return vfgetc(f);
#endif
    if (cungot) {
        cungot=0;
        return ungotc;
    }
#ifdef _WINDOWS
    while (cpos>=epos) {
        if (GetMessage(&msg, (HWND)NULL, 0, 0) != TRUE) return EOF;
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
#else
    while (cpos>=epos) {
        XNextEvent(display, &event);
        if (do_event(&event)) return EOF;
    }
#endif
    return (int)buffer[cpos++];
}

char* _CDECL wfgets(char *s, int n, FILE *f)
{
    int c;
    if (f!=stdin) return fgets(s, n, f);
#if defined(__APPLE__) || defined(__linux__)
    if (oflag&TEXTONLY) vfgets(s, n, f);
#endif
    while (n>1) {
        c=wfgetc(stdin);
        if (c=='\n'||c==EOF) break;
        *s++=(char)c;
        n--;
    }
    *s='\0';
    return s;
}

int _CDECL wfputc(int c, FILE *f)
{
    if (f!=stdout) return fputc(c, f);
#if defined(__APPLE__) || defined(__linux__)
    if (oflag&TEXTONLY) return fputc(c, f);
#endif
    *out_bf=(char)c;
    out_bf[1]=0;
    wprint(out_bf);
    return c;
}

int _CDECL wfputs(char *s, FILE *f)
{
    int l;
    if (f!=stdout) return fputs(s, f);
#if defined(__APPLE__) || defined(__linux__)
    if (oflag&TEXTONLY) return fputs(s, f);
#endif
    l=strlen(s);
    if (l>=BUFF_SIZE) return EOF;
    memcpy(out_bf, s, l+1);
    wprint(out_bf);
    return l;
}

int _CDECL wfprintf(FILE *f, char *frm, ...)
{
    int l;
    va_list vl;
    va_start(vl, frm);
#if defined(__APPLE__) || defined(__linux__)
    if (oflag&TEXTONLY) return vfprintf(f, frm, vl);
#endif
    if (f!=stdout) return vfprintf(f, frm, vl);
    l=vsprintf(out_bf, frm, vl);
    va_end(vl);
    wprint(out_bf);
    return l;
}

void set_pos(int delta)
{
    if (delta<0) {
        if (bpos>= -delta) {
            bpos+=delta;
            if (-delta>column) {
                delta+=column;
                if (-delta>row*columns) {
#ifdef _WINDOWS
                    ScrollWindow(hwnd, 0,
                                 (-delta+columns-1)/columns*ychar,
                                 (LPRECT)NULL, &rect);
#else
                    scroll_area(display, wnd, gcx,
                                0, (-delta+columns-1)/columns*ychar,
                                0, 0, wxsize, wysize);
#endif
                    row=0;
                }
                else row-=(-delta+columns-1)/columns;
                column=(delta+columns)%columns;
            }
            else column+=delta;
#ifdef _WINDOWS
            SetCaretPos(column*xchar, row*ychar);
#endif
        }
        return;
    }
    if (bpos+delta<=len) {
        bpos+=delta;
        if (delta>=columns-column) {
            delta-=columns-column;
            if (delta+1>(rows-row-1)*columns) {
#ifdef _WINDOWS
                ScrollWindow(hwnd, 0,
                             -(delta+columns)/columns*ychar,
                             (LPRECT)NULL, &rect);
#else
                scroll_area(display, wnd, gcx,
                            0, -(delta+columns)/columns*ychar, 0, 0,
                            wxsize, wysize);
#endif
                row=rows-1;
            }
            else row+=(delta+columns)/columns;
            column=delta%columns;
        }
        else column+=delta;
#ifdef _WINDOWS
        SetCaretPos(column*xchar, row*ychar);
#endif
    }
}

void show_hist(void)
{
    int s=bpos;
#ifdef _WINDOWS
    HideCaret(hwnd);
#else
    toggle_caret();
#endif
    set_pos(-bpos);
#ifdef _WINDOWS
    PatBlt(hdc, 0, row*ychar, columns*xchar, (rows-row)*ychar, WHITENESS);
#else
    XClearArea(display, wnd, 0, row*ychar, columns*xchar,
               (rows-row)*ychar, FALSE);
#endif
    bpos=len=strlen(hist[hidx]);
    strcpy(buffer, hist[hidx]);
    print(buffer);
    set_pos(s-bpos);
#ifdef _WINDOWS
    ShowCaret(hwnd);
#else
    toggle_caret();
#endif
}
#endif

#ifdef _WINDOWS
LPSTR message(DWORD n)
{
    LPSTR msg = NULL;
    FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,  NULL, n,
                  MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&msg, 0, NULL);
    return msg;
}

void report(LPCSTR msg)
{
    MessageBox((HWND)NULL, msg, "Startup error", MB_OK | MB_APPLMODAL);
}

LONG EXPORT FAR PASCAL WkProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    if (!handle_midiin(message, lParam)) return 0;
    switch (message) {
    case WM_CREATE: {
        TEXTMETRIC tm;
        hdc=GetDC(hwnd);
        SelectObject(hdc, GetStockObject(SYSTEM_FIXED_FONT));
        GetTextMetrics(hdc, &tm);
        xchar=tm.tmAveCharWidth;
        ychar=tm.tmHeight;
        return 0;
    }
    case WM_SIZE:
        GetClientRect(hwnd, &rect);
        bpos=column=row=0;
        columns=rect.right/xchar-1;
        rows=rect.bottom/ychar-1;
        return 0;
    case WM_KEYDOWN: {
        int t=LOWORD(lParam);
        switch (wParam) {
        case VK_RETURN:
            HideCaret(hwnd);
            if (len) {
                if (hnum<HIST_SIZE) hidx=hnum;
                if (hist[hidx]) memfree(hist[hidx]);
                hist[hidx]=(char *)memalloc(len+1);
                memcpy(hist[hidx], buffer, len);
                hist[hidx][len]='\0';
                if (++hidx>hnum) hnum=hidx;
                if (hidx==HIST_SIZE) hidx=0;
            }
            buffer[len]='\n';
            epos=len+1;
            set_pos(len-bpos);
            column=bpos=cpos=len=0;
            while (t--)
                if (row==rows-1) ScrollDC(hdc, 0, -ychar, &rect, &rect,
                                          (HRGN)NULL, (RECT *)NULL);
                else row++;
            SetCaretPos(column*xchar, row*ychar);
            ShowCaret(hwnd);
            return 0;
        case VK_BACK:
            while (t--)
                if (bpos>0) {
                    set_pos(-1);
                    SendMessage(hwnd, WM_KEYDOWN, VK_DELETE, 1L);
                }
            return 0;
        case VK_LEFT:
            HideCaret(hwnd);
            if (GetKeyState(VK_SHIFT)&0x8000) {
                int d=0;
                while (t--) {
                    while (bpos+d>0&&strchr(whites,
                                            buffer[bpos+d-1])) d--;
                    while (bpos+d>0&&!strchr(whites,
                                             buffer[bpos+d-1])) d--;
                }
                set_pos(d);
            }
            else if (GetKeyState(VK_CONTROL)&0x8000)
                set_pos(-bpos);
            else set_pos(-t);
            ShowCaret(hwnd);
            return 0;
        case VK_RIGHT:
            HideCaret(hwnd);
            if (GetKeyState(VK_SHIFT)&0x8000) {
                int d=0;
                while (t--) {
                    while (bpos+d<len&&!strchr(whites,
                                               buffer[bpos+d])) d++;
                    while (bpos+d<len&&strchr(whites,
                                              buffer[bpos+d])) d++;
                }
                set_pos(d);
            }
            else if (GetKeyState(VK_CONTROL)&0x8000)
                set_pos(len-bpos);
            else set_pos(t);
            ShowCaret(hwnd);
            return 0;
        case VK_UP:
            if (hnum) {
                if (hidx) hidx--;
                else hidx=hnum-1;
                show_hist();
            }
            return 0;
        case VK_HOME:
            HideCaret(hwnd);
            set_pos(-bpos);
            ShowCaret(hwnd);
            return 0;
        case VK_END:
            HideCaret(hwnd);
            set_pos(len-bpos);
            ShowCaret(hwnd);
            return 0;
        case VK_DOWN:
            if (hnum) {
                if (hidx<hnum-1) hidx++;
                else hidx=0;
                show_hist();
            }
            return 0;
        case VK_INSERT:
            if (hnum>1) {
                int i=hidx, n=hnum;
                while (n--) {
                    if (i) i--;
                    else i=hnum-1;
                    if (!strncmp(buffer, hist[i], bpos)) {
                        hidx=i;
                        show_hist();
                        return 0;
                    }
                }
            }
            return 0;
        case VK_DELETE:
            if (GetKeyState(VK_SHIFT)&0x8000) {
                if (bpos<len&&buffer[bpos]) {
                    HideCaret(hwnd);
                    PatBlt(hdc, column*xchar, row*ychar,
                           columns*xchar-column*xchar, ychar,
                           WHITENESS);
                    if (row<rows-1)
                        PatBlt(hdc, 0, (row+1)*ychar,
                               columns*xchar, (rows-row-1)*ychar,
                               WHITENESS);
                    buffer[bpos]='\0';
                    len=bpos;
                    ShowCaret(hwnd);
                }
                return 0;
            }
            while (t--)
                if (bpos<len) {
                    RECT r;
                    int l, n, p, s;
                    if ((l=len-bpos)>1)
                        memmove(buffer+bpos, buffer+bpos+1, l-1);
                    len--;
                    p=bpos;
                    n=columns-column;
                    r.left=column*xchar;
                    r.top=row*ychar;
                    r.right=rect.right;
                    s=rows*ychar;
                    HideCaret(hwnd);
                    while (l>0) {
                        r.bottom=r.top+ychar;
                        ScrollDC(hdc, -xchar, 0, &r, &r,
                                 (HRGN)NULL, (RECT *)NULL);
                        if ((l-=n)<=0) break;
                        TextOut(hdc, (columns-1)*xchar, r.top,
                                buffer+(p+=n)-1, 1);
                        if ((r.top+=ychar)>=s) break;
                        r.left=0;
                        n=columns;
                    }
                    ShowCaret(hwnd);
                }
            return 0;
        case VK_PAUSE:
            seq_brk();
        }
        return 0;
    }
    case WM_CHAR: {
        int t=LOWORD(lParam);
        switch (wParam) {
        case '\3':
            seq_brk();
        case '\23': {
            MSG msg;
            while (GetMessage(&msg, (HWND)NULL, 0, 0) == TRUE) {
                TranslateMessage(&msg);
                if (msg.hwnd != hwnd || msg.message != WM_CHAR) DispatchMessage(&msg);
                else if (msg.wParam=='\21') return 0;
            }
            PostQuitMessage(0);
            break;
        }
        default: {
            RECT r;
            int n, p, s;
            while (t--) {
                if (len==BUFF_SIZE-1||bpos==BUFF_SIZE-1)
                    return 0;
                if (wParam<32) continue;
                memmove(buffer+bpos+1, buffer+bpos, len-bpos);
                buffer[bpos]=(char)wParam;
                len++;
                p=bpos;
                n=columns-column;
                r.left=column*xchar;
                r.top=row*ychar;
                r.right=columns*xchar;
                s=rows*ychar;
                HideCaret(hwnd);
                for (;;) {
                    r.bottom=r.top+ychar;
                    ScrollDC(hdc, xchar, 0, &r, &r, (HRGN)NULL,
                             (RECT *)NULL);
                    TextOut(hdc, r.left, r.top, buffer+p, 1);
                    if ((p+=n)>=len||(r.top+=ychar)>=s)
                        break;
                    r.left=0;
                    n=columns;
                }
                set_pos(1);
                ShowCaret(hwnd);
            }
            break;
        } }
        return 0;
    }
    case WM_SETFOCUS:
        CreateCaret(hwnd, (HBITMAP)NULL, xchar, ychar);
        SetCaretPos(column*xchar, row*ychar);
        ShowCaret(hwnd);
        return 0;
    case WM_KILLFOCUS:
        HideCaret(hwnd);
        DestroyCaret();
        return 0;
    case WM_PAINT: {
        RECT r;
        int c1, c2, r1, r2, n, p;
        GetUpdateRect(hwnd, &r, FALSE);
        c1=r.left/xchar;
        r1=r.top/ychar;
        c2=(r.right+xchar-1)/xchar;
        r2=(r.bottom+ychar-1)/ychar;
        n=(row-r1+1)*columns-c1-(columns-column);
        if (bpos<n) {
            n=bpos;
            p=0;
        }
        else p=bpos-n;
        if (column<n) {
            c1=columns-(n-column-1)%columns-1;
            r1=row-(n-column+columns-1)/columns;
        }
        else {
            c1=column-n;
            r1=row;
        }
        HideCaret(hwnd);
        while (p<len&&r1<=r2) {
            n=min(columns-c1, len-p);
            TextOut(hdc, c1*xchar, r1*ychar, buffer+p, n);
            c1=0;
            p+=n;
            r1++;
        }
        SetCaretPos(column*xchar, row*ychar);
        ValidateRect(hwnd, &rect);
        ShowCaret(hwnd);
        return 0;
    }
    case WM_CLOSE:
        PostQuitMessage(0);
        return 0;
    case WM_USER+1:
        if (cstring) memfree(cstring);
        clength=lParam;
        if (!(cstring=(UBYTE *)memalloc(clength+1))) return 0;
        cindex=0;
        return 0;
    case WM_USER+2:
        if (cindex<clength) cstring[cindex++]=(UBYTE)wParam;
        return 0;
    case WM_USER+3: {
        cstring[cindex]=0;
        stack(1);
        wsp->flag=INDEXED;
        wsp->type=STRING_TYPE;
        wsp->unit.string=cstring;
        wsp->unit.line.length=clength;
        wsp++;
        wk_perform();
        if (cstring) memfree(cstring);
        cstring=NULL;
        return 0;
    }
    }
    return DefWindowProc(hwnd, message, wParam, lParam);
}

int PASCAL WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                   LPSTR lpszCmdLine, int nCmdShow)
{
    int n;
    char *env;
    if (!(cwd=getcwd(NULL, 128))) return FALSE;
    if ((env=getenv(wk_name))!=NULL) handle_params(env, -1);
    handle_params(lpszCmdLine, -1);
    if ((oflag&REMOTE)&&(hwnd=FindWindow(wk_name, wk_name))!=NULL) {
        char MQ *p=strchr(lpszCmdLine, '$');
        if (!p||!*++p) return TRUE;
        PostMessage(hwnd, WM_USER+1, 0, strlen(p));
        while (*p) PostMessage(hwnd, WM_USER+2, *p++, 0);
        PostMessage(hwnd, WM_USER+3, 0, 0);
        return TRUE;
    }
    instance=hInstance;
    if (!hPrevInstance) {
        WNDCLASS wndclass;
        memset(&wndclass, 0, sizeof wndclass);
        wndclass.style=CS_OWNDC|CS_HREDRAW|CS_VREDRAW;
        wndclass.lpfnWndProc=WkProc;
        wndclass.hInstance=hInstance;
        wndclass.hIcon=LoadIcon(instance, wk_name);
        wndclass.hCursor=LoadCursor((HINSTANCE)NULL, IDC_ARROW);
        wndclass.hbrBackground=GetStockObject(WHITE_BRUSH);
        wndclass.lpszClassName=wk_name;
        if (!RegisterClass(&wndclass)) return FALSE;
        memset(&wndclass, 0, sizeof wndclass);
        wndclass.style=CS_OWNDC|CS_NOCLOSE;
        wndclass.lpfnWndProc=WakeProc;
        wndclass.hInstance=hInstance;
        wndclass.hIcon=LoadIcon((HINSTANCE)NULL, IDI_APPLICATION);
        wndclass.hCursor=LoadCursor((HINSTANCE)NULL, IDC_ARROW);
        wndclass.hbrBackground=GetStockObject(BLACK_BRUSH);
        wndclass.lpszClassName=wake_name;
        if (!RegisterClass(&wndclass)) return FALSE;
        memset(&wndclass, 0, sizeof wndclass);
        wndclass.style=CS_OWNDC|CS_HREDRAW|CS_VREDRAW;
        wndclass.lpfnWndProc=KeyProc;
        wndclass.hInstance=hInstance;
        wndclass.hIcon=LoadIcon((HINSTANCE)NULL, IDI_APPLICATION);
        wndclass.hCursor=LoadCursor((HINSTANCE)NULL, IDC_ARROW);
        wndclass.hbrBackground=GetStockObject(DKGRAY_BRUSH);
        wndclass.lpszClassName=key_name;
        if (!RegisterClass(&wndclass)) return FALSE;
    }
    hwnd=CreateWindow(wk_name, wk_name, WS_OVERLAPPEDWINDOW,
                      CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
                      (HWND)NULL, (HMENU)NULL, hInstance, NULL);
    if (!hwnd) return FALSE;
    n=init_wake();
    ShowWindow(hwnd, nCmdShow);
    if (n) {
        fprntf(stdout, "Wake couldn't start, retval=%d\n", n);
        fprntf(stdout, "Press enter to continue\n");
        wfgetc(stdin);
        DestroyWindow(hwnd);
        term_wake();
        return FALSE;
    }
    file_name=NULL;
    wsp->flag=0;
    wsp->type=FILE_TYPE;
    wsp->unit.file=stdin;
    push(wsp);
    if (!setjmp(quit_jump)) {
        if (env) handle_params(env, 0);
        handle_params(lpszCmdLine, 0);
    }
    while ((n=setjmp(exit_jump))!=0&&n!=WAKE_EXIT);
    if (!n) {
        memcpy(quit_jump, exit_jump, sizeof(jmp_buf));
        eel=NULL;
        wake();
    }
    ReleaseDC(hwnd, hdc);
    DestroyWindow(hwnd);
    term_wake();
    if (cwd) chdir(cwd);
    return TRUE;
}
#else
#ifdef AMIGA
void __stdargs __chkabort(void)
{
    seq_brk();
}
#elif defined(__APPLE__) || defined(__linux__)
void intsig(int sig)
{
    seq_brk();
}
#elif defined(VAXC)
int xcpthndlr(void *mecharr, void *sigarr)
{
    return SS$_RESIGNAL;
}

void intsig(int sig)
{
    out_level=0;
    if (stop_level) wk_stop();
    wk_quit();
}
#endif
#ifdef XLIB
int do_event(XEvent *event)
{
#ifndef VAXC
    if (event->xany.window==kwnd) return do_key_event(event);
#endif
    if (event->xany.window!=wnd) return 0;
    switch (event->type) {
    case KeyPress: {
        char c;
        KeySym ks;
        XLookupString(&event->xkey, &c, 1, &ks, NULL);
        switch (ks) {
        case XK_BackSpace:
        case XK_Delete:
            if (bpos>0) {
                toggle_caret();
                set_pos(-1);
                toggle_caret();
                goto insert;
            }
            return 0;
        case XK_Return:
        case XK_KP_Enter:
            toggle_caret();
            if (len) {
                if (hnum<HIST_SIZE) hidx=hnum;
                if (hist[hidx]) memfree(hist[hidx]);
                hist[hidx]=(char *)memalloc(len+1);
                memcpy(hist[hidx], buffer, len);
                hist[hidx][len]='\0';
                if (++hidx>hnum) hnum=hidx;
                if (hidx==HIST_SIZE) hidx=0;
            }
            buffer[len]='\n';
            epos=len+1;
            set_pos(len-bpos);
            column=bpos=cpos=len=0;
            if (row==rows-1)
                scroll_area(display, wnd, gcx,
                            0, -ychar, 0, 0, wxsize, wysize);
            else row++;
            toggle_caret();
            return 0;
        case XK_Left:
            toggle_caret();
            if (event->xkey.state&ShiftMask) {
                int d=0;
                while (bpos+d>0&&strchr(whites,
                                        buffer[bpos+d-1])) d--;
                while (bpos+d>0&&!strchr(whites,
                                         buffer[bpos+d-1])) d--;
                set_pos(d);
            }
            else if (event->xkey.state&ControlMask)
                set_pos(-bpos);
            else set_pos(-1);
            toggle_caret();
            return 0;
        case XK_Up:
            if (hnum) {
                if (hidx) hidx--;
                else hidx=hnum-1;
                show_hist();
            }
            return 0;
        case XK_Right:
            toggle_caret();
            if (event->xkey.state&ShiftMask) {
                int d=0;
                while (bpos+d<len&&!strchr(whites,
                                           buffer[bpos+d])) d++;
                while (bpos+d<len&&strchr(whites,
                                          buffer[bpos+d])) d++;
                set_pos(d);
            }
            else if (event->xkey.state&ControlMask)
                set_pos(len-bpos);
            else set_pos(1);
            toggle_caret();
            return 0;
        case XK_Down:
            if (hnum) {
                if (hidx<hnum-1) hidx++;
                else hidx=0;
                show_hist();
            }
            return 0;
        case XK_End:
            toggle_caret();
            set_pos(len-bpos);
            toggle_caret();
            return 0;
        case XK_Begin:
            toggle_caret();
            set_pos(-bpos);
            toggle_caret();
            return 0;
        case XK_Find:
            if (hnum>1) {
                int i=hidx, n=hnum;
                while (n--) {
                    if (i) i--;
                    else i=hnum-1;
                    if (!strncmp(buffer, hist[i], bpos)) {
                        hidx=i;
                        show_hist();
                        return 0;
                    }
                }
            }
            return 0;
        case XK_Insert:
        insert:
            if (event->xkey.state&ShiftMask) {
                if (bpos<len&&buffer[bpos]) {
                    toggle_caret();
                    XClearArea(display, wnd, column*xchar,
                               row*ychar, columns*xchar-
                               column*xchar, ychar, FALSE);
                    if (row<rows-1)
                        XClearArea(display, wnd, 0,
                                   (row+1)*ychar, columns*xchar,
                                   (rows-row-1)*ychar, FALSE);
                    buffer[bpos]='\0';
                    len=bpos;
                    toggle_caret();
                }
                return 0;
            }
            if (bpos<len) {
                int l, n, p, s, x, y, w;
                if ((l=len-bpos)>1)
                    memmove(buffer+bpos, buffer+bpos+1, l-1);
                len--;
                p=bpos;
                n=columns-column;
                x=column*xchar;
                y=row*ychar;
                w=columns*xchar;
                s=rows*ychar;
                toggle_caret();
                while (l>0) {
                    scroll_area(display, wnd, gcx,
                                -xchar, 0, x, y, w-x, ychar);
                    if ((l-=n)<=0) break;
                    XDrawString(display, wnd, gcx,
                                (columns-1)*xchar, y+
                                ffontp->max_bounds.ascent,
                                buffer+(p+=n)-1, 1);
                    if ((y+=ychar)>=s) break;
                    x=0;
                    n=columns;
                }
                toggle_caret();
            }
            return 0;
        default: {
            int n, p, s, x, y, w;
            if (c=='\3') intsig(SIGINT);
            if (c=='\23') {
                XKeyboardControl kc;
                kc.led=4;
                kc.led_mode=LedModeOn;
                XChangeKeyboardControl(display, KBLed|KBLedMode, &kc);
                do {
                    XWindowEvent(display, wnd, KeyPressMask, event);
                    XLookupString(&event->xkey, &c, 1, &ks, NULL);
                }
                while (c!='\21');
                kc.led_mode=LedModeOff;
                XChangeKeyboardControl(display, KBLed|KBLedMode, &kc);
                return 0; }
            if (ks<0x20||ks>0x7e||c<0x20||
                len==BUFF_SIZE-1||bpos==BUFF_SIZE-1) return 0;
            memmove(buffer+bpos+1, buffer+bpos, len-bpos);
            buffer[bpos]=c;
            len++;
            p=bpos;
            n=columns-column;
            x=column*xchar;
            y=row*ychar;
            w=columns*xchar;
            s=rows*ychar;
            toggle_caret();
            for (;;) {
                scroll_area(display, wnd, gcx,
                            xchar, 0, x, y, w-x, ychar);
                XDrawString(display, wnd, gcx, x, y+
                            ffontp->max_bounds.ascent, buffer+p, 1);
                if ((p+=n)>=len||(y+=ychar)>=s) break;
                x=0;
                n=columns;
            }
            set_pos(1);
            toggle_caret();
            return 0;
        } }
        return 0;
    }
    case Expose: {
        int c1, c2, r1, r2, n, p;
        c1=event->xexpose.x/xchar;
        r1=event->xexpose.y/ychar;
        c2=c1+(event->xexpose.width+xchar-1)/xchar;
        r2=r1+(event->xexpose.height+ychar-1)/ychar;
        n=(row-r1+1)*columns-c1-(columns-column);
        if (bpos<n) {
            n=bpos;
            p=0;
        }
        else p=bpos-n;
        if (column<n) {
            c1=columns-(n-column-1)%columns-1;
            r1=row-(n-column+columns-1)/columns;
        }
        else {
            c1=column-n;
            r1=row;
        }
        if (row<r1||(row==r1&&column<c1)||row>r2||(row==r2&&bpos<len))
            toggle_caret();
        while (p<len&&r1<=r2) {
            n=min(columns-c1, len-p);
            XDrawImageString(display, wnd, gcx, c1*xchar, r1*ychar+
                             ffontp->max_bounds.ascent, buffer+p, n);
            c1=0;
            p+=n;
            r1++;
        }
        toggle_caret();
        return 0;
    }
    case ConfigureNotify:
        if (wxsize!=event->xconfigure.width||
            wysize!=event->xconfigure.height) {
            wxsize=event->xconfigure.width;
            wysize=event->xconfigure.height;
            bpos=column=row=0;
            columns=wxsize/xchar-1;
            rows=(wysize)/ychar-1;
        }
        return 0;
    case FocusIn:
        focus=1;
        toggle_caret();
        return 0;
    case FocusOut:
        toggle_caret();
        focus=0;
        return 0;
    }
    return 0;
}
#endif

int main(int argc, char *argv[])
{
    int n;
    char *env;
    if (argc&&!(cwd=getcwd(NULL, 128))) return -1;
    if ((env=getenv(wk_name))) handle_params(env, -1);
    for (n=1; n<argc&&*argv[n]!='$'; n++)
        if (*argv[n]=='-') handle_option(argv[n]+1);
    n=init_wake();
#ifdef XLIB
    if (!(oflag&TEXTONLY)) {
        XGCValues gcv;
        XSetWindowAttributes swa;
        XSizeHints *sizeHints;
        XWMHints *wmHints;
        XClassHint *classHint;
        XTextProperty windowName, iconName;
        wxsize=640;
        wysize=256;
        if (!(sizeHints = XAllocSizeHints()) ||
            !(wmHints = XAllocWMHints()) ||
            !(classHint = XAllocClassHint()) ||
            !XStringListToTextProperty(&wk_name, 1, &windowName) ||
            !XStringListToTextProperty(&wk_name, 1, &iconName)) exit(-1);
        sizeHints->flags = PPosition | PSize | PMinSize;
        sizeHints->min_width=wxsize/2;
        sizeHints->min_height=wysize/2;
        wmHints->flags = StateHint | InputHint;
        wmHints->initial_state = NormalState;
        wmHints->input = True;
        classHint->res_name = wk_name;
        classHint->res_class = wk_name;
        swa.background_pixel=XWhitePixel(display, screen);
        swa.event_mask=KeyPressMask|ExposureMask|FocusChangeMask|
            StructureNotifyMask;
        serial=XNextRequest(display);
        wnd=XCreateWindow(display, XDefaultRootWindow(display),
                          (width-wxsize)/2, (height-wysize)/2, wxsize, wysize, 0,
                          XDefaultDepth(display, screen), InputOutput,
                          XDefaultVisual(display, screen),
                          CWBackPixel|CWEventMask, &swa);
        XSync(display, FALSE);
        if (xerror) exit(-1);
        serial=XNextRequest(display);
        XSetWMProperties(display, wnd, &windowName, &iconName,
                         argv, argc, sizeHints, wmHints, classHint);
        XSync(display, FALSE);
        if (xerror) exit(-1);
        XStoreName(display, wnd, wk_name);
        gcv.foreground=XBlackPixel(display, screen);
        gcv.background=XWhitePixel(display, screen);
        serial=XNextRequest(display);
        gcx=XCreateGC(display, wnd, GCForeground|GCBackground, &gcv);
        XSync(display, FALSE);
        if (xerror) {
            XDestroyWindow(display, wnd);
            term_wake();
            exit(-1);
        }
        if (!(ffontp=XLoadQueryFont(display,
#if defined(__APPLE__) || defined(__linux__)
                                    "-dec-terminal-medium-r-normal--14-140-75-75-c-80-iso8859-1")))
#else
            "-DEC-Terminal-Medium-R-Normal--14-140-75-75-C-8-ISO8859-1")))
#endif
{
    XDestroyWindow(display, wnd);
    XFreeGC(display, gcx);
    term_wake();
    exit(-1);
}
XSetFont(display, gcx, ffontp->fid);
xchar=ffontp->max_bounds.rbearing-ffontp->min_bounds.lbearing;
ychar=ffontp->max_bounds.ascent+ffontp->max_bounds.descent;
columns=wxsize/xchar-1;
rows=(wysize)/ychar-1;
             serial=XNextRequest(display);
             XMapRaised(display, wnd);
             XSync(display, FALSE);
             if (xerror) {
                 XDestroyWindow(display, wnd);
                 term_wake();
                 exit(-1);
             }
XFree(sizeHints);
XFree(wmHints);
XFree(classHint); }
#endif
if (n) {
    fprntf(stdout, "Wake couldn't start, retval=%d\n", n);
#if !defined(VAXC) || defined(XLIB)
#ifdef XLIB
    if (!(oflag&TEXTONLY))
#else
        if (argc==0)
#endif
            {
                fprntf(stdout, "Press enter to continue\n");
                fgtc(stdin);
#ifdef XLIB
                XUnloadFont(display, ffontp->fid);
                XDestroyWindow(display, wnd);
                XFreeGC(display, gcx);
#endif
            }
#endif
    term_wake();
    exit(n);
 }
#if defined(__APPLE__) || defined(__linux__)
if (isatty(STDIN_FILENO)) {
    struct termios t;
    tcgetattr(STDIN_FILENO, &t);
    t.c_lflag&=~(ICANON|ECHO);
    t.c_cc[VMIN]=1;
    t.c_cc[VTIME]=0;
    tcsetattr(STDIN_FILENO, TCSAFLUSH, &t);
 }
#endif
#ifdef __TURBOC__
ctrlbrk(seq_brk);
#endif
file_name=NULL;
wsp->flag=0;
wsp->type=FILE_TYPE;
wsp->unit.file=stdin;
push(wsp);
#ifdef XLIB
if (!(oflag&TEXTONLY)) toggle_caret();
#endif
if (!setjmp(quit_jump)) {
    if (env) handle_params(env, 0);
    for (n=1; n<argc; n++)
        if (*argv[n]=='$') {
            stack(1);
            wsp->flag=INDEXED;
            wsp->type=STRING_TYPE;
            wsp->unit.sstring=argv[n]+1;
            wsp->unit.line.length=strlen(wsp->unit.sstring);
            wsp++;
            wk_perform();
            break;
        }
        else if (*argv[n]!='-') handle_name(argv[n]);
 }
while ((n=setjmp(exit_jump))&&n!=WAKE_EXIT);
#if defined(__APPLE__) || defined(__linux__)
signal(SIGINT, intsig);
#elif defined(VAXC)
VAXC$ESTABLISH(xcpthndlr);
signal(SIGINT, intsig);
#endif
if (!n) {
    memcpy(quit_jump, exit_jump, sizeof(jmp_buf));
    eel=NULL;
    wake();
 }
#if defined(__APPLE__) || defined(__linux__)
if (isatty(STDIN_FILENO)) {
    struct termios t;
    tcgetattr(STDIN_FILENO, &t);
    t.c_lflag|=ICANON|ECHO;
    t.c_cc[VMIN]=t.c_cc[VTIME]=0;
    tcsetattr(STDIN_FILENO, TCSANOW, &t);
 }
#endif
#ifdef XLIB
if (!(oflag&TEXTONLY)) {
    XUnloadFont(display, ffontp->fid);
    XDestroyWindow(display, wnd);
    XFreeGC(display, gcx); }
#endif
term_wake();
if (cwd) chdir(cwd);
exit(0);
}
#endif

#ifdef _DCC
int wbmain(void *wbmsg)
{
    int fd;
    if ((fd=open("CON:0/88/640/80/wk", O_RDWR))<0) exit(-1);
    stdin->sd_Fd=fd;
    stdout->sd_Fd=fd;
    main(0, NULL);
}
#endif
