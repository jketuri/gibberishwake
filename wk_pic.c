
/* wk_pic.c */

#include "wake.h"
#include "wk_gra.h"
#include "wk_mat.h"
#ifdef LATTICE
#define size_t int
#endif

#define A 0
#define B 1
#define C 2
#define D 3
#define Tx 4
#define Ty 5

#define MAXCODE(n) ((1<<(n))-1)
#define BITS_MIN 9
#define BITS_MAX 12
#define CODE_CLEAR 256
#define CODE_EOI 257
#define CODE_FIRST 258
#define CODE_MIN MAXCODE(BITS_MIN)
#define CODE_MAX MAXCODE(BITS_MAX)
#define CSIZE (CODE_MAX+1024)
#define HSIZE 9001
#define HSHIFT (13-8)
#define CHECK_GAP 10000

#define TAG 0
#define TYPE 1
#define COUNT 2
#define OFFSET 3
#define READCOUNT 1
#define WRITECOUNT 2
#define BIT 3
#define CANCHANGE 4
#define NAME 5
#define VERSION 42
#define BIGENDIAN 0x4d4d
#define LITTLEENDIAN 0x4949
#define BYTE 1
#define ASCII 2
#define SHORT 3
#define LONG 4
#define RATIONAL 5
#define SBYTE 6
#define UNDEFINED 7
#define SSHORT 8
#define SLONG 9
#define SRATIONAL 10
#define FLOAT 11
#define DOUBLE 12
#define SUBFILETYPE 254
#define FILETYPE_REDUCEDIMAGE 0x1
#define FILETYPE_PAGE 0x2
#define FILETYPE_MASK 0x4
#define OSUBFILETYPE 255
#define OFILETYPE_IMAGE 1
#define OFILETYPE_REDUCEDIMAGE 2
#define OFILETYPE_PAGE 3
#define IMAGEWIDTH 256
#define IMAGELENGTH 257
#define BITSPERSAMPLE 258
#define COMPRESSION 259
#define COMPRESSION_NONE 1
#define COMPRESSION_CCITTRLE 2
#define COMPRESSION_CCITTFAX3 3
#define COMPRESSION_CCITTFAX4 4
#define COMPRESSION_LZW 5
#define COMPRESSION_JPEG 6
#define COMPRESSION_NEXT 32766
#define COMPRESSION_CCITTRLEW 32771
#define COMPRESSION_PACKBITS 32773
#define COMPRESSION_THUNDERSCAN 32809
#define PHOTOMETRIC 262
#define PHOTOMETRIC_MINISWHITE 0
#define PHOTOMETRIC_MINISBLACK 1
#define PHOTOMETRIC_RGB 2
#define PHOTOMETRIC_PALETTE 3
#define PHOTOMETRIC_MASK 4
#define PHOTOMETRIC_SEPARATED 5
#define PHOTOMETRIC_YCBCR 6
#define PHOTOMETRIC_CIELAB 8
#define THRESHHOLDING 263
#define THRESHHOLD_BILEVEL 1
#define THRESHHOLD_HALFTONE 2
#define THRESHHOLD_ERRORDIFFUSE 3
#define CELLWIDTH 264
#define CELLLENGTH 265
#define FILLORDER 266
#define FILLORDER_MSB2LSB 1
#define FILLORDER_LSB2MSB 2
#define DOCUMENTNAME 269
#define IMAGEDESCRIPTION 270
#define MAKE 271
#define MODEL 272
#define STRIPOFFSETS 273
#define ORIENTATION 274
#define ORIENTATION_TOPLEFT 1
#define ORIENTATION_TOPRIGHT 2
#define ORIENTATION_BOTRIGHT 3
#define ORIENTATION_BOTLEFT 4
#define ORIENTATION_LEFTTOP 5
#define ORIENTATION_RIGHTTOP 6
#define ORIENTATION_RIGHTBOT 7
#define ORIENTATION_LEFTBOT 8
#define SAMPLESPERPIXEL 277
#define ROWSPERSTRIP 278
#define STRIPBYTECOUNTS 279
#define MINSAMPLEVALUE 280
#define MAXSAMPLEVALUE 281
#define XRESOLUTION 282
#define YRESOLUTION 283
#define PLANARCONFIG 284
#define PLANARCONFIG_CONTIG 1
#define PLANARCONFIG_SEPARATE 2
#define PAGENAME 285
#define XPOSITION 286
#define YPOSITION 287
#define FREEOFFSETS 288
#define FREEBYTECOUNTS 289
#define GRAYRESPONSEUNIT 290
#define GRAYRESPONSEUNIT_10S 1
#define GRAYRESPONSEUNIT_100S 2
#define GRAYRESPONSEUNIT_1000S 3
#define GRAYRESPONSEUNIT_10000S 4
#define GRAYRESPONSEUNIT_100000S 5
#define GRAYRESPONSECURVE 291
#define GROUP3OPTIONS 292
#define GROUP3OPT_2DENCODING 0x1
#define GROUP3OPT_UNCOMPRESSED 0x2
#define GROUP3OPT_FILLBITS 0x4
#define GROUP4OPTIONS 293
#define GROUP4OPT_UNCOMPRESSED 0x2
#define RESOLUTIONUNIT 296
#define RESUNIT_NONE 1
#define RESUNIT_INCH 2
#define RESUNIT_CENTIMETER 3
#define PAGENUMBER 297
#define COLORRESPONSEUNIT 300
#define COLORRESPONSEUNIT_10S 1
#define COLORRESPONSEUNIT_100S 2
#define COLORRESPONSEUNIT_1000S 3
#define COLORRESPONSEUNIT_10000S 4
#define COLORRESPONSEUNIT_100000S 5
#define TRANSFERFUNCTION 301
#define SOFTWARE 305
#define DATETIME 306
#define ARTIST 315
#define HOSTCOMPUTER 316
#define PREDICTOR 317
#define WHITEPOINT 318
#define PRIMARYCHROMATICITIES 319
#define COLORMAP 320
#define HALFTONEHINTS 321
#define TILEWIDTH 322
#define TILELENGTH 323
#define TILEOFFSETS 324
#define TILEBYTECOUNTS 325
#define BADFAXLINES 326
#define CLEANFAXDATA 327
#define CLEANFAXDATA_CLEAN 0
#define CLEANFAXDATA_REGENERATED 1
#define CLEANFAXDATA_UNCLEAN 2
#define CONSECUTIVEBADFAXLINES 328
#define INKSET 332
#define INKSET_CMYK 1
#define INKNAMES 333
#define DOTRANGE 336
#define TARGETPRINTER 337
#define EXTRASAMPLES 338
#define EXTRASAMPLE_UNSPECIFIED 0
#define EXTRASAMPLE_ASSOCALPHA 1
#define EXTRASAMPLE_UNASSALPHA 2
#define SAMPLEFORMAT 339
#define SAMPLEFORMAT_UINT 1
#define SAMPLEFORMAT_INT 2
#define SAMPLEFORMAT_IEEEFP 3
#define SAMPLEFORMAT_VOID 4
#define SMINSAMPLEVALUE 340
#define SMAXSAMPLEVALUE 341
#define JPEGPROC 512
#define JPEGPROC_BASELINE 1
#define JPEGPROC_LOSSLESS 14
#define JPEGIFOFFSET 513
#define JPEGIFBYTECOUNT 514
#define JPEGRESTARTINTERVAL 515
#define JPEGLOSSLESSPREDICTORS 517
#define JPEGPOINTTRANSFORM 518
#define JPEGQTABLES 519
#define JPEGDCTABLES 520
#define JPEGACTABLES 521
#define YCBCRCOEFFICIENTS 529
#define YCBCRSUBSAMPLING 530
#define YCBCRPOSITIONING 531
#define YCBCRPOSITION_CENTERED 1
#define YCBCRPOSITION_COSITED 2
#define REFERENCEBLACKWHITE 532
#define REFPTS 32953
#define REGIONTACKPOINT 32954
#define REGIONWARPCORNERS 32955
#define REGIONAFFINE 32956
#define MATTEING 32995
#define DATATYPE 32996
#define IMAGEDEPTH 32997
#define TILEDEPTH 32998

UBYTE bitmsk[]={0x80,0x40,0x20,0x10,0x8,0x4,0x2,0x1};

/* x y xsize ysize complex xmin ymin xmax ymax m n [pic] - julia - [pic] */
void wk_julia(void)
{
    char h;
    COMPLEX c;
    UBYTE MQ *p=NULL;
    float deltax,deltay,xmin,ymin,xmax,ymax,m,x,y,xx,yy,rx,ry;
    UWORD x0,y0,xsize,ysize,n,nx,ny;
    register UWORD k;
#if defined(_MSDOS)&&!defined(_WINDOWS)
    register UWORD j;
    register UBYTE o,s;
    char far *b;
#endif
    check(11);
#ifdef _WINDOWS
    SelectObject(dcmem,bitmap);
#endif
    if (wsp[-1].type==STRING_TYPE)
        {
            h=1;
            p=(UBYTE MQ *)wsp[-1].unit.string;
        }
    else h=0;
    x0=(UWORD)check_integer(wsp-11-h);
    y0=(UWORD)check_integer(wsp-10-h);
    xsize=(UWORD)check_integer(wsp-9-h);
    ysize=(UWORD)check_integer(wsp-8-h);
    if ((h&&(ULONG)xsize*ysize>wsp->unit.line.length)||
        x0+xsize>width||y0+ysize>height)
        error(WAKE_INDEX_OUT_OF_RANGE);
#if defined(_MSDOS)&&!defined(_WINDOWS)
    *(short *)bitmap=xsize-1;
    b=bitmap+4;
#endif
    check_complex(wsp-7-h,&c);
    xmin=(float)check_double(wsp-6-h);
    ymin=(float)check_double(wsp-5-h);
    xmax=(float)check_double(wsp-4-h);
    ymax=(float)check_double(wsp-3-h);
    m=(float)check_double(wsp-2-h);
    n=(UWORD)check_integer(wsp-1-h);
    deltax=(xmax-xmin)/(float)(xsize-1);
    deltay=(ymax-ymin)/(float)(ysize-1);
    for (ny=0,yy=ymin; ny<ysize; ny++,yy+=deltay)
        {
            for (nx=0,xx=xmin; nx<xsize; nx++,xx+=deltax)
                {
                    k=0;
                    x=xx;
                    y=yy;
                    rx=x*x;
                    ry=y*y;
                    do
                        {
                            if (++k==n)
                                {
                                    k=0;
                                    break;
                                }
                            y=2*x*y+c.imag;
                            x=rx-ry+c.real;
                            rx=x*x;
                            ry=y*y;
                        }
                    while (rx+ry<=m);
                    if (h) *p++=(UBYTE)k;
                    k%=n_colors;
#ifdef AMIGA
                    SetAPen(rastport2,k);
                    WritePixel(rastport2,nx,0);
                }
            BltBitMap(bitmap,0,0,rastport->BitMap,x0,y0+ny,xsize,1,0xcc,
                      0xff,NULL);
#elif defined(VGALIB)
        }
#elif defined(_WINDOWS)
    SetPixel(dcmem,nx,0,PALETTEINDEX(k));
}
BitBlt(dc,x0,y0+ny,xsize,1,dcmem,0,0,SRCCOPY);
#elif defined(XLIB)
XSetForeground(display,gcp,k);
XDrawPoint(display,pixmap,gcp,nx,0);
}
XCopyArea(display,pixmap,window,gcp,0,0,xsize,1,x0,y0+ny);
#else
s=1<<7-nx%8;
o=~s;
for (j=nx>>3; j<row_size; j+=line_size)
    {
        if (k&1) b[j]|=s;
        else b[j]&=o;
        k>>=1;
    }
}
putimage(x0,y0+ny,bitmap,COPY_PUT);
#endif
}
#if defined(_MSDOS)&&!defined(_WINDOWS)
*(short *)bitmap=width-1;
#endif
if (h) wsp[-12]=wsp[-1];
wsp-=11;
}

/* x y xsize ysize complexmin complexmax m n [pic] - mandelbrot - [pic] */
void wk_mandelbrot(void)
{
    char h;
    UBYTE MQ *p=NULL;
    COMPLEX cmin,cmax,c;
    float deltap,deltaq,m,x,y,rx,ry;
    UWORD x0,y0,xsize,ysize,n,np,nq;
    register UWORD k;
#if defined(_MSDOS)&&!defined(_WINDOWS)
    register UWORD j;
    register UBYTE o,s;
    char far *b;
#endif
    check(8);
#ifdef _WINDOWS
    SelectObject(dcmem,bitmap);
#endif
    if (wsp[-1].type==STRING_TYPE)
        {
            h=1;
            p=(UBYTE MQ *)wsp->unit.string;
        }
    else h=0;
    x0=(UWORD)check_integer(wsp-8-h);
    y0=(UWORD)check_integer(wsp-7-h);
    xsize=(UWORD)check_integer(wsp-6-h);
    ysize=(UWORD)check_integer(wsp-5-h);
    if ((h&&(ULONG)xsize*ysize>wsp->unit.line.length)||
        x0+xsize>width||y0+ysize>height)
        error(WAKE_INDEX_OUT_OF_RANGE);
#if defined(_MSDOS)&&!defined(_WINDOWS)
    *(short *)bitmap=xsize-1;
    b=bitmap+4;
#endif
    check_complex(wsp-4-h,&cmin);
    check_complex(wsp-3-h,&cmax);
    m=(float)check_double(wsp-2-h);
    n=(UWORD)check_integer(wsp-1-h);
    deltap=(cmax.real-cmin.real)/(xsize-1);
    deltaq=(cmax.imag-cmin.imag)/(ysize-1);
    for (nq=0,c.real=cmin.real; nq<ysize; nq++,c.real+=deltap)
        {
            for (np=0,c.imag=cmin.imag; np<xsize; np++,c.imag+=deltaq)
                {
                    k=0;
                    x=(float)0.0;
                    y=(float)0.0;
                    rx=(float)0.0;
                    ry=(float)0.0;
                    do
                        {
                            if (++k==n)
                                {
                                    k=0;
                                    break;
                                }
                            y=2*x*y+c.imag;
                            x=rx-ry+c.real;
                            rx=x*x;
                            ry=y*y;
                        }
                    while (rx+ry<=m);
                    if (h) *p++=(UBYTE)k;
                    k%=n_colors;
#ifdef AMIGA
                    SetAPen(rastport2,k);
                    WritePixel(rastport2,np,0);
                }
            BltBitMap(bitmap,0,0,rastport->BitMap,x0,y0+nq,xsize,1,0xcc,
                      0xff,NULL);
#elif defined(VGALIB)
        }
#elif defined(_WINDOWS)
    SetPixel(dcmem,np,0,PALETTEINDEX(k));
}
BitBlt(dc,x0,y0+nq,xsize,1,dcmem,0,0,SRCCOPY);
#elif defined(XLIB)
XSetForeground(display,gcp,k);
XDrawPoint(display,pixmap,gcp,np,0);
}
XCopyArea(display,pixmap,window,gcp,0,0,xsize,1,x0,y0+nq);
#else
s=1<<7-np%8;
o=~s;
for (j=np>>3; j<row_size; j+=line_size)
    {
        if (k&1) b[j]|=s;
        else b[j]&=o;
        k>>=1;
    }
}
putimage(x0,y0+nq,bitmap,COPY_PUT);
#endif
}
#if defined(_MSDOS)&&!defined(_WINDOWS)
*(short *)bitmap=width-1;
#endif
if (h) wsp[-9]=wsp[-1];
wsp-=8;
}

void fourier(ELEMENT *p,ELEMENT *w,ELEMENT *t,long n,long k,long m)
{
    ELEMENT s;
    long i,j,l;
    if (n==1)
        {
            s=p[k];
            addf(p+k,&s,p+k+1);
            subf(p+k+1,&s,p+k+1);
            return;
        }
    l=n/2;
    for (i=0; i<=l; i++)
        {
            j=k+(i<<1);
            t[i]=p[j];
            t[i+l+1]=p[j+1];
        }
    for (i=0; i<=n; i++) p[k+i]=t[i];
    fourier(p,w,t,l,k,m);
    fourier(p,w,t,l,k+l+1,m);
    j=m/(n+1);
    for (i=0; i<=l; i++)
        {
            mulf(&s,w+i*j,p+k+l+1+i);
            addf(t+i,p+k+i,&s);
            subf(t+i+l+1,p+k+i,&s);
        }
    for (i=0; i<=n; i++) p[k+i]=t[i];
}

struct CODE
{
    ULONG nextx;
    ULONG length;
    UBYTE value;
    UBYTE firstchar;
};

#define NEXTCODE()                                          \
    {                                                       \
        nextdata=(nextdata<<8)|(fgetc(file)&0xff);          \
        nextbits+=8;                                        \
        if (nextbits<nbits)                                 \
            {                                               \
                nextdata=(nextdata<<8)|(fgetc(file)&0xff);  \
                nextbits+=8;                                \
            }                                               \
        code=(nextdata>>(nextbits-nbits))&nbitsmask;        \
        nextbits-=nbits;                                    \
    }

long LZWdecompression(FILE *file,register UBYTE *buf,register long length)
{
    long l=length,nbits=BITS_MIN,nextdata=0,nextbits=0,nbitsmask=CODE_MIN,code,len,tx,ox=0,codex,
        oldcodex=-1,free_entx=CODE_FIRST,maxcodex;
    struct CODE *codetab;
    maxcodex=nbitsmask-1;
    if ((codetab=(struct CODE *)memcalloc(CSIZE*sizeof(struct CODE)))==NULL)
        error(WAKE_CANNOT_ALLOCATE);
    for (code=0; code<256; code++)
        {
            codetab[code].value=(UBYTE)code;
            codetab[code].firstchar=(UBYTE)code;
            codetab[code].length=1;
            codetab[code].nextx=(ULONG)-1;
        }
    while (length>0)
        {
            NEXTCODE();
            if (code==CODE_EOI) break;
            if (code==CODE_CLEAR)
                {
                    free_entx=CODE_FIRST;
                    nbits=BITS_MIN;
                    nbitsmask=(1<<BITS_MIN)-1;
                    maxcodex=nbitsmask-1;
                    NEXTCODE();
                    if (code==CODE_EOI) break;
                    buf[ox++]=(UBYTE)code;
                    length--;
                    oldcodex=code;
                    continue;
                }
            codex=code;
            codetab[free_entx].nextx=oldcodex;
            codetab[free_entx].firstchar=codetab[codetab[free_entx].nextx].firstchar;
            codetab[free_entx].length=codetab[codetab[free_entx].nextx].length+1;
            codetab[free_entx].value=(UBYTE)(codex<free_entx?codetab[codex].firstchar:
                                             codetab[free_entx].firstchar);
            if (++free_entx>maxcodex)
                {
                    if (++nbits>BITS_MAX) nbits=BITS_MAX;
                    nbitsmask=(1<<nbits)-1;
                    maxcodex=nbitsmask-1;
                }
            oldcodex=codex;
            if (code>=256)
                {
                    if ((long)codetab[codex].length>length) return -1;
                    len=codetab[codex].length;
                    tx=ox+len;
                    do buf[--tx]=codetab[codex].value;
                    while ((codex=codetab[codex].nextx)!=-1&&tx>ox);
                    if (codex!=-1) return -1;
                    ox+=len;
                    length-=len;
                }
            else
                {
                    buf[ox++]=(UBYTE)code;
                    length--;
                }
        }
    memfree(codetab);
    return l-length;
}

/* string file - decompress - string */
void wk_decompress(void)
{
    long l;
    check(2);
    if (wsp[-2].type!=STRING_TYPE||wsp[-1].type!=FILE_TYPE)
        error(WAKE_INVALID_TYPE);
    if ((l=LZWdecompression(wsp[-1].unit.file,wsp[-2].unit.string,wsp[-2].unit.line.length))==-1)
        error(WAKE_FAULTY_FORM);
    wsp[-2].unit.line.length=l;
    wsp--;
}

/* polynomial rootsofunity degree - fourier - polynomial */
void wk_fourier(void)
{
    long n;
    check(3);
    if (wsp[-3].type!=ARRAY_TYPE||wsp[-2].type!=ARRAY_TYPE||
        !(wsp[-1].flag&INTEGER)) error(WAKE_INVALID_TYPE);
    n=wsp[-1].unit.integer+1;
    while (n)
        {
            if (n&1)
                {
                    if (n^1) error(WAKE_ILLEGAL_USE);
                    else break;
                }
            n>>=1;
        }
    if (wsp[-3].unit.line.length<=(ULONG)wsp[-1].unit.integer||
        wsp[-2].unit.line.length<(ULONG)(2*wsp[-1].unit.integer))
        error(WAKE_ILLEGAL_USE);
    temp_mem=memalloc((wsp[-1].unit.integer+1)*sizeof(ELEMENT));
    if (!temp_mem) error(WAKE_CANNOT_ALLOCATE);
    fourier(wsp[-3].unit.array,wsp[-2].unit.array,
            (ELEMENT *)temp_mem,wsp[-1].unit.integer,0L,
            2L*wsp[-1].unit.integer);
    memfree(temp_mem);
    temp_mem=NULL;
    wsp-=2;
}

/* picture width picture1 width1 x0 y0 x1 y1 matrix - commute - picture */
void wk_commute(void)
{
    long k;
    float *r;
    int i,j,m,n,x,y,xx,yy;
    check(9);
    if (wsp[-1].type!=SINGLEARRAY_TYPE) error(WAKE_INVALID_TYPE);
    if (wsp[-1].unit.line.length<6) error(WAKE_INDEX_OUT_OF_RANGE);
    for (i=1; i<8; i++)
        if (i!=2&&!(wsp[i-9].flag&INTEGER)) error(WAKE_INVALID_TYPE);
    r=wsp[-1].unit.singlearray;
    m=(int)ceil(fabs(r[A]));
    n=(int)ceil(fabs(r[D]));
    switch (wsp[-9].type) {
    case FLOATARRAY_TYPE: { double d;
            if (wsp[-7].type!=FLOATARRAY_TYPE) error(WAKE_INVALID_TYPE);
            memset(wsp[-9].unit.floatarray,0,
                   (size_t)wsp[-9].unit.line.length*sizeof(double));
            for (x=wsp[-5].unit.integer; x<wsp[-3].unit.integer; x++)
                for (y=wsp[-4].unit.integer; y<wsp[-2].unit.integer; y++)
                    {
                        xx=(int)(r[A]*(float)x+r[C]*(float)y+r[Tx]+.5);
                        yy=(int)(r[B]*(float)x+r[D]*(float)y+r[Ty]+.5);
                        d=wsp[-7].unit.floatarray[(long)x+
                                                  (long)y*wsp[-6].unit.integer];
                        for (i=xx; i<xx+m; i++)
                            for (j=yy; j<yy+n; j++)
                                {
                                    k=(long)i+(long)j*wsp[-8].unit.integer;
                                    if (k<0||k>=(long)wsp[-9].unit.line.length||
                                        wsp[-9].unit.floatarray[k]) continue;
                                    wsp[-9].unit.floatarray[k]=d;
                                }
                    }
            break; }
    case INTARRAY_TYPE: { long v;
            if (wsp[-7].type!=INTARRAY_TYPE) error(WAKE_INVALID_TYPE);
            memset(wsp[-9].unit.intarray,0,
                   (size_t)wsp[-9].unit.line.length*sizeof(long));
            for (x=wsp[-5].unit.integer; x<wsp[-3].unit.integer; x++)
                for (y=wsp[-4].unit.integer; y<wsp[-2].unit.integer; y++)
                    {
                        xx=(int)(r[A]*(float)x+r[C]*(float)y+r[Tx]+.5);
                        yy=(int)(r[B]*(float)x+r[D]*(float)y+r[Ty]+.5);
                        v=wsp[-7].unit.intarray[(long)x+
                                                (long)y*wsp[-6].unit.integer];
                        for (i=xx; i<xx+m; i++)
                            for (j=yy; j<yy+n; j++)
                                {
                                    k=(long)i+(long)j*wsp[-8].unit.integer;
                                    if (k<0||k>=(long)wsp[-9].unit.line.length||
                                        wsp[-9].unit.intarray[k]) continue;
                                    wsp[-9].unit.intarray[k]=v;
                                }
                    }
            break; }
    case STRING_TYPE: { UBYTE c;
            if (wsp[-7].type!=STRING_TYPE) error(WAKE_INVALID_TYPE);
            memset(wsp[-9].unit.string,0,
                   (size_t)wsp[-9].unit.line.length);
            for (x=wsp[-5].unit.integer; x<wsp[-3].unit.integer; x++)
                for (y=wsp[-4].unit.integer; y<wsp[-2].unit.integer; y++)
                    {
                        xx=(int)(r[A]*(float)x+r[C]*(float)y+r[Tx]+.5);
                        yy=(int)(r[B]*(float)x+r[D]*(float)y+r[Ty]+.5);
                        c=wsp[-7].unit.string[(long)x+
                                              (long)y*wsp[-6].unit.integer];
                        for (i=xx; i<xx+m; i++)
                            for (j=yy; j<yy+n; j++)
                                {
                                    k=(long)i+(long)j*wsp[-8].unit.integer;
                                    if (k<0||k>=(long)wsp[-9].unit.line.length||
                                        wsp[-9].unit.string[k]) continue;
                                    wsp[-9].unit.string[k]=c;
                                }
                    }
            break; }
    default:
        error(WAKE_INVALID_TYPE); }
    wsp-=8;
}

struct HASH
{
    long hash,code;
};

#define PUTNEXTCODE(c)                          \
    nextdata=(nextdata<<nbits)|c;               \
    nextbits+=nbits;                            \
    fputc(nextdata>>(nextbits-8),file);         \
    nextbits-=8;                                \
    if (nextbits>=8)                            \
        {                                       \
            fputc(nextdata>>(nextbits-8),file); \
            nextbits-=8;                        \
        }                                       \
    outcount+=nbits;

long LZWcompression(FILE *file,register UBYTE *buf,register long length)
{
    struct HASH *hashtab;
    long c,disp,ent,fcode,h,t,nbits=BITS_MIN,maxcode=CODE_MIN,free_ent=CODE_FIRST,
        nextbits=0,nextdata=0,checkpoint=CHECK_GAP,ratio=0,incount=0,outcount=0;
    if ((hashtab=(struct HASH *)memcalloc(HSIZE*sizeof(struct HASH)))==NULL)
        error(WAKE_CANNOT_ALLOCATE);
    t=ftell(file);
    PUTNEXTCODE(CODE_CLEAR)
        ent=*buf++;
    length--;
    incount++;
    while (length>0)
        {
            c=*buf++;
            length--;
            incount++;
            fcode=((long)c<<BITS_MAX)+ent+1;
            h=c<<HSHIFT^ent;
            if (hashtab[h].hash==fcode)
                {
                    ent=hashtab[h].code;
                    continue;
                }
            if (hashtab[h].hash>0)
                {
                    disp=HSIZE-h;
                    if (h==0) disp=1;
                    do {
                        if ((h-=disp)<0) h+=HSIZE;
                        if (hashtab[h].hash==fcode)
                            {
                                ent=hashtab[h].code;
                                goto hit;
                            }
                    } while (hashtab[h].hash>0);
                }
            PUTNEXTCODE(ent)
                ent=c;
            hashtab[h].code=free_ent++;
            hashtab[h].hash=fcode;
            if (free_ent==CODE_MAX-1)
                {
                    memset(hashtab,0,HSIZE*sizeof(struct HASH));
                    ratio=0;
                    incount=0;
                    outcount=0;
                    free_ent=CODE_FIRST;
                    PUTNEXTCODE(CODE_CLEAR)
                        nbits=BITS_MIN;
                    maxcode=CODE_MIN;
                }
            else if (free_ent>maxcode)
                {
                    nbits++;
                    if (nbits>BITS_MAX)
                        {
                            memfree(hashtab);
                            return -1;
                        }
                    maxcode=MAXCODE(nbits);
                }
            else if (incount>=checkpoint)
                {
                    long rat;
                    checkpoint=incount+CHECK_GAP;
                    if (incount>0x007fffff)
                        {
                            rat=outcount>>8;
                            rat=rat?incount/rat:0x7fffffff;
                        }
                    else rat=(incount<<8)/outcount;
                    if (rat<=ratio)
                        {
                            memset(hashtab,0,HSIZE*sizeof(struct HASH));
                            ratio=0;
                            incount=0;
                            outcount=0;
                            free_ent=CODE_FIRST;
                            PUTNEXTCODE(CODE_CLEAR)
                                nbits=BITS_MIN;
                            maxcode=CODE_MIN;
                        }
                    else ratio=rat;
                }
        hit:
            ;
        }
    if (ent!=-1) { PUTNEXTCODE(ent) }
    PUTNEXTCODE(CODE_EOI)
        if (nextbits>0) fputc(nextdata<<(8-nextbits),file);
    memfree(hashtab);
    return ftell(file)-t;
}

/* string file - compress - length */
void wk_compress(void)
{
    long l;
    check(2);
    if (wsp[-2].type!=STRING_TYPE||wsp[-1].type!=FILE_TYPE)
        error(WAKE_INVALID_TYPE);
    if ((l=LZWcompression(wsp[-1].unit.file,wsp[-2].unit.string,wsp[-2].unit.line.length))==-1)
        error(WAKE_CANNOT_ALLOCATE);
    wsp[-2].type=INTEGER_TYPE;
    wsp[-2].flag=NUMBER|INTEGER;
    wsp[-2].unit.integer=l;
    wsp--;
}

/* pic pic1 width x0 y0 x1 y1 kernel divide modify - convolve - pic */
void wk_convolve(void)
{
    int i,j,k,l,m,n,o,p,r,s,x,y;
    ELEMENT *a;
    check(10);
    if (wsp[-3].type!=ARRAY_TYPE) error(WAKE_INVALID_TYPE);
    for (i= -8; i<0; i++)
        if (i!=7&&!(wsp[i-10].flag&INTEGER)) error(WAKE_INVALID_TYPE);
    a=wsp[-3].unit.array;
    n=wsp[-3].unit.line.length;
    if (!n) error(WAKE_INDEX_OUT_OF_RANGE);
    for (i=0; i<n; i++)
        if (a[i].type!=ARRAY_TYPE) error(WAKE_INVALID_TYPE);
    m=a[0].unit.line.length;
    if (!m) error(WAKE_INDEX_OUT_OF_RANGE);
    for (i=1; i<n; i++)
        if (a[i].unit.line.length!=(ULONG)m) error(WAKE_INDEX_OUT_OF_RANGE);
    for (i=0; i<n; i++)
        for (j=0; j<m; j++)
            if (!(a[i].unit.array[j].flag&INTEGER))
                error(WAKE_INVALID_TYPE);
    k=m/2;
    l=n/2;
    switch (wsp[-10].type) {
    case FLOATARRAY_TYPE: { double d;
            if (wsp[-9].type!=FLOATARRAY_TYPE||wsp[-2].type!=FLOATING_TYPE)
                error(WAKE_INVALID_TYPE);
            if (wsp[-2].unit.floating==0.0) error(WAKE_DIVISION_BY_ZERO);
            for (x=wsp[-7].unit.integer; x<wsp[-5].unit.integer; x++)
                for (y=wsp[-6].unit.integer; y<wsp[-4].unit.integer; y++)
                    {
                        d=0.0;
                        for (i=x-k<0?0:x-k,
                                 o=i+m<wsp[-5].unit.integer?i+m:wsp[-8].unit.integer,r=0;
                             i<o; i++,r++)
                            for (j=y-l<0?0:y-l,
                                     p=j+n<wsp[-4].unit.integer?j+n:wsp[-7].unit.integer,s=0;
                                 j<p; j++,s++)
                                d+=wsp[-9].unit.floatarray[i+j*wsp[-8].unit.integer]*
                                    a[s].unit.array[r].unit.integer;
                        d/=wsp[-2].unit.floating;
                        switch (wsp[-1].unit.integer) {
                        case -1:
                            if (d>0.0) d=0.0;
                            else d= -d;
                            break;
                        case 0:
                            if (d<0.0) d= -d;
                            break;
                        case 1:
                            if (d<0.0) d=0.0;
                            break;
                        }
                        wsp[-10].unit.floatarray[
                                                 (long)x+(long)y*wsp[-8].unit.integer]=d;
                    }
            break; }
    case INTARRAY_TYPE: { long v;
            if (wsp[-9].type!=INTARRAY_TYPE||!(wsp[-2].flag&INTEGER))
                error(WAKE_INVALID_TYPE);
            if (wsp[-2].unit.integer==0) error(WAKE_DIVISION_BY_ZERO);
            for (x=wsp[-7].unit.integer; x<wsp[-5].unit.integer; x++)
                for (y=wsp[-6].unit.integer; y<wsp[-4].unit.integer; y++)
                    {
                        v=0;
                        for (i=x-k<0?0:x-k,
                                 o=i+m<wsp[-5].unit.integer?i+m:wsp[-8].unit.integer,r=0;
                             i<o; i++,r++)
                            for (j=y-l<0?0:y-l,
                                     p=j+n<wsp[-4].unit.integer?j+n:wsp[-7].unit.integer,s=0;
                                 j<p; j++,s++)
                                v+=wsp[-9].unit.intarray[i+j*wsp[-8].unit.integer]*
                                    a[s].unit.array[r].unit.integer;
                        v/=wsp[-2].unit.integer;
                        switch (wsp[-1].unit.integer) {
                        case -1:
                            if (v>0) v=0;
                            else v= -v;
                            break;
                        case 0:
                            if (v<0) v= -v;
                            break;
                        case 1:
                            if (v<0) v=0;
                            break;
                        }
                        wsp[-10].unit.intarray[
                                               (long)x+(long)y*wsp[-8].unit.integer]=v;
                    }
            break; }
    case STRING_TYPE: { long v;
            if (wsp[-9].type!=STRING_TYPE||!(wsp[-2].flag&INTEGER))
                error(WAKE_INVALID_TYPE);
            if (wsp[-2].unit.integer==0) error(WAKE_DIVISION_BY_ZERO);
            for (x=wsp[-7].unit.integer; x<wsp[-5].unit.integer; x++)
                for (y=wsp[-6].unit.integer; y<wsp[-4].unit.integer; y++)
                    {
                        v=0;
                        for (i=x-k<0?0:x-k,
                                 o=i+m<wsp[-5].unit.integer?i+m:wsp[-8].unit.integer,r=0;
                             i<o; i++,r++)
                            for (j=y-l<0?0:y-l,
                                     p=j+n<wsp[-4].unit.integer?j+n:wsp[-7].unit.integer,s=0;
                                 j<p; j++,s++)
                                v+=wsp[-9].unit.string[i+j*wsp[-8].unit.integer]*
                                    a[s].unit.array[r].unit.integer;
                        v/=wsp[-2].unit.integer;
                        switch (wsp[-1].unit.integer) {
                        case -1:
                            if (v>0) v=0;
                            else v= -v;
                            break;
                        case 0:
                            if (v<0) v= -v;
                            break;
                        case 1:
                            if (v<0) v=0;
                            break;
                        }
                        wsp[-10].unit.string[
                                             (long)x+(long)y*wsp[-8].unit.integer]=(UBYTE)v;
                    }
            break; }
    default:
        error(WAKE_INVALID_TYPE); }
    wsp-=9;
}

struct DIR
{
    short tag,type;
    long count,offset;
};

void putRGBseparate8bit(void *v,void *b,long i,long k,long m,long n)
{
    long t,u;
    UBYTE MQ *v1=v,MQ *b1=b;
    while (m-->0)
        {
            t=i;
            for (u=0; u<n; u++,i+=3) v1[i]=*b1++;
            i=t+k;
        }
}

void putRGBseparate16bit(void *v,void *b,long i,long k,long m,long n)
{
    long t,u;
    UWORD MQ *v1=v,MQ *b1=b;
    while (m-->0)
        {
            t=i;
            for (u=0; u<n; u++,i+=3) v1[i]=*b1++;
            i=t+k;
        }
}

void putRGBcontig8bit(void *v,void *b,long i,long k,long m,long n)
{
    long t,u;
    UBYTE MQ *v1=v,MQ *b1=b;
    while (m-->0)
        {
            t=i;
            for (u=0; u<n; u++,i+=3)
                {
                    v1[i]=b1[2];
                    v1[i+1]=b1[1];
                    v1[i+2]=b1[0];
                    b1+=3;
                }
            i=t+k;
        }
}

void putRGBcontig16bit(void *v,void *b,long i,long k,long m,long n)
{
    long t,u;
    UWORD MQ *v1=v,MQ *b1=b;
    while (m-->0)
        {
            t=i;
            for (u=0; u<n; u++,i+=3)
                {
                    v1[i]=b1[2];
                    v1[i+1]=b1[1];
                    v1[i+2]=b1[0];
                    b1+=3;
                }
            i=t+k;
        }
}

void put8bit(void *v,void *b,long i,long k,long m,long n)
{
    UBYTE MQ *v1=v,MQ *b1=b;
    while (m-->0)
        {
            memcpy(v1+i,b,n);
            b1+=n;
            i+=k;
        }
}

void put4bit(void *v,void *b,long i,long k,long m,long n)
{
    UBYTE MQ *v1=v,MQ *b1=b;
    UBYTE e;
    long t,u;
    n>>=1;
    while (m-->0)
        {
            t=i;
            for (u=0; u<n; u++,i+=2)
                {
                    e=*b1++;
                    v1[i]=(UBYTE)(e>>4&0xf);
                    v1[i+1]=(UBYTE)(e&0xf);
                }
            i=t+k;
        }
}

void put2bit(void *v,void *b,long i,long k,long m,long n)
{
    UBYTE MQ *v1=v,MQ *b1=b;
    UBYTE e;
    long t,u;
    n>>=2;
    while (m-->0)
        {
            t=i;
            for (u=0; u<n; u++,i+=4)
                {
                    e=*b1++;
                    v1[i]=(UBYTE)(e>>6&0x3);
                    v1[i+1]=(UBYTE)(e>>4&0x3);
                    v1[i+2]=(UBYTE)(e>>2&0x3);
                    v1[i+3]=(UBYTE)(e&0x3);
                }
            i=t+k;
        }
}

void put1bit(void *v,void *b,long i,long k,long m,long n)
{
    UBYTE MQ *v1=v,MQ *b1=b;
    UBYTE e;
    long t,u;
    n>>=3;
    while (m-->0)
        {
            t=i;
            for (u=0; u<n; u++,i+=8)
                {
                    e=*b1++;
                    v1[i]=(UBYTE)(e>>7&0x1);
                    v1[i+1]=(UBYTE)(e>>6&0x1);
                    v1[i+2]=(UBYTE)(e>>5&0x1);
                    v1[i+3]=(UBYTE)(e>>4&0x1);
                    v1[i+4]=(UBYTE)(e>>3&0x1);
                    v1[i+5]=(UBYTE)(e>>2&0x1);
                    v1[i+6]=(UBYTE)(e>>1&0x1);
                    v1[i+7]=(UBYTE)(e&0x1);
                }
            i=t+k;
        }
}

void putRGBcontig8bitCMYK(void *v,void *b,long i,long k,long m,long n)
{
}

void putContig8bitYCbCr(void *v,void *b,long i,long k,long m,long n)
{
}

static int read_tile(int x,int y,int z,int s,UBYTE MQ *buf,long length,long planarconfig,
                     long imagewidth,long imagelength,long imagedepth,long tilewidth,long tilelength,long tiledepth,
                     long *stripoffset,long *stripbytecount,long (*decompression)(FILE *,register UBYTE *,register long),FILE *file,
                     void (*hordiff)(register void *,register long,register int),long rowsize,int stride)
{
    int tile;
    if (imagedepth==1) z=0;
    if (tilewidth&&tilelength&&tiledepth)
        {
            int xpt=(imagewidth+tilewidth-1)/tilewidth;
            int ypt=(imagelength+tilelength-1)/tilelength;
            int zpt=(imagedepth+tiledepth-1)/tiledepth;
            if (planarconfig==PLANARCONFIG_SEPARATE)
                tile=(xpt*ypt*zpt)*s+(xpt*ypt)*(z/tiledepth)+xpt*(y/tilelength)+x/tilewidth;
            else tile=(xpt*ypt)*(z/tiledepth)+xpt*(y/tilelength)+x/tilewidth+s;
        }
    else tile=1;
    if (fseek(file,stripoffset[tile],SEEK_SET)) return -1;
    if (decompression)
        {
            if ((length=(*decompression)(file,buf,length))==-1) return -1;
            if (hordiff)
                while (length>0)
                    {
                        (*hordiff)(buf,rowsize,stride);
                        buf+=rowsize;
                        length-=rowsize;
                    }
        }
    else if (bigread(file,buf,stripbytecount[tile])!=(ULONG)stripbytecount[tile]) return -1;
    return 0;
}

static int read_strip(int r,int s,UBYTE MQ *buf,long length,long planarconfig,long rowsperstrip,
                      long stripsperimage,long *stripoffset,long *stripbytecount,
                      long (*decompression)(FILE *,register UBYTE *,register long),FILE *file,
                      void (*hordiff)(register void *,register long,register int),long rowsize,int stride)
{
    int strip=r/rowsperstrip;
    if (planarconfig==PLANARCONFIG_SEPARATE) strip+=s*stripsperimage;
    if (fseek(file,stripoffset[strip],SEEK_SET)) return -1;
    if (decompression)
        {
            if ((length=(*decompression)(file,buf,length))==-1) return -1;
            if (hordiff)
                while (length>0)
                    {
                        (*hordiff)(buf,rowsize,stride);
                        buf+=rowsize;
                        length-=rowsize;
                    }
        }
    else if (bigread(file,buf,stripbytecount[strip])!=(ULONG)stripbytecount[strip]) return -1;
    return 0;
}

int read_short(FILE *file,short magic,short *value)
{
    UBYTE bytes[2];
    if (fread(bytes,1,2,file)!=2) return -1;
    if (magic==BIGENDIAN) *value=(short)(bytes[0]<<8|bytes[1]);
    else *value=(short)(bytes[0]|bytes[1]<<8);
    return 0;
}

int read_long(FILE *file,short magic,long *value)
{
    UBYTE bytes[4];
    if (fread(bytes,1,4,file)!=4) return -1;
    if (magic==BIGENDIAN)
        *value=bytes[0]<<24|bytes[1]<<16|bytes[2]<<8|bytes[3];
    else *value=bytes[0]|bytes[1]<<8|bytes[2]<<16|bytes[3]<<24;
    return 0;
}

void hor_acc8(register void *buf,register long length,register int stride)
{
    register long i;
    for (i=stride; i<length; i++)
        ((UBYTE MQ *)buf)[i]=(UBYTE)(((UBYTE MQ *)buf)[i]+((UBYTE MQ *)buf)[i-stride]);
}

void hor_acc16(register void *buf,register long length,register int stride)
{
    register long i;
    length>>=1;
    for (i=stride; i<length; i++)
        ((UWORD MQ *)buf)[i]=(UWORD)(((UWORD MQ *)buf)[i]+((UWORD MQ *)buf)[i-stride]);
}

/* filename - readtiff - picture width height colormap | true */
void wk_readtiff(void)
{
    static int zigzag[]={
        0,1,5,6,14,15,27,28,
        2,4,7,13,16,26,29,42,
        3,8,12,17,25,30,41,43,
        9,11,18,24,31,40,44,53,
        10,19,23,32,39,45,52,54,
        20,22,33,38,46,51,55,60,
        21,34,37,47,50,56,59,61,
        35,36,48,49,57,58,62,63};
    long imagewidth= -1;
    long imagelength= -1;
    long imagedepth=1;
    long subfiletype= -1;
    long bitspersample=1;
    long sampleformat=SAMPLEFORMAT_VOID;
    long (*decompression)(FILE *,register UBYTE *,register long)=NULL;
    long photometric= -1;
    long threshholding=THRESHHOLD_BILEVEL;
    long fillorder=FILLORDER_MSB2LSB;
    long orientation=ORIENTATION_TOPLEFT;
    long samplesperpixel=1;
    long predictor=1;
    long rowsperstrip= -1;
    long minsamplevalue=0,maxsamplevalue=255;
    float xresolution=(float)-1,yresolution=(float)-1;
    long resolutionunit=RESUNIT_INCH;
    long planarconfig=PLANARCONFIG_CONTIG;
    float xposition=(float)-1,yposition=(float)-1;
    long group3options= -1;
    long group4options= -1;
    long *pagenumber=NULL;
    long cleanfaxdata= -1;
    long badfaxrun= -1;
    long badfaxlines= -1;
    long *colormap=NULL;
    long *halftonehints=NULL;
    long extrasamples=0,*sampleinfo=NULL;
    long stripsperimage;
    long *stripoffset=NULL;
    long *stripbytecount=NULL;
    float *ycbcrcoeffs=NULL;
    long *ycbcrsubsampling=NULL;
    long ycbcrpositioning=YCBCRPOSITION_CENTERED;
    long jpegproc= -1;
    long jpegrestartinterval= -1;
    UWORD *qtab=NULL;
    UWORD **dctab=NULL;
    UWORD **actab=NULL;
    float *whitepoint=NULL;
    float *primarychromas=NULL;
    float *refblackwhite=NULL;
    long *transferfunction=NULL;
    long inkset=INKSET_CMYK;
    long *dotrange=NULL;
    long tilewidth= -1;
    long tilelength= -1;
    long tiledepth= -1;
    char istiled=0;
    FILE *file=NULL;
    long count,offset,h,i,j,k,l,n,t,u,y,diroff,rowsize,size=0;
    UWORD magic,tag,type,version,dircount,**tab;
    struct DIR *dir;
    float D1,D2,D3,D4;
    void (*hf)(register void *,register long,register int)=NULL;
    void (*pf)(void *,void *,long,long,long,long)=NULL;
    void *b,*v=NULL;
    UBYTE arty;
    int stride=0;
    int errn;
    check(1);
    if (wsp[-1].type!=STRING_TYPE) error(WAKE_INVALID_TYPE);
    if ((file=fopen(wsp[-1].unit.sstring+
                    strspn(wsp[-1].unit.sstring,whites),"rb"))==NULL)
        access_error(wsp[-1].unit.sstring);
    if ((ycbcrsubsampling=(long *)memcalloc(2*sizeof(long)))==NULL) goto cantall;
    ycbcrsubsampling[0]=2;
    ycbcrsubsampling[1]=2;
    magic=(UWORD)(fgetc(file)<<8|fgetc(file));
    if (magic!=BIGENDIAN&&magic!=LITTLEENDIAN) goto fault;
    if (read_short(file,magic,(short *)&version)||
        read_long(file,magic,&diroff)) goto fault;
    if (version!=VERSION) goto fault;
    if (fseek(file,diroff,SEEK_SET)) goto fault;
    if (read_short(file,magic,(short *)&dircount)) goto fault;
    if ((dir=(struct DIR *)memcalloc(dircount*sizeof(struct DIR)))==NULL)
        goto cantall;
    for (i=0; i<dircount; i++)
        if (read_short(file,magic,&dir[i].tag)||
            read_short(file,magic,&dir[i].type)||
            read_long(file,magic,&dir[i].count)||
            read_long(file,magic,&dir[i].offset)) goto fault;
    for (i=0; i<dircount; i++)
        {
            tag=dir[i].tag;
            type=dir[i].type;
            count=dir[i].count;
            offset=dir[i].offset;
            switch (type) {
            case BYTE:
            case SBYTE: {
                int c;
                short *a=memcalloc(count*sizeof(short));
                if (!a) goto cantall;
                v=(void *)a;
                if (count>4) {
                    if (fseek(file,offset,SEEK_SET)) goto fault;
                    for (u=0; u<count; u++)
                        if ((c=fgetc(file))==EOF) goto fault;
                        else a[u]=(short)c;
                }
                else if (magic==BIGENDIAN) switch (count) {
                    case 4: a[3]=(short)(offset&0xff);
                    case 3: a[2]=(short)(offset>>8&0xff);
                    case 2: a[1]=(short)(offset>>16&0xff);
                    case 1: a[0]=(short)(offset>>24&0xff); }
                else switch (count) {
                    case 4: a[3]=(short)(offset>>24&0xff);
                    case 3: a[2]=(short)(offset>>16&0xff);
                    case 2: a[1]=(short)(offset>>8&0xff);
                    case 1: a[0]=(short)(offset&0xff); }
                break;
            }
            case SHORT:
            case SSHORT: {
                short s;
                int *a=memcalloc(count*sizeof(long));
                if (!a) goto cantall;
                v=(void *)a;
                if (count>2)
                    {
                        if (fseek(file,offset,SEEK_SET)) goto fault;
                        for (u=0; u<count; u++)
                            if (read_short(file,magic,&s)) goto fault;
                            else a[u]=s;
                    }
                else if (magic==BIGENDIAN) switch (count) {
                    case 2: a[1]=offset&0xffff;
                    case 1: a[0]=offset>>16&0xffff; }
                else switch (count) {
                    case 2: a[1]=offset>>16&0xffff;
                    case 1: a[0]=offset&0xffff; }
                break;
            }
            case LONG:
            case SLONG: {
                long *a=memcalloc(count*sizeof(long));
                if (!a) goto cantall;
                v=(void *)a;
                if (count>1)
                    {
                        if (fseek(file,offset,SEEK_SET)) goto fault;
                        for (u=0; u<count; u++)
                            if (read_long(file,magic,a+u)) goto fault;
                    }
                else a[0]=offset;
                break;
            }
            case RATIONAL:
            case SRATIONAL: {
                long n,d;
                float *a=memcalloc(count*sizeof(float));
                if (!a) goto cantall;
                v=(void *)a;
                if (fseek(file,offset,SEEK_SET)) continue;
                for (u=0; u<count; u++)
                    if (read_long(file,magic,&n)||read_long(file,magic,&d)) goto fault;
                    else a[u]=(float)n/(float)d;
                break;
            }
            case FLOAT: {
                float *a=memcalloc(count*sizeof(float));
                if (!a) goto cantall;
                v=(void *)a;
                if (count>1) {
                    if (fseek(file,offset,SEEK_SET)||
                        bigread(file,a,sizeof(float)*count)!=sizeof(float)*count) goto fault;
                }
                else {
                    UBYTE bytes[4];
                    if (magic==BIGENDIAN) {
                        bytes[3]=(UBYTE)(offset&0xff);
                        bytes[2]=(UBYTE)(offset>>8&0xff);
                        bytes[1]=(UBYTE)(offset>>16&0xff);
                        bytes[0]=(UBYTE)(offset>>24&0xff);
                    }
                    else {
                        bytes[3]=(UBYTE)(offset>>24&0xff);
                        bytes[2]=(UBYTE)(offset>>16&0xff);
                        bytes[1]=(UBYTE)(offset>>8&0xff);
                        bytes[0]=(UBYTE)(offset&0xff);
                    }
                    memcpy(a,bytes,4);
                }
                break;
            }
            case ASCII: {
                UBYTE *a=memcalloc(count+1);
                if (!a) goto cantall;
                v=(void *)a;
                if (count>4) {
                    if (fseek(file,offset,SEEK_SET)||
                        bigread(file,a,count)!=(ULONG)count) goto fault;
                }
                else if (magic==BIGENDIAN) switch (count) {
                    case 4: a[3]=(UBYTE)(offset&0xff);
                    case 3: a[2]=(UBYTE)(offset>>8&0xff);
                    case 2: a[1]=(UBYTE)(offset>>16&0xff);
                    case 1: a[0]=(UBYTE)(offset>>24&0xff); }
                else switch (count) {
                    case 4: a[3]=(UBYTE)(offset>>24&0xff);
                    case 3: a[2]=(UBYTE)(offset>>16&0xff);
                    case 2: a[1]=(UBYTE)(offset>>8&0xff);
                    case 1: a[0]=(UBYTE)(offset&0xff); }
                a[count]=0;
                break;
            }
            default:
                continue; }
            switch (tag) {
            case SUBFILETYPE:
                subfiletype=((long *)v)[0];
                memfree(v);
                break;
            case OSUBFILETYPE:
                switch (((long *)v)[0]) {
                case OFILETYPE_REDUCEDIMAGE:
                    subfiletype=FILETYPE_REDUCEDIMAGE;
                    break;
                case OFILETYPE_PAGE:
                    subfiletype=FILETYPE_PAGE;
                    break; }
                memfree(v);
                break;
            case IMAGEWIDTH:
                imagewidth=((long *)v)[0];
                memfree(v);
                break;
            case IMAGELENGTH:
                imagelength=((long *)v)[0];
                memfree(v);
                break;
            case BITSPERSAMPLE:
                bitspersample=((long *)v)[0];
                memfree(v);
                break;
            case COMPRESSION:
                switch (((long *)v)[0]&0xffff) {
                case COMPRESSION_NONE:
                    decompression=NULL;
                    break;
                case COMPRESSION_LZW:
                    decompression=LZWdecompression;
                    break;
                default:
                    goto fault; }
                memfree(v);
                break;
            case PHOTOMETRIC:
                photometric=((long *)v)[0];
                memfree(v);
                break;
            case THRESHHOLDING:
                threshholding=((long *)v)[0];
                memfree(v);
                break;
            case FILLORDER:
                t=((long *)v)[0];
                memfree(v);
                if (t!=FILLORDER_LSB2MSB&&t!=FILLORDER_MSB2LSB) continue;
                fillorder=t;
                break;
            case STRIPOFFSETS:
            case TILEOFFSETS:
                if (stripoffset) memfree(stripoffset);
                stripoffset=(long *)v;
                break;
            case STRIPBYTECOUNTS:
            case TILEBYTECOUNTS:
                if (stripbytecount) memfree(stripbytecount);
                stripbytecount=(long *)v;
                break;
            case ORIENTATION:
                t=((long *)v)[0];
                memfree(v);
                if (t<ORIENTATION_TOPLEFT&&ORIENTATION_LEFTBOT<t) continue;
                orientation=t;
                break;
            case SAMPLESPERPIXEL:
                t=((long *)v)[0];
                memfree(v);
                if (t) samplesperpixel=t;
                break;
            case ROWSPERSTRIP:
                t=((long *)v)[0];
                memfree(v);
                if (t)
                    {
                        rowsperstrip=t;
                        tilelength=t;
                        tilewidth=imagewidth;
                    }
                break;
            case MINSAMPLEVALUE:
                minsamplevalue=((long *)v)[0]&0xffff;
                memfree(v);
                break;
            case MAXSAMPLEVALUE:
                maxsamplevalue=((long *)v)[0]&0xffff;
                memfree(v);
                break;
            case XRESOLUTION:
                xresolution=((float *)v)[0];
                memfree(v);
                break;
            case YRESOLUTION:
                yresolution=((float *)v)[0];
                memfree(v);
                break;
            case PLANARCONFIG:
                t=((long *)v)[0];
                memfree(v);
                if (t!=PLANARCONFIG_CONTIG&&t!=PLANARCONFIG_SEPARATE) continue;
                planarconfig=t;
                break;
            case XPOSITION:
                xposition=((float *)v)[0];
                memfree(v);
                break;
            case YPOSITION:
                yposition=((float *)v)[0];
                memfree(v);
                break;
            case GROUP3OPTIONS:
                group3options=((long *)v)[0];
                memfree(v);
                break;
            case GROUP4OPTIONS:
                group4options=((long *)v)[0];
                memfree(v);
                break;
            case RESOLUTIONUNIT:
                t=((long *)v)[0];
                memfree(v);
                if (t<RESUNIT_NONE||RESUNIT_CENTIMETER<t) continue;
                resolutionunit=t;
                break;
            case PAGENUMBER:
                if (pagenumber) memfree(pagenumber);
                pagenumber=(long *)v;
                break;
            case HALFTONEHINTS:
                if (halftonehints) memfree(halftonehints);
                halftonehints=(long *)v;
                break;
            case COLORMAP:
            case TRANSFERFUNCTION:
                t=1<<bitspersample;
                if ((tag==COLORMAP||count!=t)&&count!=t*3) continue;
                if (tag==COLORMAP) colormap=(long *)v;
                else transferfunction=(long *)v;
                break;
            case PREDICTOR:
                predictor=((long *)v)[0];
                memfree(v);
                switch (predictor) {
                case 1:
                case 2:
                    break;
                default:
                    goto fault; }
                break;
            case EXTRASAMPLES:
                if (sampleinfo) memfree(sampleinfo);
                t=((long *)v)[0];
                if (t>samplesperpixel)
                    {
                        memfree(v);
                        continue;
                    }
                for (u=1; u<=t; u++) if (((long *)v)[u]>EXTRASAMPLE_UNASSALPHA) break;
                if (u<=t) continue;
                extrasamples = t;
                if ((sampleinfo=memcalloc(t*sizeof(long)))==NULL) goto cantall;
                for (u=1; u<=t; u++) sampleinfo[u-1]=((long *)v)[u];
                memfree(v);
                break;
            case MATTEING:
                extrasamples=((long *)v)[0];
                memfree(v);
                if (extrasamples)
                    {
                        if ((sampleinfo=memcalloc(sizeof(long)))==NULL) goto cantall;
                        sampleinfo[0]=EXTRASAMPLE_ASSOCALPHA;
                    }
                break;
            case BADFAXLINES:
                badfaxlines=((long *)v)[0];
                memfree(v);
                break;
            case CLEANFAXDATA:
                cleanfaxdata=((long *)v)[0];
                memfree(v);
                break;
            case CONSECUTIVEBADFAXLINES:
                badfaxrun=((long *)v)[0];
                memfree(v);
                break;
            case TILEWIDTH:
                t=((long *)v)[0];
                memfree(v);
                if (t%16) continue;
                tilewidth=t;
                istiled=1;
                break;
            case TILELENGTH:
                t=((long *)v)[0];
                memfree(v);
                if (t%16) continue;
                tilelength=t;
                istiled=1;
                break;
            case TILEDEPTH:
                t=((long *)v)[0];
                memfree(v);
                if (!t) continue;
                tiledepth=t;
                break;
            case DATATYPE:
            case SAMPLEFORMAT:
                t=((long *)v)[0];
                memfree(v);
                if (tag==DATATYPE&&!t) t=SAMPLEFORMAT_VOID;
                if (t<SAMPLEFORMAT_UINT||SAMPLEFORMAT_VOID<t) continue;
                sampleformat=t;
                break;
            case IMAGEDEPTH:
                imagedepth=((long *)v)[0];
                memfree(v);
                break;
            case YCBCRCOEFFICIENTS:
                if (ycbcrcoeffs) memfree(ycbcrcoeffs);
                ycbcrcoeffs=(float *)v;
                break;
            case YCBCRPOSITIONING:
                ycbcrpositioning=((long *)v)[0];
                memfree(v);
                break;
            case YCBCRSUBSAMPLING:
                if (ycbcrsubsampling) memfree(ycbcrsubsampling);
                ycbcrsubsampling=(long *)v;
                break;
            case JPEGPROC:
                jpegproc=((long *)v)[0];
                memfree(v);
                break;
            case JPEGRESTARTINTERVAL:
                jpegrestartinterval=((long *)v)[0];
                memfree(v);
                break;
            case JPEGQTABLES:
                if (qtab) memfree(qtab);
                if ((qtab=(UWORD *)memcalloc(
                                             samplesperpixel*64*sizeof(short)))==NULL) {
                    memfree(v);
                    goto cantall;
                }
                for (h=0,j=0; j<samplesperpixel; j++)
                    for (u=0; u<64; u++,h++)
                        qtab[j*64+zigzag[u]]=((UWORD *)v)[h];
                memfree(v);
                break;
            case JPEGDCTABLES:
            case JPEGACTABLES:
                tab=(UWORD **)memcalloc(samplesperpixel*sizeof(UWORD *));
                if (!tab) {
                    memfree(v);
                    goto cantall;
                }
                for (h=0,j=0; j<samplesperpixel; j++) {
                    for (t=16,u=0; u<16; u++,h++) t+=((UWORD *)v)[h];
                    if ((tab[j]=(UWORD *)memcalloc(t*sizeof(UWORD)))==NULL) goto cantall;
                    for (h-=16,u=0; u<t; u++,h++) tab[j][u]=((UWORD *)v)[h];
                }
                if (tag==JPEGDCTABLES) {
                    if (dctab) {
                        for (i=0; i<samplesperpixel; i++)
                            if (dctab[i]) memfree(dctab[i]);
                        memfree(dctab);
                    }
                    dctab=tab;
                }
                else {
                    if (actab) {
                        for (i=0; i<samplesperpixel; i++)
                            if (actab[i]) memfree(actab[i]);
                        memfree(actab);
                    }
                    actab=tab;
                }
                memfree(v);
                break;
            case WHITEPOINT:
                if (whitepoint) memfree(whitepoint);
                whitepoint=(float *)v;
                break;
            case PRIMARYCHROMATICITIES:
                if (primarychromas) memfree(primarychromas);
                if ((primarychromas=memcalloc(6*sizeof(float)))==NULL) {
                    memfree(v);
                    goto cantall;
                }
                for (j=0; j<6; j++) primarychromas[j]=(float)((long *)v)[j];
                memfree(v);
                break;
            case REFERENCEBLACKWHITE:
                if (refblackwhite) memfree(refblackwhite);
                if (type==LONG||type==SLONG) {
                    if ((refblackwhite=memcalloc(count*sizeof(float)))==NULL) {
                        memfree(v);
                        goto cantall;
                    }
                    for (u=0; u<count; u++) refblackwhite[u]=(float)((long *)v)[u];
                    memfree(v);
                }
                else refblackwhite=(float *)v;
                break;
            case INKSET:
                inkset=((long *)v)[0];
                memfree(v);
                break;
            case DOTRANGE:
                if (dotrange) memfree(dotrange);
                dotrange=(long *)v;
                break;
            default:
                memfree(v);
                continue; }
        }
    if (imagelength==-1||!stripoffset) goto fault;
    if (bitspersample==-1) bitspersample=8;
    switch (bitspersample) {
    case 1:
    case 2:
    case 4:
    case 8:
    case 16:
        break;
    default:
        goto fault; }
    if (samplesperpixel>4) goto fault;
    switch (samplesperpixel - extrasamples) {
    case 3:
        break;
    case 1: case 4:
        if ((extrasamples==1&&sampleinfo[0]==EXTRASAMPLE_ASSOCALPHA)||
            planarconfig!=PLANARCONFIG_CONTIG) break;
    default:
        goto fault; }
    if (photometric==-1)
        switch (samplesperpixel) {
        case 1:
            photometric=PHOTOMETRIC_MINISBLACK;
            break;
        case 3: case 4:
            photometric=PHOTOMETRIC_RGB;
            break;
        default:
            goto fault; }
    switch (photometric) {
    case PHOTOMETRIC_MINISWHITE:
    case PHOTOMETRIC_MINISBLACK:
    case PHOTOMETRIC_RGB:
    case PHOTOMETRIC_PALETTE:
    case PHOTOMETRIC_YCBCR:
        break;
    case PHOTOMETRIC_SEPARATED:
        if (inkset!=INKSET_CMYK) goto fault;
        break;
    default:
        goto fault; }
    if (rowsperstrip==-1) rowsperstrip=imagelength;
    if (predictor==2) {
        stride=planarconfig==PLANARCONFIG_CONTIG?samplesperpixel:1;
        switch (bitspersample) {
        case 8:
            hf=hor_acc8;
            break;
        case 16:
            hf=hor_acc16;
            break;
        default:
            goto fault;
        }
    }
    l=imagewidth*imagelength;
    k=1;
    switch (photometric) {
    case PHOTOMETRIC_YCBCR:
        D1=2-2*ycbcrcoeffs[0];
        D2=D1*ycbcrcoeffs[0]/ycbcrcoeffs[1];
        D3=2-2*ycbcrcoeffs[2];
        D4=D3*ycbcrcoeffs[2]/ycbcrcoeffs[1];
    case PHOTOMETRIC_RGB:
    case PHOTOMETRIC_SEPARATED:
        k=3;
        break;
    case PHOTOMETRIC_MINISBLACK:
    case PHOTOMETRIC_MINISWHITE:
        n=1<<bitspersample;
        if ((colormap=(long *)memcalloc(n*3*sizeof(long)))==NULL) goto cantall;
        if (photometric==PHOTOMETRIC_MINISWHITE)
            for (i=0; i<n; i++) colormap[i]=colormap[i+n]=colormap[i+2*n]=(n-i)*255/n;
        for (i=0; i<n; i++) colormap[i]=colormap[i+n]=colormap[i+2*n]=i*255/n;
        break;
    case PHOTOMETRIC_PALETTE:
        if (!colormap) goto fault;
        n=(1<<bitspersample)*3;
        for (i=0; i<n; i++)
            if (colormap[i]>=256)
                {
                    for (i=0; i<n; i++) colormap[i]=(colormap[i]*255)/((1<<16)-1);
                    break;
                }
        break;
    }
    l*=k;
    arty=(UBYTE)(bitspersample==16?USHORTARRAY_TYPE:STRING_TYPE);
    if ((v=get_mem(l+(arty==STRING_TYPE),arty,FALSE))==NULL) goto cantall;
    switch (orientation) {
    case ORIENTATION_BOTRIGHT:
    case ORIENTATION_RIGHTBOT:
    case ORIENTATION_LEFTBOT:
        orientation=ORIENTATION_BOTLEFT;
    case ORIENTATION_BOTLEFT:
        y=imagelength-1;
        break;
    case ORIENTATION_TOPRIGHT:
    case ORIENTATION_RIGHTTOP:
    case ORIENTATION_LEFTTOP:
    default:
        orientation=ORIENTATION_TOPLEFT;
    case ORIENTATION_TOPLEFT:
        y=0;
        break;
    }
    if (istiled) {
        if (tilewidth==-1) tilewidth=imagewidth;
        if (tilelength==-1) tilelength=imagelength;
        if (tiledepth==-1) tiledepth=imagedepth;
        stripsperimage=tilewidth&&tilelength&&tiledepth?
            ((imagewidth+tilewidth-1)/tilewidth)*
            ((imagelength+tilelength-1)/tilelength)*
            ((imagedepth+tiledepth-1)/tiledepth):0;
        if (planarconfig==PLANARCONFIG_SEPARATE) stripsperimage*=samplesperpixel;
        if (planarconfig==PLANARCONFIG_CONTIG&&photometric==PHOTOMETRIC_YCBCR) {
            long w=(tilewidth+ycbcrsubsampling[0]-1)/ycbcrsubsampling[0]*ycbcrsubsampling[0];
            long samplingarea=ycbcrsubsampling[0]*ycbcrsubsampling[1];
            rowsize=(w*bitspersample+7)/8;
            w=(tilelength+ycbcrsubsampling[1]-1)/ycbcrsubsampling[1]*ycbcrsubsampling[1];
            size=w*rowsize+2*(w*rowsize/samplingarea);
        }
        else {
            rowsize=bitspersample*tilewidth;
            if (planarconfig==PLANARCONFIG_CONTIG) rowsize*=samplesperpixel;
            rowsize=(rowsize+7)/8;
            size=tilelength*rowsize;
        }
        size*=tiledepth;
    }
    else {
        stripsperimage=(imagelength+rowsperstrip-1)/rowsperstrip;
        tilewidth=imagewidth;
        tilelength=rowsperstrip;
        tiledepth=imagedepth;
        if (planarconfig==PLANARCONFIG_CONTIG&&photometric==PHOTOMETRIC_YCBCR) {
            long w=(imagewidth+ycbcrsubsampling[0]-1)/ycbcrsubsampling[0]*ycbcrsubsampling[0];
            long samplingarea=ycbcrsubsampling[0]*ycbcrsubsampling[1];
            rowsize=(w*bitspersample+7)/8;
            w=(rowsperstrip+ycbcrsubsampling[1]-1)/ycbcrsubsampling[1]*ycbcrsubsampling[1];
            size=w*rowsize+2*(w*rowsize/samplingarea);
        }
        else {
            rowsize=bitspersample*imagewidth;
            if (planarconfig==PLANARCONFIG_CONTIG) rowsize*=samplesperpixel;
            rowsize=(rowsize+7)/8;
            size=rowsperstrip*rowsize;
        }
    }
    if (!size) goto fault;
    if ((b=memcalloc(size))==NULL) goto cantall;
    if (!decompression) {
        if (!stripbytecount&&(stripbytecount=(long *)memcalloc(sizeof(long)))==NULL) goto cantall;
        if (stripsperimage==1&&stripbytecount[0]==0) stripbytecount[0]=size;
    }
    t=imagewidth*k;
    if (planarconfig==PLANARCONFIG_SEPARATE&&samplesperpixel-extrasamples>1) {
        long col,row;
        switch (photometric) {
        case PHOTOMETRIC_RGB:
            if (bitspersample==8) pf=putRGBseparate8bit;
            else pf=putRGBseparate16bit;
            break; }
        if (!pf) goto fault;
        if (istiled)
            for (row=0; row<imagelength; row+=tilelength) {
                h=row+tilelength>imagelength?imagelength-row:tilelength;
                j=y*t;
                for (col=0; col<imagewidth; col+=tilewidth) {
                    u=col+tilewidth>imagewidth?imagewidth-col:tilewidth;
                    for (i=0; i<3; i++)
                        {
                            if (read_tile(col,row,0,i,b,tilewidth,planarconfig,
                                          imagewidth,imagelength,imagedepth,
                                          tilewidth,tilelength,tiledepth,
                                          stripoffset,stripbytecount,
                                          decompression,file,hf,rowsize,stride)==-1) goto fault;
                            (*pf)(v,b,j+(i==0?2:i==2?0:1),t,h,u);
                        } }
                y+=(orientation==ORIENTATION_TOPLEFT?tilelength:-tilelength);
            }
        else for (row=0; row<imagelength; row+=rowsperstrip) {
                h=row+rowsperstrip>imagelength?imagelength-row:rowsperstrip;
                j=y*t;
                for (i=0; i<3; i++) {
                    if (read_strip(row,i,b,size,planarconfig,rowsperstrip,
                                   stripsperimage,stripoffset,stripbytecount,decompression,file,
                                   hf,rowsize,stride)==-1) goto fault;
                    (*pf)(v,b,j+(i==0?2:i==2?0:1),t,h,imagewidth);
                }
                y+=orientation==ORIENTATION_TOPLEFT?rowsperstrip:-rowsperstrip;
            }
    }
    else {
        long col,row;
        switch (photometric) {
        case PHOTOMETRIC_RGB:
            if (bitspersample==8) pf=putRGBcontig8bit;
            else pf=putRGBcontig16bit;
            break;
        case PHOTOMETRIC_SEPARATED:
            if (bitspersample==8) pf=putRGBcontig8bitCMYK;
            break;
        case PHOTOMETRIC_PALETTE:
        case PHOTOMETRIC_MINISWHITE:
        case PHOTOMETRIC_MINISBLACK:
            switch (bitspersample) {
            case 8: pf=put8bit; break;
            case 4: pf=put4bit; break;
            case 2: pf=put2bit; break;
            case 1: pf=put1bit; break;
            }
            break;
        case PHOTOMETRIC_YCBCR:
            switch (bitspersample) {
            case 8: pf=putContig8bitYCbCr; break;
            }
            break;
        }
        if (!pf) goto fault;
        if (istiled)
            for (row=0; row<imagelength; row+=tilelength) {
                h=row+tilelength>imagelength?imagelength-row:tilelength;
                for (col=0; col<imagewidth; col+=tilewidth) {
                    if (read_tile(col,row,0,0,b,tilelength,planarconfig,
                                  imagewidth,imagelength,imagedepth,
                                  tilewidth,tilelength,tiledepth,
                                  stripoffset,stripbytecount,
                                  decompression,file,hf,rowsize,stride)==-1) goto fault;
                    (*pf)(v,b,(y+col)*k,t,h,
                          col+tilewidth>imagewidth?imagewidth-col:tilewidth);
                }
                y+=(orientation==ORIENTATION_TOPLEFT?tilelength:-tilelength);
            }
        else for (row=0; row<imagelength; row+=rowsperstrip) {
                h=row+rowsperstrip>imagelength?imagelength-row:rowsperstrip;
                if (read_strip(row,0,b,size,planarconfig,rowsperstrip,stripsperimage,
                               stripoffset,stripbytecount,decompression,file,hf,rowsize,stride)==-1)
                    goto fault;
                (*pf)(v,b,y*t,t,h,imagewidth);
                y+=(orientation==ORIENTATION_TOPLEFT?rowsperstrip:-rowsperstrip);
            }
    }
    stack(3);
    if (colormap) {
        UBYTE *s;
        n=1<<bitspersample;
        h=n<<1;
        t=n*3;
        if ((s=(UBYTE *)get_mem(t+1,STRING_TYPE,FALSE))==NULL) goto cantall;
        u=0;
        for (i=0; i<n; i++) {
            s[u++]=(UBYTE)colormap[i];
            s[u++]=(UBYTE)colormap[i+n];
            s[u++]=(UBYTE)colormap[i+h];
        }
        wsp[2].unit.string=s;
        wsp[2].unit.line.length=t;
        wsp[2].type=STRING_TYPE;
        wsp[2].flag=COMPOSITE|INDEXED;
    }
    else {
        wsp[2].type=TRUE_TYPE;
        wsp[2].flag=0;
    }
    wsp[-1].unit.data=v;
    wsp[-1].unit.line.length=l;
    wsp[-1].type=arty;
    wsp[-1].flag=COMPOSITE|INDEXED;
    wsp->type=INTEGER_TYPE;
    wsp->flag=NUMBER|INTEGER;
    wsp->unit.integer=imagewidth;
    wsp[1].type=INTEGER_TYPE;
    wsp[1].flag=NUMBER|INTEGER;
    wsp[1].unit.integer=imagelength;
    wsp+=3;
    errn=0;
    goto end;
 cantall:
    errn=WAKE_CANNOT_ALLOCATE;
    goto end;
 fault:
    errn=WAKE_FAULTY_FORM;
    goto end;
 end:
    if (colormap) memfree(colormap);
    if (pagenumber) memfree(pagenumber);
    if (halftonehints) memfree(halftonehints);
    if (sampleinfo) memfree(sampleinfo);
    if (stripoffset) memfree(stripoffset);
    if (stripbytecount) memfree(stripbytecount);
    if (ycbcrcoeffs) memfree(ycbcrcoeffs);
    if (ycbcrsubsampling) memfree(ycbcrsubsampling);
    if (qtab) memfree(qtab);
    if (dctab) {
        for (i=0; i<samplesperpixel; i++) if (dctab[i]) memfree(dctab[i]);
        memfree(dctab);
    }
    if (actab) {
        for (i=0; i<samplesperpixel; i++) if (actab[i]) memfree(actab[i]);
        memfree(actab);
    }
    if (whitepoint) memfree(whitepoint);
    if (primarychromas) memfree(primarychromas);
    if (refblackwhite) memfree(refblackwhite);
    if (transferfunction) memfree(transferfunction);
    if (dotrange) memfree(dotrange);
    fclose(file);
    if (errn) error(errn);
}

/* picture width height null | true | colormap x y - render - */
void wk_render(void)
{
    int l,m,x,y;
    check(6);
    if (wsp[-6].type!=STRING_TYPE) error(WAKE_INVALID_TYPE);
    l=check_integer(wsp-5);
    m=check_integer(wsp-4);
    x=check_integer(wsp-2);
    y=check_integer(wsp-1);
#ifdef AMIGA
    switch (wsp[-3].type) {
    case NULL_TYPE: {
        int i,j,k,n;
        PLANEPTR p;
        UBYTE b,*q,*s=wsp[-6].unit.string;
        for (j=0; j<m; j++,s+=l) {
            for (k=0; k<bitmap->Depth; k++) {
                b=bitmsk[7-k];
                p=bitmap->Planes[k];
                *p=0;
                q=s;
                for (i=0,n=0; i<l; i++,n++) {
                    if (n==8) {
                        *++p=0;
                        n=0;
                    }
                    if (*q++&b) *p|=bitmsk[n];
                }
            }
            BltBitMap(bitmap,0,0,rastport->BitMap,x,y+j,l,1,0xcc,0xff,NULL);
        }
        break; }
    case TRUE_TYPE: {
        int i,j,k,n;
        PLANEPTR p;
        UBYTE b,*q,*r,*s=wsp[-6].unit.string;
        if ((r=(UBYTE *)malloc(l))==NULL) error(WAKE_CANNOT_ALLOCATE);
        for (j=0; j<m; j++,s+=l) {
            for (i=0,q=s; i<l; i++,q+=3) r[i]=setrgb(q[2],q[1],q[0]);
            for (k=0; k<bitmap->Depth; k++) {
                b=bitmsk[7-k];
                p=bitmap->Planes[k];
                *p=0;
                q=r;
                for (i=0,n=0; i<l; i++,n++) {
                    if (n==8) {
                        *++p=0;
                        n=0;
                    }
                    if (*q++&b) *p|=bitmsk[n];
                }
            }
            BltBitMap(bitmap,0,0,rastport->BitMap,x,y+j,l,1,0xcc,0xff,NULL);
        }
        free(r);
        break; }
    case STRING_TYPE: {
        int i,j,k,n;
        PLANEPTR p;
        UBYTE b,*q,*r,*s=wsp[-6].unit.string,*c=wsp[-3].unit.string;
        ULONG u,v=wsp[-3].unit.line.length/3;
        if ((r=(UBYTE *)malloc(l))==NULL) error(WAKE_CANNOT_ALLOCATE);
        for (j=0; j<m; j++,s+=l) {
            for (i=0,q=s; i<l; i++,q++)
                if (*q<v) {
                    u=((ULONG)*q<<1)+(ULONG)*q;
                    r[i]=setrgb(c[u],c[u+1],c[u+2]);
                }
                else r[i]= *q;
            for (k=0; k<bitmap->Depth; k++) {
                b=bitmsk[7-k];
                p=bitmap->Planes[k];
                *p=0;
                q=s;
                for (i=0,n=0; i<l; i++,n++) {
                    if (n==8) {
                        *++p=0;
                        n=0;
                    }
                    if (*q++&b) *p|=bitmsk[n];
                }
            }
            BltBitMap(bitmap,0,0,rastport->BitMap,x,y+j,l,1,0xcc,0xff,NULL);
        }
        break; }
    default:
        error(WAKE_INVALID_TYPE); }
#elif defined(VGALIB)
#elif defined(_WINDOWS)
    switch (wsp[-3].type) {
    case NULL_TYPE: {
        int j,n;
        BITMAPINFO *bmi;
        UBYTE MQ *p=(UBYTE MQ *)wsp[-6].unit.string;
        if ((bmi=(BITMAPINFO *)memcalloc(sizeof(BITMAPINFO)+
                                         256*sizeof(unsigned)))==NULL) error(WAKE_CANNOT_ALLOCATE);
        bmi->bmiHeader.biSize=sizeof(BITMAPINFOHEADER);
        bmi->bmiHeader.biWidth=l;
        bmi->bmiHeader.biHeight=1;
        bmi->bmiHeader.biPlanes=1;
        bmi->bmiHeader.biBitCount=8;
        bmi->bmiHeader.biCompression=BI_RGB;
        for (n=0; n<256; n++) ((unsigned *)bmi->bmiColors)[n]=n%n_colors;
        for (j=y,y+=m; j<y; j++,p+=l)
            SetDIBitsToDevice(dc,x,j,l,1,0,0,0,1,p,bmi,DIB_PAL_COLORS);
        memfree(bmi);
        break; }
    case TRUE_TYPE: {
        int j,n;
        BITMAPINFO *bmi;
        UBYTE MQ *p=(UBYTE MQ *)wsp[-6].unit.string;
        if ((bmi=(BITMAPINFO *)memcalloc(sizeof(BITMAPINFO)+
                                         3*sizeof(long)))==NULL) error(WAKE_CANNOT_ALLOCATE);
        bmi->bmiHeader.biSize=sizeof(BITMAPINFOHEADER);
        bmi->bmiHeader.biWidth=l;
        bmi->bmiHeader.biHeight=1;
        bmi->bmiHeader.biPlanes=1;
        bmi->bmiHeader.biBitCount=24;
        bmi->bmiHeader.biCompression=BI_RGB;
        n=l*3;
        if (n_colors<=256) {
            UBYTE MQ *e;
            for (e=p+(long)m*(long)n; p<e&&color_n<n_colors; p+=3)
                setrgb(p[0],p[1],p[2],FALSE);
            RealizePalette(dc);
            p=(UBYTE MQ *)wsp[-6].unit.string;
        }
        for (j=y,y+=m; j<y; j++,p+=n)
            SetDIBitsToDevice(dc,x,j,l,1,0,0,0,1,p,bmi,DIB_RGB_COLORS);
        memfree(bmi);
        break; }
    case STRING_TYPE: {
        int j,n;
        BITMAPINFO *bmi;
        UBYTE MQ *p=(UBYTE MQ *)wsp[-6].unit.string,
            MQ *c=(UBYTE MQ *)wsp[-3].unit.string;
        int v=wsp[-3].unit.line.length/3;
        if ((bmi=(BITMAPINFO *)memcalloc(sizeof(BITMAPINFO)+
                                         v*sizeof(RGBQUAD)))==NULL) error(WAKE_CANNOT_ALLOCATE);
        bmi->bmiHeader.biSize=sizeof(BITMAPINFOHEADER);
        bmi->bmiHeader.biWidth=l;
        bmi->bmiHeader.biHeight=1;
        bmi->bmiHeader.biPlanes=1;
        bmi->bmiHeader.biBitCount=8;
        bmi->bmiHeader.biCompression=BI_RGB;
        bmi->bmiHeader.biClrUsed=v;
        for (n=0; n<v; n++,c+=3) {
            bmi->bmiColors[n].rgbRed=c[0];
            bmi->bmiColors[n].rgbGreen=c[1];
            bmi->bmiColors[n].rgbBlue=c[2];
        }
        if (n_colors<=256) {
            c=(UBYTE MQ *)wsp[-3].unit.string;
            for (n=0; n<v&&color_n<n_colors; n++,c+=3)
                setrgb(c[0],c[1],c[2],FALSE);
            RealizePalette(dc);
        }
        for (j=y,y+=m; j<y; j++,p+=l)
            SetDIBitsToDevice(dc,x,j,l,1,0,0,0,1,p,bmi,DIB_RGB_COLORS);
        memfree(bmi);
        break; }
    default:
        error(WAKE_INVALID_TYPE); }
#elif defined(XLIB)
    switch (wsp[-3].type) {
    case NULL_TYPE: {
        int i,j;
        UBYTE MQ *p=(UBYTE MQ *)wsp[-6].unit.string;
        for (j=y,y+=m; j<y; j++) {
            for (i=0; i<l; i++,p++)
                {
                    XSetForeground(display,gcp,*p);
                    XDrawPoint(display,pixmap,gcp,i,0);
                }
            XCopyArea(display,pixmap,window,gcp,0,0,width,1,x,j);
        }
        break; }
    case TRUE_TYPE: {
        int i,j;
        UBYTE MQ *p=(UBYTE MQ *)wsp[-6].unit.string;
        for (j=y,y+=m; j<y; j++) {
            for (i=0; i<l; i++,p+=3) {
                XSetForeground(display,gcp,alloc_color(p[2],p[1],p[0]));
                XDrawPoint(display,pixmap,gcp,i,0);
            }
            XCopyArea(display,pixmap,window,gcp,0,0,width,1,x,j);
        }
        break; }
    case STRING_TYPE: {
        int i,j;
        UBYTE MQ *p=(UBYTE MQ *)wsp[-6].unit.string,
            MQ *c=(UBYTE MQ *)wsp[-3].unit.string;
        ULONG u,v=wsp[-3].unit.line.length/3;
        for (j=y,y+=m; j<y; j++) {
            for (i=0; i<l; i++,p++)
                if (*p<v) {
                    u=((ULONG)*p<<1)+(ULONG)*p;
                    XSetForeground(display,gcp,
                                   alloc_color(c[u],c[u+1],c[u+2]));
                    XDrawPoint(display,pixmap,gcp,i,0);
                }
                else {
                    XSetForeground(display,gcp,*p);
                    XDrawPoint(display,pixmap,gcp,i,0);
                }
            XCopyArea(display,pixmap,window,gcp,0,0,width,1,x,j);
        }
        break; }
    default:
        error(WAKE_INVALID_TYPE); }
#else
    switch (wsp[-3].type) {
    case NULL_TYPE: {
        int i,j,k,n;
        UBYTE b,*o,*p,MQ *q,*r,MQ *s=(UBYTE MQ *)wsp[-6].unit.string;
        *(short *)bitmap=l-1;
        o=bitmap+4;
        for (j=y,y+=m; j<y; j++,s+=l) {
            for (k=0,p=o; k<n_planes; k++,p+=line_size) {
                n=0;
                b=bitmsk[7-k];
                *p=0;
                q=s;
                for (i=0; i<l; i++) {
                    if (n==8) {
                        *++p=0;
                        n=0;
                    }
                    if (*q++&b) *p|=bitmsk[n];
                    n++;
                }
            }
            putimage(x,j,bitmap,COPY_PUT);
        }
        *(short *)bitmap=width-1;
        break; }
    case TRUE_TYPE: {
        int i,j,k,n;
        UBYTE b,*o,*p,MQ *q,*r,MQ *s=(UBYTE MQ *)wsp[-6].unit.string;
        *(short *)bitmap=l-1;
        if ((r=(UBYTE *)memalloc(l))==NULL) error(WAKE_CANNOT_ALLOCATE);
        o=bitmap+4;
        for (j=y,y+=m; j<y; j++,s+=l) {
            for (i=0,q=s; i<l; i++,q+=3) r[i]=setrgb(q[2],q[1],q[0]);
            for (k=0,p=o; k<n_planes; k++,p+=line_size) {
                n=0;
                b=bitmsk[7-k];
                *p=0;
                q=r;
                for (i=0; i<l; i++) {
                    if (n==8) {
                        *++p=0;
                        n=0;
                    }
                    if (*q++&b) *p|=bitmsk[n];
                    n++;
                }
            }
            putimage(x,j,bitmap,COPY_PUT);
        }
        *(short *)bitmap=width-1;
        free(r);
        break; }
    case STRING_TYPE: {
        int i,j,k,n;
        UBYTE b,*o,*p,MQ *q,*r,MQ *s=(UBYTE MQ *)wsp[-6].unit.string,
            MQ *c=(UBYTE MQ *)wsp[-3].unit.string;
        ULONG u,v=wsp[-3].unit.line.length/3;
        *(short *)bitmap=l-1;
        if ((r=(UBYTE *)memalloc(l))==NULL) error(WAKE_CANNOT_ALLOCATE);
        o=bitmap+4;
        for (j=y,y+=m; j<y; j++,s+=l) {
            for (i=0,q=s; i<l; i++,q++)
                if (*q<v) {
                    u=((ULONG)*q<<1)+(ULONG)*q;
                    r[i]=setrgb(c[u],c[u+1],c[u+2]);
                }
                else r[i]= *q;
            for (k=0,p=o; k<n_planes; k++,p+=line_size) {
                n=0;
                b=bitmsk[7-k];
                *p=0;
                q=r;
                for (i=0; i<l; i++) {
                    if (n==8) {
                        *++p=0;
                        n=0;
                    }
                    if (*q++&b) *p|=bitmsk[n];
                    n++;
                }
            }
            putimage(x,j,bitmap,COPY_PUT);
        }
        *(short *)bitmap=width-1;
        free(r);
        break; }
    default:
        error(WAKE_INVALID_TYPE); }
#endif
    wsp-=6;
}

/* array degree - rootsofunity - array */
void wk_rootsofunity(void)
{
    double d;
    long j,k,l;
    ELEMENT *a;
    check(2);
    if (wsp[-2].type!=ARRAY_TYPE||!(wsp[-1].flag&INTEGER))
        error(WAKE_INVALID_TYPE);
    l=2*wsp[-1].unit.integer;
    if (wsp[-2].unit.line.length<(ULONG)l) error(WAKE_INDEX_OUT_OF_RANGE);
    k=wsp[-1].unit.integer+1;
    a=wsp[-2].unit.array;
    for (j=0; j<l; j++) {
        d=2.0*PI*(double)j/(double)k;
        a[j].unit.compl.imag=(float)sin(d);
        if (a[j].unit.compl.imag==0.0) {
            a[j].type=FLOATING_TYPE;
            a[j].unit.floating=cos(d);
        }
        else {
            a[j].type=COMPLEX_TYPE;
            a[j].unit.compl.real=(float)cos(d);
        }
    }
    wsp--;
}

void write_short(FILE *file,UWORD value)
{
    fputc(value>>8&0xff,file);
    fputc(value&0xff,file);
}

void write_long(FILE *file,long value)
{
    fputc(value>>24&0xff,file);
    fputc(value>>16&0xff,file);
    fputc(value>>8&0xff,file);
    fputc(value&0xff,file);
}

void write_tag(FILE *file,UWORD tag,UWORD type,long count,long offset)
{
    write_short(file,tag);
    write_short(file,type);
    write_long(file,count);
    write_long(file,offset);
}

void hor_diff8(register void *buf,register long length,register int stride)
{
    register long i;
    for (i=length-1; i>=stride; i--)
        ((char MQ *)buf)[i]=(char)(((char MQ *)buf)[i]-((char MQ *)buf)[i-stride]);
}

void hor_diff16(register void *buf,register long length,register int stride)
{
    register long i;
    length>>=1;
    for (i=length-1; i>=stride; i--)
        ((short MQ *)buf)[i]=(short)(((short MQ *)buf)[i]-((short MQ *)buf)[i-stride]);
}

/* picture width height colormap | true compression filename - writetiff - */
void wk_writetiff(void)
{
    void *b;
    long l,n;
    FILE *file;
    check(6);
    if ((wsp[-6].type!=STRING_TYPE&&wsp[-6].type!=USHORTARRAY_TYPE)||
        !(wsp[-5].flag&INTEGER)||!(wsp[-4].flag&INTEGER)||
        (wsp[-3].type!=STRING_TYPE&&wsp[-3].type!=TRUE_TYPE)||
        wsp[-1].type!=STRING_TYPE) error(WAKE_INVALID_TYPE);
    if ((file=fopen(wsp[-1].unit.sstring+
                    strspn(wsp[-1].unit.sstring,whites),"wb"))==NULL)
        access_error(wsp[-1].unit.sstring);
    write_short(file,BIGENDIAN);
    write_short(file,VERSION);
    write_long(file,0);
    n=wsp[-6].unit.line.length;
    l=n*type_size[wsp[-6].type];
    if (wsp[-3].type==TRUE_TYPE) {
        long i;
        if ((b=memcalloc(l))==NULL) {
            fclose(file);
            error(WAKE_CANNOT_ALLOCATE);
        }
        switch (wsp[-6].type) {
        case STRING_TYPE: {
            UBYTE MQ *s=wsp[-6].unit.string;
            for (i=0; i<n; i+=3) {
                ((UBYTE MQ *)b)[i]=s[i+2];
                ((UBYTE MQ *)b)[i+1]=s[i+1];
                ((UBYTE MQ *)b)[i+2]=s[i];
            }
            break;
        }
        case USHORTARRAY_TYPE: {
            UWORD MQ *s=wsp[-6].unit.ushortarray;
            for (i=0; i<n; i+=3) {
                ((UWORD MQ *)b)[i]=s[i+2];
                ((UWORD MQ *)b)[i+1]=s[i+1];
                ((UWORD MQ *)b)[i+2]=s[i];
            }
            break;
        } }
    }
    else b=wsp[-6].unit.data;
    if (wsp[-2].type) {
        long i;
        int t=wsp[-3].type==TRUE_TYPE?3:1;
        n=l/wsp[-4].unit.integer;
        switch (wsp[-6].type) {
        case STRING_TYPE:
            for (i=0; i<l; i+=n) hor_diff8((UBYTE MQ *)b+i,n,t);
            break;
        case USHORTARRAY_TYPE:
            for (i=0; i<l; i+=n) hor_diff16((UBYTE MQ *)b+i,n,t);
            break;
        }
        if ((l=LZWcompression(file,b,l))==-1) {
            if (wsp[-3].type==TRUE_TYPE) memfree(b);
            error(WAKE_CANNOT_ALLOCATE);
        }
    }
    else l=bigwrite(file,b,l);
    if (wsp[-3].type==TRUE_TYPE) memfree(b);
    fseek(file,4,SEEK_SET);
    write_long(file,8+l);
    fseek(file,8+l,SEEK_SET);
    write_short(file,(short)(12+(wsp[-3].type==STRING_TYPE)));
    write_tag(file,IMAGEWIDTH,LONG,1,wsp[-5].unit.integer);
    write_tag(file,IMAGELENGTH,LONG,1,wsp[-4].unit.integer);
    write_tag(file,BITSPERSAMPLE,LONG,1,wsp[-6].type==STRING_TYPE?8:16);
    write_tag(file,COMPRESSION,LONG,1,wsp[-2].type?COMPRESSION_LZW:COMPRESSION_NONE);
    write_tag(file,PHOTOMETRIC,LONG,1,wsp[-3].type==TRUE_TYPE?PHOTOMETRIC_RGB:PHOTOMETRIC_PALETTE);
    write_tag(file,STRIPOFFSETS,LONG,1,8);
    write_tag(file,ORIENTATION,LONG,1,ORIENTATION_LEFTTOP);
    write_tag(file,SAMPLESPERPIXEL,LONG,1,wsp[-3].type==TRUE_TYPE?3:1);
    write_tag(file,ROWSPERSTRIP,LONG,1,wsp[-4].unit.integer);
    write_tag(file,STRIPBYTECOUNTS,LONG,1,l);
    write_tag(file,PLANARCONFIG,LONG,1,PLANARCONFIG_CONTIG);
    write_tag(file,PREDICTOR,LONG,1,wsp[-2].type?2:1);
    if (wsp[-3].type==STRING_TYPE)
        write_tag(file,COLORMAP,LONG,wsp[-3].unit.line.length,ftell(file)+12*2);
    write_tag(file,IMAGEDEPTH,LONG,1,1);
    if (wsp[-3].type==STRING_TYPE) {
        long i;
        UBYTE MQ *s=wsp[-3].unit.string;
        n=wsp[-3].unit.line.length;
        for (i=0; i<n; i+=3) write_long(file,s[i]);
        for (i=1; i<n; i+=3) write_long(file,s[i]);
        for (i=2; i<n; i+=3) write_long(file,s[i]);
    }
    fclose(file);
    wsp-=6;
}

WAKE_FUNC pic_funcs[]={
    {"commute",     wk_commute},
    {"compress",        wk_compress},
    {"convolve",        wk_convolve},
    {"decompress",      wk_decompress},
    {"fourier",     wk_fourier},
    {"julia",       wk_julia},
    {"mandelbrot",      wk_mandelbrot},
    {"readtiff",        wk_readtiff},
    {"render",      wk_render},
    {"rootsofunity",    wk_rootsofunity},
    {"writetiff",       wk_writetiff}};

int init_pic(void)
{
    return enter_funcs(pic_funcs,ELEMS(pic_funcs));
}
