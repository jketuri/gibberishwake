
/* wk_seq.h */

#ifdef MAIN
#define VARIABLE
#else
#define VARIABLE extern
#endif

#ifdef AMIGA
#include <exec/memory.h>
#include <exec/devices.h>
#include <devices/timer.h>
#include <devices/serial.h>
#include <devices/audio.h>
#include <devices/narrator.h>
#include <graphics/gfxbase.h>
#include <graphics/gfxmacros.h>
#include <proto/dos.h>
#include <proto/exec.h>
#include <proto/timer.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/translator.h>
#include <clib/alib_protos.h>
#elif defined(_WINDOWS)
#include <dos.h>
#include <time.h>
#include <mmsystem.h>
#elif defined(XLIB)
#include <X11/keysym.h>
#else
#include <dos.h>
#include <time.h>
#include <conio.h>
#include <graphics.h>
#endif
#ifdef __linux__
#include <fcntl.h>
#include <pthread.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <alsa/asoundlib.h>
#elif defined(__APPLE__)
#include <pthread.h>
#include <sys/signal.h>
#define NSIG __DARWIN_NSIG
#include <AudioUnit/AudioUnit.h>
#include <AudioToolbox/AudioToolbox.h>
#include <CoreFoundation/CFRunLoop.h>
#include <CoreMIDI/MIDIServices.h>
#include <CoreServices/CoreServices.h>
#include <unistd.h>
#endif
#include <math.h>

#ifdef _MSDOS
UBYTE MQ *bigmove(UBYTE MQ *target,UBYTE MQ *source,long l);
#endif

#ifdef LATTICE
#define size_t int
#endif

#define KEY_LEFT 4
#define KEY_TOP 14
#define N_KEYS 128
#define N_WHITES 76

#ifdef AMIGA
#define CMD_BUF_SIZE 512

#define WH 58
#define BH 40
#else
#define CMD_BUF_SIZE 256

#define WH 116
#define BH 80
#endif
#define WW 14
#define BW 8
#define BW1 3
#define BW2 4
#define BW3 5
#define KW ((WW + 2) * N_WHITES)
#define DD0 (WW + 2)
#define DD1 (WW - BW1 + 1)
#define DD2 (WW - BW2 + 1)
#define DD3 (WW - BW3 + 1)

#define C_ 0
#define CIS_ 1
#define D_ 2
#define DIS_ 3
#define E_ 4
#define F_ 5
#define FIS_ 6
#define G_ 7
#define GIS_ 8
#define A_ 9
#define AIS_ 10
#define B_ 11

#define PRIME 1
#define SECOND 2
#define THIRD 3
#define FOURTH 5
#define QUINTE 7
#define SIXTH 8
#define SEVENTH 10
#define OCTAVE 12

#define ALL_NOTES_OFF 0x7b
#define NOTE_OFF 0x80
#define NOTE_ON 0x90
#define POLY_KEY_PRESSURE 0xa0
#define CONTROL_CHANGE 0xb0
#define PROGRAM_CHANGE 0xc0
#define CHANNEL_PRESSURE 0xd0
#define PITCH_BEND_CHANGE 0xe0
#define SYSTEM_EXCLUSIVE 0xf0
#define SPECIAL 0xff

#define ALLOCERROR (1<<0)
#define CONVERTING (1<<1)
#define DISPLAYING (1<<2)
#define PROCEEDING (1<<3)
#define INVERTING (1<<4)
#define RECORDING (1<<5)
#define KEYBOARD (1<<6)
#define STARTED (1<<7)
#define ECHOING (1<<8)
#define PLAYING (1<<9)
#define RATING (1<<10)
#define READY (1<<11)
#define SYSEX (1<<12)
#define RAISED (1<<13)

#define CHANNELS (1<<0)
#define PROGRAMS (1<<1)

#define NOTSTOP 0
#define NONSTOP 1
#define AUTOSTOP 2

#define NOEMULATION 0
#define ONLYEMULATION 1
#define ALSOEMULATION 2

typedef struct SEQSHARE
{
	short wh,bh,ww,bw,bw1,bw2,bw3,dd0,dd1,dd2,dd3;
	ULONG delta;
	ULONG cstamp;
	ULONG dstamp;
	ULONG lstamp;
	ULONG nstamp;
	ULONG pstamp;
	UWORD gflag;
	UWORD row_num;
	UBYTE pmode;
	UBYTE mou_note;
	UBYTE mouse_note;
	UBYTE MQ *seq_pos;
	UBYTE MQ *sequence;
	UBYTE MQ *seq_end;
	UBYTE MQ *rec_sequence;
	UBYTE MQ *rec_seq_end;
	UBYTE MQ *rec_seq_pos;
	UBYTE MQ *len_pos;
	UBYTE MQ *msg_pos;
	UBYTE msg_len;
	UBYTE emulation;
	UBYTE key_octave;
	UBYTE *key_table;
	int n_key_elems;
	UBYTE oct_key[N_KEYS];
	UBYTE cmd_buf[CMD_BUF_SIZE];
	UBYTE cnv_old[16][128];
	UBYTE cnv_str[16][128];
	UBYTE vel_str[16][128];
	UBYTE cmd_str[255];
	UBYTE key_str[5];
	UBYTE channels[16];
	UBYTE programs[128];
	UBYTE key_chan;
    UBYTE program_number;
	UWORD note_flag;
	UWORD vel_flag;
	UBYTE conv_flag;
	ULONG rate;
	float note_freq[128];
	int num_chans,drum_pos;
	int disp_width,disp_pos;
	UBYTE key_volume;
#ifdef AMIGA
	VOICE *voice_map[16];
#elif defined(_WINDOWS)
	HWND hwnd;
	HWND key_wnd;
	HDC key_dc;
	HDC dc,dcmem;
	HBITMAP bitmap;
	COLORREF colors[3];
	HBRUSH brushes[3];
	HBRUSH red_brush;
	HPEN red_pen;
	HPEN pens[3];
	HMIDIOUT midiout[2];
	HWAVEOUT waveout;
	MIDIHDR *midihdr[2][2];
	WAVEHDR *wavehdr;
	LPCSTR key_name;
	HANDLE events[2];
	HANDLE instance;
	DWORD stime;
	UBYTE *key_msg;
	ULONG key_len;
	int midihdr_n[2];
	int width,height;
#elif defined(__linux__) || defined(__APPLE__)
	struct timeval stv;
#endif
} SEQSHARE;

#ifdef AMIGA
VARIABLE UBYTE aud_use;
VARIABLE UBYTE ch_mask;
VARIABLE UBYTE ser_ion;
VARIABLE UBYTE pennum[3];
VARIABLE UBYTE recent[16];
VARIABLE UBYTE n_pitchbends;
VARIABLE BYTE pitchbend[4];
VARIABLE UWORD period[4];
VARIABLE struct Window *key_win;
VARIABLE struct RastPort *kwrp;
VARIABLE struct Library *TimerBase;
VARIABLE struct timerequest *timerequest[3];
VARIABLE struct MsgPort *tmrMsgPort;
VARIABLE struct IOExtSer *IOExtSer[3];
VARIABLE struct MsgPort *serMsgPort;
VARIABLE struct IOAudio *IOAudio[4][3];
VARIABLE struct MsgPort *audMsgPort;
VARIABLE struct Library *TranslatorBase;
VARIABLE struct narrator_rb *narrator_rb;
VARIABLE struct MsgPort *narMsgPort;
VARIABLE struct MsgPort *tmrmp;
VARIABLE struct MsgPort *sermp;
VARIABLE struct MsgPort *audmp;
VARIABLE struct Task *procTask;
VARIABLE struct Task *thatTask;
VARIABLE struct timeval timeval;
VARIABLE SEQSHARE *seqshare;
VARIABLE ULONG clock_value;
#ifdef MAIN
UBYTE free_chan[]={0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,255};
UBYTE btms[]={1,2,2*2,2*2*2};
UBYTE key_table[]={
0,0,FIS_+OCTAVE*2,GIS_+OCTAVE*2,AIS_+OCTAVE*2,0,CIS_+OCTAVE*3,DIS_+OCTAVE*3,0,
FIS_+OCTAVE*3,GIS_+OCTAVE*3,AIS_+OCTAVE*3,0,0,0,0,F_+OCTAVE*2,G_+OCTAVE*2,
A_+OCTAVE*2,B_+OCTAVE*2,C_+OCTAVE*3,D_+OCTAVE*3,E_+OCTAVE*3,F_+OCTAVE*3,
G_+OCTAVE*3,A_+OCTAVE*3,B_+OCTAVE*3,C_+OCTAVE*4,0,0,0,0,AIS_,0,CIS_+OCTAVE,
DIS_+OCTAVE,0,FIS_+OCTAVE,GIS_+OCTAVE,AIS_+OCTAVE,0,CIS_+OCTAVE*2,
DIS_+OCTAVE*2,0,0,0,0,0,A_,B_,C_+OCTAVE,D_+OCTAVE,E_+OCTAVE,F_+OCTAVE,
G_+OCTAVE,A_+OCTAVE,B_+OCTAVE,C_+OCTAVE*2,D_+OCTAVE*2,0,0,0,0,0,0,0,
E_+OCTAVE*2};
char *sc="������";
char *cs="[\\][\\]";
char *finn_alph[]=
{
	"AAAA",
	"BEH1EH1",
	"SEH1EH1",
	"DEH1EH1",
	"EH1EH1",
	"EH1F",
	"GEH1EH1",
	"/HAOAO",
	"IH1IH1",
	"YIH1IH1",
	"KAOAO",
	"EH1L",
	"EH1M",
	"EH1N",
	"AOAO",
	"PEH1EH1",
	"KUH1UH1",
	"EH1R",
	"EH1S",
	"TEH1EH1",
	"UH1UH1",
	"VEH1EH1",
	"VEH1EH1",
	"EH1KS",
	"UW1UW1",
	"ZEH1T",
	"AOAO",
	"AE1AE1",
	"ER1ER1"
};
char *finn_phon[]=
{
	" ",
	".",
	"",
	"RIH1SUH1AAIH1TAA",
	"DAOLLAARIH1AA",
	"PRAOSEH1NTTIH1AA",
	"EH1T",
	"",
	" ",
	" ",
	"KEH1RTAAAA",
	"PLUH1S",
	",",
	"MIH1IH1NUH1S",
	".",
	"YAAEH1TTUH1NAA",
	"NAOLLAA",
	"UW1KSIH1",
	"KAAKSIH1",
	"KAOLMEH1",
	"NEH1LYAE1",
	"VIH1IH1SIH1",
	"KUH1UH1SIH1",
	"SEH1IH1TSEH1MAE1N",
	"KAA/HDEH1KSAAN",
	"UW1/HDEH1KSAE1N",
	".",
	".",
	"AON PIH1EH1NEH1MPIH1 KUH1IH1N",
	"AON UW1/HTAE1 KUH1IH1N",
	"AON SUH1UH1REH1MPIH1 KUH1IH1N",
	".",
	"KIH1SSAAN/HAE1NTAE1",
	"AA",
	"B",
	"K",
	"D",
	"EH1",
	"F",
	"G",
	"/H",
	"IH1",
	"Y",
	"K",
	"L",
	"M",
	"N",
	"AO",
	"P",
	"K",
	"R",
	"S",
	"T",
	"UH1",
	"V",
	"V",
	"KS",
	"UW1",
	"Z",
	"AO",
	"AE1",
	"ER1"
};
#else
VARIABLE UBYTE free_chan[];
VARIABLE UBYTE btms[];
VARIABLE UBYTE key_table[];
VARIABLE char *sc;
VARIABLE char *cs;
VARIABLE char *finn_alph[];
VARIABLE char *finn_phon[];
#endif

void show_note(int c,int n,int s);
BOOL start_note(VOICE *voice,UBYTE note,UBYTE volume);
void clear_note(VOICE *voice,UBYTE note);
void bend_pitch(UWORD amount,UBYTE channel);
void emulate(UBYTE *msg,UBYTE len);
void draw_key(int b,int n);
int trans_finnish(char *char_string,char *phon_buffer,int buffer_len);
#elif defined(__APPLE__)
VARIABLE UBYTE status;
VARIABLE UBYTE chan_progs[16];
VARIABLE int device_nr;
VARIABLE int nr_voices;
VARIABLE char *midiindev;
VARIABLE char *midioutdev[2];
VARIABLE MIDIClientRef midiClientRef;
VARIABLE MIDIEndpointRef midiInEndpointRef;
VARIABLE MIDIEndpointRef midiOutEndpointRef;
VARIABLE MIDIPortRef midiInPortRef;
VARIABLE MIDIPortRef midiOutPortRef;
#ifdef MAIN
SEQSHARE *seqshare=(SEQSHARE *)-1;
VARIABLE AUGraph auGraph = NULL;
VARIABLE AudioUnit audioUnit = NULL;
pthread_t taskproc_id=(pthread_t)0;
#else
VARIABLE SEQSHARE *seqshare;
VARIABLE int rawmidi_in_handle;
VARIABLE int rawmidi_out_handle;
VARIABLE AUGraph auGraph;
VARIABLE AudioUnit audioUnit;
VARIABLE pthread_t taskproc_id;
#endif
#elif defined(__linux__)
VARIABLE UBYTE status;
VARIABLE UWORD *free_cells;
VARIABLE UBYTE *cell_progs;
VARIABLE UBYTE chan_progs[16];
VARIABLE int device_nr;
VARIABLE int nr_voices;
VARIABLE snd_seq_event_t event;
VARIABLE int client_id;
VARIABLE int port_in_id, port_out_id;
VARIABLE char *midiindev;
VARIABLE char *midioutdev[2];
#ifdef MAIN
SEQSHARE *seqshare=(SEQSHARE *)-1;
VARIABLE snd_rawmidi_t *rawmidi_in_handle=(snd_rawmidi_t *)NULL;
VARIABLE snd_rawmidi_t *rawmidi_out_handle=(snd_rawmidi_t *)NULL;
VARIABLE snd_seq_t *seq_handle=(snd_seq_t *)NULL;
pthread_t taskproc_id=(pthread_t)0;
int shmid=-1;
#else
VARIABLE SEQSHARE *seqshare;
VARIABLE snd_rawmidi_t *rawmidi_in_handle;
VARIABLE snd_rawmidi_t *rawmidi_out_handle;
VARIABLE snd_seq_t *seq_handle;
VARIABLE pthread_t taskproc_id;
VARIABLE int shmid;
#endif
#elif defined(_WINDOWS)
extern HANDLE instance;
extern HWND hwnd;

VARIABLE char sample_type;
VARIABLE WAVEFORMATEX waveformat;
VARIABLE MIDIHDR *midiinhdr;
VARIABLE HMIDIIN midiin;
VARIABLE UBYTE status;
VARIABLE UINT timerid;
VARIABLE UINT midiindev;
VARIABLE SEQSHARE *seqshare;
#ifdef MAIN
LPCSTR key_name="Key";
UBYTE key_table[]={0,0,0,0,0,0,0,0,CIS_+OCTAVE*4,E_+OCTAVE*2,0,0,0,D_+OCTAVE*4,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
GIS_+OCTAVE*3,0,FIS_+OCTAVE*2,GIS_+OCTAVE*2,AIS_+OCTAVE*2,0,CIS_+OCTAVE*3,
DIS_+OCTAVE*3,0,FIS_+OCTAVE*3,0,0,0,0,0,0,0,AIS_,F_+OCTAVE,D_+OCTAVE,CIS_+OCTAVE,
A_+OCTAVE*2,DIS_+OCTAVE,0,FIS_+OCTAVE,F_+3*OCTAVE,GIS_+OCTAVE,AIS_+OCTAVE,0,
A_+OCTAVE,G_+OCTAVE,G_+OCTAVE*3,A_+OCTAVE*3,F_+OCTAVE*2,B_+OCTAVE*2,0,C_+OCTAVE*3,
E_+OCTAVE*3,E_+OCTAVE,G_+OCTAVE*2,C_+OCTAVE,D_+OCTAVE*3,B_,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,C_+OCTAVE*4,AIS_+OCTAVE*3,B_+OCTAVE,D_+OCTAVE*2,C_+OCTAVE*2,0,
CIS_+OCTAVE*2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
B_+OCTAVE*3,DIS_+OCTAVE*2,0,0,0,A_};
VARIABLE UINT midioutdev[2] = {1, 0};
#else
VARIABLE LPCSTR key_name;
VARIABLE UBYTE key_table[];
VARIABLE UINT midioutdev[2];
#endif
#else
VARIABLE UBYTE pennum[3];
VARIABLE UBYTE status;
#endif
#ifdef XLIB
VARIABLE int pennum[4];
VARIABLE GC gck;
VARIABLE Window kwnd;
VARIABLE XImage *kimage;
#ifdef MAIN
#ifdef __APPLE__
UBYTE key_table[]={
    0,0,0,0,0,0,0,0,
    /* A */ AIS_,
    /* S */ 0,
    /* D */ CIS_+OCTAVE,
    /* F */ DIS_+OCTAVE,
    /* H */ FIS_+OCTAVE,
    /* G */ 0,
    /* Z */ B_,
    /* X */ C_+OCTAVE,
    /* C */ D_+OCTAVE,
    /* V */ E_+OCTAVE,
    0,
    /* B */ F_+OCTAVE,
    /* Q */ E_+OCTAVE*2,
    /* W */ F_+OCTAVE*2,
    /* E */ G_+OCTAVE*2,
    /* R */ A_+OCTAVE*2,
    /* Y */ C_+OCTAVE*3,
    /* T */ B_+OCTAVE*2,
    /* 1 */ DIS_+OCTAVE*2,
    /* 2 */ 0,
    /* 3 */ FIS_+OCTAVE*2,
    /* 4 */ GIS_+OCTAVE*2,
    /* 6 */ 0,
    /* 5 */ AIS_+OCTAVE*2,
    /* � */ AIS_+OCTAVE*3,
    /* 9 */ 0,
    /* 7 */ CIS_+OCTAVE*3,
    /* + */ GIS_+OCTAVE*3,
    /* 8 */ DIS_+OCTAVE*3,
    /* 0 */ FIS_+OCTAVE*3,
    /* � */ B_+OCTAVE*3,
    /* O */ F_+OCTAVE*3,
    /* U */ D_+OCTAVE*3,
    /* � */ A_+OCTAVE*3,
    /* I */ E_+OCTAVE*3,
    /* P */ G_+OCTAVE*3,
    /* n */ C_+OCTAVE*4,
    /* L */ 0,
    /* J */ GIS_+OCTAVE,
    /* � */ DIS_+OCTAVE*2,
    /* K */ AIS_+OCTAVE,
    /* � */ CIS_+OCTAVE*2,
    /* ' */ 0,
    /* , */ B_+OCTAVE,
    /* - */ D_+OCTAVE*2,
    /* N */ G_+OCTAVE,
    /* M */ A_+OCTAVE,
    /* . */ C_+OCTAVE*2,
    /* t */ 0,
    /*   */ 0,
    /* < */ A_};
#else
UBYTE key_table[]={0,0,0,0,0,0,0,0,0,0,DIS_+OCTAVE*2,0,FIS_+OCTAVE*2,
GIS_+OCTAVE*2,AIS_+OCTAVE*2,0,CIS_+OCTAVE*3,DIS_+OCTAVE*3,0,FIS_+OCTAVE*3,
GIS_+OCTAVE*3,AIS_+OCTAVE*3,0,0,E_+OCTAVE*2,F_+OCTAVE*2,G_+OCTAVE*2,A_+OCTAVE*2,
B_+OCTAVE*2,C_+OCTAVE*3,D_+OCTAVE*3,E_+OCTAVE*3,F_+OCTAVE*3,G_+OCTAVE*3,
A_+OCTAVE*3,B_+OCTAVE*3,C_+OCTAVE*4,0,AIS_,0,CIS_+OCTAVE,DIS_+OCTAVE,0,
FIS_+OCTAVE,GIS_+OCTAVE,AIS_+OCTAVE,0,CIS_+OCTAVE*2,DIS_+OCTAVE*2,0,0,0,B_,
C_+OCTAVE,D_+OCTAVE,E_+OCTAVE,F_+OCTAVE,G_+OCTAVE,A_+OCTAVE,B_+OCTAVE,
C_+OCTAVE*2,D_+OCTAVE*2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,A_};
#endif
#else
VARIABLE UBYTE key_table[];
#endif
#endif

extern UBYTE MQ *getvarnum(UBYTE MQ *pos,ULONG *num);
extern UBYTE MQ *putvarnum(UBYTE MQ *pos,ULONG num);
extern void free_voice(void *voice);
extern void halt_seq(void);
extern void emulate(UBYTE *msg,UBYTE len);
#ifdef _WINDOWS
void wait_midi(int n);
void midi_out(struct SEQSHARE *seqshare,UBYTE *data,ULONG length,int n);
#else
void midi_out(UBYTE *data,ULONG length);
#endif
#ifdef AMIGA
extern void allocate(void);
extern void free_audio(void);
extern void allocate(void);
#elif defined(_WINDOWS)
extern int handle_midiin(UINT message,LPARAM lParam);
extern LONG EXPORT FAR PASCAL KeyProc(HWND hwnd,UINT message,
WPARAM wParam,LPARAM lParam);
#elif defined(XLIB)
int do_key_event(XEvent *event);
#endif
void taskproc_clean(void);
int taskproc_init(void);
#ifdef _DCC
extern __geta4 void taskproc(void);
#elif defined(LATTICE)
extern __saveds void taskproc(void);
#elif defined(_WINDOWS)
extern void EXPORT CALLBACK taskproc(UINT wTimerID,UINT wMsg,DWORD dwUser,
DWORD dw1,DWORD dw2);
#else
extern void *taskproc(void *data);
#endif
