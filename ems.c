
#include <dos.h>
#include <alloc.h>
#include "ems.h"

#define EMS_INT 0x67

#define EMS_ADDRESS(offset,log_page) \
(unsigned long)1<<31|(unsigned long)offset<<16|(unsigned long)log_page

typedef struct EMS_CHUNK
{
	unsigned long length;
	unsigned long address;
	unsigned long address1;
	struct EMS_CHUNK *p,*n;
} EMS_CHUNK;

char ems_on;
unsigned char far *ems_frame_ptr;
unsigned short ems_handle;
EMS_CHUNK *ems_chunk_list,*ems_chunk_last;

void ems_startup(void)
{
	char *p,*q;
	union REGS r;
	struct SREGS s;
	char *en="EMMXXXX0";
	r.x.ax=0x3567;
	intdosx(&r,&r,&s);
	p=(char *)MK_FP(s.es,10);
	q=en;
	while (*q&&*p++==*q++);
	if (*q) return;
	r.h.ah=0x40;
	int86(EMS_INT,&r,&r);
	if (r.h.ah) return;
	r.h.ah=0x41;
	int86(EMS_INT,&r,&r);
	if (r.h.ah) return;
	ems_frame_ptr=(unsigned char far *)MK_FP(r.x.bx,0);
	r.h.ah=0x42;
	int86(EMS_INT,&r,&r);
	if (r.h.ah) return;
	r.h.ah=0x43;
	int86(EMS_INT,&r,&r);
	if (r.h.ah) return;
	ems_chunk_list=(EMS_CHUNK *)calloc(1,sizeof(EMS_CHUNK));
	ems_chunk_list->length=r.x.bx<<4;
	ems_on=-1;
}

#pragma startup ems_startup

void ems_exit(void)
{
	union REGS r;
	r.h.ah=0x45;
	r.x.dx=ems_handle;
	int86(EMS_INT,&r,&r);
}

#pragma exit ems_exit

static unsigned long ems_inc_address(unsigned long address,unsigned long inc)
{
	unsigned short log_page,offset;
	log_page=address&0xffff;
	offset=address>>16&0x7fff;
	inc+=offset;
	log_page+=inc>>14;
	offset=inc&0x3fff;
	return EMS_ADDRESS(offset,log_page);
}

static void ems_free_chunk(EMS_CHUNK *chunk)
{
	if (chunk->p) chunk->p->n=chunk->n;
	else ems_chunk_list=chunk->n;
	if (chunk->n) chunk->n->p=chunk->p;
	else ems_chunk_last=chunk->p;
	free(chunk);
}

static void ems_place_chunk(EMS_CHUNK *chunk)
{
	EMS_CHUNK *ck;
	ck=chunk->p;
	while (ck&&ck->length>chunk->length) ck=ck->p;
	if (ck!=chunk->p)
	{
		if (chunk->p) chunk->p->n=chunk->n;
		if (chunk->n) chunk->n->p=chunk->p;
		if (ck)
		{
			if (ck->n) ck->n->p=chunk;
			chunk->n=ck->n;
			ck->n=chunk;
			chunk->p=ck;
		}
		else
		{
			ems_chunk_list->p=chunk;
			chunk->n=ems_chunk_list;
			ems_chunk_list=chunk;
		}
		return;
	}
	ck=chunk->n;
	while (ck&&ck->length<chunk->length) ck=ck->n;
	if (ck!=chunk->n)
	{
		if (chunk->p) chunk->p->n=chunk->n;
		if (chunk->n) chunk->n->p=chunk->p;
		if (ck)
		{
			if (ck->p) ck->p->n=chunk;
			chunk->n=ck;
			chunk->p=ck->p;
			ck->p=chunk;
		}
		else
		{
			ems_chunk_last->n=chunk;
			chunk->p=ems_chunk_last;
			ems_chunk_last=chunk;
		}
	}
}

EMS ems_alloc(unsigned long size)
{
	EMS ems;
	EMS_CHUNK *ck=ems_chunk_list;
	while (ck)
	{
		if (ck->length>=size)
		{
			ems.address=ck->address;
			if (ck->length-=size)
			{
				ck->address=ems_inc_address(ck->address,size);
				ems_place_chunk(ck);
			}
			else ems_free_chunk(ck);
			return ems;
		}
		ck=ck->n;
	}
	ems.address=(unsigned long)farmalloc(size);
	return ems;
}

void ems_release(EMS ems,unsigned long length)
{
	if (ems.address&1<<31)
	{
		EMS_CHUNK *ck=ems_chunk_list,*ck1;
		unsigned long address1=ems_inc_address(ems.address,length);
		while (ck)
		{
			if (ck->address==address1)
			{
				ck->length+=length;
				ck1=ems_chunk_list;
				while (ck1)
				{
					if (ck1->address1==ems.address)
					{
						ck->length+=ck1->length;
						ck->address=ck1->address;
						ems_free_chunk(ck1);
						ems_place_chunk(ck);
						return;
					}
					ck1=ck1->n;
				}
				ck->address=ems.address;
				ems_place_chunk(ck);
				return;
			}
			ck=ck->n;
		}
		ck=ems_chunk_list;
		while (ck)
		{
			if (ck->address1==ems.address)
			{
				ck->length+=length;
				ck->address1=address1;
				ems_place_chunk(ck);
				return;
			}
			ck=ck->n;
		}
		ck=(EMS_CHUNK *)calloc(1,sizeof(EMS_CHUNK));
		ck->length=length;
		ck->address=ems.address;
		ck->address1=address1;
		ck1=ems_chunk_list;
		while (ck1&&ck1->length<length) ck1=ck1->n;
		if (ck1)
		{
			if (ck1->p) ck1->p->n=ck;
			ck->n=ck1;
			ck->p=ck1->p;
			ck1->p=ck;
		}
		else
		{
			if (ems_chunk_last)
			{
				ems_chunk_last->n=ck;
				ck->p=ems_chunk_last;
				ems_chunk_last=ck;
			}
			else ems_chunk_last=ck;
			if (!ems_chunk_list) ems_chunk_list=ck;
		}
		return;
	}
	farfree((void far *)ems.address);
}

unsigned long ems_availmem(void)
{
	EMS_CHUNK *ck=ems_chunk_list;
	unsigned long total=0;
	while (ck)
	{
		total+=ck->length;
		ck=ck->n;
	}
	return total+coreleft();
}