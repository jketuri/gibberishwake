
OD = /o/
BIN = /tools/wk
SRCS = wk.c wake.c wk_fun.c wk_fun2.c wk_gra.c wk_gra2.c wk_log.c wk_mat.c\
 wk_mat2.c wk_pic.c wk_seq.c wk_seq2.c
OBJS = $(OD)wk.o $(OD)wake.o $(OD)wk_fun.o $(OD)wk_fun2.o $(OD)wk_gra.o\
 $(OD)wk_gra2.o $(OD)wk_log.o $(OD)wk_mat.o $(OD)wk_mat2.o $(OD)wk_pic.o\
 $(OD)wk_seq.o $(OD)wk_seq2.o
F_BIN = lib:file_acs.lib
F_SRCS = f_access.c f_addkey.c f_getkey.c f_delkey.c f_setkey.c
F_OBJS = $(OD)f_access.o $(OD)f_addkey.o $(OD)f_getkey.o $(OD)f_delkey.o\
 $(OD)f_setkey.o

$(BIN) : $(OBJS) $(F_OBJS) lib:file_acs.lib dlib:m.lib
	blink with wkl.w
	list $(BIN)

$(F_BIN) : $(F_OBJS)
	oml lib:file_acs.lib r $(F_OBJS)

$(OBJS) $(F_OBJS) : $(SRCS) $(F_SRCS)
	lc -d3 -inn:sas/include -inn:include -m0 -o%(left) %(*.c)
