
/* SMUS to Seq file format converter */

#ifdef AMIGA
#include <exec/types.h>
#else
#define BYTE char
#define UBYTE unsigned char
#define UWORD unsigned short
#define ULONG unsigned long
#endif

#include <stdio.h>
#include <stdlib.h>

#define MAX_TRACKS 255

#define NOTE_OFF 0x80
#define NOTE_ON 0x90
#define POLY_KEY_PRESSURE 0xa0
#define CONTROL_CHANGE 0xb0
#define PROGRAM_CHANGE 0xc0
#define CHANNEL_PRESSURE 0xd0
#define PITCH_BEND_CHANGE 0xe0
#define TERMINATOR 0xfd

typedef struct {
	UWORD tempo;
	UBYTE volume;
	UBYTE ctTrack;
} SScoreHeader;

#define INS1_Name 0
#define INS1_MIDI 1

typedef struct {
	UBYTE reg;
	UBYTE type;
	UBYTE data1,data2;
	UBYTE *name;
} RefInstrument;

#define SID_FirstNote	0
#define SID_LastNote	127
#define SID_Rest	128
#define SID_Instrument	129
#define SID_TimeSig	130
#define SID_KeySig	131
#define SID_Dynamic	132
#define SID_MIDI_Chnl	133
#define SID_MIDI_Preset	134
#define SID_Tempo	136
#define SID_Mark	255

typedef union
{
	struct {
		UBYTE ID,data;
	} SEvent;
	struct {
		unsigned
		tone	:8,
		chord	:1,
		tieOut	:1,
		nTuplet	:2,
		dot	:1,
		division:3;
	} SNote;
} ScoreRec;

ScoreRec *trackrecs[MAX_TRACKS];
UWORD eventn[MAX_TRACKS],events[MAX_TRACKS];
ULONG durations[MAX_TRACKS];
UBYTE channels[MAX_TRACKS];
UBYTE volumes[MAX_TRACKS];
UWORD tempos[MAX_TRACKS];
BYTE noteson[MAX_TRACKS];
UBYTE buf[255];

void cancel(char *s,int r)
{
	puts(s);
	printf("r=%d\n",r);
	exit(r);
}

int check(FILE *f,char *s)
{
	char *p=s;
	while (*p&&fgetc(f)==*p) p++;
	fseek(f,s-p-(*p!=0),SEEK_CUR);
	return *p;
}

int match(FILE *f,char *p)
{
	while (*p&&fgetc(f)==*p) p++;
	return *p;
}

int skip(FILE *f,char *s)
{
	ULONG ckSize;
	if (check(f,s)) return 1;
	fseek(f,4,SEEK_CUR);
	fread(&ckSize,sizeof ckSize,1,f);
	fseek(f,ckSize+ckSize%2,SEEK_CUR);
	return 0;
}

ULONG duration(UWORD tempo,unsigned division,unsigned dot,unsigned tuplet)
{
	double d,t;
	d=60000.0/((double)tempo/(double)(512>>division));
	if (tuplet)
	{
		t=(double)tuplet*2.0;
		d*=t/(t+1.0);
	}
	if (dot) d*=1.5;
	return (ULONG)(d+.5);
}

void writevarnum(FILE *f,ULONG num)
{
	ULONG m;
	m=num&0x7f;
	while (num>>=7)
	{
		m<<=8;
		m|=0x80;
		m+=num&0x7f;
	}
	for (;;)
	{
		fputc(m&0xff,f);
		if (m&0x80) m>>=8;
		else break;
	}
}

void main(int argc,char *argv[])
{
	char g;
	FILE *f;
	SScoreHeader shdr;
	int i,l,n,tracks=0;
	char *inv_form="Invalid format";
	ULONG ckSize,d,s,pstamp,stamp;
	UBYTE b,status=0;
	if (argc<3)
	{
		printf("Usage: %s <SMUS file> ... <seq file>\n",argv[0]);
		exit(0);
	}
	for (i=1; i<argc-1; i++)
	{
		if (!(f=fopen(argv[i],"rb"))) cancel("file not found",1);
		if (match(f,"FORM")||fread(&ckSize,sizeof ckSize,1,f)!=1||
		match(f,"SMUSSHDR")||fread(&ckSize,sizeof ckSize,1,f)!=1||
		fread(&shdr,sizeof shdr,1,f)!=1) cancel(inv_form,2);
		while (!skip(f,"NAME")||!skip(f,"(c) ")||!skip(f,"AUTH")||
		!skip(f,"IRev")||!skip(f,"ANNO")||!skip(f,"SNX1")||
		!skip(f,"BIAS")||!skip(f,"INS1"));
		n=tracks;
		tracks+=shdr.ctTrack;
		if (tracks>16)
		{
			printf("ignored further tracks\n");
			tracks=16;
		}
		while (n<tracks)
		{
			if (match(f,"TRAK")) cancel("TRAK not found",3);
			if (fread(&ckSize,sizeof ckSize,1,f)!=1)
			cancel(inv_form,4);
			events[n]=ckSize/sizeof(ScoreRec);
			trackrecs[n]=(ScoreRec *)malloc(events[n]*
			sizeof(ScoreRec));
			if (!trackrecs[n]) cancel("Memory overflow",5);
			if (fread(trackrecs[n],sizeof(ScoreRec),
			events[n],f)!=events[n])
			cancel(inv_form,6);
			volumes[n]=shdr.volume;
			tempos[n]=shdr.tempo;
			n++;
		}
		fclose(f);
		if (tracks==16) break;
	}
	f=fopen(argv[argc-1],"wb");
	if (!f) cancel("cannot open file",7);
	pstamp=stamp=0;
	l=0;
	for (;;)
	{
		d=0;
		g=0;
		for (n=0; n<tracks; n++)
		if (events[n])
		{
			if (durations[n]==0)
			{
				if (noteson[n])
				{
					b=NOTE_ON|channels[n];
					if (b!=status) buf[l++]=status=b;
					i=eventn[n]-1;
					while (i>=0&&
					trackrecs[n][i].SEvent.ID>127) i--;
					do
					{
						buf[l++]=
						trackrecs[n][i].SNote.tone;
						buf[l++]=0;
					}
					while (--i>=0&&
					trackrecs[n][i].SEvent.ID<128&&
					trackrecs[n][i].SNote.chord);
					noteson[n]=0;
				}
				while (eventn[n]<events[n]&&!durations[n]) {
				if (trackrecs[n][eventn[n]].SEvent.ID<128) {
				noteson[n]=-1;
				b=NOTE_ON|channels[n];
				if (b!=status) buf[l++]=status=b;
				durations[n]=duration(tempos[n],
				trackrecs[n][eventn[n]].SNote.division,
				trackrecs[n][eventn[n]].SNote.dot,
				trackrecs[n][eventn[n]].SNote.nTuplet);
				do {
				buf[l++]=trackrecs[n][eventn[n]].SNote.tone;
				buf[l++]=volumes[n];
				}
				while (trackrecs[n][eventn[n]].SEvent.ID<128&&
				trackrecs[n][eventn[n]].SNote.chord&&
				++eventn[n]<events[n]);
				if (eventn[n]==events[n]) cancel(inv_form,8);
				while (trackrecs[n][eventn[n]].SNote.tieOut&&
				++eventn[n]<events[n]) {
				durations[n]+=duration(tempos[n],
				trackrecs[n][eventn[n]].SNote.division,
				trackrecs[n][eventn[n]].SNote.dot,
				trackrecs[n][eventn[n]].SNote.nTuplet);
				while (trackrecs[n][eventn[n]].SNote.chord)
				if (++eventn[n]==events[n])
				cancel(inv_form,9);
				}
				if (eventn[n]==events[n]) cancel(inv_form,10);
				}
				else switch
				(trackrecs[n][eventn[n]].SEvent.ID)
				{
				case SID_Rest:
				durations[n]=duration(tempos[n],
				trackrecs[n][eventn[n]].SNote.division,
				trackrecs[n][eventn[n]].SNote.dot,
				trackrecs[n][eventn[n]].SNote.nTuplet);
				break;
				case SID_Instrument:
				channels[n]=
				trackrecs[n][eventn[n]].SEvent.data;
				break;
				case SID_Dynamic:
				volumes[n]=
				trackrecs[n][eventn[n]].SEvent.data;
				break;
				case SID_Tempo:
				tempos[n]=(UWORD)((double)tempos[n]*
				(double)(trackrecs[n][eventn[n]].SEvent.data+
				1)/128.0+.5);
				break;
				}
				eventn[n]++;
				}
				if (durations[n]) g=-1;
				else events[n]=0;
			}
			else g=-1;
			if (g) d=d?d<durations[n]?d:durations[n]:durations[n];
		}
		for (n=0; n<tracks; n++)
		if (durations[n]) durations[n]-=d;
		stamp+=d;
		if (stamp!=pstamp||!g)
		{
			if (l)
			{
				writevarnum(f,stamp-pstamp);
				fputc(l,f);
				fwrite(buf,l,1,f);
				pstamp=stamp;
				status=0;
				l=0;
			}
			if (!g) break;
		}
	}
	writevarnum(f,stamp-pstamp);
	fputc(1,f);
	fputc(TERMINATOR,f);
	fclose(f);
}
