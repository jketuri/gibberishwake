
/* wk_gra.h */

#ifdef MAIN
#define VARIABLE
#else
#define VARIABLE extern
#endif

#include <math.h>

#ifdef AMIGA
#include <exec/memory.h>
#include <devices/console.h>
#include <devices/inputevent.h>
#include <graphics/gfxbase.h>
#include <graphics/gfxmacros.h>
#include <proto/dos.h>
#include <proto/exec.h>
#include <proto/layers.h>
#include <proto/console.h>
#include <proto/graphics.h>
#include <proto/diskfont.h>
#include <proto/intuition.h>
#include <clib/alib_protos.h>

#ifdef _DCC
#define ARGQUAL __stkargs
#else
#define ARGQUAL
#endif

typedef UBYTE Masking;

#define mskNone	0
#define mskHasMask 1
#define mskHasTransparentColor 2
#define mskLasso 3

typedef UBYTE Compression;

#define cmpNone	0
#define cmpByteRun1 1

typedef struct {
	UWORD w,h;
	WORD  x,y;
	UBYTE nPlanes;
	Masking masking;
	Compression compression;
	UBYTE pad1;
	UWORD transparentColor;
	UBYTE xAspect,yAspect;
	WORD  pageWidth,pageHeight;
} BMHD;

VARIABLE struct Window *window;
VARIABLE struct RastPort *rastport;
VARIABLE struct BitMap *bbitmap,*dbitmap;
VARIABLE struct ViewPort *viewport;
VARIABLE struct ColorMap *colormap;
VARIABLE struct BitMap *bitmap;
VARIABLE struct RastPort *rastport2;
VARIABLE struct Layer_Info *layerinfo;
VARIABLE struct Layer *layer;
VARIABLE struct TextFont *textfont;
VARIABLE struct Library *ConsoleDevice;
VARIABLE struct IOStdReq *conIOStdReq;
VARIABLE struct InputEvent *inputevent;
VARIABLE RGBVAL rgbbackcolor,rgboutcolor;
VARIABLE UWORD *palette;
VARIABLE UBYTE depth;
VARIABLE int color_n;
#ifdef MAIN
VARIABLE RGBVAL rgbcolor={15,15,15};
VARIABLE int rule=1;
#else
VARIABLE RGBVAL rgbcolor;
VARIABLE int rule;
#endif

extern struct GfxBase *GfxBase;

#elif defined(VGALIB)

#include <vga.h>
#include <vgagl.h>

#define RGB2GRAY(r,g,b) ((.3*(float)r+.59*(float)g+.11*(float)b)/255.0)

VARIABLE int bytesperpixel;
VARIABLE int color_n,x_coord,y_coord;
VARIABLE RGBVAL rgbbackcolor,rgboutcolor;
VARIABLE unsigned char *fontbmap;
VARIABLE int fontsize;
#ifdef MAIN
VARIABLE char *wake_name="Wake";
VARIABLE RGBVAL rgbcolor={0xff,0xff,0xff};
VARIABLE char exposed=1;
VARIABLE int rule=1;
#else
VARIABLE char *wake_name;
VARIABLE RGBVAL rgbcolor;
VARIABLE char exposed;
VARIABLE int rule;
#endif

#elif defined(_WINDOWS)

extern HANDLE instance;
extern HDC hdc;

VARIABLE HRGN rgn;
VARIABLE HWND window;
VARIABLE HBITMAP bitmap;
VARIABLE HFONT dfont,font;
VARIABLE HPEN dpen,pen,out_pen;
VARIABLE HDC dc,ddc,dcalt,dcmem;
VARIABLE HPALETTE dpalette,palette;
VARIABLE HBRUSH dbrush,cbrush,brush;
VARIABLE LOGPEN logpen;
VARIABLE RGBVAL rgbbackcolor,rgboutcolor;
VARIABLE COLORREF backcolor,out_color;
VARIABLE unsigned line_size,row_size;
VARIABLE int color_n,n_planes;
VARIABLE char paletteBased;
VARIABLE POINT pt;
#ifdef MAIN
VARIABLE LPCSTR wake_name="Wake";
VARIABLE RGBVAL rgbcolor={0xff,0xff,0xff};
#else
VARIABLE LPCSTR wake_name;
VARIABLE RGBVAL rgbcolor;
#endif
#ifdef _WIN32
#define MoveTo(dc,x,y) MoveToEx(dc,x,y,NULL)
#else
VARIABLE DWORD dw;
#define GetCurrentPositionEx(dc,p) { dw=GetCurrentPosition(dc); (p)->x=LOWORD(dw); (p)->y=HIWORD(dw); }
#endif
#elif defined(XLIB)

#define RGB2GRAY(r,g,b) ((.3*(float)r+.59*(float)g+.11*(float)b)/65535.0)

VARIABLE Display *display;
VARIABLE Drawable drawable;
VARIABLE Window window;
VARIABLE Region region;
VARIABLE int screen;
VARIABLE int x_coord,y_coord;
VARIABLE Pixmap pixmap;
VARIABLE GC gc,gcb,gcp;
VARIABLE unsigned long serial;
VARIABLE unsigned long pixel_min,pixel_max;
VARIABLE float xdensity,ydensity;
VARIABLE char xerror;
VARIABLE char opaque;
VARIABLE char dither;
VARIABLE RGBVAL rgbbackcolor,rgboutcolor;
VARIABLE XFontStruct *font_ptr;
VARIABLE Font def_font;
VARIABLE Pixmap bitmap;
VARIABLE Pixmap *tiles;
#ifdef MAIN
VARIABLE char *wake_name="Wake";
VARIABLE RGBVAL rgbcolor={0xffff,0xffff,0xffff};
#else
VARIABLE char *wake_name;
VARIABLE RGBVAL rgbcolor;
#endif
#else
#include <graphics.h>
#include <bios.h>

VARIABLE RGBVAL *rgb_map;
VARIABLE struct fillsettingstype fillsettings;
VARIABLE struct linesettingstype linesettings;
VARIABLE char *graphpath,graphready;
VARIABLE int graphdriver=DETECT,graphmode;
VARIABLE unsigned bitmap_size,line_size,row_size;
VARIABLE int mode,out_color,color_n,n_planes;
VARIABLE char far *bitmap;
VARIABLE RGBVAL rgbbackcolor,rgboutcolor;
#ifdef MAIN
VARIABLE RGBVAL rgbcolor={255,255,255};
VARIABLE int rule=1;
#else
VARIABLE RGBVAL rgbcolor;
VARIABLE int rule;
#endif
#endif

#define A 0
#define B 1
#define C 2
#define D 3
#define Tx 4
#define Ty 5
#define AREA (1<<0)
#define GENERAL (1<<1)
#define NUM_COUNTS 16
#define INC_COUNTS 8
#define NUM_POINTS 4096
#define INC_POINTS 2048
#define XCOORD(x,y) (int)(trans[A]*x+trans[C]*y+trans[Tx]+.5)
#define YCOORD(x,y) (int)(trans[B]*x+trans[D]*y+trans[Ty]+.5)
#define XRELAT(x,y) (int)((xrou=(float)(trans[A]*x+trans[C]*y))>=.0?xrou+.5:xrou-.5)
#define YRELAT(x,y) (int)((yrou=(float)(trans[B]*x+trans[D]*y))>=.0?yrou+.5:yrou-.5)
#define XARC(a) (int)((xrou=(float)(r*cos(a)))>=.0?xrou+.5:xrou-.5)
#define YARC(a) (int)((yrou=(float)(r*sin(a)))>=.0?yrou+.5:yrou-.5)
#define SHIFTS 9
#define ONE (1<<SHIFTS)
#define S10(x) ((x)>>SHIFTS)
#define S20(x) ((x)>>(2*SHIFTS))
#define MUL3(x) ((x)+((x)<<1))
#ifdef LATTICE
#define size_t int
#endif

VARIABLE UBYTE gflag;
VARIABLE float xrou,yrou;
VARIABLE int aspectx,aspecty;
VARIABLE int n_colors;
VARIABLE int width,height;
VARIABLE float trans[6];
VARIABLE POINT first;
VARIABLE POINT MQ *points;
VARIABLE int n_points,point_o;
VARIABLE int *counts,n_counts;
#ifdef MAIN
VARIABLE int num_points=NUM_POINTS,no_counts=NUM_COUNTS;
#else
VARIABLE int num_points,no_counts;
#endif

extern void fill_area(void);
extern void *make_image(int width,int height);
extern void setpen(void);
extern void setbackbrush(void);
extern void mul_mat(float *mat,float *mat1,float *mat2);
extern void inv_mat(float *mat,float *mat1);
extern void inv_trans(float *x,float *y,float *mat);
extern void inv_dtrans(float *dx,float *dy,float *mat);
extern void add_point(int x,int y);
extern void close_poly(void);
extern void store_poly(void);
extern void start_poly(int x,int y);
extern void start_rpoly(int x,int y);
extern void free_image(void *image);
extern void free_state(STATE *state);
#ifdef AMIGA
extern int handle_imsg(struct IntuiMessage *imsg);
extern LONG look_image(char *name,BMHD *bmhd);
extern int read_scanline(LONG file,struct BitMap *bp,int offset,int rrl);
extern int read_packed_scanline(LONG file,struct BitMap *bp,int offset,int rrl);
#elif defined(_WINDOWS)
extern int handle_msg();
extern LONG EXPORT FAR PASCAL WakeProc(HWND hwnd,UINT message,
WPARAM wParam,LPARAM lParam);
extern UBYTE MQ *read_bitmap(char *name,BITMAPINFO **bmi);
#elif defined(XLIB)
extern int handle_event(XEvent *event);
extern void free_tiles(void);
extern int setup_halftone(float screenangle,float screenfreq);
extern unsigned long alloc_color(UWORD r,UWORD g,UWORD b);
extern void set_dithered_fg(float gray);
extern void set_dithered_bg(float gray);
#endif
#ifdef _WINDOWS
extern COLORREF setrgb(int,int,int,BOOL);
#else
extern int setrgb(int,int,int);
#endif
#if defined(_MSDOS)&&!defined(_WINDOWS)
void scroll_screen(int dx,int dy,int x0,int y0,int x1,int y1);
#elif defined(XLIB)
void scroll_area(Display *dsp,Drawable drw,GC g,int dx,int dy,int x,int y,
int h,int v);
#endif
