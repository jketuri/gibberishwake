
/* wk_mat2.c */

#include "wake.h"
#include "wk_mat.h"

REGQUAL ELEMENT *divf_float_float(register ELEMENT *elem,
                                  register ELEMENT *elem1,register ELEMENT *elem2)
{
    if (elem2->unit.floating==0.0) error(WAKE_DIVISION_BY_ZERO);
    elem->type=FLOATING_TYPE;
    elem->unit.floating=elem1->unit.floating/elem2->unit.floating;
    return elem;
}

REGQUAL ELEMENT *divf_float_fract(register ELEMENT *elem,
                                  register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=FLOATING_TYPE;
    elem->unit.floating=elem1->unit.floating/
        ((double)elem2->unit.fract.num/(double)elem2->unit.fract.den);
    return elem;
}

REGQUAL ELEMENT *divf_float_int(register ELEMENT *elem,
                                register ELEMENT *elem1,register ELEMENT *elem2)
{
    if (elem2->unit.integer==0) error(WAKE_DIVISION_BY_ZERO);
    elem->type=FLOATING_TYPE;
    elem->unit.floating=elem1->unit.floating/(double)elem2->unit.integer;
    return elem;
}

REGQUAL ELEMENT *divf_float_uint(register ELEMENT *elem,
                                 register ELEMENT *elem1,register ELEMENT *elem2)
{
    if (elem2->unit.integer==0) error(WAKE_DIVISION_BY_ZERO);
    elem->type=FLOATING_TYPE;
    elem->unit.floating=elem1->unit.floating/(double)elem2->unit.uinteger;
    return elem;
}

REGQUAL ELEMENT *divf_float_complex(register ELEMENT *elem,
                                    register ELEMENT *elem1,register ELEMENT *elem2)
{
    float im;
    double d;
    if (elem2->unit.compl.real==0.0) error(WAKE_DIVISION_BY_ZERO);
    d=(double)elem2->unit.compl.real*(double)elem2->unit.compl.real;
    im=(float)((elem1->unit.floating*elem2->unit.compl.imag)/d);
    if (im==0.0) {
        elem->type=FLOATING_TYPE;
        elem->unit.floating=((double)elem1->unit.floating*
                             (double)elem2->unit.compl.real)/d;
    }
    else {
        elem->type=COMPLEX_TYPE;
        elem->unit.compl.real=(float)((elem1->unit.floating*
                                       (double)elem2->unit.compl.real)/d);
        elem->unit.compl.imag=im;
    }
    return elem;
}

REGQUAL ELEMENT *divf_fract_float(register ELEMENT *elem,
                                  register ELEMENT *elem1,register ELEMENT *elem2)
{
    if (elem2->unit.floating==0.0) error(WAKE_DIVISION_BY_ZERO);
    elem->type=FLOATING_TYPE;
    elem->unit.floating=(double)elem1->unit.fract.num/
        (double)elem1->unit.fract.den/elem2->unit.floating;
    return elem;
}

REGQUAL ELEMENT *divf_fract_fract(register ELEMENT *elem,
                                  register ELEMENT *elem1,register ELEMENT *elem2)
{
    if (elem2->unit.fract.num<0) {
        elem->unit.fract.num= -elem1->unit.fract.num*
            elem2->unit.fract.den;
        elem->unit.fract.den= -elem1->unit.fract.den*
            elem2->unit.fract.num;
    }
    else {
        elem->unit.fract.num=elem1->unit.fract.num*
            elem2->unit.fract.den;
        elem->unit.fract.den=elem1->unit.fract.den*
            elem2->unit.fract.num;
    }
    canon(elem);
    return elem;
}

REGQUAL ELEMENT *divf_fract_int(register ELEMENT *elem,
                                register ELEMENT *elem1,register ELEMENT *elem2)
{
    if (elem2->unit.integer==0) error(WAKE_DIVISION_BY_ZERO);
    if (elem2->unit.integer<0) {
        elem->unit.fract.num= -elem1->unit.fract.num;
        elem->unit.fract.den= -elem1->unit.fract.den*
            elem2->unit.integer;
    }
    else {
        elem->unit.fract.num=elem1->unit.fract.num;
        elem->unit.fract.den=elem1->unit.fract.den*
            elem2->unit.integer;
    }
    canon(elem);
    return elem;
}

REGQUAL ELEMENT *divf_fract_uint(register ELEMENT *elem,
                                 register ELEMENT *elem1,register ELEMENT *elem2)
{
    if (elem2->unit.uinteger==0) error(WAKE_DIVISION_BY_ZERO);
    elem->unit.fract.num=elem1->unit.fract.num;
    elem->unit.fract.den=elem1->unit.fract.den*elem2->unit.uinteger;
    canon(elem);
    return elem;
}

REGQUAL ELEMENT *divf_fract_complex(register ELEMENT *elem,
                                    register ELEMENT *elem1,register ELEMENT *elem2)
{
    float i;
    double d,f;
    if (elem2->unit.compl.real==0.0) error(WAKE_DIVISION_BY_ZERO);
    f=(double)elem1->unit.fract.num/(double)elem1->unit.fract.den;
    d=(double)elem2->unit.compl.real*(double)elem2->unit.compl.real;
    i=(float)((f*elem2->unit.compl.imag)/d);
    elem->type=COMPLEX_TYPE;
    elem->unit.compl.real=
        (float)((f*(double)elem2->unit.compl.real)/d);
    elem->unit.compl.imag=i;
    return elem;
}

REGQUAL ELEMENT *divf_int_float(register ELEMENT *elem,
                                register ELEMENT *elem1,register ELEMENT *elem2)
{
    if (elem2->unit.floating==0.0) error(WAKE_DIVISION_BY_ZERO);
    elem->type=FLOATING_TYPE;
    elem->unit.floating=(double)elem1->unit.integer/elem2->unit.floating;
    return elem;
}

REGQUAL ELEMENT *divf_int_fract(register ELEMENT *elem,
                                register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=FRACTION_TYPE;
    if (elem2->unit.fract.num<0) {
        elem->unit.fract.num= -elem1->unit.integer*
            elem2->unit.fract.den;
        elem->unit.fract.den= -elem2->unit.fract.num;
    }
    else {
        elem->unit.fract.num=elem1->unit.integer*
            elem2->unit.fract.den;
        elem->unit.fract.den=elem2->unit.fract.num;
    }
    canon(elem);
    return elem;
}

REGQUAL ELEMENT *divf_int_int(register ELEMENT *elem,
                              register ELEMENT *elem1,register ELEMENT *elem2)
{
    if (elem2->unit.integer==0) error(WAKE_DIVISION_BY_ZERO);
    elem->type=FRACTION_TYPE;
    if (elem2->unit.integer<0) {
        elem->unit.fract.num= -elem1->unit.integer;
        elem->unit.fract.den= -elem2->unit.integer;
    }
    else {
        elem->unit.fract.num=elem1->unit.integer;
        elem->unit.fract.den=elem2->unit.integer;
    }
    canon(elem);
    return elem;
}

REGQUAL ELEMENT *divf_int_uint(register ELEMENT *elem,
                               register ELEMENT *elem1,register ELEMENT *elem2)
{
    if (elem2->unit.uinteger==0) error(WAKE_DIVISION_BY_ZERO);
    elem->type=FRACTION_TYPE;
    elem->unit.fract.num=elem1->unit.integer;
    elem->unit.fract.den=elem2->unit.uinteger;
    canon(elem);
    return elem;
}

REGQUAL ELEMENT *divf_int_complex(register ELEMENT *elem,
                                  register ELEMENT *elem1,register ELEMENT *elem2)
{
    float im;
    double d;
    if (elem2->unit.compl.real==0.0) error(WAKE_DIVISION_BY_ZERO);
    d=(double)elem2->unit.compl.real*(double)elem2->unit.compl.real;
    im=(float)(((float)elem1->unit.integer*elem2->unit.compl.imag)/d);
    if (im==0.0) {
        elem->type=FLOATING_TYPE;
        elem->unit.floating=((double)elem1->unit.integer*
                             (double)elem2->unit.compl.real)/d;
    }
    else {
        elem->type=COMPLEX_TYPE;
        elem->unit.compl.real=
            (float)(((double)elem1->unit.integer*
                     (double)elem2->unit.compl.real)/d);
        elem->unit.compl.imag=im;
    }
    return elem;
}

REGQUAL ELEMENT *divf_uint_float(register ELEMENT *elem,
                                 register ELEMENT *elem1,register ELEMENT *elem2)
{
    if (elem2->unit.floating==0.0) error(WAKE_DIVISION_BY_ZERO);
    elem->type=FLOATING_TYPE;
    elem->unit.floating=(double)elem1->unit.uinteger/elem2->unit.floating;
    return elem;
}

REGQUAL ELEMENT *divf_uint_fract(register ELEMENT *elem,
                                 register ELEMENT *elem1,register ELEMENT *elem2)
{
    elem->type=FRACTION_TYPE;
    if (elem2->unit.fract.num<0) {
        elem->unit.fract.num= -(int)elem1->unit.uinteger*
            elem2->unit.fract.den;
        elem->unit.fract.den= -elem2->unit.fract.num;
    }
    else {
        elem->unit.fract.num=elem1->unit.uinteger*
            elem2->unit.fract.den;
        elem->unit.fract.den=elem2->unit.fract.num;
    }
    canon(elem);
    return elem;
}

REGQUAL ELEMENT *divf_uint_int(register ELEMENT *elem,
                               register ELEMENT *elem1,register ELEMENT *elem2)
{
    if (elem2->unit.integer==0) error(WAKE_DIVISION_BY_ZERO);
    elem->type=FRACTION_TYPE;
    if (elem2->unit.integer<0) {
        elem->unit.fract.num= -(int)elem1->unit.uinteger;
        elem->unit.fract.den= -elem2->unit.integer;
    }
    else {
        elem->unit.fract.num=elem1->unit.uinteger;
        elem->unit.fract.den=elem2->unit.integer;
    }
    canon(elem);
    return elem;
}

REGQUAL ELEMENT *divf_uint_uint(register ELEMENT *elem,
                                register ELEMENT *elem1,register ELEMENT *elem2)
{
    if (elem2->unit.uinteger==0) error(WAKE_DIVISION_BY_ZERO);
    elem->type=FRACTION_TYPE;
    elem->unit.fract.num=elem1->unit.uinteger;
    elem->unit.fract.den=elem2->unit.uinteger;
    canon(elem);
    return elem;
}

REGQUAL ELEMENT *divf_uint_complex(register ELEMENT *elem,
                                   register ELEMENT *elem1,register ELEMENT *elem2)
{
    float im;
    double d;
    if (elem2->unit.compl.real==0.0) error(WAKE_DIVISION_BY_ZERO);
    d=(double)elem2->unit.compl.real*(double)elem2->unit.compl.real;
    im=(float)(((float)elem1->unit.uinteger*elem2->unit.compl.imag)/d);
    if (im==0.0) {
        elem->type=FLOATING_TYPE;
        elem->unit.floating=((double)elem1->unit.uinteger*
                             (double)elem2->unit.compl.real)/d;
    }
    else {
        elem->type=COMPLEX_TYPE;
        elem->unit.compl.real=
            (float)(((double)elem1->unit.uinteger*
                     (double)elem2->unit.compl.real)/d);
        elem->unit.compl.imag=im;
    }
    return elem;
}

REGQUAL ELEMENT *divf_complex_float(register ELEMENT *elem,
                                    register ELEMENT *elem1,register ELEMENT *elem2)
{
    float im;
    double d;
    if (elem2->unit.floating==0.0) error(WAKE_DIVISION_BY_ZERO);
    d=(double)elem2->unit.floating*(double)elem2->unit.floating;
    im=(float)((elem1->unit.compl.imag*(float)elem2->unit.floating)/d);
    if (im==0.0) {
        elem->type=FLOATING_TYPE;
        elem->unit.floating=((double)elem1->unit.compl.real*
                             elem2->unit.floating)/d;
    }
    else {
        elem->type=COMPLEX_TYPE;
        elem->unit.compl.real=
            (float)(((double)elem1->unit.compl.real*
                     elem2->unit.floating)/d);
        elem->unit.compl.imag=im;
    }
    return elem;
}

REGQUAL ELEMENT *divf_complex_fract(register ELEMENT *elem,
                                    register ELEMENT *elem1,register ELEMENT *elem2)
{
    float im;
    double d,f;
    f=(double)elem2->unit.fract.num/(double)elem2->unit.fract.den;
    if (f==0.0) error(WAKE_DIVISION_BY_ZERO);
    d=f*f;
    im=(float)((elem1->unit.compl.imag*f)/d);
    if (im==0.0) {
        elem->type=FLOATING_TYPE;
        elem->unit.floating=((double)elem1->unit.compl.real*f)/d;
    }
    else {
        elem->type=COMPLEX_TYPE;
        elem->unit.compl.real=
            (float)(((double)elem1->unit.compl.real*f)/d);
        elem->unit.compl.imag=im;
    }
    return elem;
}

REGQUAL ELEMENT *divf_complex_int(register ELEMENT *elem,
                                  register ELEMENT *elem1,register ELEMENT *elem2)
{
    float im;
    double d;
    if (elem2->unit.integer==0) error(WAKE_DIVISION_BY_ZERO);
    d=(double)elem2->unit.integer*(double)elem2->unit.integer;
    im=(float)((elem1->unit.compl.imag*(float)elem2->unit.integer)/d);
    if (im==0.0) {
        elem->type=FLOATING_TYPE;
        elem->unit.floating=((double)elem1->unit.compl.real*
                             (double)elem2->unit.integer)/d;
    }
    else {
        elem->type=COMPLEX_TYPE;
        elem->unit.compl.real=
            (float)(((double)elem1->unit.compl.real*
                     (double)elem2->unit.integer)/d);
        elem->unit.compl.imag=im;
    }
    return elem;
}

REGQUAL ELEMENT *divf_complex_uint(register ELEMENT *elem,
                                   register ELEMENT *elem1,register ELEMENT *elem2)
{
    float im;
    double d;
    if (elem2->unit.uinteger==0) error(WAKE_DIVISION_BY_ZERO);
    d=(double)elem2->unit.uinteger*(double)elem2->unit.uinteger;
    im=(float)((elem1->unit.compl.imag*(float)elem2->unit.uinteger)/d);
    if (im==0.0) {
        elem->type=FLOATING_TYPE;
        elem->unit.floating=((double)elem1->unit.compl.real*
                             (double)elem2->unit.uinteger)/d;
    }
    else {
        elem->type=COMPLEX_TYPE;
        elem->unit.compl.real=
            (float)(((double)elem1->unit.compl.real*
                     (double)elem2->unit.uinteger)/d);
        elem->unit.compl.imag=im;
    }
    return elem;
}

REGQUAL ELEMENT *divf_complex_complex(register ELEMENT *elem,
                                      register ELEMENT *elem1,register ELEMENT *elem2)
{
    float d,im;
    d=elem2->unit.compl.real*elem2->unit.compl.real+
        elem2->unit.compl.imag*elem2->unit.compl.imag;
    if (d==0.0) error(WAKE_DIVISION_BY_ZERO);
    im=(elem1->unit.compl.imag*elem2->unit.compl.real-
        elem1->unit.compl.real*elem2->unit.compl.imag)/d;
    if (im==0.0) {
        elem->type=FLOATING_TYPE;
        elem->unit.floating=(double)((elem1->unit.compl.real*
                                      elem2->unit.compl.real+elem1->unit.compl.imag*
                                      elem2->unit.compl.imag)/d);
    }
    else {
        elem->type=COMPLEX_TYPE;
        elem->unit.compl.real=(elem1->unit.compl.real*
                               elem2->unit.compl.real+elem1->unit.compl.imag*
                               elem2->unit.compl.imag)/d;
        elem->unit.compl.imag=im;
    }
    return elem;
}

REGQUAL ELEMENT *divf_floatarray_float(register ELEMENT *elem,
                                       register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    if (elem2->unit.floating==0) error(WAKE_DIVISION_BY_ZERO);
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.floatarray[i]=
                                                  elem1->unit.floatarray[i]/elem2->unit.floating;
    return elem;
}

REGQUAL ELEMENT *divf_floatarray_fract(register ELEMENT *elem,
                                       register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    double d=(double)elem2->unit.fract.num/(double)elem2->unit.fract.den;
    if (d==0.0) error(WAKE_DIVISION_BY_ZERO);
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.floatarray[i]=
                                                  elem1->unit.floatarray[i]/d;
    return elem;
}

REGQUAL ELEMENT *divf_floatarray_int(register ELEMENT *elem,
                                     register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    if (elem2->unit.integer==0) error(WAKE_DIVISION_BY_ZERO);
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.floatarray[i]=
                                                  elem1->unit.floatarray[i]/(double)elem2->unit.integer;
    return elem;
}

REGQUAL ELEMENT *divf_floatarray_uint(register ELEMENT *elem,
                                      register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    if (elem2->unit.integer==0) error(WAKE_DIVISION_BY_ZERO);
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.floatarray[i]=
                                                  elem1->unit.floatarray[i]/(double)elem2->unit.uinteger;
    return elem;
}

REGQUAL ELEMENT *divf_floatarray_floatarray(register ELEMENT *elem,
                                            register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++)
        if (elem2->unit.floatarray[i]==0.0)
            error(WAKE_DIVISION_BY_ZERO);
        else elem->unit.floatarray[i]=
                 elem1->unit.floatarray[i]/elem2->unit.floatarray[i];
    return elem;
}

REGQUAL ELEMENT *divf_floatarray_intarray(register ELEMENT *elem,
                                          register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++)
        if (elem2->unit.intarray[i]==0)
            error(WAKE_DIVISION_BY_ZERO);
        else elem->unit.floatarray[i]=
                 elem1->unit.floatarray[i]/(double)elem2->unit.intarray[i];
    return elem;
}

REGQUAL ELEMENT *divf_floatarray_uintarray(register ELEMENT *elem,
                                           register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++)
        if (elem2->unit.uintarray[i]==0)
            error(WAKE_DIVISION_BY_ZERO);
        else elem->unit.floatarray[i]=
                 elem1->unit.floatarray[i]/(double)elem2->unit.uintarray[i];
    return elem;
}

REGQUAL ELEMENT *divf_floatarray_singlearray(register ELEMENT *elem,
                                             register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++)
        if (elem2->unit.singlearray[i]==0.0)
            error(WAKE_DIVISION_BY_ZERO);
        else elem->unit.floatarray[i]=
                 elem1->unit.floatarray[i]/(double)elem2->unit.singlearray[i];
    return elem;
}

REGQUAL ELEMENT *divf_floatarray_shortarray(register ELEMENT *elem,
                                            register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++)
        if (elem2->unit.shortarray[i]==0)
            error(WAKE_DIVISION_BY_ZERO);
        else elem->unit.floatarray[i]=
                 elem1->unit.floatarray[i]/(double)elem2->unit.shortarray[i];
    return elem;
}

REGQUAL ELEMENT *divf_floatarray_ushortarray(register ELEMENT *elem,
                                             register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++)
        if (elem2->unit.ushortarray[i]==0)
            error(WAKE_DIVISION_BY_ZERO);
        else elem->unit.floatarray[i]=
                 elem1->unit.floatarray[i]/(double)elem2->unit.ushortarray[i];
    return elem;
}

REGQUAL ELEMENT *divf_floatarray_string(register ELEMENT *elem,
                                        register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++)
        if (elem2->unit.string[i]==0)
            error(WAKE_DIVISION_BY_ZERO);
        else elem->unit.floatarray[i]=
                 elem1->unit.floatarray[i]/(double)elem2->unit.string[i];
    return elem;
}

REGQUAL ELEMENT *divf_intarray_int(register ELEMENT *elem,
                                   register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    if (elem2->unit.integer==0) error(WAKE_DIVISION_BY_ZERO);
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.intarray[i]=
                                                  elem1->unit.intarray[i]/elem2->unit.integer;
    return elem;
}

REGQUAL ELEMENT *divf_intarray_uint(register ELEMENT *elem,
                                    register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    if (elem2->unit.uinteger==0) error(WAKE_DIVISION_BY_ZERO);
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.intarray[i]=
                                                  elem1->unit.intarray[i]/elem2->unit.uinteger;
    return elem;
}

REGQUAL ELEMENT *divf_intarray_intarray(register ELEMENT *elem,
                                        register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++)
        if (elem2->unit.intarray[i]==0)
            error(WAKE_DIVISION_BY_ZERO);
        else elem->unit.intarray[i]=
                 elem1->unit.intarray[i]/elem2->unit.intarray[i];
    return elem;
}

REGQUAL ELEMENT *divf_intarray_uintarray(register ELEMENT *elem,
                                         register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++)
        if (elem2->unit.uintarray[i]==0)
            error(WAKE_DIVISION_BY_ZERO);
        else elem->unit.intarray[i]=
                 elem1->unit.intarray[i]/elem2->unit.uintarray[i];
    return elem;
}

REGQUAL ELEMENT *divf_intarray_shortarray(register ELEMENT *elem,
                                          register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++)
        if (elem2->unit.shortarray[i]==0)
            error(WAKE_DIVISION_BY_ZERO);
        else elem->unit.intarray[i]=
                 elem1->unit.intarray[i]/elem2->unit.shortarray[i];
    return elem;
}

REGQUAL ELEMENT *divf_intarray_ushortarray(register ELEMENT *elem,
                                           register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++)
        if (elem2->unit.ushortarray[i]==0)
            error(WAKE_DIVISION_BY_ZERO);
        else elem->unit.intarray[i]=
                 elem1->unit.intarray[i]/elem2->unit.ushortarray[i];
    return elem;
}

REGQUAL ELEMENT *divf_intarray_string(register ELEMENT *elem,
                                      register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++)
        if (elem2->unit.string[i]==0)
            error(WAKE_DIVISION_BY_ZERO);
        else elem->unit.intarray[i]=
                 elem1->unit.intarray[i]/elem2->unit.string[i];
    return elem;
}

REGQUAL ELEMENT *divf_uintarray_int(register ELEMENT *elem,
                                    register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    if (elem2->unit.integer==0) error(WAKE_DIVISION_BY_ZERO);
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.uintarray[i]=
                                                  elem1->unit.uintarray[i]/elem2->unit.integer;
    return elem;
}

REGQUAL ELEMENT *divf_uintarray_uint(register ELEMENT *elem,
                                     register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    if (elem2->unit.uinteger==0) error(WAKE_DIVISION_BY_ZERO);
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.uintarray[i]=
                                                  elem1->unit.uintarray[i]/elem2->unit.uinteger;
    return elem;
}

REGQUAL ELEMENT *divf_uintarray_intarray(register ELEMENT *elem,
                                         register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++)
        if (elem2->unit.intarray[i]==0)
            error(WAKE_DIVISION_BY_ZERO);
        else elem->unit.uintarray[i]=
                 elem1->unit.uintarray[i]/elem2->unit.intarray[i];
    return elem;
}

REGQUAL ELEMENT *divf_uintarray_uintarray(register ELEMENT *elem,
                                          register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++)
        if (elem2->unit.uintarray[i]==0)
            error(WAKE_DIVISION_BY_ZERO);
        else elem->unit.uintarray[i]=
                 elem1->unit.uintarray[i]/elem2->unit.uintarray[i];
    return elem;
}

REGQUAL ELEMENT *divf_uintarray_shortarray(register ELEMENT *elem,
                                           register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++)
        if (elem2->unit.shortarray[i]==0)
            error(WAKE_DIVISION_BY_ZERO);
        else elem->unit.uintarray[i]=
                 elem1->unit.uintarray[i]/elem2->unit.shortarray[i];
    return elem;
}

REGQUAL ELEMENT *divf_uintarray_ushortarray(register ELEMENT *elem,
                                            register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++)
        if (elem2->unit.ushortarray[i]==0)
            error(WAKE_DIVISION_BY_ZERO);
        else elem->unit.uintarray[i]=
                 elem1->unit.uintarray[i]/elem2->unit.ushortarray[i];
    return elem;
}

REGQUAL ELEMENT *divf_uintarray_string(register ELEMENT *elem,
                                       register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++)
        if (elem2->unit.string[i]==0)
            error(WAKE_DIVISION_BY_ZERO);
        else elem->unit.uintarray[i]=
                 elem1->unit.uintarray[i]/elem2->unit.string[i];
    return elem;
}

REGQUAL ELEMENT *divf_singlearray_float(register ELEMENT *elem,
                                        register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    if (elem2->unit.floating==0) error(WAKE_DIVISION_BY_ZERO);
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.singlearray[i]=
                                                  elem1->unit.singlearray[i]/elem2->unit.floating;
    return elem;
}

REGQUAL ELEMENT *divf_singlearray_fract(register ELEMENT *elem,
                                        register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    float f=(float)elem2->unit.fract.num/(float)elem2->unit.fract.den;
    if (f==0.0) error(WAKE_DIVISION_BY_ZERO);
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.singlearray[i]=
                                                  elem1->unit.singlearray[i]/f;
    return elem;
}

REGQUAL ELEMENT *divf_singlearray_int(register ELEMENT *elem,
                                      register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    if (elem2->unit.integer==0) error(WAKE_DIVISION_BY_ZERO);
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.singlearray[i]=
                                                  elem1->unit.singlearray[i]/(float)elem2->unit.integer;
    return elem;
}

REGQUAL ELEMENT *divf_singlearray_uint(register ELEMENT *elem,
                                       register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    if (elem2->unit.integer==0) error(WAKE_DIVISION_BY_ZERO);
    *elem= *elem1;
    for (i=0; i<elem1->unit.line.length; i++) elem->unit.singlearray[i]=
                                                  elem1->unit.singlearray[i]/(float)elem2->unit.uinteger;
    return elem;
}

REGQUAL ELEMENT *divf_singlearray_floatarray(register ELEMENT *elem,
                                             register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++)
        if (elem2->unit.floatarray[i]==0.0)
            error(WAKE_DIVISION_BY_ZERO);
        else elem->unit.singlearray[i]=
                 elem1->unit.singlearray[i]/elem2->unit.floatarray[i];
    return elem;
}

REGQUAL ELEMENT *divf_singlearray_intarray(register ELEMENT *elem,
                                           register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++)
        if (elem2->unit.intarray[i]==0)
            error(WAKE_DIVISION_BY_ZERO);
        else elem->unit.singlearray[i]=
                 elem1->unit.singlearray[i]/(float)elem2->unit.intarray[i];
    return elem;
}

REGQUAL ELEMENT *divf_singlearray_uintarray(register ELEMENT *elem,
                                            register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++)
        if (elem2->unit.uintarray[i]==0)
            error(WAKE_DIVISION_BY_ZERO);
        else elem->unit.singlearray[i]=
                 elem1->unit.singlearray[i]/(float)elem2->unit.uintarray[i];
    return elem;
}

REGQUAL ELEMENT *divf_singlearray_singlearray(register ELEMENT *elem,
                                              register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++)
        if (elem2->unit.singlearray[i]==0.0)
            error(WAKE_DIVISION_BY_ZERO);
        else elem->unit.singlearray[i]=
                 elem1->unit.singlearray[i]/(float)elem2->unit.singlearray[i];
    return elem;
}

REGQUAL ELEMENT *divf_singlearray_shortarray(register ELEMENT *elem,
                                             register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++)
        if (elem2->unit.shortarray[i]==0)
            error(WAKE_DIVISION_BY_ZERO);
        else elem->unit.singlearray[i]=
                 elem1->unit.singlearray[i]/(float)elem2->unit.shortarray[i];
    return elem;
}

REGQUAL ELEMENT *divf_singlearray_ushortarray(register ELEMENT *elem,
                                              register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++)
        if (elem2->unit.ushortarray[i]==0)
            error(WAKE_DIVISION_BY_ZERO);
        else elem->unit.singlearray[i]=
                 elem1->unit.singlearray[i]/(float)elem2->unit.ushortarray[i];
    return elem;
}

REGQUAL ELEMENT *divf_singlearray_string(register ELEMENT *elem,
                                         register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++)
        if (elem2->unit.string[i]==0)
            error(WAKE_DIVISION_BY_ZERO);
        else elem->unit.singlearray[i]=
                 elem1->unit.singlearray[i]/(float)elem2->unit.string[i];
    return elem;
}

REGQUAL ELEMENT *divf_string_int(register ELEMENT *elem,
                                 register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    if (elem2->unit.integer==0) error(WAKE_DIVISION_BY_ZERO);
    *elem= *elem1;
    for (i=0; i<elem->unit.line.length; i++) elem->unit.string[i]=
                                                 (UBYTE)(elem1->unit.string[i]/elem2->unit.integer);
    return elem;
}

REGQUAL ELEMENT *divf_string_uint(register ELEMENT *elem,
                                  register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i;
    if (elem2->unit.uinteger==0) error(WAKE_DIVISION_BY_ZERO);
    *elem= *elem1;
    for (i=0; i<elem->unit.line.length; i++) elem->unit.string[i]=
                                                 (UBYTE)(elem1->unit.string[i]/elem2->unit.uinteger);
    return elem;
}

REGQUAL ELEMENT *divf_string_string(register ELEMENT *elem,
                                    register ELEMENT *elem1,register ELEMENT *elem2)
{
    ULONG i,l=min(elem1->unit.line.length,elem2->unit.line.length);
    *elem= *elem1;
    for (i=0; i<l; i++)
        if (elem2->unit.string[i]==0)
            error(WAKE_DIVISION_BY_ZERO);
        else elem->unit.string[i]=(UBYTE)
                 (elem1->unit.string[i]/elem2->unit.string[i]);
    return elem;
}

MAT_FUNC divf_func[12][12]={
    {divf_float_float,divf_float_fract,divf_float_int,divf_float_uint,
     divf_float_complex},
    {divf_fract_float,divf_fract_fract,divf_fract_int,divf_fract_uint,
     divf_fract_complex},
    {divf_int_float,divf_int_fract,divf_int_int,divf_int_uint,
     divf_int_complex},
    {divf_uint_float,divf_uint_fract,divf_uint_int,divf_uint_uint,
     divf_uint_complex},
    {divf_complex_float,divf_complex_fract,divf_complex_int,divf_complex_uint,
     divf_complex_complex},
    {divf_floatarray_float,divf_floatarray_fract,divf_floatarray_int,
     divf_floatarray_uint,NULL,divf_floatarray_floatarray,
     divf_floatarray_intarray,divf_floatarray_uintarray,
     divf_floatarray_singlearray,divf_floatarray_shortarray,
     divf_floatarray_ushortarray,divf_floatarray_string},
    {NULL,NULL,divf_intarray_int,divf_intarray_uint,NULL,NULL,
     divf_intarray_intarray,divf_intarray_uintarray,NULL,
     divf_intarray_shortarray,divf_intarray_ushortarray,divf_intarray_string},
    {NULL,NULL,divf_uintarray_int,divf_uintarray_uint,NULL,NULL,
     divf_uintarray_intarray,divf_uintarray_uintarray,NULL,
     divf_uintarray_shortarray,divf_uintarray_ushortarray,divf_uintarray_string},
    {divf_singlearray_float,divf_singlearray_fract,divf_singlearray_int,
     divf_singlearray_uint,NULL,divf_singlearray_floatarray,
     divf_singlearray_intarray,divf_singlearray_uintarray,
     divf_singlearray_singlearray,divf_singlearray_shortarray,
     divf_singlearray_ushortarray,divf_singlearray_string},
    {NULL},
    {NULL},
    {NULL,NULL,divf_string_int,divf_string_uint,NULL,NULL,NULL,NULL,NULL,NULL,
     NULL,divf_string_string}};

REGQUAL ELEMENT *divf(register ELEMENT *elem,register ELEMENT *elem1,
                      register ELEMENT *elem2)
{
    if (elem1->type==ARRAY_TYPE)
        switch (elem2->type) {
        case ARRAY_TYPE: {
            ULONG i,l=min(elem1->unit.line.length,
                          elem2->unit.line.length);
            *elem= *elem1;
            for (i=0; i<l; i++)
                divf(elem->unit.array+i,
                     elem1->unit.array+i,
                     elem2->unit.array+i);
            return elem;
        }
        case FLOATING_TYPE:
        case FRACTION_TYPE:
        case INTEGER_TYPE:
        case UINTEGER_TYPE:
        case COMPLEX_TYPE: {
            ULONG i;
            *elem= *elem1;
            for (i=0; i<elem1->unit.line.length; i++)
                divf(elem->unit.array+i,
                     elem1->unit.array+i,elem2);
            return elem;
        }
        }
    else if (elem1->type>=FLOATING_TYPE&&elem1->type<=STRING_TYPE&&
             elem2->type>=FLOATING_TYPE&&elem2->type<=STRING_TYPE) {
        register MAT_FUNC func=
            divf_func[elem1->type-FLOATING_TYPE][elem2->type-FLOATING_TYPE];
        if (func) return (*func)(elem,elem1,elem2); }
    error(WAKE_INVALID_TYPE);
    return NULL;
}

REGQUAL long gcd(register long n,register long m)
{
    unsigned long n1,m1,r;
    if (n>m)
        {
            n1=n;
            m1=m;
        }
    else if (m>n)
        {
            n1=m;
            m1=n;
        }
    else return n;
    while (m1 != 0)
        {
            r=n1%m1;
            n1=m1;
            m1=r;
        }
    return (long)n1;
}

REGQUAL int canon(register ELEMENT *elem)
{
    if (!elem->unit.fract.den)
        {
            elem->flag=NUMBER|INTEGER;
            elem->type=INTEGER_TYPE;
            elem->unit.integer=0;
            return INTEGER_TYPE;
        }
    if (elem->unit.fract.num%elem->unit.fract.den)
        {
            long n=elem->unit.fract.num,m=elem->unit.fract.den,r;
            if (n<0) n= -n;
            if (m<0) m= -m;
            while ((r=m%n)!=0)
                {
                    m=n;
                    n=r;
                }
            if (n!=1)
                {
                    elem->unit.fract.num/=n;
                    elem->unit.fract.den/=n;
                }
            elem->type=FRACTION_TYPE;
            elem->flag=NUMBER;
            return FRACTION_TYPE;
        }
    elem->flag=NUMBER|INTEGER;
    elem->type=INTEGER_TYPE;
    elem->unit.fract.num/=elem->unit.fract.den;
    return INTEGER_TYPE;
}

REGQUAL ELEMENT *absf(register ELEMENT *elem)
{
    switch (elem->type) {
    case FLOATING_TYPE:
        elem->unit.floating=fabs(elem->unit.floating);
        break;
    case FRACTION_TYPE:
        elem->unit.fract.num=labs(elem->unit.fract.num);
        break;
    case INTEGER_TYPE:
        elem->unit.integer=labs(elem->unit.integer);
        break;
    case UINTEGER_TYPE:
        break;
    default:
        error(WAKE_INVALID_TYPE);
    }
    return elem;
}

REGQUAL ELEMENT *negf(register ELEMENT *elem)
{
    switch (elem->type) {
    case FLOATING_TYPE:
        elem->unit.floating= -elem->unit.floating;
        break;
    case FRACTION_TYPE:
        elem->unit.fract.num= -elem->unit.fract.num;
        break;
    case INTEGER_TYPE:
        elem->unit.integer= -elem->unit.integer;
        break;
    default:
        error(WAKE_INVALID_TYPE);
    }
    return elem;
}

void matmul(ELEMENT *c,ELEMENT *a,ELEMENT *b,long p,long n,long q)
{
    long i,j,k;
    ELEMENT *e,*d,*g,r;
    for (i=0; i<p; i++)
        for (j=0; j<q; j++) {
            d=c+i*q+j;
            d->flag=NUMBER|INTEGER;
            d->type=INTEGER_TYPE;
            d->unit.integer=0;
            for (k=0; k<n; k++) {
                e=a+i*n+k;
                g=b+k*q+j;
                addf(d,d,mulf(&r,e,g));
            }
        }
}

/* number - abs - number */
void wk_abs(void)
{
    check(1);
    absf(wsp-1);
}

/* number - acos - real */
void wk_acos(void)
{
    check(1);
    wsp[-1].unit.floating=acos(check_double(wsp-1));
    wsp[-1].type=FLOATING_TYPE;
}

/* addendum addendum - add - sum */
void wk_add(void)
{
    check(2);
    addf(wsp-2,wsp-2,wsp-1);
    wsp--;
}

/* number - asin - real */
void wk_asin(void)
{
    check(1);
    wsp[-1].unit.floating=asin(check_double(wsp-1));
    wsp[-1].type=FLOATING_TYPE;
}

/* number - atan - real */
void wk_atan(void)
{
    check(1);
    wsp[-1].unit.floating=atan(check_double(wsp-1));
    wsp[-1].type=FLOATING_TYPE;
}

/* number - ceiling - number */
void wk_ceiling(void)
{
    check(1);
    switch (wsp[-1].type) {
    case FLOATING_TYPE:
        wsp[-1].unit.floating=ceil(wsp[-1].unit.floating);
        return;
    case FRACTION_TYPE:
        wsp[-1].flag|=INTEGER;
        wsp[-1].type=INTEGER_TYPE;
        wsp[-1].unit.integer=(wsp[-1].unit.fract.num+
                              wsp[-1].unit.fract.den)/wsp[-1].unit.fract.den;
        return;
    case INTEGER_TYPE:
    case UINTEGER_TYPE:
        return;
    default:
        error(WAKE_INVALID_TYPE);
    }
}

/* number - cos - real */
void wk_cos(void)
{
    check(1);
    wsp[-1].unit.floating=cos(check_double(wsp-1));
    wsp[-1].type=FLOATING_TYPE;
}

/* number - cosh - real */
void wk_cosh(void)
{
    check(1);
    wsp[-1].unit.floating=cosh(check_double(wsp-1));
    wsp[-1].type=FLOATING_TYPE;
}

/* rational - denominator - denominator */
void wk_denominator(void)
{
    check(1);
    switch (wsp[-1].type) {
    case FRACTION_TYPE:
        wsp[-1].flag|=INTEGER;
        wsp[-1].type=INTEGER_TYPE;
        wsp[-1].unit.integer=wsp[-1].unit.fract.den;
        return;
    case INTEGER_TYPE:
    case UINTEGER_TYPE:
        wsp[-1].unit.integer=1;
        return;
    default:
        error(WAKE_INVALID_TYPE);
    }
}

/* some some - difference - some */
void wk_difference(void)
{
    check(2);
    switch (wsp[-2].type) {
    case FLOATARRAY_TYPE:
        if (wsp[-1].type==FLOATARRAY_TYPE) {
            char p;
            ULONG i,j;
            for (i=0; i<wsp[-1].unit.line.length&&
                     wsp[-1].unit.floatarray[i]; i++) {
                p=0;
                for (j=0; j<wsp[-2].unit.line.length&&
                         wsp[-2].unit.floatarray[j]; j++)
                    if (wsp[-1].unit.floatarray[i]==
                        wsp[-2].unit.floatarray[j]) {
                        p= -1;
                        break;
                    }
                if (p) {
                    if (j<wsp[-2].unit.line.length-1)
                        memmove(wsp[-2].unit.floatarray+j,
                                wsp[-2].unit.floatarray+j+1,
                                ((int)wsp[-2].unit.line.length-j-1)*
                                sizeof(double));
                    wsp[-2].unit.floatarray[
                                            wsp[-2].unit.line.length-1]=0.0; }
            }
            wsp--;
            return;
        }
        break;
    case INTARRAY_TYPE:
        if (wsp[-1].type==INTARRAY_TYPE) {
            char p;
            ULONG i,j;
            for (i=0; i<wsp[-1].unit.line.length&&
                     wsp[-1].unit.intarray[i]; i++) {
                p=0;
                for (j=0; j<wsp[-2].unit.line.length&&
                         wsp[-2].unit.intarray[j]; j++)
                    if (wsp[-1].unit.intarray[i]==
                        wsp[-2].unit.intarray[j]) {
                        p= -1;
                        break;
                    }
                if (p) {
                    if (j<wsp[-2].unit.line.length-1)
                        memmove(wsp[-2].unit.intarray+j,
                                wsp[-2].unit.intarray+j+1,
                                ((int)wsp[-2].unit.line.length-j-1)*
                                sizeof(long));
                    wsp[-2].unit.intarray[
                                          wsp[-2].unit.line.length-1]=0; }
            }
            wsp--;
            return;
        }
        break;
    case STRING_TYPE:
        if (wsp[-1].type==STRING_TYPE) {
            char p;
            ULONG i,j;
            for (i=0; i<wsp[-1].unit.line.length&&
                     wsp[-1].unit.string[i]; i++) {
                p=0;
                for (j=0; j<wsp[-2].unit.line.length&&
                         wsp[-2].unit.string[j]; j++)
                    if (wsp[-1].unit.string[i]==
                        wsp[-2].unit.string[j]) {
                        p= -1;
                        break;
                    }
                if (p) {
                    if (j<wsp[-2].unit.line.length-1)
                        memmove(wsp[-2].unit.string+j,
                                wsp[-2].unit.string+j+1,
                                ((int)wsp[-2].unit.line.length-j-1)*
                                sizeof(UBYTE));
                    wsp[-2].unit.string[
                                        wsp[-2].unit.line.length-1]=0; }
            }
            wsp--;
            return;
        }
        break;
    case ARRAY_TYPE:
        if (wsp[-1].type==ARRAY_TYPE) {
            char p;
            ULONG i,j;
            for (i=0; i<wsp[-1].unit.line.length&&
                     wsp[-1].unit.array[i].type; i++) {
                p=0;
                for (j=0; j<wsp[-2].unit.line.length&&
                         wsp[-2].unit.array[j].type; j++)
                    if (eq(wsp[-1].unit.array+i,
                           wsp[-2].unit.array+j)) {
                        p= -1;
                        break;
                    }
                if (p) {
                    if (j<wsp[-2].unit.line.length-1)
                        memmove(wsp[-2].unit.array+j,
                                wsp[-2].unit.array+j+1,
                                ((int)wsp[-2].unit.line.length-j-1)*
                                sizeof(ELEMENT));
                    wsp[-2].unit.array[
                                       wsp[-2].unit.line.length-1].type=
                        NULL_TYPE;
                }
            }
            wsp--;
            return;
        }
        break;
    }
    error(WAKE_INVALID_TYPE);
}

/* dividend divisor - div - quotient */
void wk_div(void)
{
    check(2);
    divf(wsp-2,wsp-2,wsp-1);
    wsp--;
}

/* base exponent - exp - real */
void wk_exp(void)
{
    check(2);
    wsp[-2].unit.floating=pow(check_double(wsp-2),check_double(wsp-1));
    wsp[-2].type=FLOATING_TYPE;
    wsp--;
}

/* number - floor - number */
void wk_floor(void)
{
    check(1);
    switch (wsp[-1].type) {
    case FLOATING_TYPE:
        wsp[-1].unit.floating=floor(wsp[-1].unit.floating);
        return;
    case FRACTION_TYPE:
        wsp[-1].flag|=INTEGER;
        wsp[-1].type=INTEGER_TYPE;
        wsp[-1].unit.integer=wsp[-1].unit.fract.num/
            wsp[-1].unit.fract.den;
        return;
    case INTEGER_TYPE:
    case UINTEGER_TYPE:
        return;
    default:
        error(WAKE_INVALID_TYPE);
    }
}

/* integer integer - idiv - integer */
void wk_idiv(void)
{
    check(2);
    if (!(wsp[-2].flag&INTEGER)||!(wsp[-1].flag&INTEGER))
        error(WAKE_INVALID_TYPE);
    if (wsp[-1].unit.integer==0)
        error(WAKE_DIVISION_BY_ZERO);
    wsp[-2].unit.integer/=wsp[-1].unit.integer;
    wsp--;
}

/* number - ln - real */
void wk_ln(void)
{
    check(1);
    wsp[-1].unit.floating=log(check_double(wsp-1));
    wsp[-1].type=FLOATING_TYPE;
}

/* number - log - real */
void wk_log(void)
{
    check(1);
    wsp[-1].unit.floating=log10(check_double(wsp-1));
    wsp[-1].type=FLOATING_TYPE;
}

/* matrix n - matinv - matrix */
void wk_matinv(void)
{
    long i,j,k,l,m,n,*d;
    ELEMENT *a,*e,*g,*h,p,r,t,v,one,zero;
    check(2);
    if (wsp[-2].type!=ARRAY_TYPE||!(wsp[-1].flag&INTEGER))
        error(WAKE_INVALID_TYPE);
    n=wsp[-1].unit.integer;
    l=n*n;
    if ((long)wsp[-2].unit.line.length<l) error(WAKE_INDEX_OUT_OF_RANGE);
    a=wsp[-2].unit.array;
    d=(long *)memalloc(n*sizeof(long));
    if (!d) error(WAKE_CANNOT_ALLOCATE);
    temp_mem=(void *)d;
    one.flag=NUMBER|INTEGER;
    one.type=INTEGER_TYPE;
    one.unit.integer=1;
    zero.flag=NUMBER|INTEGER;
    zero.type=INTEGER_TYPE;
    zero.unit.integer=0;
    for (i=0; i<n; i++) {
        v=p=a[i*n+i];
        absf(&v);
        k= -1;
        j=i;
        while (++j<n) {
            t=r=a[j*n+i];
            if (le(absf(&t),&v)) continue;
            p=r;
            v=t;
            k=j;
        }
        d[i]=k;
        if (k!= -1) {
            e=a+k*n;
            g=a+i*n;
            for (j=0; j<n; j++) {
                v=e[j];
                e[j]=g[j];
                g[j]=v;
            }
        }
        a[i*n+i]=one;
        e=a+i*n;
        for (j=0; j<n; j++) divf(e+j,e+j,&p);
        h=a+i;
        g=a+i*n;
        for (j=0,m=0; j<n; j++,m+=n) {
            if (i==j) continue;
            r=h[m];
            h[m]=zero;
            e=a+m;
            for (k=0; k<n; k++)
                subf(e+k,e+k,mulf(&t,&r,g+k));
        }
    }
    for (i=n-1; i>=0; i--) {
        k=d[i];
        if (k!= -1) {
            e=a+i;
            g=a+k;
            for (j=0; j<l; j+=n) {
                v=e[j];
                e[j]=g[j];
                g[j]=v;
            }
        }
    }
    memfree(d);
    temp_mem=NULL;
    wsp--;
}

/* (un)knownvector permutation matrix n - matlub - knownvector */
void wk_matlub(void)
{
    long i,ii= -1,ip,j,n,*p;
    ELEMENT *a,*b,*e,r,s,zero;
    check(4);
    if (wsp[-4].type!=ARRAY_TYPE||wsp[-3].type!=INTARRAY_TYPE||
        wsp[-2].type!=ARRAY_TYPE||!(wsp[-1].flag&INTEGER))
        error(WAKE_INVALID_TYPE);
    n=wsp[-1].unit.integer;
    if ((long)wsp[-4].unit.line.length<n||(long)wsp[-3].unit.line.length<n||
        (long)wsp[-2].unit.line.length<n*n) error(WAKE_INDEX_OUT_OF_RANGE);
    p=wsp[-3].unit.intarray;
    a=wsp[-2].unit.array;
    b=wsp[-4].unit.array;
    zero.flag=NUMBER|INTEGER;
    zero.type=INTEGER_TYPE;
    zero.unit.integer=0;
    for (i=0; i<n; i++) {
        ip=p[i];
        s=b[ip];
        b[ip]=b[i];
        if (ii!= -1) {
            e=a+i*n;
            for (j=ii; j<i; j++) subf(&s,&s,mulf(&r,e+j,b+j));
        }
        else if (!eq(&s,&zero)) ii=i;
        b[i]=s;
    }
    for (i=n-1; i>=0; i--) {
        s=b[i];
        e=a+i*n;
        for (j=i+1; j<n; j++) subf(&s,&s,mulf(&r,e+j,b+j));
        e=a+i*n+i;
        divf(b+i,&s,e);
    }
    wsp-=3;
}

/* permutation matrix n - matlud - permutation lu-decomposition determinant */
void wk_matlud(void)
{
    char d;
    long i,j,k,l,m,n,x=0,*p;
    ELEMENT *a,*e,*g,*h,*v,r,s,t,one,zero;
    check(3);
    if (wsp[-3].type!=INTARRAY_TYPE||wsp[-2].type!=ARRAY_TYPE||
        !(wsp[-1].flag&INTEGER)) error(WAKE_INVALID_TYPE);
    n=wsp[-1].unit.integer;
    if ((long)wsp[-2].unit.line.length<n*n||(long)wsp[-3].unit.line.length<n)
        error(WAKE_INDEX_OUT_OF_RANGE);
    a=wsp[-2].unit.array;
    p=wsp[-3].unit.intarray;
    v=(ELEMENT *)memalloc(n*sizeof(ELEMENT));
    if (!v) error(WAKE_CANNOT_ALLOCATE);
    one.flag=NUMBER|INTEGER;
    one.type=INTEGER_TYPE;
    one.unit.integer=1;
    zero.flag=NUMBER|INTEGER;
    zero.type=INTEGER_TYPE;
    zero.unit.integer=0;
    for (i=0,l=0; i<n; i++,l+=n) {
        e=a+l;
        t=zero;
        for (j=0; j<n; j++) {
            r=e[j];
            if (gt(absf(&r),&t)) t=r;
        }
        if (eq(&t,&zero)) error(WAKE_ILLEGAL_USE);
        divf(v+i,&one,&t);
    }
    d=1;
    for (j=0; j<n; j++) {
        e=a+j;
        h=a+j;
        for (i=0,l=0; i<j; i++,l+=n) {
            g=a+l;
            s=e[l];
            for (k=0,m=0; k<i; k++,m+=n)
                subf(&s,&s,mulf(&r,g+k,h+m));
            e[l]=s;
        }
        t=zero;
        for (i=j,l=j*n; i<n; i++,l+=n) {
            g=a+l;
            s=e[l];
            for (k=0,m=0; k<j; k++,m+=n)
                subf(&s,&s,mulf(&r,g+k,h+m));
            e[l]=s;
            if (ge(mulf(&r,v+i,absf(&s)),&t)) {
                t=r;
                x=i;
            }
        }
        if (j!=x) {
            g=a+x*n;
            h=a+j*n;
            for (k=0; k<n; k++) {
                s=g[k];
                g[k]=h[k];
                h[k]=s;
            }
            d=(char)-d;
            v[x]=v[j];
        }
        p[j]=x;
        if (j<n-1) {
            g=e+j*n;
            divf(&r,&one,g);
            for (i=j+1,l=i*n; i<n; i++,l+=n) mulf(e+l,e+l,&r);
        }
    }
    memfree(v);
    temp_mem=NULL;
    wsp[-1]=one;
    if (d== -1) negf(wsp-1);
    for (j=0,l=0; j<n; j++,l+=n+1) mulf(wsp-1,wsp-1,a+l);
}

/* matrix matrix1 matrix2 p n q - matmul - matrix */
/* matrix (p x q) = matrix1 (p x n) matrix2 (n x q) */
void wk_matmul(void)
{
    long i,p,n,q;
    check(6);
    for (i=0; i<3; i++)
        if (wsp[i-6].type!=ARRAY_TYPE) error(WAKE_INVALID_TYPE);
    for (i=3; i<6; i++)
        if (!(wsp[i-6].flag&INTEGER)) error(WAKE_INVALID_TYPE);
    p=wsp[-3].unit.integer;
    n=wsp[-2].unit.integer;
    q=wsp[-1].unit.integer;
    if ((long)wsp[-6].unit.line.length<p*q||(long)wsp[-5].unit.line.length<p*n||
        (long)wsp[-4].unit.line.length<n*q) error(WAKE_INDEX_OUT_OF_RANGE);
    matmul(wsp[-6].unit.array,wsp[-5].unit.array,wsp[-4].unit.array,
           p,n,q);
    wsp-=5;
}

/* matrix matrix1 p n - mattra - matrix */
void wk_mattra(void)
{
    long i,j,n,p;
    ELEMENT *a,*c;
    check(4);
    if (wsp[-4].type!=ARRAY_TYPE||wsp[-3].type!=ARRAY_TYPE||
        !(wsp[-2].flag&INTEGER)||!(wsp[-1].flag&INTEGER))
        error(WAKE_INVALID_TYPE);
    p=wsp[-2].unit.integer;
    n=wsp[-1].unit.integer;
    if ((long)wsp[-4].unit.line.length<p*n||(long)wsp[-3].unit.line.length<p*n)
        error(WAKE_INDEX_OUT_OF_RANGE);
    c=wsp[-4].unit.array;
    a=wsp[-3].unit.array;
    for (i=0; i<n; i++)
        for (j=0; j<p; j++) c[i*p+j]=a[j*n+i];
    wsp-=3;
}

double mod(double a,double b)
{
    a/=b;
    if (a>0) b=floor(a);
    else b=ceil(a);
    return a-b;
}

/* number number - mod - number */
void wk_mod(void)
{
    check(2);
    switch (wsp[-2].type) {
    case INTEGER_TYPE:
        switch (wsp[-1].type) {
        case INTEGER_TYPE:
            if (wsp[-1].unit.integer==0)
                error(WAKE_DIVISION_BY_ZERO);
            wsp[-2].unit.integer%=
                wsp[-1].unit.integer;
            break;
        case UINTEGER_TYPE:
            if (wsp[-1].unit.integer==0)
                error(WAKE_DIVISION_BY_ZERO);
            wsp[-2].unit.integer%=
                wsp[-1].unit.uinteger;
            break;
        case FLOATING_TYPE:
            if (wsp[-1].unit.floating==0)
                error(WAKE_DIVISION_BY_ZERO);
            wsp[-2].flag=NUMBER;
            wsp[-2].type=FLOATING_TYPE;
            wsp[-2].unit.floating=mod((double)wsp[-2].unit.integer,
                                      wsp[-1].unit.floating);
            break;
        case FRACTION_TYPE:
            wsp[-2].flag=NUMBER;
            wsp[-2].type=FLOATING_TYPE;
            wsp[-2].unit.floating=mod((double)wsp[-2].unit.integer,
                                      (double)wsp[-1].unit.fract.num/
                                      (double)wsp[-1].unit.fract.den);
            break;
        default:
            error(WAKE_INVALID_TYPE);
        }
        break;
    case UINTEGER_TYPE:
        switch (wsp[-1].type) {
        case INTEGER_TYPE:
            if (wsp[-1].unit.integer==0)
                error(WAKE_DIVISION_BY_ZERO);
            wsp[-2].unit.uinteger%=
                wsp[-1].unit.integer;
            wsp[-2].type=INTEGER_TYPE;
            break;
        case UINTEGER_TYPE:
            if (wsp[-1].unit.uinteger==0)
                error(WAKE_DIVISION_BY_ZERO);
            wsp[-2].unit.uinteger%=
                wsp[-1].unit.uinteger;
            break;
        case FLOATING_TYPE:
            if (wsp[-1].unit.floating==0)
                error(WAKE_DIVISION_BY_ZERO);
            wsp[-2].flag=NUMBER;
            wsp[-2].type=FLOATING_TYPE;
            wsp[-2].unit.floating=mod((double)wsp[-2].unit.uinteger,
                                      wsp[-1].unit.floating);
            break;
        case FRACTION_TYPE:
            wsp[-2].flag=NUMBER;
            wsp[-2].type=FLOATING_TYPE;
            wsp[-2].unit.floating=mod((double)wsp[-2].unit.integer,
                                      (double)wsp[-1].unit.fract.num/
                                      (double)wsp[-1].unit.fract.den);
            break;
        default:
            error(WAKE_INVALID_TYPE);
        }
        break;
    case FLOATING_TYPE:
        switch (wsp[-1].type) {
        case INTEGER_TYPE:
            if (wsp[-1].unit.integer==0)
                error(WAKE_DIVISION_BY_ZERO);
            wsp[-2].unit.floating=mod(wsp[-2].unit.floating,
                                      (double)wsp[-1].unit.integer);
            break;
        case UINTEGER_TYPE:
            if (wsp[-1].unit.integer==0)
                error(WAKE_DIVISION_BY_ZERO);
            wsp[-2].unit.floating=mod(wsp[-2].unit.floating,
                                      (double)wsp[-1].unit.uinteger);
            break;
        case FLOATING_TYPE:
            if (wsp[-1].unit.floating==0)
                error(WAKE_DIVISION_BY_ZERO);
            wsp[-2].unit.floating=mod(wsp[-2].unit.floating,
                                      wsp[-1].unit.floating);
            break;
        case FRACTION_TYPE:
            wsp[-2].unit.floating=mod(wsp[-2].unit.floating,
                                      (double)wsp[-1].unit.fract.num/
                                      (double)wsp[-1].unit.fract.den);
            break;
        default:
            error(WAKE_INVALID_TYPE);
        }
        break;
    case STRING_TYPE:
        switch (wsp[-1].type) {
        case INTEGER_TYPE:
        case UINTEGER_TYPE: {
            ULONG i;
            if (wsp[-1].unit.integer==0)
                error(WAKE_DIVISION_BY_ZERO);
            for (i=0; i<wsp[-2].unit.line.length;
                 i++)
                wsp[-2].unit.string[i]=(UBYTE)
                    (wsp[-2].unit.string[i]%
                     wsp[-1].unit.integer);
            break;
        }
        case STRING_TYPE: {
            ULONG i;
            for (i=0; i<wsp[-2].unit.line.length; i++)
                if (wsp[-1].unit.string[i])
                    wsp[-2].unit.string[i]=(UBYTE)
                        (wsp[-2].unit.string[i]%
                         wsp[-1].unit.string[i]);
                else error(WAKE_DIVISION_BY_ZERO);
            break;
        }
        default:
            error(WAKE_INVALID_TYPE);
        }
        break;
    default:
        error(WAKE_INVALID_TYPE);
    }
    wsp--;
}

/* factor factor - mul - product */
void wk_mul(void)
{
    check(2);
    mulf(wsp-2,wsp-2,wsp-1);
    wsp--;
}

/* number - neg - number */
void wk_neg(void)
{
    check(1);
    negf(wsp-1);
}

/* rational - numerator - numerator */
void wk_numerator(void)
{
    check(1);
    switch (wsp[-1].type) {
    case FRACTION_TYPE:
        wsp[-1].flag|=INTEGER;
        wsp[-1].type=INTEGER_TYPE;
        wsp[-1].unit.integer=wsp[-1].unit.fract.num;
        return;
    case INTEGER_TYPE:
    case UINTEGER_TYPE:
        return;
    default:
        error(WAKE_INVALID_TYPE);
    }
}

/* - rand - integer */
void wk_rand(void)
{
    long k1;
    stack(1);
    wsp->flag=NUMBER|INTEGER;
    wsp->type=INTEGER_TYPE;
    if (rseed==0L) rseed=1L;
    k1=rseed/127773L;
    rseed=16807L*(rseed-k1*127773L)-k1*2836L;
    wsp->unit.integer=rseed>>1;
    wsp++;
}

/* - random - real */
void wk_random(void)
{
    stack(1);
    wsp->flag=NUMBER;
    wsp->type=FLOATING_TYPE;
    if (rseed1==0) rseed1=1;
    if (rseed2==0) rseed2=1;
    if (rseed3==0) rseed3=1;
    if ((rseed1=(short)(171*(rseed1%177)-2*(rseed1/177)))<0) rseed1+=30269;
    if ((rseed2=(short)(172*(rseed2%176)-35*(rseed2/176)))<0) rseed2+=30307;
    if ((rseed3=(short)(170*(rseed3%178)-63*(rseed3/178)))<0) rseed3+=30323;
    wsp->unit.floating=rseed1/30269.0+rseed2/30307.0+rseed3/30323.0;
    wsp->unit.floating-=floor(wsp->unit.floating);
    wsp++;
}

/* number - round - number */
void wk_round(void)
{
    check(1);
    switch (wsp[-1].type) {
    case FLOATING_TYPE:
        wsp[-1].unit.floating=wsp[-1].unit.floating>0.0?
            floor(wsp[-1].unit.floating+0.5):
        ceil(wsp[-1].unit.floating-0.5);
        return;
    case FRACTION_TYPE: {
        double d=(double)wsp[-1].unit.fract.num/
            (double)wsp[-1].unit.fract.den;
        wsp[-1].flag|=INTEGER;
        wsp[-1].type=INTEGER_TYPE;
        wsp[-1].unit.integer=(long)(float)
            (d>0.0?floor(d+0.5):ceil(d-0.5));
        return;
    }
    case INTEGER_TYPE:
    case UINTEGER_TYPE:
        return;
    default:
        error(WAKE_INVALID_TYPE);
    }
}

/* - rrand - integer */
void wk_rrand(void)
{
    stack(1);
    wsp->flag=NUMBER|INTEGER;
    wsp->type=INTEGER_TYPE;
    wsp->unit.integer=rseed;
    wsp++;
}

/* - rrandom - real */
void wk_rrandom(void)
{
    short *p;
    stack(1);
    wsp->flag=NUMBER;
    wsp->type=FLOATING_TYPE;
    p=(short *)((char *)&wsp->unit.floating+sizeof(double));
    wsp->unit.floating=0.0;
    *--p=rseed3;
    *--p=rseed2;
    *--p=rseed1;
    wsp++;
}

/* number - sin - real */
void wk_sin(void)
{
    check(1);
    wsp[-1].unit.floating=sin(check_double(wsp-1));
    wsp[-1].type=FLOATING_TYPE;
}

/* number - sinh - real */
void wk_sinh(void)
{
    check(1);
    wsp[-1].unit.floating=sinh(check_double(wsp-1));
    wsp[-1].type=FLOATING_TYPE;
}

/* number - sqrt - real */
void wk_sqrt(void)
{
    double d;
    check(1);
    d=check_double(wsp-1);
    if (d<0.0) {
        wsp[-1].type=COMPLEX_TYPE;
        wsp[-1].unit.compl.real=(float)0.0;
        wsp[-1].unit.compl.imag=(float)sqrt(-d);
    }
    else {
        wsp[-1].unit.floating=sqrt(d);
        wsp[-1].type=FLOATING_TYPE;
    }
}

/* integer - srand - */
void wk_srand(void)
{
    check(1);
    if (!(wsp[-1].flag&INTEGER)) error(WAKE_INVALID_TYPE);
    rseed=wsp[-1].unit.integer;
    wsp--;
}

/* real - srandom - */
void wk_srandom(void)
{
    short *p;
    check(1);
    if (wsp[-1].type!=FLOATING_TYPE) error(WAKE_INVALID_TYPE);
    p=(short *)((char *)&wsp[-1].unit.floating+sizeof(double));
    rseed3= *--p;
    rseed2= *--p;
    rseed1= *--p;
    wsp--;
}

/* minuend subtrahend - sub - difference */
void wk_sub(void)
{
    check(2);
    subf(wsp-2,wsp-2,wsp-1);
    wsp--;
}

/* number - tan - real */
void wk_tan(void)
{
    check(1);
    wsp[-1].unit.floating=tan(check_double(wsp-1));
    wsp[-1].type=FLOATING_TYPE;
}

/* number - tanh - real */
void wk_tanh(void)
{
    check(1);
    wsp[-1].unit.floating=tanh(check_double(wsp-1));
    wsp[-1].type=FLOATING_TYPE;
}

/* number - truncate - number */
void wk_truncate(void)
{
    check(1);
    switch (wsp[-1].type) {
    case INTEGER_TYPE:
    case UINTEGER_TYPE:
        return;
    case FLOATING_TYPE:
        wsp[-1].unit.floating=wsp[-1].unit.floating>0?
            floor(wsp[-1].unit.floating):
        ceil(wsp[-1].unit.floating);
        return;
    case FRACTION_TYPE:
        wsp[-1].flag|=INTEGER;
        wsp[-1].type=INTEGER_TYPE;
        wsp[-1].unit.integer=wsp[-1].unit.fract.num/
            wsp[-1].unit.fract.den;
        return;
    default:
        error(WAKE_INVALID_TYPE);
    }
}

WAKE_FUNC mat_funcs[]={
    {"abs",         wk_abs},
    {"acos",        wk_acos},
    {"add",         wk_add},
    {"asin",        wk_asin},
    {"atan",        wk_atan},
    {"ceiling",     wk_ceiling},
    {"cos",         wk_cos},
    {"cosh",        wk_cosh},
    {"denominator",     wk_denominator},
    {"difference",      wk_difference},
    {"div",         wk_div},
    {"exp",         wk_exp},
    {"floor",       wk_floor},
    {"idiv",        wk_idiv},
    {"ln",          wk_ln},
    {"log",         wk_log},
    {"matinv",      wk_matinv},
    {"matlub",      wk_matlub},
    {"matlud",      wk_matlud},
    {"matmul",      wk_matmul},
    {"mattra",      wk_mattra},
    {"mod",         wk_mod},
    {"mul",         wk_mul},
    {"neg",         wk_neg},
    {"numerator",       wk_numerator},
    {"rand",        wk_rand},
    {"random",      wk_random},
    {"round",       wk_round},
    {"rrand",       wk_rrand},
    {"rrandom",     wk_rrandom},
    {"sin",         wk_sin},
    {"sinh",        wk_sinh},
    {"sqrt",        wk_sqrt},
    {"srand",       wk_srand},
    {"srandom",     wk_srandom},
    {"sub",         wk_sub},
    {"tan",         wk_tan},
    {"tanh",        wk_tanh},
    {"truncate",        wk_truncate}};

int init_mat2(void)
{
    return enter_funcs(mat_funcs,ELEMS(mat_funcs));
}
